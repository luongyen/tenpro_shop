<!DOCTYPE html>
<html lang="kr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/jquery-ui.js"></script>
	<script src="../assets/js/naverLogin.js"></script>
</head>

<body>
	<script>
		var naverLogin = new naver.LoginWithNaverId(
			{
				clientId: "9PTpAxDpTlCrWQ6j_8W5",
				callbackUrl: "http://10proshop.com",
				isPopup: false,
				callbackHandle: true
			}
		);

		//alert(naverLogin.accessToken() );
		naverLogin.init();
		var token = naverLogin.accessToken.accessToken;//  alert(naverLogin.accessToken);
		//alert(naverLogin.accessToken);
		//		console.log(naverLogin.accessToken.accessToken);
		window.addEventListener('load', function () {
			naverLogin.getLoginStatus(function (status) {
				console.log(status);
				if (status) {
					var params = {
						mode:'isNoneApp', 
						gubun:'naver', 
						nickname:naverLogin.user.getNickName(), 
						email:naverLogin.user.getEmail(), 
						profile:naverLogin.user.getProfileImage(), 
						thum:naverLogin.user.getProfileImage(), 
						id:naverLogin.user.getId(), 
						token:token
					}
					$.ajax({
						url:"../proc/_setting.php",
						data:params,
						type:"POST",
						dataType:"json",
						success:function(data){
							console.log(data);
							if ( data.content == "1" ) {
								location.replace('http://10proshop.com/main/?reurl=<?=urlencode($reurl)?>');
							}else { // response content 0=query error 2=invalid value
								alert("로그인 정보를 저장하지 못했습니다. [code:000"+data.content+"]");
								//location.reload();
							}
						},
						error:function(jqXHR,textStatus,errorThrown){	
							console.log(jqXHR,textStatus,errorThrown);
						}
					});
				} else {
					console.log("callback 처리에 실패하였습니다.");
				}
			});
		});
	</script>
</body>

</html>
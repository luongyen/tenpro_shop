<?
$PAGE_TITLE = "15%적립받기";
include ("../include/header.php");
?>
<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">15% 적립</h2>
	<div class="earn-area">
		<div class="earn-box">
			<div class="earn">
				<p>15% 적립받기<br><span>포인트 적립</span></p>
			</div>
			<button type="button" name="" class="btn btn-point">리셀러 신청</button>
		</div>
		<div class="text">
			<p><strong>친구를 초대하고, 친구가 구매한 <br>상품금액의 15%를 적립받으세요!!</strong><br><br>
			친구에게 또는 내 블로그,카페,인스타에 상품을 등록후,<br> 링크를 통해 상품구매시 구매금액의 15%를 <br>적립받으실수 있습니다<br>적립된 포인트는 마이페이지에서 확인하실수 있습니다.</p>
			<div class="earn-form">
				<div class="earn-input"><input type="text" name="" class=""></div>
				<button type="button" name="" class="btn-point"  id="copyContent" data-clipboard-text ="<?=$short_url?>" >복사</button>
			</div>
			<p class="gray">※ 리셀러 신청하시면 링크주소가 나옵니다.<br>※ 승인후에 받으세요</p>
		</div>
	</div>
	<? include ("../include/quick_btn.php"); ?>
</section>
<?
include ("../include/footer.php");
?>
<script type="text/javascript" src="../assets/js/clipboard.min.js"></script>
<script type="text/javascript">
<!--
		var clipboard = new ClipboardJS('#copyContent');
		clipboard.on('success', function(e) {
			console.log(e);
			pop_policy_new('3', 'URL 이 복사 되었습니다.', '', '' );
		});
		clipboard.on('error', function(e) {
			console.log(e);
		});
//-->
</script>
<?
$PAGE_TITLE = "검색하기";
//내가 찾은 단어 저장
$exist_key = false;
if ( $_COOKIE["my_auto_search_yn"] != "2" ) { //자동 저장 2이면 사용안함
	if( $_COOKIE["my_search"] !=  "" ) {
		$my_search = $_COOKIE["my_search"] ;
		$tmp_key = explode("|^|" , $my_search );
		for ( $i = 0 ; $i < count( $tmp_key ) ; $i++ ) {
			$tmp_keys = explode("|@|" , $tmp_key[$i] );
			if ( $tmp_keys[0] == $_GET["skey"] ) {
				$exist_key = true;
				break;
			}
		}
	}

	if ( $exist_key == false && $_GET["skey"] != "" ) {
		if ( $my_search !=  "" ) {
			@setcookie( "my_search" , $_GET["skey"] . "|@|" . date("Y-m-d H:i:s") . "|^|" . $my_search , time() + 39999999 , "/" );		
		}else {
			@setcookie( "my_search" , $_GET["skey"] . "|@|" . date("Y-m-d H:i:s") , time() + 39999999 , "/" );
		}
	}
}
//echo $my_search;
//내가 찾은 단어 저장 끝
include ("../include/header.php");
$cate1 = mysql_real_escape_string( $_GET["cate1"] );
$cate2 = mysql_real_escape_string( $_GET["cate2"] );
$cate3 = mysql_real_escape_string( $_GET["cate3"] );
$skey = mysql_real_escape_string( $_GET["skey"] );
$page = mysql_real_escape_string( $_GET["page"] );
if ( $page == "" ) {
	$page = "2";
}
$offset = ( $page - 1 ) * 20;
$sql_append = "";
$sql_append_search = "";

if ( $sort == "" || $sort == "1" ) {
	$sql_order = " lastUpdate desc "; //최신순 기본
}elseif ( $sort == "2" ) {
	$sql_order = " price asc "; //최저가순
}elseif ( $sort == "3" ) {
	$sql_order = " price desc "; //최고가순
}

if ( $cate1 != "" ) {		$sql_append .= " and category1 = '" . $cate1 . "' "; }
if ( $cate2 != "" ) {		$sql_append .= " and category2 = '" . $cate2 . "' "; }
if ( $cate3 != "" ) {		$sql_append .= " and category3 = '" . $cate3 . "' "; }
if ( $skey != "" ) {		$sql_append .= " and ( title like '%" . $skey . "%' or keywords = '%" . $skey . "%' ) "; $sql_append_search = " and ( title like '%" . $skey . "%' or keywords = '%" . $skey . "%' ) "; }

$paging = true;
$tot = getdata("SELECT count(*) as cnt FROM `goods` where status='판매중' and preSales = '0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append);

if ( $tot["cnt"] <= 20 ) {
	$paging = false;
}
$rst = mysql_query("SELECT * FROM `goods` where status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by " . $sql_order . " limit 0, " . $offset );

$goods_total = getdata( "SELECT count(no) as cnt FROM `goods` where status='판매중' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . "");
?>
<style type="text/css" title="">
	#cate2_list ul , #cate2_list li{ list-style:none; padding:0; margin:0; }
	#cate2_list li{ float:left; }
	.container{ width:100%; overflow:hidden; }
	#cate2_list{ width:1600px; }

	.swiper-area {position:static;}
	.category-swiper {margin-left:150px;}
	@media (min-width: 900px) {
		.swiper-area {position:static;margin-left:50px !important;}
	}
	
</style>
<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">검색카테고리</h2>
	<div class="search-list-header">
		<div class="search-category" style="width:145px;right:12px;"><!--position:fixed;top:45px;width:115px;background:none;background-color:#fff;-->
			<div class="category-select">
				<div class="category">
				<select name="cate_1" id="cate_1" style="background:none;">
					<option value="" <?= ( $row_cate1["code_id"] == "" ) ? "selected" : ""?>>전체</option>
					<?
					$rst_cate1 = mysql_query("select * from category where depth='1' and code_id <> '99_00_00_00_00' and cname not like '%삭제%' order by code_id ");
					while ( $row_cate1 = mysql_fetch_array( $rst_cate1 ) ) {
					?>
					<option value="<?=$row_cate1["code_id"]?>" <?= ( $row_cate1["code_id"] == $cate1 ) ? "selected" : ""?>><?=$row_cate1["cname"]?></option>		
					<?}?>
				</select>
				</div>
			</div>
		</div>
		<!--NAV-->
		<!--
		<div class="category-swiper">
			<div class="swiper-area" style="left:120px;">
				<nav class="swiper-container" id="cate2_list">
					<ul class="swiper-wrapper tabs" style="overflow-x:scroll;padding-bottom:5px;">
						<li><a href="./category_list.php?skey=<?=$skey?>&cate1=<?=$cate1?>" class="swiper-slide <?= ( $cate2 == "" ) ? "active" : "" ?>">전체</a></li>
						<?
						if ( $cate1 != "" ) {
							$rst_cate2 = mysql_query("select * from category where depth='2' and code_id like '" . substr( $cate1 , 0 , 2 ) . "%' and cname not like '%삭제%' order by code_id ");
						}else {
							$rst_cate2 = mysql_query("select * from category where depth='2' and code_id like '01%' and cname not like '%삭제%' order by code_id ");
						}
						while ( $row_cate2 = mysql_fetch_array( $rst_cate2 ) ) {
						?>
						<li><a href="./category_list.php?skey=<?=$skey?>&cate1=<?= ( $cate1 == "" ) ? "01_00_00_00_00" : $cate1 ?>&cate2=<?=$row_cate2["code_id"]?>" class="swiper-slide <?= ( $row_cate2["code_id"] == $cate2 ) ? "active" : "" ?>"><?=$row_cate2["cname"]?></a></li>
						<?}?>
					</ul>
				</nav>
			</div>
		</div>-->
	</div>

	<!--LIST-->
	<div class="search-list">
		<h2 class="hide">카테고리</h2>
		<div class="title">
			<div class="left">총 <span class="point"><?=number_format( $goods_total["cnt"] )?>개</span> 상품</div>
			<!--
			<div class="right">
			<a href="#" id="sort_layer" class="arrow-up">상세검색</a>
			</div>
			-->
		</div>
			
		<!--sort-area hide--> 	
		<div class="sort-area" style="z-index:999;display:none;background-color:#fff;height:100%;">
			<ul>
				<li class="title" style="border-top:1px solid #eee;">정렬 <span class="arrow"></span></li>
				<li>
					<div class="radio" style="display: inline-block;"><input type="radio" id="sort5" class="submit_condition" name="sort_rdo" <?= ( $sort =="" || $sort =="1" ) ? "checked" : "" ?> value="1"><label for="sort5">최신순</label></div>
					<div class="radio" style="display: inline-block;margin-left:10px;"><input type="radio" class="submit_condition" id="sort2" name="sort_rdo" <?= ( $sort =="2" ) ? "checked" : "" ?> value="2"><label for="sort2">낮은 가격순 </label></div>
					<div class="radio" style="display: inline-block;margin-left:10px;"><input type="radio" class="submit_condition" id="sort3" name="sort_rdo" <?= ( $sort =="3" ) ? "checked" : "" ?> value="3"><label for="sort3">높은 가격순 </label></div>
				</li>
			</ul>
			<!--
			<ul>		
				<li  class="title">카테고리</li>
				<?
				//$rst_cate2 = mysql_query("select * from category where depth='2' and code_id like '" . substr( $cate1 , 0 , 2 ) . "%' and cname not like '%삭제%' order by code_id asc ");
				//while ( $row_cate2 = mysql_fetch_array( $rst_cate2 ) ) {
				//	$cate2_total = getdata("select count(no) as cnt from goods where category2='".$row_cate2["code_id"]."' and status = '판매중' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_search . " ");
				?>
				<li><a href="#" class="arrow sub_cate_list" data-cateid="<?//= substr( $row_cate2["code_id"] , 0 , 5 ) ?>"><?//=$row_cate2["cname"]?> <span class="gray">(<?//=number_format( $cate2_total["cnt"] )?>)</span></a></li>
				<li class="blind" id="sub_cate_list_<?//= substr( $row_cate2["code_id"] , 0 , 5 ) ?>">
					<ul>
					<?
				//	$rst_cate3 = mysql_query("select * from category where depth='3' and code_id like '" . substr( $row_cate2["code_id"] , 0 , 5 ) . "%' and cname not like '%삭제%' order by code_id asc ");
				//	while ( $row_cate3 = mysql_fetch_array( $rst_cate3 ) ) {
				//		$cate3_total = getdata("select count(no) as cnt from goods where category3='".$row_cate3["code_id"]."' and status = '판매중' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_search . " ");
					?>
						<li><a href="#" class="submit_condition2" data-cateid1="<?//= $cate1 ?>" data-cateid2="<?//= $row_cate2["code_id"] ?>" data-cateid3="<?//= $row_cate3["code_id"] ?>"> <?//= $row_cate3["cname"] ?> <span class="gray">(<?//=number_format( $cate3_total["cnt"] )?>)</span></a></li>
					<?//}?>
					</ul>
				</li>
				<?//}?>
			</ul>-->
		</div>
	</div>

	<!-- LIST -->
	<div class="list">
		<ul id="listOfBoard">
		<?
		while ( $row = mysql_fetch_array( $rst ) ) {
			$batge = "";
			$goods_info_detail = getdata("select * from goods_detail where no='" . $row["no"] . "' ");
			
			//if ( $goods_info_detail["fastDeli"] == "1" ) { $batge .= "빠른배송 | "; }
			if ( $row["best_code"] == "1" ) { $batge .= "인기상품 | "; }
			if ( $row["packed"] == "1" ) { $batge .= "묶음배송상품 | "; }
			if ( $row["ttang"] == "1" ) { $batge .= "땡처리상품 | "; }
			if ( $row["fromOversea"] == "1" ) { $batge .= "해외직배송 | "; }
			$batge = substr( $batge , 0 , -2 );
		?>
			<li><a href="product_view.php?no=<?=$row["no"]?>" data-idx="<?=$row["no"]?>" class="detail_link">
				<div class="img"><img src="<?=$row["thumb"]?>"></div>
				<div class="text">
					<h3 class="ellipsis2"><?=$row["title"]?></h3>
					<div class="pay"><?=number_format( $row["priceWe"] )?>원</div>
					<div class="count"><?=$batge ?></div>
					<!--div class="delivery">무료배송</div-->
				</div>
			</a></li>		
		<?}?>
		</ul>
	</div>
	<? include ("../include/quick_btn.php"); ?>
</section>
<?
include ("../include/footer.php");
?>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$(".blind").hide();
	$(".sub_cate_list").click(function(){
		var cid = $(this).data("cateid");
		//alert(cid);
		$("#sub_cate_list_"+cid).slideToggle(300);
		$(".blind").not("#sub_cate_list_"+$(this).data("cateid")).slideUp(300);
		return false;
	});
	
	<?
	if ( $cate1 != "" ) {
	?>
	$("#sub_cate_list_<?= substr( $cate1 , 0 , 2 )?>").show();
	<?}?>
	//$("ul li a").eq(0).trigger("click");
});

//var swiper = new Swiper('.swiper-container');
$(document).on( "click" , ".search-button" , function(){
	if ( $("#skey").val() == "" ) {
		alert("검색어를 입력해주세요");
		return false;
	}else {
		$("#search_frm").submit();
	}
});

$(document).on( "click" , ".submit_condition" , function(){
	//var chk_free = "";
	//if ( $("#chk_free").is(":checked") ) {
	//	$("#free_yn").val("1");
	//}else {
	//	$("#free_yn").val("0");
	//}
	$("#cate1").val("<?=$cate1?>");
	$("#cate2").val("<?=$cate2?>");
	$("#cate3").val("<?=$cate3?>");
	$("#sort").val($("input[type=radio][name=sort_rdo]:checked").val());
	$("#search_frm").submit();
});

$(document).on( "click" , ".submit_condition2" , function(){
	var cate1 = $(this).data("cateid1");
	var cate2 = $(this).data("cateid2");
	var cate3 = $(this).data("cateid3");
	/*
	if ( $("#chk_free").is(":checked") ) {
		$("#free_yn").val("1");
	}else {
		$("#free_yn").val("0");
	}*/
	$("#cate1").val(cate1);
	$("#cate2").val(cate2);
	$("#cate3").val(cate3);
	$("#sort").val($("input[type=radio][name=sort_rdo]:checked").val());
	$("#search_frm").submit();
});

$(document).on( "click" , "#search" , function(){
	var chk_free = "";
	if ( $("#chk_free").is(":checked") ) {
		var chk_free = "1";
	}else {
		var chk_free = "0";
	}

	if ( $("#skey").val() == "" ) {
		alert("검색어를 입력해주세요");
		return false;
	}else {
		$("#search_frm").action("./category_list.php");
		$("#search_frm").submit();
	}
});

$(document).on("change" , "#cate_1" , function(){
	var val = $("#cate_1 option:selected").val();
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: '../proc/_product.php',
		data: 'mode=sel_cate&dep=2&skey=<?=$skey?>&val='+val,
		success:function (data) {
			//alert(data);
				console.log(data);
			if (data.rst != "1" ) {
				//alert("문제가 발생하여 정상적으로 반영되지 않았습니다.");
				//location.reload();
			}else {
				$("#cate2_list").html(data.content);
				$('#cate2_list').touchMoveMenu();
			}
		},
		error : function (data) {
			console.log(data);
		} 
	});
});

var swich_sort = false;
$(document).on( "click" , "#sort_layer" , function(){
	if ( swich_sort == false ) {
		$(".sort-area").show();
		$(this).removeClass("arrow-up");
		$(this).addClass("arrow-down");
		swich_sort = true;
	}else {
		$(".sort-area").hide();
		$(this).removeClass("arrow-down");
		$(this).addClass("arrow-up");
		swich_sort = false;
	}
	
});

var page = <?=$page?>;
var page_ex = true;
var paging_ing = false;

$(document).ready(function(){
	show_loading();
});

$(window).load(function(){
	<? if ( $paging != false ) {?>
	$(window).scroll(function(){
		if($(window).scrollTop() >= $(document).height() - $(window).height() -200 ){
			if ( paging_ing == false ) {
				paging();
				paging_ing = true;
			}
			return false;
		}
	});
	<?}?>

	$('#div_ajax_load_image').hide();  
	function paging(){
		if ( page_ex == true ) {
			var params = { mode:'paging10' , page:page , cate1:"<?=$cate1?>" , cate2:"<?=$cate2?>" , cate3:"<?=$cate3?>" , skey:"<?=$skey?>", sort:"<?=$sort?>" }
			$.ajax({
				url:"../product/_proc_json.php",
				data:params,
				type:"GET",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.content != "" ) {
						$("#listOfBoard").append( data.content);
						paging_ing = false;
						page++;
					}else {
						page_ex = false;
					}
				}, error:function(jqXHR,textStatus,errorThrown){
					console.log(errorThrown);
					alert("페이지 로딩 도중 에러가 발생하였습니다.");
				}, beforeSend: function () {
					show_loading();
				}, complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
	}

});

$(document).on( "change" , "#cate_1" , function(){
	location.href="./category_list.php?cate1="+$(this).val()+"&skey=<?=$skey?>";
});

//-->
</script>
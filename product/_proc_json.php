<?
include ("../config.php");

if ( $mode == "product_detail_review_paging" ) {
	$now = date("Y-m-d H:i:s");
	$ndate = date("Y-m-d");
	$page = mysql_real_escape_string( $_GET["page"] );
	$c = mysql_real_escape_string( $_GET["c"] );
	$ref_idx = mysql_real_escape_string( $_GET["ref_idx"] );
	$_start = $page * 5;
	$_end = 5;
	
	$tot = getdata("select count(*) as cnt from goods_review where no='" . $ref_idx . "' and del_ok='0' ");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = 1;	
	if ( $page > 0 ) {
		$pageIdx = $page;
	}
	$page_set = 5; //한페이지 줄수-기본값
	$block_size = 5;
	if ( $pageIdx % $block_size == 0 ) {
		$start_num = $pageIdx - $block_size + 1;
	}else {
		$start_num = floor( $pageIdx / $block_size ) * $block_size + 1;
	}
	$end_num = $start_num + $block_size - 1;
	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수

	if ( $pageIdx == 1 ) {
		$limit_idx = 0;
	}else {
		$limit_idx = $pageIdx * $page_set - $page_set;
	}

	$rst_review = mysql_query(" select * from goods_review where no='" . $ref_idx . "' and del_ok='0' order by idx desc limit ".$limit_idx.", ".$page_set);
	
	$i=0;

	$content = "<div class=\"review-list\"><h3 class=\"hide\">상품평 리스트</h3><ul>";
	$j = 0;
	while ( $row_review = mysql_fetch_array( $rst_review ) ) {
		$j++;
		$content .= "<li><div class=\"id-data\">" . mb_substr( $row_review["uid"] , 0 , -3 , "UTF-8" ) . "*** <span class=\"gray\">" . str_replace( "-" , "." , substr( $row_review["reg_date"] , 0 , 10 ) ) . "</span><div class=\"marks right\">평점:" . REVIEW_STAR( $row_review["score"] ) . "</div></div><p>" . nl2br( $row_review["title"] ) . "</p></li>";
	}
	$content .= "</ul>";
	
	if ( $cnt > 0 ) {
		$content .= "<div class=\"pageview\" id=\"review-paging\">";
		$prev_page = $pageIdx - 1;
		$next_page = $pageIdx + 1;
		$content .=  ($pageIdx>1)? "<button type=\"button\" class=\"pre review_paging\" data-page=\"" . $prev_page . "\">이전리스트</button>" : "<button type=\"button\" class=\"pre\" >이전리스트</button>";
		if ( $total_page < 10 ) {
			$vpage = 1;
		}else{
			$vpage = ( ( (int)( ( $pageIdx - 1 ) / $page_set ) ) * $page_set ) + 1;
		}
		$spage = $vpage + $page_set - 1;
		if ( $spage >= $total_page ) $spage = $total_page;

		for ( $i = $vpage ; $i <= $spage ; $i++ ){ 
			if ( $pageIdx == $i ) {
				$content .=  "<a href=\"#\" class=\"on review_paging\"  data-page=\"" . $i . "\">" . $i . "</a>";
			}else {
				$content .=  "<a href=\"#\" class=\"review_paging\"  data-page=\"" . $i . "\">" . $i . "</a>";
			}
		}
		$content .=  ( $pageIdx > 1 && $pageIdx < $total_page )? "<button type=\"button\" class=\"next review_paging\" data-page=\"" . $next_page . "\" >다음리스트</button>" : "<button type=\"button\" class=\"next\" >다음리스트</button>";

		$content .= "</div>";
	}
				
	$content .= "<div class=\"btn-area\"><button type=\"button\" class=\"btn btn-point layer-open-review\" >리뷰등록</a></div></div>";

	$data["content"] = $content;
	$data["page"] = $page;
	echo json_encode($data);
}

if ($mode == "review_reg" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$title = mysql_real_escape_string($_POST["title"]);
	$score = mysql_real_escape_string($_POST["score"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$query = mysql_query("insert into goods_review SET no='" . $ref_idx . "' , uid = '" . $_SESSION["mstsp_id"] . "' , wname = '" . $_SESSION["mstsp_name"] . "' , title = '" . $title . "'  , score = '" . $score . "'  , reg_date = '" . date("Y-m-d H:i:s") . "' , del_ok = '0' , view_admin = '0' ");
		if ( $query ) {
			$data["content"] = "succ";
		}else {
			$data["content"] = "error";
		}
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "inquiry_reg" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$title = mysql_real_escape_string($_POST["title"]);
	$content = mysql_real_escape_string($_POST["content"]);
	$sec = mysql_real_escape_string($_POST["sec"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$query = mysql_query("insert into goods_qna SET no='" . $ref_idx . "' , uid = '" . $_SESSION["mstsp_id"] . "' , wname = '" . $_SESSION["mstsp_name"] . "' , title = '" . $title . "' , content = '" . $content . "' , sec = '" . $sec . "'  , reg_date = '" . date("Y-m-d H:i:s") . "' , del_ok = '0' , view_admin = '0' ");
		if ( $query ) {
			$data["content"] = "succ";
		}else {
			$data["content"] = "error";
		}
	}
	echo json_encode( $data );
	exit;
}


if ($mode == "lotto_regi" ) {	
	$eidx = mysql_real_escape_string($_POST["eidx"]);
	$gid = mysql_real_escape_string($_POST["gid"]);
	$again = mysql_real_escape_string($_POST["again"]);
	$win = explode( "," , $gid );
	if ( $_SESSION["mstsp_id"] != "" ) {
		$chk_ext = getdata("select count(*) as cnt from event_lotto_data where event_idx='" . $eidx . "' and uid='" . $_SESSION["mstsp_id"] . "' ");
		if ( $again == "1" && $chk_ext["cnt"] == "1" ) {
			$ungmo_ok = true;
		}elseif ($again == "1" && $chk_ext["cnt"] == "0") {
			$ungmo_ok = false;		
			$pre_umgmo = true;		
		}elseif ($chk_ext["cnt"] == "0") {
			$ungmo_ok = true;		
		}else {			
			$ungmo_ok = false;
		}
		
		if ( $ungmo_ok ) {
			$query = mysql_query( "insert into event_lotto_data set event_idx='" . $eidx . "' , uid='" . $_SESSION["mstsp_id"] . "' , win_1='" . $win[0] . "' , win_2='" . $win[1] . "' , win_3='" . $win[2] . "' , win_4='" . $win[3] . "' , ip='" . $_SERVER["REMOTE_ADDR"] . "' , reg_date='" . date("Y-m-d H:i:s") . "'  ");
			if ( $query ) {
				echo "1";
			}else {
				echo "2";
			}
		}else {
			if ($pre_umgmo == true ) {
				echo "5";			
			}else {
				echo "4";				
			}
		}
	}else {
		echo "3";
	}
	exit;
}

if ($mode == "up_share" ) {	
	$gcode = mysql_real_escape_string($_POST["gcode"]);
	$gubun = mysql_real_escape_string($_POST["gubun"]);
	$sns = mysql_real_escape_string($_POST["sns"]);
	@mysql_query( "insert into share_chk_none set media='" . $MEDIA_CODE . "' , sns='" . $sns . "' , gubun='" . $gubun . "' , event_idx='" . $gcode . "' , reg_date='" . date("Y-m-d H:i:s") . "'  ");
	exit;
}

if ($mode == "auto_share_set_cookie" ) {	
	$t = mktime(0,0,0,date("m"),date("d")+1,date("Y"));
	setcookie("auto_share" , "Y" , $t , "/");
	exit;
}

if ($mode == "short_url" ) {	
	global $gkey;

	mysql_query("insert into time_chk set ss='".$_SESSION["time_chk_tmp"]."' , pg='shorturl start' , time='".time()."' ");
	$url = mysql_real_escape_string($_POST["url"]);
	if ( strpos( $url , "&recom_uid=") === false && $_SESSION["mstsp_id"] != "" ) {
		$url .="&recom_uid=" . $_SESSION["mstsp_id"];
	}
	if ( strpos( $url , "&kakao=y" ) !== false ) {
		$url = str_replace( "&kakao=y", "" , $url );
	}
	include ("../lib/googleUrlApi.php");
	$googer = new GoogleURLAPI($gkey);
	$short_url = $googer->shorten($url);
	if ( $short_url == false ) {
		$short_url = $url;
	}
	$data = array();	

	$data["url"] = $short_url;
	echo json_encode( $data );
	
	mysql_query("insert into time_chk set ss='".$_SESSION["time_chk_tmp"]."' , pg='shorturl end' , time='".time()."' ");
	exit;
}

if ($mode == "rep_insert" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$query = mysql_query("insert into board_contents_reply SET `ref_idx`='$ref_idx' , `uid` = '" . $_SESSION["mstsp_id"] . "' , `uname` = '" . $_SESSION["mstsp_name"] . "' , `content` = '$msg'  , `reg_date` = '" . date("Y-m-d H:i:s") . "'");
		if ( $query ) {
			$data["content"] = "succ";
		}else {
			$data["content"] = "error";
		}
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "reply_detail_delt" ) {
	$idx = mysql_real_escape_string($_POST["idx"]);
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update board_detail SET del_ok = '1'  where `idx`='$idx' ");
		if ( $query ) {
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}		
	}
	echo json_encode( $data );
	exit;	
}

if ($mode == "reply_detail_edit" ) {
	$content = mysql_real_escape_string($_POST["contents"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update board_detail SET `content` = '$content'  where `idx`='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "' ");
		if ( $query ) {
			$data["content"] = "1";
			$data["content_txt"] = nl2br( $content );
		}else {
			$data["content"] = "0";
		}		
	}
	echo json_encode( $data );
	exit;	
}

if ($mode == "reply_detail_inst" ) {
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$content = mysql_real_escape_string($_POST["contents"]);
	//$parent_id = mysql_real_escape_string( $_POST["parent_id"] );
	$category = mysql_real_escape_string( $_POST["category"] );
	//$parent_id = "A";
	$data = array();

	//if ( $_SESSION["mstsp_id"] == "" ) {
	//	$data["content"] = "2";
	//}else {
		$row = getdata("SELECT parent_id,right(parent_id,1) AS RID FROM board_detail WHERE group_id = '$ref_idx' AND length(parent_id) = length('$parent_id')+1 AND locate('$parent_id',parent_id) = 1 ORDER BY parent_id DESC LIMIT 1");
		
		if($row) {
			$thread_head = substr($row["parent_id"],0,-1);

			//$thread_foot = ++$row[1];
			if ($row["RID"] == "A") {
				$thread_foot = "B";
			}else if ($row["RID"] == "B") {
				$thread_foot = "C";			
			}else if ($row["RID"] == "C") {
				$thread_foot = "D";			
			}else if ($row["RID"] == "D") {
				$thread_foot = "E";			
			}else if ($row["RID"] == "E") {
				$thread_foot = "F";			
			}else if ($row["RID"] == "F") {
				$thread_foot = "G";			
			}else if ($row["RID"] == "G") {
				$thread_foot = "H";			
			}else if ($row["RID"] == "H") {
				$thread_foot = "I";			
			}
			$new_thread =  $thread_head . $thread_foot;
		} else {
			$new_thread = $parent_id . "A";
		}
		if ( $_SESSION["mstsp_id"] != "" ) {
			$sid = $_SESSION["mstsp_id"];
		}else {
			$sid = $_UID;
		}
		$sql_set = " set shop_id = '" . $MEDIA_CODE . "' , category='$category' , group_id = '" . $ref_idx . "' , parent_id = '' , profile='" . $_SESSION[$MEDIA_CODE.'_thumbnail'] . "' , uid = '" . $sid  . "' , wname = '" . $_SESSION[$MEDIA_CODE.'_name'] . "' , content = '" . $content . "' , reg_date='" . date("Y-m-d H:i:s") . "' ";
		$query = mysql_query("insert into board_detail $sql_set ");
		if ( $query ) {
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}		
	//}
	echo json_encode( $data );
	exit;
}

if ($mode == "rep_edit" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update board_contents_reply SET `content` = '$msg'  where `idx`='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "' ");
		if ( $query ) {
			$msg = str_replace( "\\r\\n" , "<br />" , $msg );
			$msg = str_replace( "\\n" , "<br />" , $msg );
			$data["content"] = "1";
			$data["content_txt"] = $msg;
		}else {
			$data["content"] = "0";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "rep_del" ) {	
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update board_contents_reply SET del_ok='1' where idx='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "'");
		if ( $query ) {
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "rep_more" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$page = mysql_real_escape_string($_POST["page"]);
	$start = $page * 10;
	$content = "";
	$i = 0;
	$rst_rep = mysql_query("select * from board_contents_reply where shop_id='" . $MEDIA_CODE . "' and ref_idx='" . $ref_idx . "' and del_ok='0' order by reg_date desc limit " . $start . " , 10 ");
	$tot = getdata("select count(*) as cnt from board_contents_reply where shop_id='" . $MEDIA_CODE . "' and ref_idx='" . $ref_idx . "' and del_ok='0' ");
	$data["content"] = "";
	while ( $row_rep = mysql_fetch_array( $rst_rep ) ) {
		$row_pro = getdata("select profile_img, thumb_img from users where id='" . $row_rep["uid"] . "' and del_ok='0' and status='1' and del_force='0' ");
		
		$data["content"] .= "<div class=\"textL sub01CommentBox bordert1\" id=\"div_" . $row_rep["idx"] . "\"><div class='icon'><img onerror=\"this.src='/images/common/icon_noimg_m.png'\" alt=\"\" src=\"" . $row_pro['thumb_img'] . "\"></div><div class='conts'><p class=\"B\" style=\"margin-bottom:10px;\">" . $row_rep["uname"] . "<span class=\"font11 inlineR normal\">" . $row_rep["reg_date"] . "</span></p><p><span id=\"sp_" . $row_rep["idx"] . "\" style=\"display:;\">" . nl2br( $row_rep["content"] ) . "</span><span id=\"tb_" . $row_rep["idx"] . "\" style=\"display:none;\"><textarea name=\"ip_" . $row_rep["idx"] . "\" id=\"ip_" . $row_rep["idx"] . "\">" . $row_rep["content"] . "</textarea><button data-idx=\"" . $row_rep["idx"] . "\" class=\"submit_edit\">확인</button></span></p>";
		if ( $_SESSION["mstsp_id"] == $row_rep["uid"] ) {
			$data["content"] .= "<div class=\"font11 sub01Edit_Del_Btn\"><p class=\"inlineR edIt\" data-idx=\"" . $row_rep["idx"] . "\"><a href=\"#\">수정</a></p><p class=\"inlineR dEl\" id=\"del_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\"><a href=\"#\">삭제</a></p><p class=\"inlineR canc\" id=\"canc_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\" style=\"display:none;\"><a href=\"#\">취소</a></p></div>";
		}
		$data["content"] .= "</div></div>";
		$i++;
	}
	$data["last"] = false;
	if ( $tot["cnt"] == ( $start + $i ) ) {
		$data["last"] = true;
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_rep_insert" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$query = mysql_query("insert into event_reply SET shop_id='" . $MEDIA_CODE . "' , `event_idx`='$ref_idx' , `uid` = '" . $_SESSION["mstsp_id"] . "' , `writer` = '" . $_SESSION[$MEDIA_CODE.'_name'] . "' , `content` = '$msg'  , `reg_date` = '" . date("Y-m-d H:i:s") . "'");
		if ( $query ) {
			$data["content"] = "succ";		
		}else {
			$data["content"] = "error";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_rep_edit" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update event_reply SET `content` = '$msg'  where `idx`='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "' ");
		if ( $query ) {
			$msg = str_replace( "\\r\\n" , "<br />" , $msg );
			$msg = str_replace( "\\n" , "<br />" , $msg );
			$data["content"] = "1";
			$data["content_txt"] = $msg;
		}else {
			$data["content"] = "0";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_rep_del" ) {	
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update event_reply SET del_ok='1' where idx='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "'");
		if ( $query ) {
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_rep_more" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$page = mysql_real_escape_string($_POST["page"]);
	$start = $page * 5;
	$content = "";
	$i = 0;
	$rst_rep = mysql_query("select * from event_reply where event_idx='" . $ref_idx . "' and del_ok='0' order by reg_date desc limit " . $start . " , 5 ");
	$tot = getdata("select count(*) as cnt from event_reply where event_idx='" . $ref_idx . "' and del_ok='0' ");
	while ( $row_rep = mysql_fetch_array( $rst_rep ) ) {
		$row_pro = getdata("select thumb_img from users where id='" . $row_rep["uid"] . "' and del_ok='0' and status='1' and del_force='0' ");

		$data["content"] .= "<div class=\"textL sub01CommentBox bordert1\" id=\"div_" . $row_rep["idx"] . "\"><div class='icon'><img onerror=\"this.src='/images/common/icon_noimg_m.png'\" alt=\"\" src=\"" . $row_pro['thumb_img'] . "\"></div><div class='conts'><p class=\"B\" style=\"margin-bottom:10px;\">" . $row_rep["uname"] . "<span class=\"font11 inlineR normal\">" . $row_rep["reg_date"] . "</span></p><p><span id=\"sp_" . $row_rep["idx"] . "\" style=\"display:;\">" . nl2br( $row_rep["content"] ) . " </span><span id=\"tb_" . $row_rep["idx"] . "\" style=\"display:none;\"><textarea name=\"ip_" . $row_rep["idx"] . "\" id=\"ip_" . $row_rep["idx"] . "\">" . $row_rep["content"] . "</textarea><button data-idx=\"" . $row_rep["idx"] . "\" class=\"submit_edit\">확인</button></span></p>";
		if ( $_SESSION["mstsp_id"] == $row_rep["uid"] ) {
			$data["content"] .= "<div class=\"font11 sub01Edit_Del_Btn\"><p class=\"inlineR edIt\" data-idx=\"" . $row_rep["idx"] . "\"><a href=\"#\">수정</a></p><p class=\"inlineR dEl\" id=\"del_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\"><a href=\"#\">삭제</a></p><p class=\"inlineR canc\" id=\"canc_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\" style=\"display:none;\"><a href=\"#\">취소</a></p></div>";
		}
		$data["content"] .= "</div></div>";
		$i++;
	}
	$data["last"] = false;
	if ( $tot["cnt"] == ( $start + $i ) ) {
		$data["last"] = true;
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_noti_rep_insert" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$query = mysql_query("insert into event_noti_reply SET shop_id='" . $MEDIA_CODE . "' , `event_idx`='$ref_idx' , `uid` = '" . $_SESSION["mstsp_id"] . "' , `writer` = '" . $_SESSION[$MEDIA_CODE.'_name'] . "' , `content` = '$msg'  , `reg_date` = '" . date("Y-m-d H:i:s") . "'");
		if ( $query ) {
			$data["content"] = "succ";		
		}else {
			$data["content"] = "error";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_noti_rep_edit" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update event_noti_reply SET `content` = '$msg'  where `idx`='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "' ");
		if ( $query ) {
			$msg = str_replace( "\\r\\n" , "<br />" , $msg );
			$msg = str_replace( "\\n" , "<br />" , $msg );
			$data["content"] = "1";
			$data["content_txt"] = $msg;
		}else {
			$data["content"] = "0";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_noti_rep_del" ) {	
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update event_noti_reply SET del_ok='1' where idx='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "'");
		if ( $query ) {
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "event_noti_rep_more" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$page = mysql_real_escape_string($_POST["page"]);
	$start = $page * 5;
	$content = "";
	$i = 0;
	$rst_rep = mysql_query("select * from event_noti_reply where event_idx='" . $ref_idx . "' and del_ok='0' order by reg_date desc limit " . $start . " , 5 ");
	$tot = getdata("select count(*) as cnt from event_noti_reply where event_idx='" . $ref_idx . "' and del_ok='0' ");
	while ( $row_rep = mysql_fetch_array( $rst_rep ) ) {
		$row_pro = getdata("select thumb_img from users where id='" . $row_rep["uid"] . "' and del_ok='0' and status='1' and del_force='0' ");

		$data["content"] .= "<div class=\"textL sub01CommentBox bordert1\" id=\"div_" . $row_rep["idx"] . "\"><div class='icon'><img onerror=\"this.src='/images/common/icon_noimg_m.png'\" alt=\"\" src=\"" . $row_pro['thumb_img'] . "\"></div><div class='conts'><p class=\"B\" style=\"margin-bottom:10px;\">" . $row_rep["uname"] . "<span class=\"font11 inlineR normal\">" . $row_rep["reg_date"] . "</span></p><p><span id=\"sp_" . $row_rep["idx"] . "\" style=\"display:;\">" . nl2br( $row_rep["content"] ) . " </span><span id=\"tb_" . $row_rep["idx"] . "\" style=\"display:none;\"><textarea name=\"ip_" . $row_rep["idx"] . "\" id=\"ip_" . $row_rep["idx"] . "\">" . $row_rep["content"] . "</textarea><button data-idx=\"" . $row_rep["idx"] . "\" class=\"submit_edit\">확인</button></span></p>";
		if ( $_SESSION["mstsp_id"] == $row_rep["uid"] ) {
			$data["content"] .= "<div class=\"font11 sub01Edit_Del_Btn\"><p class=\"inlineR edIt\" data-idx=\"" . $row_rep["idx"] . "\"><a href=\"#\">수정</a></p><p class=\"inlineR dEl\" id=\"del_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\"><a href=\"#\">삭제</a></p><p class=\"inlineR canc\" id=\"canc_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\" style=\"display:none;\"><a href=\"#\">취소</a></p></div>";
		}
		$data["content"] .= "</div></div>";
		$i++;
	}
	$data["last"] = false;
	if ( $tot["cnt"] == ( $start + $i ) ) {
		$data["last"] = true;
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "free_rep_insert" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	$parent_id = mysql_real_escape_string($_POST["parent_id"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		if ( $idx == "" ) {
			$row = getdata("SELECT parent_id,right(parent_id,1) AS RID FROM board_free_reply WHERE group_id = $ref_idx AND length(parent_id) = 1 ORDER BY parent_id DESC LIMIT 1");		
		}else {
			$row = getdata("SELECT parent_id,right(parent_id,1) AS RID FROM board_free_reply WHERE group_id = $ref_idx AND length(parent_id) = length('$parent_id')+1 AND locate('$parent_id',parent_id) = 1 ORDER BY parent_id DESC LIMIT 1");			
		}
		
		if($row) {
			$thread_head = substr($row["parent_id"],0,-1);

			//$thread_foot = ++$row[1];
			if ($row["RID"] == "A") {
				$thread_foot = "B";
			}else if ($row["RID"] == "B") {
				$thread_foot = "C";			
			}else if ($row["RID"] == "C") {
				$thread_foot = "D";			
			}else if ($row["RID"] == "D") {
				$thread_foot = "E";			
			}else if ($row["RID"] == "E") {
				$thread_foot = "F";			
			}else if ($row["RID"] == "F") {
				$thread_foot = "G";			
			}else if ($row["RID"] == "G") {
				$thread_foot = "H";			
			}else if ($row["RID"] == "H") {
				$thread_foot = "I";			
			}
			$new_thread =  $thread_head . $thread_foot;
		} else {
			$new_thread = $parent_id . "A";
		}

		$query = mysql_query("insert into board_free_reply SET shop_id='" . $MEDIA_CODE . "' , group_id='$ref_idx' , parent_id='$new_thread' , uid = '" . $_SESSION["mstsp_id"] . "' , wname = '" . $_SESSION[$MEDIA_CODE.'_name'] . "' , title = '$msg'  , reg_date = '" . date("Y-m-d H:i:s") . "'");
		if ( $query ) {
			$data["content"] = "succ";
		}else {
			$data["content"] = "error";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "free_rep_edit" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$query = mysql_query("update board_free_reply SET `title` = '$msg'  where `idx`='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "' ");
		if ( $query ) {
			$data["content"] = "succ";
		}else {
			$data["content"] = "error";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "free_re_rep_edit" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$msg = mysql_real_escape_string($_POST["msg"]);
	$idx = mysql_real_escape_string($_POST["idx"]);
	$parent_id = mysql_real_escape_string( $_POST["parent_id"] );
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "login fail";
	}else {
		$row = getdata("SELECT parent_id,right(parent_id,1) AS RID FROM board_free_reply WHERE group_id = $ref_idx AND length(parent_id) = length('$parent_id')+1 AND locate('$parent_id',parent_id) = 1 ORDER BY parent_id DESC LIMIT 1");
		
		if($row) {
			$thread_head = substr($row["parent_id"],0,-1);

			//$thread_foot = ++$row[1];
			if ($row["RID"] == "A") {
				$thread_foot = "B";
			}else if ($row["RID"] == "B") {
				$thread_foot = "C";			
			}else if ($row["RID"] == "C") {
				$thread_foot = "D";			
			}else if ($row["RID"] == "D") {
				$thread_foot = "E";			
			}else if ($row["RID"] == "E") {
				$thread_foot = "F";			
			}else if ($row["RID"] == "F") {
				$thread_foot = "G";			
			}else if ($row["RID"] == "G") {
				$thread_foot = "H";			
			}else if ($row["RID"] == "H") {
				$thread_foot = "I";			
			}
			$new_thread =  $thread_head . $thread_foot;
		} else {
			$new_thread = $parent_id . "A";
		}
		
		$sql_set = " set shop_id = '" . $MEDIA_CODE . "' , group_id = '" . $ref_idx . "' , parent_id = '" . $new_thread . "' , uid = '" . $_SESSION["mstsp_id"] . "' , wname = '" . $_SESSION[$MEDIA_CODE.'_name'] . "' , title = '" . $msg . "' , likeit = 0 , reg_date='" . date("Y-m-d H:i:s") . "' ";
		$query = mysql_query("insert into board_free_reply $sql_set ");
		if ( $query ) {
			$data["content"] = "succ";
		}else {
			$data["content"] = "error";
		}		
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "free_rep_del" ) {	
	$idx = mysql_real_escape_string($_POST["idx"]);
	$data = array();
	if ( $_SESSION["mstsp_id"] == "" ) {
		$data["content"] = "2";
	}else {
		$query = mysql_query("update board_free_reply SET del_ok='1' where idx='$idx' and `uid` = '" . $_SESSION["mstsp_id"] . "'");
		if ( $query ) {
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}
	}
	echo json_encode( $data );
	exit;
}

if ($mode == "free_rep_more" ) {	
	$ref_idx = mysql_real_escape_string($_POST["ref_idx"]);
	$page = mysql_real_escape_string($_POST["page"]);
	$start = $page * 10;
	$content = "";
	$i = 0;
	$rst_rep = mysql_query("select * from board_free_reply where group_id='" . $ref_idx . "' order by parent_id asc  limit " . $start . " , 10 ");
	$tot = getdata("select count(*) as cnt from board_free_reply where group_id='" . $ref_idx . "' ");
	while ( $row_rep = mysql_fetch_array( $rst_rep ) ) {
		$mem_thumb = getdata("select * from users where id='" . $row_rep["uid"] . "' ");
		$step = "";
		if ( strlen( $row_rep["parent_id"] ) > 1 ) {
			$margin = ( strlen( $row_rep["parent_id"] ) - 1 ) * 15;
			$step = "padding-left:" . $margin . "px;";
			$step1 = "margin-top:5px;";
		}
		if ( $row_rep["del_ok"] == "1" ) {
			$title_str = "<span style='display:inline-block;" . $step1 . "'>" . "삭제된 댓글입니다.</span>";
		}else {
			$title_str = "<span style='display:inline-block;" . $step1 . "'>" . nl2br( $row_rep["title"] ) . "</span>";
		}
		if ( strlen( $row_rep["parent_id"] ) > 1 ) {
			$row_rep_parent = getdata("select wname from board_free_reply where group_id='" . $row["idx"] . "' and parent_id='".substr( $row_rep["parent_id"] , -1 )."' ");
			$rep_parent = $row_rep_parent["wname"];
		}else {
			$rep_parent = "";
		}
		if ( $row["gubun"] == "7001" || $row_rep["del_ok"] == "1" ) {
			$pImg = "/images/common/icon_noimg_m.png";
		} else {
			$pImg = $mem_thumb['thumb_img'];
		}
		$data["content"] .= "<div class=\"textL sub01CommentBox bordert1\" id=\"div_" . $row_rep["idx"] . "\" style=\"" . $step . "\"><div class='icon'><img onerror=\"this.src='/images/common/icon_noimg_m.png'\" alt=\"\" src=\"" . $pImg . "\"></div><div class='conts'><p class=\"B\" style=\"margin-bottom:10px;\">";
		if ( $row["gubun"] != "7001" ) {
			if ( $row_rep["del_ok"] == "1" ) {
				$del_done1 = "";
				$del_done2 = "";
				$del_done3 = "";
			}else {
				$del_done1 = $row_rep["idx"];
				$del_done2 = $row_rep["wname"];
				$del_done3 = $row_rep["parent_id"];
			}
			if ( $rep_parent != "" && $row_rep["del_ok"] == "0" ) {
				$del_done4 = "<span class='colorP '>" . $rep_parent . "</span>";			
			}else {
				$del_done4 = "";
			}
			$data["content"] .= "<a href=\"#\" class=\"re_rep_show\" style=\"display:inline-block;\" data-idx=\"" . $del_done1 . "\" data-wname=\"" . $del_done2 . "\" data-parent_id=\"" . $del_done3 . "\">" . $del_done2 . "<br />" . $del_done4 . "</a>";
		}
		$data["content"] .= "<span class=\"font11 inlineR normal\">" . CONV_DATE( $row_rep["reg_date"] , 0 , 10 ) . "</span></p><p><span id=\"sp_" . $row_rep["idx"] . "\" style=\"display:;\">" . nl2br( $title_str ) . "</span><span id=\"tb_" . $row_rep["idx"] . "\" style=\"display:none;\"><textarea name=\"ip_" . $row_rep["idx"] . "\" id=\"ip_" . $row_rep["idx"] . "\" style=\"width:60%;\">" . $row_rep["title"] . "</textarea><button class=\"rep_edit_com\" data-idx=\"" . $row_rep["idx"] . "\" data-pid=\"" . $row_rep["parent_id"] . "\">확인</button></span></p>";
		if ( $_SESSION["mstsp_id"] == $row_rep["uid"] && $_SESSION["mstsp_id"] != "" && $row_rep["del_ok"] == "0" ) {
			$data["content"] .= "<div class=\"font11 sub01Edit_Del_Btn\"><p class=\"inlineR edIt\"><a href=\"#\" class=\"rep_edit\" data-idx=\"" . $row_rep["idx"] . "\">수정</a></p><p class=\"inlineR dEl\" id=\"del_" . $row_rep["idx"] . "\"><a href=\"#\" class=\"rep_del\" data-idx=\"" . $row_rep["idx"] . "\">삭제</a></p><p class=\"inlineR canc\" id=\"canc_" . $row_rep["idx"] . "\" data-idx=\"" . $row_rep["idx"] . "\" style=\"display:none;\"><a href=\"#\">취소</a></p></div>";
		}
		$data["content"] .= "</div></div>";
		$i++;
	}
	$data["last"] = false;
	if ( $tot["cnt"] == ( $start + $i ) ) {
		$data["last"] = true;
	}
	echo json_encode( $data );
	exit;
}

if ( $mode == "likeit_contents" ) {
	$idx = mysql_real_escape_string( $_POST["idx"] );
	if ( $_COOKIE["likeit_contents_".$idx] != "" ) {
		$data["content"] = "2";
		setcookie("likeit_contents_".$idx , "" , time() , "/");
	}else {
		$sql = "update goods set likeit=likeit+1 where no = '" . $idx . "'";
		$query = mysql_query( $sql );	
		if ( $query ) {
			setcookie("likeit_contents_".$idx , "done" , (time() + ( 86400 * 90 ) ) , "/");
			$data["content"] = "1";
		}else {
			$data["content"] = "0";
		}
	}
	echo json_encode( $data );
	exit;
}

if ( $mode == "photo_upload" ) {	 
	$s_uploadDir = $DOCUMENT_ROOT . "/images/contents/" ;
	$s_uploadFileAllowExt = " jpg, jpeg, png, gif "; //allow image
	$AllowSize=1024*1024*10; //30Mb
	$data["msg"] = "";
	$data["text"] = "";
	$data["htm"] = "";
	$img_cnt = count( $_FILES["file_name"]["name"] );
	//echo $img_cnt;
	for ( $i = 0 ; $i < $img_cnt ; $i++ ) {
		$sql_append = "";
		if($_FILES["file_name"]["name"][$i]!="") {
			$l_fileExt = substr(strrchr($_FILES["file_name"]["name"][$i], "."), 1); // 업로드 화일의 확장자를 얻는다.
			$l_fileConvName =  substr(strrchr($_FILES["file_name"]["name"][$i], "."), 0); // 업로드 화일의 이름을 얻는다.
			$fileConvName = md5(uniqid($l_fileConvName, true));
			if(strpos($s_uploadFileAllowExt,strtolower( $l_fileExt ) )>0) {  // 확장자가 맞으면 업로드를 진행한다.
				if($AllowSize > $_FILES["file_name"]["size"][$i]) { //사이즈가 맞으면
					$l_uploadFile = $s_uploadDir.$fileConvName.".".$l_fileExt;
					if(move_uploaded_file($_FILES["file_name"]["tmp_name"][$i], $l_uploadFile)) {
						exif_rotate( $l_uploadFile );
						//썸네일 생성
						make_thumbnail($l_uploadFile, 200, 200, $DOCUMENT_ROOT . "/images/contents/thumb_" . $fileConvName.".".$l_fileExt);
						img_resize( 800 , 800 , $l_uploadFile ); 
						$uid = md5(time().$fileConvName);
						$data["text"] .=  "," . $fileConvName . "." . $l_fileExt;
						$data["msg"] = "1";
						$data["htm"] .= "<li id=\"img_li_".$uid."\" data-txt=\"," . $fileConvName . "." . $l_fileExt . "\"><div class=\"view-img\"><span class=\"imgbox\"><img src=\"/images/contents/thumb_" . $fileConvName . "." . $l_fileExt . "\" ></span><a href=\"#\" class=\"del_img\" data-idx=\"".$uid."\"><span>삭제</span></a></div></li>";
					} else {
						$data["msg"] = "201";
					}
				}else {
					$data["msg"] = "203";
				}
			} else {
				$data["msg"] = "202";
			}
		}
	}
	echo json_encode($data);
	exit;
}


if ( $mode == "paging" ) {
	$cate1 = mysql_real_escape_string( $_GET["cate1"] );
	$cate2 = mysql_real_escape_string( $_GET["cate2"] );
	$skey = mysql_real_escape_string( $_GET["skey"] );
	$page = mysql_real_escape_string( $_GET["page"] );
		
	if ( $sort == "" || $sort == "1" ) {
		$sql_order = " lastUpdate desc "; //최신순 기본
	}elseif ( $sort == "2" ) {
		$sql_order = " price asc "; //최저가순
	}elseif ( $sort == "3" ) {
		$sql_order = " price desc "; //최고가순
	}

	$sql_append = "";

	if ( $cate1 != "" ) {		$sql_append .= " and category1 = '" . $cate1 . "' "; }
	if ( $cate2 != "" ) {		$sql_append .= " and category2 = '" . $cate2 . "' "; }
	if ( $skey != "" ) {		$sql_append .= " and ( title like '%" . $skey . "%' or keywords = '%" . $skey . "%' ) "; }

	$tot = getdata("SELECT count(no) as cnt FROM `goods` where status='판매중' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . "");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 30; //한페이지 줄수-기본값
	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$sql = "SELECT * FROM `goods` where status = '판매중' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by " . $sql_order . " limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$batge = "";
		$goods_info_detail = getdata("select * from goods_detail where no='" . $row["no"] . "' ");
		
		if ( $goods_info_detail["fastDeli"] == "1" ) { $batge .= "빠른배송 | "; }
		if ( $row["best_code"] == "1" ) { $batge .= "인기상품 | "; }
		if ( $row["packed"] == "1" ) { $batge .= "묶음배송상품 | "; }
		if ( $row["ttang"] == "1" ) { $batge .= "땡처리상품 | "; }
		if ( $row["fromOversea"] == "1" ) { $batge .= "해외직배송 | "; }
		$batge = substr( $batge , 0 , -2 );

		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"  data-idx=\"" . $row["no"] . "\"  class=\"detail_link\" ><div class=\"img\"><img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><h3 class=\"ellipsis2\">" . $row["title"] . "</h3><div class=\"pay\">" . number_format( $row["priceWe"] ) . "원</div><div class=\"count\">" . $batge . "</div></div></a></li>";

	}
	if ( $j < 3 || $limit_idx + $j >= $cnt ) {
		$btn = "";
	}else {
		$btn = "<a id=\"page_more\" style=\"margin: 0px auto; text-align: center;\" href=\"#\"><span class=\"_1bt_more\">더보기</span></a>";
	}

	$data["content"] = $content;
	$data["more"] = $btn;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging1" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$groupId = mysql_real_escape_string( $_GET["groupId"] );
	$sql_order = " lastUpdate desc "; //최신순 기본
	
	$tot = getdata("SELECT count(A.no) as cnt FROM goods_detail as A left join goods as B on A.no = B.no where shippingArea='" . $groupId . "' and status='판매중' and preSales = '0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " );
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$sql = "SELECT * FROM goods_detail as A left join goods as B on A.no = B.no where shippingArea='" . $groupId . "' and status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' order by " . $sql_order . " limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["no"] . "' ");		
		
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"><div class=\"img\"><img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $row["title"] . "</h3><span class=\"left bold\">" . number_format( $row["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매" . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging2" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate = mysql_real_escape_string( $_GET["cate"] );
	$cate2 = mysql_real_escape_string( $_GET["cate2"] );
	$sql_order = " lastUpdate desc "; //최신순 기본
	
	$sql_append = "";
	if ( $cate != "" ) { $sql_append .= " and cate_code1 = '" . $cate . "' "; }
	if ( $cate2 != "" ) {	$sql_append .= " and cate_code2 = '" . $cate2 . "' "; }
	
	$tot = getdata("select count(*) as cnt from list_new where 1=1 " . $sql_append . " order by rank asc ");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$sql = "select * from list_new where 1=1 " . $sql_append . "  order by rank asc limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$goods = getdata("select * from goods where no='" . $row["goods_no"] . "' ");		
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["goods_no"] . "' ");		
		
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["goods_no"] . "\"><div class=\"new-icon\"></div><div class=\"img\"><img src=\"" . $goods["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $goods["title"] . "</h3><span class=\"left bold\">" . number_format( $goods["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매" . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
	}
	
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging3" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate = mysql_real_escape_string( $_GET["cate"] );
	$cnt = 100;
	$data["cnt"] = $cnt;
	if ( $page > 5 ) {
		$page = 100000;
	}
	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;
	$sql_append = "";
		
	if ( $cate != "" ) {
		$sql_append = " cate_code like '" . $cate . "%' and ";
	}
	$rst = mysql_query("SELECT * FROM goods_top100 as A left join goods as B ON A.goods_no=B.no where " . $sql_append . " status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' and sdate = '" . date("Ymd") . "' order by rank asc limit " . $limit_idx . "," . $page_set );

	$j = $limit_idx+1;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$goods = getdata("select * from goods where no='" . $row["goods_no"] . "' ");
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["goods_no"] . "' ");
		if ( $row["rank"] == "1" ) {
			$rank_str = "1";
		}else {
			$rank_str = "";
		}
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["goods_no"] . "\"><div class=\"best-num" .  $rank_str . "\">" . $j . "</div><div class=\"img\"><img src=\"" . $goods["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $goods["title"] ."</h3><span class=\"left bold\">" . number_format( $goods["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매 " . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
		$j++;
	}
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging4" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate = mysql_real_escape_string( $_GET["cate"] );
	$cate2 = mysql_real_escape_string( $_GET["cate2"] );
	$sql_order = " lastUpdate desc "; //최신순 기본
	
	$sql_append = "";
	if ( $cate != "" ) { $sql_append .= " and cate_code1 = '" . $cate . "' "; }
	if ( $cate2 != "" ) {	$sql_append .= " and cate_code2 = '" . $cate2 . "' "; }
	$tot = getdata("select count(*) as cnt from list_ttang where 1=1 " . $sql_append . " order by rank asc ");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$sql = "select * from list_ttang where 1=1 " . $sql_append . "  order by rank asc limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$goods = getdata("select * from goods where no='" . $row["goods_no"] . "' ");
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["goods_no"] . "' ");	
		
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["goods_no"] . "\"><div class=\"img\"><img src=\"" . $goods["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $goods["title"] . "</h3><span class=\"left bold\">" . number_format( $goods["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매" . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging5" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$sql_order = " lastUpdate desc "; //최신순 기본
	
	$tot = getdata("SELECT count(no) as cnt FROM `goods` where status='판매중' and fromOversea = '1' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . "");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;
	$sql_append = "";

	$sql = "SELECT * FROM `goods` where status = '판매중' and fromOversea = '1' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by " . $sql_order . " limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["no"] . "' ");		
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"><div class=\"img\"><img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $row["title"] . "</h3><span class=\"left bold\">" . number_format( $row["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매 " . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging6" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate = mysql_real_escape_string( $_GET["cate"] );
	$cate2 = mysql_real_escape_string( $_GET["cate2"] );
	$sql_order = " lastUpdate desc "; //최신순 기본
	
	$sql_append = "";
	if ( $cate != "" ) { $sql_append .= " and cate_code1 = '" . $cate . "' "; }
	if ( $cate2 != "" ) {	$sql_append .= " and cate_code2 = '" . $cate2 . "' "; }
	$tot = getdata("select count(*) as cnt from list_10000 where 1=1 " . $sql_append . " order by rank asc ");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;


	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$sql = "select * from list_10000 where 1=1 " . $sql_append . "  order by rank asc limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$goods = getdata("select * from goods where no='" . $row["goods_no"] . "' ");
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["goods_no"] . "' ");		
		
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["goods_no"] . "\"><div class=\"img\"><img src=\"" . $goods["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $goods["title"] . "</h3><span class=\"left bold\">" . number_format( $goods["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매" . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging7" ) { //15일 판매상품
	$page = mysql_real_escape_string( $_GET["page"] );
	$sql_order = " datePreEnd desc "; //최신순 기본
	
	$tot = getdata("SELECT count(no) as cnt FROM `goods` where status='판매중' and preSales = '1' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . "");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;
	$sql_append = "";

	$sql = "SELECT * FROM `goods` where status = '판매중' and preSales = '1' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by " . $sql_order . " limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {		
		$datePreStart = str_replace( "-" , "." , substr( $row["datePreStart"] , 0 , 10 ) );
		$mk_s_1 = (int)( substr( $row["datePreStart"] , 0 , 4 ) ); //y
		$mk_s_2 = (int)( substr( $row["datePreStart"] , 5 , 2 ) ); //m
		$mk_s_3 = (int)( substr( $row["datePreStart"] , 8 , 2 ) ); //d
		$mk_s_4 = (int)( substr( $row["datePreStart"] , 11 , 2 ) ); //h
		$mk_s_5 = (int)( substr( $row["datePreStart"] , 14 , 2 ) ); //i
		$mk_s_6 = (int)( substr( $row["datePreStart"] , 17 , 2 ) ); //s
		$start_week = date( "D" , mktime( $mk_s_4 , $mk_s_5 , $mk_s_6 , $mk_s_2 , $mk_s_3 , $mk_s_1 ) );
		$mk_e_1 = (int)( substr( $row["datePreEnd"] , 0 , 4 ) ); //y
		$mk_e_2 = (int)( substr( $row["datePreEnd"] , 5 , 2 ) ); //m
		$mk_e_3 = (int)( substr( $row["datePreEnd"] , 8 , 2 ) ); //d
		$mk_e_4 = (int)( substr( $row["datePreEnd"] , 11 , 2 ) ); //h
		$mk_e_5 = (int)( substr( $row["datePreEnd"] , 14 , 2 ) ); //i
		$mk_e_6 = (int)( substr( $row["datePreEnd"] , 17 , 2 ) ); //s
		$end_count = mktime( $mk_e_4 , $mk_e_5 , $mk_e_6 , $mk_e_2 , $mk_e_3 , $mk_e_1 ) - strtotime("now") ;
		$script_content .= "countdown('countdown_" . $row["no"] . "', " . $end_count . ");";
		$status_txt = ( $row["status"] != "판매중" ) ? "<div class=\"soldout\"><img src=\"../assets/images/sub/soldout.png\"></div>":"";
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"><h3 class=\"clear ellipsis1\">" . $row["title"] . "</h3><div class=\"img\">" . $status_txt . "<img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><span>15일 간만 할인 가격</span><span class=\"bold\">" . number_format( $row["priceWe"] ) . "원</span><div class=\"day\"><span>" . $datePreStart . " " . $start_week . "</span><span class=\"time\" id=\"countdown_" . $row["no"] . "\" data-countdown=\"" . $mk_e_2 . "/" . $mk_e_3 . "/" . $mk_e_1 . " " . $mk_e_4 . ":" . $mk_e_5 . ":" . $mk_e_6 . "\"></span></div></div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;

	echo json_encode($data);
}

if ( $mode == "paging10" ) { //카테고리검색
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate1 = mysql_real_escape_string( $_GET["cate1"] );
	$cate2 = mysql_real_escape_string( $_GET["cate2"] );
	$skey = mysql_real_escape_string( $_GET["skey"] );
	$sort = mysql_real_escape_string( $_GET["sort"] );

	if ( $sort == "" || $sort == "1" ) {
		$sql_order = " lastUpdate desc "; //최신순 기본
	}elseif ( $sort == "2" ) {
		$sql_order = " price asc "; //최저가순
	}elseif ( $sort == "3" ) {
		$sql_order = " price desc "; //최고가순
	}

	if ( $cate1 != "" ) {		$sql_append .= " cateWe1 = '" . $cate1 . "' and "; }
	if ( $cate2 != "" ) {		$sql_append .= " cateWe = '" . $cate2 . "' and "; }
	if ( $skey != "" ) {		$sql_append .= " ( title like '%" . $skey . "%' or keywords = '%" . $skey . "%' ) and "; }

	$tot = getdata("SELECT count(*) as cnt FROM `goods` where " . $sql_append . " status='판매중' and preSales = '0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " );

	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;


	$rst = mysql_query("SELECT * FROM `goods` where " . $sql_append . " status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' order by " . $sql_order . " limit " . $limit_idx . " , " . $page_set );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$batge = "";
		$goods_info_detail = getdata("select * from goods_detail where no='" . $row["no"] . "' ");
		
		//if ( $goods_info_detail["fastDeli"] == "1" ) { $batge .= "빠른배송 | "; }
		if ( $row["best_code"] != "" ) { $batge .= "인기상품 | "; }
		if ( $row["packed"] == "1" ) { $batge .= "묶음배송상품 | "; }
		if ( $row["ttang"] == "1" ) { $batge .= "땡처리상품 | "; }
		if ( $row["fromOversea"] == "1" ) { $batge .= "해외직배송 | "; }
		$batge = substr( $batge , 0 , -2 );
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"><div class=\"img\"><img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><h3 class=\"ellipsis2\">" . $row["title"] . "</h3><div class=\"pay\">" . number_format( $row["priceWe"] ) . "원</div><div class=\"count\">" . $batge . "</div></div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;

	$data["sql"] = "SELECT * FROM `goods` where " . $sql_append . " status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' order by " . $sql_order . " limit " . $limit_idx . " , " . $page_set;
	echo json_encode($data);
}

if ( $mode == "paging11" ) { //카테고리검색
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate1 = mysql_real_escape_string( $_GET["cate1"] );
	$cate2 = mysql_real_escape_string( $_GET["cate2"] );
	$skey = mysql_real_escape_string( $_GET["skey"] );
	$sort = mysql_real_escape_string( $_GET["sort"] );

	if ( $sort == "" ) {
		$sql_order = " rscore desc "; //정확도
	}elseif ( $sort == "1" ) {
		$sql_order = " lastUpdate desc "; //최저가순
	}elseif ( $sort == "2" ) {
		$sql_order = " price asc "; //최저가순
	}elseif ( $sort == "3" ) {
		$sql_order = " price desc "; //최고가순
	}
	if ( $cate1 != "" ) {		$sql_append .= " and cateWe1 = '" . $cate1 . "' "; }
	if ( $cate2 != "" ) {		$sql_append .= " and cateWe = '" . $cate2 . "' "; }
	if ( $skey != "" ) {		
		$sql_append .= " and MATCH( title , keywords ) AGAINST('" . $skey . "') ";  
		$sql_append1 = " , MATCH( title , keywords ) AGAINST('" . $skey . "') as rscore ";  
		$sql_search = " and MATCH( title , keywords ) AGAINST('" . $skey . "') ";
	}
	$tot = getdata("SELECT count(*) as cnt FROM `goods` where status='판매중' and preSales = '0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append);

	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$rst = mysql_query("SELECT * $sql_append1 FROM `goods` where status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by " . $sql_order . " limit " . $limit_idx . " , " . $page_set );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$batge = "";
		if ( $row["best_code"] != "" ) { $batge .= "인기상품 | "; }
		if ( $row["packed"] == "1" ) { $batge .= "묶음배송상품 | "; }
		if ( $row["ttang"] == "1" ) { $batge .= "땡처리상품 | "; }
		if ( $row["fromOversea"] == "1" ) { $batge .= "해외직배송 | "; }
		$batge = substr( $batge , 0 , -2 );
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"><div class=\"img\"><img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><h3 class=\"ellipsis2\">" . $row["title"] . "</h3><div class=\"pay\">" . number_format( $row["priceWe"] ) . "원</div><div class=\"count\">" . $batge . "</div></div></a></li>";
	}
	$data["content"] = $content;
	$data["page"] = $page;
	$data["sql"] = "SELECT * $sql_append1 FROM `goods` where status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by " . $sql_order . " limit " . $limit_idx . " , " . $page_set;

	echo json_encode($data);
}

if ( $mode == "paging12" ) {
	$page = mysql_real_escape_string( $_GET["page"] );
	$cate = mysql_real_escape_string( $_GET["cate"] );
	$sql_order = " lastUpdate desc "; //최신순 기본
	
	$sql_append = "";
	if ( $cate != "" ) { $sql_append .= "  cateWe1 = '" . $cate . "' and "; }
	
	$tot = getdata("select count(*) as cnt from goods where  " . $sql_append . " status = '판매중' and descLicenseUsable = '1' and preSales='0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' order by lastUpdate asc ");
	$cnt = $tot["cnt"];
	$data["cnt"] = $cnt;

	$pageIdx = $page;
	$page_set = 20; //한페이지 줄수-기본값

	$total_page = ceil( $cnt / $page_set ); // 총 페이지 수
	$limit_idx = $pageIdx * $page_set - $page_set;

	$sql = "select * from goods where  " . $sql_append . " status = '판매중' and descLicenseUsable = '1' and preSales='0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "'  order by lastUpdate asc limit " . $limit_idx . "," . $page_set;
	$rst = mysql_query( $sql );
	
	$j = 0;
	$content = "";
	$btn = "";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["no"] . "' ");		
		
		$content .= "<li><a href=\"../product/product_view.php?no=" . $row["no"] . "\"><div class=\"img\"><img src=\"" . $row["thumb"] . "\"></div><div class=\"text\"><h3 class=\"clear ellipsis1\">" . $row["title"] . "</h3><span class=\"left bold\">" . number_format( $row["priceWe"] ) . "원</span><span class=\"box-point\">적1%</span>";
		if ( $orders["cnt"] > 0 ) {
			$content .= "<span class=\"right purple\">구매" . $orders["cnt"] . "건</span>";
		}
		$content .= "</div></a></li>";
	}
	
	$data["content"] = $content;
	$data["page"] = $page;
	$data["sql"] = "select * from goods where  " . $sql_append . " status = '판매중' and descLicenseUsable = '1' and preSales='0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "'  order by lastUpdate asc limit " . $limit_idx . "," . $page_set;

	echo json_encode($data);
}

if ( $mode == "question_del" ) {
	$idx = mysql_real_escape_string( $_POST["idx"] );
	$sql = "update goods_qna set del_ok='1' where idx = '" . $idx . "'";
	$query = mysql_query( $sql );
	if ( $query ) {
		$data["rst"] = "1";
	}else {
		$data["rst"] = "0";
	}
	echo json_encode( $data );
	exit;
}

if ( $mode == "review_del" ) {
	$idx = mysql_real_escape_string( $_POST["idx"] );
	$sql = "update goods_review set del_ok='1' where idx = '" . $idx . "'";
	$query = mysql_query( $sql );
	if ( $query ) {
		$data["rst"] = "1";
	}else {
		$data["rst"] = "0";
	}
	echo json_encode( $data );
	exit;
}
?>

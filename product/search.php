<?
$PAGE_TITLE = "검색하기";
include ("../include/header.php");
$rst = mysql_query("select * from search_tbl order by mark asc limit 0, 10 ");
//echo $_COOKIE["mstsp_keyword"];
?>
<!-- BODY -->
<section class="sub-body">
	<div class="tab n2 search-tab fixd">
		<a class="current" data-tab="tab1">인기검색어</a>
		<a class="" data-tab="tab2">최근검색어</a>
	</div>
	<div id="tab1" class="tabcontent current best-search">
		<h2 class="hide">인기검색어</h2>
		<ul class="bg-wite">
		<?
		while ( $row = mysql_fetch_array( $rst ) ) {
			if ( $row["rank"] > 0 ) {
				$rank_str = "up";
			}elseif ( $row["rank"] < 0 ) {
				$rank_str = "down";
			}else {
				$rank_str = "place";
			}
		?>
			<li><a href="./search_list.php?skey=<?=urlencode($row["kwd"])?>"><span><?=$row["mark"]?></span><?=$row["kwd"]?> <span class="<?=$rank_str?>"></span></a></li>
		<?}?>
		<!--
			<li><a href="#"><span>1</span>에어팟 <span class="up"></span></a></li>
			<li><a href="#"><span>2</span>아이폰케이스<span class="up"></span></a></li>
			<li><a href="#"><span>3</span>에어팟케이스<span class="down"></span></a></a></li>
			<li><a href="#"><span>4</span>다우니섬유유연제<span class="up"></span></a></a></li>
			<li><a href="#"><span>5</span>행거<span class="place"></span></a></a></li>
			<li><a href="#"><span>6</span>과자<span class="up"></span></a></a></li>
			<li><a href="#"><span>7</span>고무장갑<span class="up"></span></a></a></li>
			<li><a href="#"><span>8</span>물<span class="place"></span></a></a></li>
			<li><a href="#"><span>9</span>이어폰<span class="up"></span></a></a></li>
			<li><a href="#"><span>10</span>캐리어<span class="up"></span></a></a></li>
			<li><a href="#"><span>11</span>햇반<span class="down"></span></a></a></li>
			<li><a href="#"><span>12</span>과자<span class="place"></span></a></a></li>
			<li><a href="#"><span>13</span>의자<span class="up"></span></a></a></li>
			<li><a href="#"><span>14</span>유아용품<span class="up"></span></a></a></li>
			<li><a href="#"><span>15</span>화장품<span class="up"></span></a></a></li>
		-->
		</ul>
	</div>
	<div id="tab2"  class="tabcontent new-search">
		<h2 class="hide">최근검색어</h2>
		<ul class="group">
<?
$j = 0;
if ( $_COOKIE["my_search"] != "" ) {
	$tmp_history = explode( "|^|" , $_COOKIE["my_search"] );
	for ( $i = 0 ; $i < count( $tmp_history ) ; $i++ ) {
		$data = array();
		$sp_data = explode( "|@|" , $tmp_history[$i] );
		$tmpid = "C".uniqid();
		?>
		<li class="sc_list" id="<?=$tmpid?>"> <a href="./search_list.php?skey=<?=urlencode($sp_data[0])?>"><?=$sp_data[0]?></a> <div class="right"><span class="day"><?=substr( $sp_data[1] , 5 , 2 )?>.<?=substr( $sp_data[1] , 8 , 2 )?></span><a href="#" class="btn-delete" data-keyword="<?=$sp_data[0]?>" data-cid="<?=$tmpid?>"><span class="hide">삭제</span></a></div></li>
		<?
	}
}
?>
		</ul>
		<div class="search-footer">
			<a href="#" class="btn-auto-save-keyword">자동저장기능 <?=( $_COOKIE["my_auto_search_yn"] == "2" ) ? "켜기" : "끄기" ?></a>
			<a href="#" class="btn-delete-all">전체삭제</a>
		</div>
	</div>
	<? include ("../include/quick_btn.php"); ?>
</section>
<?
include ("../include/footer.php");
?>
<script type="text/javascript">
<!--
$(document).on( "click" , ".search-button" , function(){
	if ( $("#skey").val() == "" ) {
		alert("검색어를 입력해주세요");
		return false;
	}else {
		$("#search_frm").submit();
	}
});

$(document).on( "click" , ".btn-delete" , function(){
	var keyword = $(this).data("keyword");
	var cid = $(this).data("cid");
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: '../proc/_product.php',
		data: 'mode=del_keyword&kwd='+keyword,
		success:function (data) {
			//alert(data);
			if (data != "1" ) {
				alert("문제가 발생하여 정상적으로 반영되지 않았습니다.");
				location.reload();
			}else {
				console.log(data);
				$("#"+cid).hide();
			}
		},
		error : function (data) {
			console.log(data);
		} 
	});
});

$(document).on( "click" , ".btn-delete-all" , function(){
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: '../proc/_product.php',
		data: 'mode=del_keyword_all',
		success:function (data) {
			//alert(data);
			if (data != "1" ) {
				alert("문제가 발생하여 정상적으로 반영되지 않았습니다.");
				location.reload();
			}else {
				console.log(data);
				$(".sc_list").hide();
			}
		},
		error : function (data) {
			console.log(data);
		} 
	});
	return false;
});

$(document).on( "click" , ".btn-auto-save-keyword" , function(){
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: '../proc/_product.php',
		data: 'mode=auto_save_keyword',
		success:function (data) {
			//alert(data);
			if ( data == "1" ) {
				$(".btn-auto-save-keyword").html("자동저장기능 켜기");
			}else if ( data == "2" ) {
				$(".btn-auto-save-keyword").html("자동저장기능 끄기");
			}else {
				alert("문제가 발생하여 정상적으로 반영되지 않았습니다.");
				console.log(data);
			}
			location.reload();
		},
		error : function (data) {
			console.log(data);
		} 
	});
	return false;
});

//-->
</script>
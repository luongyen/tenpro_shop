<?
$PAGE_TITLE = "상품정보";
include ("../include/header.php");
$no = mysql_real_escape_string( $_GET["no"] );
$row = getdata("SELECT A.*, B.* FROM `goods` as A left join goods_detail as B ON A.no=B.no where A.no = '" . $no . "' ");

/******************************배송정책관련******************************
무료배송		13879		pay=무료배송 , type='' , fee=0
고정배송비		252700		fee=2500
미입력			2316		pay=xxxxx , type='미입력' , fee=0
수량별비례		80740		tbl='25+2500|25+2500'
수량별차등		10759		tbl='1+2500|11+5000|21+7500|31+9000|41+11500'
************************************************************************/
if ( $row["status"] == "" ) {
	BACK__("상품을 찾을 수 없습니다.");
}elseif ( $row["status"] != "판매중" ||  $row["dateStart"] > date("Y-m-d h:i:s") || $row["dateEnd"] < date("Y-m-d h:i:s") ) {
	BACK__("판매 중지 중인 상품입니다.");
}
if ( $row["type"] == "" || $row["type"] == "미입력" || $row["pay"] == "무료배송" ) { //무료배송
	$delivery_str = "무료배송";
}elseif ( $row["type"] == "고정배송비" ) {
	$delivery_str = $row["pay"] . " " . number_format( $row["fee"] ) . "원";//구매자(선불,착불)선택, 무료배송, 선결제, 착불
}elseif ( $row["type"] == "수량별비례" || $row["type"] == "수량별차등" ) {
	$arr_deli_tbl = explode( "|" , $row["tbl"] );
	$arr_deli_fee = explode( "+" , $arr_deli_tbl[0] );
	$delivery_str = $row["pay"] . " " . number_format( $arr_deli_fee[1] ) . "원";
}
if ( $row["selectOpt"] != "" ) {
	$goods_option = json_decode( $row["selectOpt"] );
	//print_r($goods_option);
}
if ( $row["infoDutyContent"] != "" ) {
	$duty_content = explode( "|^|" , $row["infoDutyContent"] );
}
$review_info = getdata("select count(*) as cnt, avg(score) as score from goods_review where no='" . $row["no"] . "' and del_ok='0' ");
$inquiry_info = getdata("select count(*) as cnt from goods_qna where no='" . $row["no"] . "' and del_ok='0' ");
$rst_review = mysql_query("select * from goods_review where no='" . $row["no"] . "' and del_ok='0' order by idx desc limit 0 , 5 ");
$rst_inquiry = mysql_query("select * from goods_qna where no='" . $row["no"] . "' and del_ok='0' order by idx desc limit 0 , 5 ");
if ( $_SESSION["mstsp_id"] != "" ) {
	$inquiry_my_info = getdata("select count(*) as cnt from goods_qna where no='" . $row["no"] . "' and uid='" . $_SESSION["mstsp_id"] . "' and del_ok='0' ");
	$rst_my_inquiry = mysql_query("select * from goods_qna where no='" . $row["no"] . "' and uid='" . $_SESSION["mstsp_id"] . "' and del_ok='0' order by idx desc");
}

//페이징준비처리
$review_pageIdx = 1;	
$review_page_set = 5; //한페이지 줄수-기본값
$review_block_size = 5;
$review_start_num = 1;
$review_end_num = 5;
$review_total_page = ceil( $review_info["cnt"] / 5 ); // 총 페이지 수
$review_limit_idx = 0;

$inquiry_pageIdx = 1;	
$inquiry_page_set = 5; //한페이지 줄수-기본값
$inquiry_block_size = 5;
$inquiry_start_num = 1;
$inquiry_end_num = 5;
$inquiry_total_page = ceil( $inquiry_info["cnt"] / 5 ); // 총 페이지 수
$inquiry_limit_idx = 0;
//페이징준비 끝

/****************************************************옵션******************************************************/
if ( $row["optCnt"] != "" ) {
	$rst_option = mysql_query("select * from goods_option where goods_no='" . $row["no"] . "' and depth='1' ");
	$rst_option_data = mysql_query("select * from goods_option_data where goods_no='" . $row["no"] . "' order by okey asc ");
	if ( $row["optType"] == "c" ) {
		$rst_option = mysql_query("select * from goods_option where goods_no='" . $row["no"] . "' and depth='1' ");	
	}elseif ( $row["optType"] == "i" ) {
		for ( $i = 1 ; $i <= count( $row["optCnt"] ) ; $i++ ) {
			${"rst_option_".$i} = mysql_query("select * from goods_option where goods_no='" . $row["no"] . "' and depth='" . $i . "' ");
		}	
	}
}
/****************************************************옵션******************************************************/
$no = mysql_real_escape_string($_GET["no"]);
if ( $_COOKIE["mstsp_recent"] != "") {
	$recent = explode( "," , $_COOKIE["mstsp_recent"] );
	$recent_save = true;
	$recent_new = "";
	for ( $i = 0 ; $i < count( $recent ) ; $i++ ) {
		if ( $recent[$i] == $no ) {
			$recent_save = false;
		}else{
			if ( $i == 0 ) {
				$recent_new = $recent[$i];
			}else {
				$recent_new .= "," . $recent[$i];
			}
		}
	}
	if ( $recent_save == false ) {
		if ( $i == 1 ) {
			$recent_new = $no;
		}else {
			$recent_new .= "," . $no;
		}
	}else {
		$recent_new .= "," . $no;
	}
	setcookie("mstsp_recent" , $recent_new , time()+31536000 , "/");
}else {
	setcookie("mstsp_recent" , $no , time()+31536000 , "/");
}
//	setcookie("mstsp_recent" , "" , time()+31536000 , "/");
?>
<!-- BODY -->
<section class="sub-body">
	<div class="product-view">
		<div class="top_bn_zone">
			<div class="img"><img src="<?=str_replace( "_stt_330.png" , "_img_760" , $row["thumb"])?>"></div>
			<div class="text" style="padding-top:10px;width:60%;display:inline-block;vertical-align:top;">
				<div class="title group">
					<h2><?=$row["title"]?></h2>
					<div class="like"><a href="#"><?=$row["likeit"]?></a></div>
					<div class="share"><a href="#"><?=$row["share"]?></a></div>
					<!--공유팝업 추가-->
					<div class="share-pop" style="display:none;">
						<h3>이 인사이트를 sns에 공유하세요</h3>
						<div class="share-area">
							<a href="#" class="share_icon1"><span class="hide">인스타</span></a>
							<a href="#" class="share_icon2"><span class="hide">페이스북</span></a>
							<a href="#" class="share_icon3"><span class="hide">카카오톡</span></a>
							<a href="#" class="share_icon4"><span class="hide">링크</span></a>
						</div>
					</div>
				</div>
			</div>
			<?
			if ( $_MEM_INFO["reseller"] == "1" ) {
			?>
			<div class="reseller">
				<label>리셀러 홍보용 URL</label>
				<div class="input-box"><input type="text" name="" value="<?=$_MEM_INFO["reseller_url"]?>" readonly></div>
				<button type="button" class="btn"  id="copyContent" data-clipboard-text ="<?=$_MEM_INFO["reseller_url"]?>">복사</button>
			</div>
			<?}?>
				<div class="delivery" style="padding:10px;">
					<h3 style="font-size:14px; padding-bottom:5px;">배송정보</h3>
					<p style="line-height:1.8em;color:#555;"><?=$row["method"]?> : <?=$delivery_str?><br><?=$row["wating"]?>(평균출고일 <?=$row["sendAvg"]+1?>일 <span><?= ( $row["fastDeli"] == "1" ) ? "빠른배송" : "" ?></span>)<br>추가배송비 : 제주지역 + <?=number_format( $row["jeju"] )?>원 /도서산간 + <?=number_format( $row["islands"] )?>원</p>
				</div>
		</div>
		<div class="top_fix_zone" id="topBar">
			<div class="tab product-tab n4">
				<a href="#product-tab1" class="current">상세설명</a>
				<a href="#product-tab2" class="">상품평<span> <?=$review_info["cnt"]?></span></a>
				<a href="#product-tab3" class="">상품문의<span> <?=$inquiry_info["cnt"]?></span></a>
				<a href="#product-tab4" class="">교환/반품</a>
			</div>
		</div>
		
		<div class="top_con_zone" id="fixNextTag">

			<!--상세 설명-->
			<div id="product-tab1">
				<h2 class="hide">상세설명</h2>
				<div class="product-info">
					<div id='contents_info'>
					<?
						if ( $row["contentsRenewal"] != "" ) {
							$contents = $row["contentsRenewal"];
						}else {
							$contents = $row["contents"];							
						}
						$contents_div_open = substr_count( $contents , "<div" );
						$contents_div_close = substr_count( $contents , "</div" );
						if ( $contents_div_open > $contents_div_close ) {
							for ( $i = 0 ; $i < ( $contents_div_open - $contents_div_close ) ; $i++ ) {
								$contents .= "</div>";
							}
						}
						$contents_p_open = substr_count( $contents , "<p " );
						$contents_p_close = substr_count( $contents , "</p" );
						if ( $contents_p_open > $contents_p_close ) {
							for ( $i = 0 ; $i < ( $contents_p_open - $contents_p_close ) ; $i++ ) {
								$contents .= "</p>";
							}
						}
						$contents = strip_tags( $contents , "<p><b><br><strong><center><div><img><font><table><tbody><tfoot><thead><tr><td><col><colgroup>" );
						echo $contents;
					?>
					</div>
					<? if ( strpos( $row["infoDutyContent"] , "item|@|" ) !== false ) {?>
					<div class="title">전자상거래 등에서의 상품정보제공고시</div>
					<div class="group info">
						<table>
							<colgroup><col width="20%"><col width=""></colgroup>
							<? for ( $i = 0 ; $i < count( $duty_content ) ; $i++ ) {
								$duty_item = explode( "|@|" , $duty_content[$i] );
								if ( $duty_item[0] == "item" ) {
									echo "<tr><th>" . $duty_item[1] . "</th><td>" . $duty_item[2] . "</td></tr>";
								}
							}
							?>
						</table>
					</div>
					<?}?>
					<? if ( strpos( $row["infoDutyContent"] , "transaction|@|" ) !== false ) {?>
					<div class="title">거래조건에 관한 정보</div>
					<div class="group info">
						<table>
							<colgroup><col width="20%"><col width=""></colgroup>
							<? for ( $i = 0 ; $i < count( $duty_content ) ; $i++ ) {
								$duty_item = explode( "|@|" , $duty_content[$i] );
								if ( $duty_item[0] == "transaction" ) {
									echo "<tr><th>" . $duty_item[1] . "</th><td>" . $duty_item[2] . "</td></tr>";
								}
							}
							?>
						</table>
					</div>
					<?}?>
					<div class="group guide-text">
						<ul>
							<li>신기한쇼핑에 등록된 상품과 상품의 내용은 판매원 및 공급사로부터
							제공받은 것으로 신기한 쇼핑에서는 그내용에 대하여 일체의 책임을 지지않습니다.</li>
							<li>신기한 쇼핑 내 모든 사진 및 콘텐츠를 무단으로 도용한 사실이 적발될 시 법적 조치를 받을 수 있습니다.</li>
							<li>판매원 정보 상품고시 참고</li>
						</ul>
					</div>
				</div>
			</div>
			<!--//상세 설명-->
			<!--상품평-->
			<div id="product-tab2">
				<h2 class="hide">상품평</h2>
				<div class="title">
					<h3>구매자 평균 별점</h3>
					<div class="title-marks">
						<div class="marks">
						<?						
						$avg_score = REVIEW_STAR($review_info["score"]);
						echo $avg_score;
						?>
						</div> <?=($review_info["score"] != "" ) ? number_format( $review_info["score"] , 1 ) : "0.0"?>점
					</div>
				</div>
				<div class="review-list">
					<h3 class="hide">상품평 리스트</h3>
					<ul>
					<?
					while ( $row_review = mysql_fetch_array( $rst_review ) ) {
					?>
						<li>
							<div class="id-data">
								<?=mb_substr( $row_review["uid"] , 0 , -3 , "UTF-8" ) ?>*** 
								<span class="gray"><?=str_replace( "-" , "." , substr( $row_review["reg_date"] , 0 , 10 ) )?></span>
								<? if ( $_SESSION["mstsp_id"] == $row_review["uid"] ) {?><a class="btn btn-gray review_del " style="color:#777;" data-idx="<?=$row_review["idx"]?>" href="#" >x 삭제</a><?}?>
								<div class="marks right">평점:<?=REVIEW_STAR($row_review["score"])?></div>
							</div>
							<p><?=nl2br( $row_review["title"] )?></p>
						</li>
					<?}?>
					</ul>
					
					<? if($review_info["cnt"]>0) {?>
					<div class="pageview" id="review-paging">
						<button type="button" class="pre" >이전리스트</button>
						<?
							$review_vpage = 1;
							$review_spage = 5;
							if ( $review_spage >= $review_total_page ) { $review_spage = $review_total_page; }

							for ( $review_i = $review_vpage ; $review_i <= $review_spage ; $review_i++ ) { 
								if ( $review_pageIdx == $review_i ) {
									echo "<a href=\"#\" class=\"on review_paging\"  data-page=\"" . $review_i . "\">" . $review_i . "</a>";
								}else {
									echo "<a href=\"#\" class=\"review_paging\"  data-page=\"" . $review_i . "\">" . $review_i . "</a>";
								}
							}
							echo ( $review_pageIdx < $review_total_page )? "<button type=\"button\" class=\"next review_paging\" data-page=\"2\" >다음리스트</button>" : "<button type=\"button\" class=\"next\" >다음리스트</button>";
						?>
					</div>
					<?}?>
					<div class="btn-area"><button type="button" class="btn btn-point layer-open-review" >리뷰등록</a></div>
				</div>
			</div>
			<!--//상품평-->

			<!--상품문의-->
			<div id="product-tab3"  class="inquiry">
				<h2 class="hide">상품문의</h2>
				<div class="tab n2">
					<a class="current" data-tab="tab1">전체<span class="point"><?=$inquiry_info["cnt"]?></span></a>
					<a class="" data-tab="tab2">내문의<span class="point"><?=$inquiry_my_info["cnt"]?></span></a>
				</div>
				<div id="tab1" class="tabcontent current">
					<h3 class="hide">전체</h3>
					<ul>
					<?
					while ( $row_inquiry = mysql_fetch_array( $rst_inquiry ) ) {
						if ( $row_inquiry["ans_content"] != "" && $row_inquiry["ans_date"] != "" ) {
							$inquiry = "<div class=\"answer-icon\">답변<br>완료</div>";
						}else {
							$inquiry = "<div class=\"answer-icon2\">답변<br>대기</div>";
						}
					?>
						<li class="<?=( $row_inquiry["content"] != "" || $row_inquiry["ans_content"] != "" ) ? "inquiry_title" : "" ?>">
							<?=$inquiry?>
							<div class="question">
								<div class="id-data"><span class="gray"><?=mb_substr( $row_inquiry["uid"] , 0 , -3 , "UTF-8" ) ?>*** | <?=str_replace( "-" , "." , substr( $row_inquiry["reg_date"] , 0 , 10 ) )?></span>
								<?= ( $row_inquiry["sec"] == "1" ) ? "&nbsp;&nbsp;<img src=\"../assets/images/icon_sec1.png\" border=\"0\" alt=\"\" style=\"width:12px;vertical-align:middle;\" />" : "" ?>
								<? if ( $_SESSION["mstsp_id"] == $row_inquiry["uid"] ) {?><a class="btn btn-gray question_del" data-idx="<?=$row_inquiry["idx"]?>" href="#" >x 삭제</a><?}?>
							</div>
								<p class="title ellipsis1"><?=$row_inquiry["title"]?></p>
								<div class="inquiry_arrow arrow-up" data-idx="<?=$row_inquiry["idx"]?>"></div>
							</div>
						</li>
						<? if ( ( $row_inquiry["content"] != "" || $row_inquiry["ans_content"] != "" ) && ( $row_inquiry["sec"] != "1" || ( $row_inquiry["sec"] == "1" && $_SESSION["mstsp_id"] == $row_inquiry["uid"] ) ) ) {?>
						<li class="question-answer inquiry_sub">
							<div class="inquiry-text">
								<? if ( $row_inquiry["content"] != "" ) {?>
								<div class="question-text"><?=nl2br( $row_inquiry["content"] )?></div>
								<?}?>								
								<? if ( $row_inquiry["ans_content"] != "" ) {?>
								<div class="answer">
									<div class="id-data"><span class="answer-id">판매자답변</span><span class="gray"> <?=str_replace( "-" , "." , substr( $row_inquiry["ans_date"] , 0 , 10 ) )?></span></div>
									<p><?=nl2br( $row_inquiry["ans_content"] )?></p>
								</div>
								<?}else{?>
								<div class="answer">
									<div class="id-data"><span class="answer-id">답변대기중</span></div>
									<p>빠른 시간내에 답변드리도록 하겠습니다.</p>
								</div>
								<?}?>
							</div>
						</li>
						<?}?>
					<?}?>
					</ul>
					
					<? if($inquiry_info["cnt"]>0) {?>
					<div class="pageview" id="inquiry-paging">
						<button type="button" class="pre" >이전리스트</button>
						<?
							$inquiry_vpage = 1;
							$inquiry_spage = 5;
							if ( $inquiry_spage >= $inquiry_total_page ) { $inquiry_spage = $inquiry_total_page; }

							for ( $inquiry_i = $inquiry_vpage ; $inquiry_i <= $inquiry_spage ; $inquiry_i++ ) { 
								if ( $inquiry_pageIdx == $inquiry_i ) {
									echo "<a href=\"#\" class=\"on inquiry_paging\"  data-page=\"" . $inquiry_i . "\">" . $inquiry_i . "</a>";
								}else {
									echo "<a href=\"#\" class=\"inquiry_paging\"  data-page=\"" . $inquiry_i . "\">" . $inquiry_i . "</a>";
								}
							}
							echo ( $inquiry_pageIdx < $inquiry_total_page )? "<button type=\"button\" class=\"next inquiry_paging\" data-page=\"2\" >다음리스트</button>" : "<button type=\"button\" class=\"next\" >다음리스트</button>";
						?>
					</div>
					<?}?>
				</div>
				<div id="tab2" class="tabcontent">
					<h3 class="hide">내문의</h3>
					<ul>
					<?
					while ( $row_my_inquiry = mysql_fetch_array( $rst_my_inquiry ) ) {
						if ( $row_my_inquiry["ans_content"] != "" && $row_my_inquiry["ans_date"] != "" ) {
							$my_inquiry = "<div class=\"answer-icon\">답변<br>완료</div>";						
						}else {
							$my_inquiry = "<div class=\"answer-icon2\">답변<br>대기</div>";
						}
					?>
						<li class="<?=( $row_my_inquiry["content"] != "" || $row_my_inquiry["ans_content"] != "" ) ? "inquiry_title" : "" ?>">
							<?=$my_inquiry?>
							<div class="question">
								<div class="id-data">
									<span class="gray"><?=mb_substr( $row_my_inquiry["uid"] , 0 , -3 , "UTF-8" ) ?>*** | <?=str_replace( "-" , "." , substr( $row_my_inquiry["reg_date"] , 0 , 10 ) )?></span>
									<?= ( $row_my_inquiry["sec"] == "1" ) ? "&nbsp;&nbsp;<img src=\"../assets/images/icon_sec1.png\" border=\"0\" alt=\"\" style=\"width:12px;vertical-align:middle;\" />" : "" ?>
									<? if ( $_SESSION["mstsp_id"] == $row_inquiry["uid"] ) {?><a class="btn btn-gray question_del" data-idx="<?=$row_inquiry["idx"]?>" href="#" >x 삭제</a><?}?>
								</div>
								<p class="title ellipsis1"><?=$row_my_inquiry["title"]?></p>
								<div class="inquiry_arrow arrow-up" data-idx="<?=$row_my_inquiry["idx"]?>"></div>
							</div>
						</li>
						<? if ( $row_my_inquiry["content"] != "" || $row_my_inquiry["ans_content"] != "" ) {?>						
						<li class="question-answer inquiry_sub">
							<div class="inquiry-text">
								<? if ( $row_my_inquiry["content"] != "" ) {?>
								<div class="question-text"><?=nl2br( $row_my_inquiry["content"] )?></div>
								<?}?>								
								<? if ( $row_my_inquiry["ans_content"] != "" ) {?>
								<div class="answer">
									<div class="id-data"><span class="answer-id">판매자답변</span><span class="gray"> <?=str_replace( "-" , "." , substr( $row_my_inquiry["ans_date"] , 0 , 10 ) )?></span></div>
									<p><?=nl2br( $row_my_inquiry["ans_content"] )?></p>
								</div>
								<?}else{?>
								<div class="answer">
									<div class="id-data"><span class="answer-id">답변대기중</span></div>
									<p>빠른 시간내에 답변드리도록 하겠습니다.</p>
								</div>
								<?}?>
							</div>
						</li>
						<?}?>
					<?}?>
					</ul>
				</div>
				<div class="btn-area"><button type="button" class="btn btn-point layer-open-inquiry">문의하기</a></div>
			</div>
			<!--//상품문의-->
			<!--교환/반품-->
			<div id="product-tab4"  class="inquiry">
				<h2>교환/반품</h2>

				<div class="product-info">
					<div class="group info">
						<table>
							<tbody>					
								<tr>
									<th>반품/교환기준</th>
									<td>
										반품 및 교환은 상품 수령확인을 하지 않은 상태에서 상품 수령 후 7일 이내 판매자와 협의하여 신청할 수 있습니다.<br><br>
										<br>
										<b>반품/교환이 불가능한 경우</b>
										<br>
										· 구매자 단순 변심 시, 반품가능기간(상품 수령 후 7일)이 경과된 경우<br>
										· 포장을 개봉하였거나 포장이 훼손되어 상품가치가 현저히 상실된 경우(예. 식품, 화장품, 향수류 등)<br>
										· 구매자의 사용(라벨이 떨어진 의류 등) 또는 시간의 경과에 의하여 상품의 가치가 현저히 감소한 경우<br>
										· 고객주문 확인 후, 상품제작에 진행되는 주문제작 상품인 경우<br>
										· 사은품이 있는 경우 제품과 같이 발송된 해당 사은품을 반품하지 않은 경우<br>
										· 복제가 가능한 상품 등의 포장을 훼손한 경우(CD, DVD, 게임, 도서 등의 경우 포장 개봉 시)<br />
										· 냉동, 냉장식품의 경우 반송 시 상품의 품질이 유지되지 못하므로 변심에 의한 교환 및 반품은 불가<br />
										· 기타 전자상거래 등에서의 소비자보호에 관한 법률이 정하는 소비자 청약철회 제한에 해당하는 경우
									</td>
								</tr>
								<tr>
									<th>배송비 부담 정책</th>
									<td>
										· 판매자 부담 : 판매자의 관리소홀 및 부주의로 인해 훼손/파손된 제품 및 상품설명과 상이한 제품이 배송된 경우<br>
										· 구매자 부담 : 구매자의 단순변심 및 판단착오로 인한 교환/반품의 경우
									</td>
								</tr>
								<tr>
									<th>반품 및 교환 절차</th>
									<td>
										판매자와 전화협의 : <?=$CSC_PHONE__?>
									</td>
								</tr>
								<tr> 
									<th>반품배송비</th>
									<td>
										<?= number_format( $row["return_deliAmt"] ) ?>원 (주문시 배송비가 무료인 경우 구매자 부담 사유로 반품신청 시 왕복배송비가 부과될 수 있습니다)
									</td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--//교환/반품-->
		</div>

	</div>

	<div class="product-footer">
		<div class="pay">특별회원가<span class="point bold"><?=number_format( $row["priceWe"] )?>원</span></div>
		<div class="btn-area"><button type="button"  onclick = "location.href = '../order/order.php?no=<?=$no?>' " class="btn btn-point">구매하기</button></div>
	</div>
	<div class="btn-top"><a href="#top"><span class="hide">위로</span></a></div>
</section>

<!--popup/ open = block -->
<!--review popup/ 리뷰팝업-->
<div id="layer-box-review" class="review-popup layer-box">
	<div id="layer-popup-review" class="layer-popup">
		<div class="right"><a href="#" class="pop-close" onclick="return false;"><span class="hide">닫기</span></a></div>
		<div class="title">리뷰등록</div>
		<form id="frm_review" >
		<input type="hidden" name="mode" value="review_reg">
			<div class="body">
				<!--input type="text" name="review_title" id="review_title"-->
				<textarea name="review_title" id="review_title" placeholder="내용을 입력해주세요"></textarea>
				<!--div class="filebox bs3-primary">
                	<div class="upload-file"><input class="upload-name" value="파일선택" disabled="disabled"></div>
                 	<label for="ex_filename">사진첨부</label> 
                    <input type="file" id="ex_filename" class="upload-hidden"> 
                </div-->
                <div class="marks-star right">
                	<label for="">평점</label>
                	<select id="review_score" name="review_score" style="width:130px;">
                		<option value="5">★★★★★ 5점</option>
                		<option value="4">★★★★☆ 4점</option>
                		<option value="3">★★★☆☆ 3점</option>
                		<option value="2">★★☆☆☆ 2점</option>
                		<option value="1">★☆☆☆☆ 1점</option>
                	</select>
                </div>
     		</div>
			<div class="footer">
				<button type="button" class="btn btn-point review_reg">리뷰 등록</button>
			</div>
		</form>
	</div>
</div>
<!--//review popup-->

<!--inquiry popup/ 문의팝업-->
<div id="layer-box-inquiry" class="inquiry-popup layer-box">
	<div id="layer-popup-inquiry" class="layer-popup">
		<div class="right"><a href="#" class="pop-close" onclick="return false;"><span class="hide">닫기</span></a></div>
		<div class="title" style="padding-right:40px;">
			<div class="checks"><input type="checkbox" id="secret-text" name="secret-check" checked><label for="secret-text">비밀글로 문의하기</label></div>
		</div>
		<div class="body">
			<form>
				<input type="text" name="inquiry_title" id="inquiry_title" placeholder="제목입력">
				<textarea name="inquiry_contents" id="inquiry_contents"></textarea>
            </form>
		</div>
		<div class="footer">
        	<button type="button" class="btn btn-gray pop-close" onclick="return false;">취소하기</button>
        	<button type="button" class="btn btn-point inquiry_reg">문의하기</button>
        </div>
	</div>
</div>
<!--//inquiry popup-->
<div class="layer-background" ></div>

<script type="text/javascript" src="../assets/js/clipboard.min.js"></script>
<script type="text/javascript">
<!--
	var clipboard = new ClipboardJS('#copyContent');
	clipboard.on('success', function(e) {
		console.log(e);
		alert( "URL 이 복사 되었습니다." );
	});
	clipboard.on('error', function(e) {
		console.log(e);
	});
	
	$( document ).on( "click", ".layer-open-review", function( e ) {
		//set_menu_toggle('false');
		//$('body').addClass("fixed");
		$("#layer-popup-review.layer-popup").removeClass("hide");
		$(".layer-background").addClass("open");
		$(".review-popup").addClass("open"); 
		$("#layer-box-review").show();
	});
	$( document ).on( "click", ".layer-open-inquiry", function( e ) {
		//set_menu_toggle('false');
		//$('body').addClass("fixed");
		$("#layer-popup-inquiry.layer-popup").removeClass("hide");
		$(".layer-background").addClass("open");
		$(".inquiry-popup").addClass("open"); 
		$("#layer-box-inquiry").show();
	});
	
	$( document ).on( "click", ".review_paging", function( e ) {
		var page = $(this).data("page");
		var ref_idx = "<?=$no?>";
		var params = { page:page, ref_idx:ref_idx, mode:'product_detail_review_paging' }
		$.ajax({
			url:"./_proc_json.php",
			data:params,
			type:"GET",
			dataType:"json",
			success:function(data){
				console.log(data);
				$(".review-list").html(data.content);
			},
			error:function(jqXHR,textStatus,errorThrown){		
				console.log("error");	
				//pop_policy_new("3", "로딩에러", "", "" );
			},
			beforeSend: function () {
				show_loading();
			}
			,complete: function () {
				$("#div_ajax_load_image").hide();
			}
		});
		return false;
	});

	$( document ).on( "click", ".review_reg", function( e ) {
		var score = $("#review_score option:selected").val();
		var title = $("#review_title").val();
		var ref_idx = "<?=$no?>";
		var params = { score:score, title:title, ref_idx:ref_idx, mode:'review_reg' }
		if ( title.length < 5 ) {
			alert("리뷰 내용을 입력해주세요.");
		}else {
			$.ajax({
				url:"./_proc_json.php",
				data:params,
				type:"POST",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.content == "login fail" ) {
						alert("로그인 후 이용가능합니다.");
						location.reload();
					}else if ( data.content == "succ" ) {
						alert("등록되었습니다.");
						location.reload();
					}else if ( data.content == "error" ) {
						alert("등록 도중 오류가 발생 하였습니다. \n오류가 계속될 경우 하단 고객센터로 전화 부탁드립니다.\nTEL:<?=$CSC_PHONE__?>");
						location.reload();
					}
				},
				error:function(jqXHR,textStatus,errorThrown){		
					console.log("error");
					//pop_policy_new("3", "로딩에러", "", "" );
				},
				beforeSend: function () {
					show_loading();
				}
				,complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
		return false;
	});

	$( document ).on( "click", ".inquiry_reg", function( e ) {
		var title = $("#inquiry_title").val();
		var content = $("#inquiry_contents").val();
		var ref_idx = "<?=$no?>";
		if ( $('#secret-text').is(":checked") ) {
			var sec = "1";
		}else {
			var sec = "0";
		}
		var params = { sec:sec, title:title, content:content, ref_idx:ref_idx, mode:'inquiry_reg' }
		if ( title.length < 5 ) {
			alert("문의 제목을 입력해주세요.");
		}else if ( content.length < 5 ) {
			alert("문의 내용을 입력해주세요.");
		}else {
			$.ajax({
				url:"./_proc_json.php",
				data:params,
				type:"POST",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.content == "login fail" ) {
						alert("로그인 후 이용가능합니다.");
						location.reload();
					}else if ( data.content == "succ" ) {
						alert("등록되었습니다.");
						location.reload();
					}else if ( data.content == "error" ) {
						alert("등록 도중 오류가 발생 하였습니다. \n오류가 계속될 경우 하단 고객센터로 전화 부탁드립니다.\nTEL:<?=$CSC_PHONE__?>");
						location.reload();
					}
				},
				error:function(jqXHR,textStatus,errorThrown){		
					console.log("error");
					//pop_policy_new("3", "로딩에러", "", "" );
				},
				beforeSend: function () {
					show_loading();
				}
				,complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
		return false;
	});
	$(function(){		
		$('.inquiry_sub').slideUp();
		$('.inquiry_title').click(function(){
			if ( $(this).next().css("display") == "none" ) {
				$('.inquiry_sub').slideUp();
				$('.inquiry_arrow').addClass("arrow-up").removeClass("arrow-down");
				$(this).next().slideDown();
				$(this).find(".inquiry_arrow").addClass("arrow-down").removeClass("arrow-up");
			}else {
				$('.inquiry_sub').slideUp();
				$('.inquiry_arrow').addClass("arrow-up").removeClass("arrow-down");
			}
		});
	});
	
	$( document ).on( "click", ".question_del", function( e ) {
		var idx = $(".question_del").data("idx");
		var con = confirm("삭제하시겠습니까?");
		var params = { idx:idx, mode:'question_del' }
		if ( con ) {
			$.ajax({
				url:"./_proc_json.php",
				data:params,
				type:"POST",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.rst == "0" ) {
						alert("삭제 도중 오류가 발생 하였습니다. \n오류가 계속될 경우 하단 고객센터로 전화 부탁드립니다.\nTEL:<?=$CSC_PHONE__?>");
					}
					location.reload();
				},
				error:function(jqXHR,textStatus,errorThrown){		
					console.log("error");
				},
				beforeSend: function () {
					show_loading();
				}
				,complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
		return false;
	});
	$( document ).on( "click", ".review_del", function( e ) {
		var idx = $(".review_del").data("idx");
		var con = confirm("삭제하시겠습니까?");
		var params = { idx:idx, mode:'review_del' }
		if ( con ) {
			$.ajax({
				url:"./_proc_json.php",
				data:params,
				type:"POST",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.rst == "0" ) {
						alert("삭제 도중 오류가 발생 하였습니다. \n오류가 계속될 경우 하단 고객센터로 전화 부탁드립니다.\nTEL:<?=$CSC_PHONE__?>");
					}
					location.reload();
				},
				error:function(jqXHR,textStatus,errorThrown){		
					console.log("error");
				},
				beforeSend: function () {
					show_loading();
				}
				,complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
		return false;
	});
	
//-->
</script>		
<?
include ("../include/footer.php");
?>
<?
$PAGE_TITLE = "검색하기";
//내가 찾은 단어 저장
$exist_key = false;
if ( $_COOKIE["my_auto_search_yn"] != "2" ) { //자동 저장 2이면 사용안함
	if( $_COOKIE["my_search"] !=  "" ) {
		$my_search = $_COOKIE["my_search"] ;
		$tmp_key = explode("|^|" , $my_search );
		for ( $i = 0 ; $i < count( $tmp_key ) ; $i++ ) {
			$tmp_keys = explode("|@|" , $tmp_key[$i] );
			if ( $tmp_keys[0] == $_GET["skey"] ) {
				$exist_key = true;
				break;
			}
		}
	}

	if ( $exist_key == false && $_GET["skey"] != "" ) {
		if ( $my_search !=  "" ) {
			@setcookie( "my_search" , $_GET["skey"] . "|@|" . date("Y-m-d H:i:s") . "|^|" . $my_search , time() + 39999999 , "/" );		
		}else {
			@setcookie( "my_search" , $_GET["skey"] . "|@|" . date("Y-m-d H:i:s") , time() + 39999999 , "/" );
		}
	}
}
//echo $my_search;
//내가 찾은 단어 저장 끝
include ("../include/header.php");
$cate1 = mysql_real_escape_string( $_GET["cate1"] );
$cate2 = mysql_real_escape_string( $_GET["cate2"] );
$skey = mysql_real_escape_string( $_GET["skey"] );
$page = mysql_real_escape_string( $_GET["page"] );
if ( $page == "" ) {
	$page = "2";
}
$offset = ( $page - 1 ) * 20;
$sql_append = "";
$sql_append_search = "";

if ( $sort == "" || $sort == "1" ) {
	$sql_order = " lastUpdate desc "; //최신순 기본
}elseif ( $sort == "2" ) {
	$sql_order = " price asc "; //최저가순
}elseif ( $sort == "3" ) {
	$sql_order = " price desc "; //최고가순
}

if ( $cate1 != "" ) {		$sql_append .= " cateWe1 = '" . $cate1 . "' and "; }
if ( $cate2 != "" ) {		$sql_append .= " cateWe = '" . $cate2 . "' and "; }
if ( $skey != "" ) {		$sql_append .= " ( title like '%" . $skey . "%' or keywords = '%" . $skey . "%' ) and "; $sql_append_search = " and ( title like '%" . $skey . "%' or keywords = '%" . $skey . "%' ) and "; }

$paging = true;
$tot = getdata("SELECT count(*) as cnt FROM `goods` where " . $sql_append . " status='판매중' and preSales = '0' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1'  and adminAgree = '1' " );

if ( $tot["cnt"] <= 20 ) {
	$paging = false;
}
$stime = time();
$rst = mysql_query("SELECT * FROM `goods` where " . $sql_append . " status = '판매중' and preSales = '0' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1'  and adminAgree = '1' order by " . $sql_order . " limit 0, " . $offset );
//$goods_total = getdata( "SELECT count(no) as cnt FROM `goods` where status='판매중' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . "");
?>
<style type="text/css" title="">
	#cate2_list ul , #cate2_list li{ list-style:none; padding:0; margin:0; }
	#cate2_list li{ float:left; }
	.container{ width:100%; overflow:hidden; }
	#cate2_list{ width:1600px; }

	.swiper-area {position:static;}
	.category-swiper {margin-left:150px;}
	@media (min-width: 900px) {
		.swiper-area {position:static;margin-left:50px !important;}
	}
	
</style>
<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">검색카테고리</h2>

	<!--LIST-->
	<div class="search-list" style="margin-top:45px;">
		<h2 class="hide">카테고리</h2>
		<div class="title"><div class="left">총 <span class="point"><?=number_format( $tot["cnt"] )?>개</span> 상품</div></div>			
	</div>

	<!-- LIST -->
	<div class="list">
		<ul id="listOfBoard">
		<?
		while ( $row = mysql_fetch_array( $rst ) ) {
			$batge = "";
			$goods_info_detail = getdata("select * from goods_detail where no='" . $row["no"] . "' ");
			
			//if ( $goods_info_detail["fastDeli"] == "1" ) { $batge .= "빠른배송 | "; }
			if ( $row["best_code"] == "1" ) { $batge .= "인기상품 | "; }
			if ( $row["packed"] == "1" ) { $batge .= "묶음배송상품 | "; }
			if ( $row["ttang"] == "1" ) { $batge .= "땡처리상품 | "; }
			if ( $row["fromOversea"] == "1" ) { $batge .= "해외직배송 | "; }
			$batge = substr( $batge , 0 , -2 );
		?>
			<li><a href="product_view.php?no=<?=$row["no"]?>" data-idx="<?=$row["no"]?>" class="detail_link">
				<div class="img"><img src="<?=$row["thumb"]?>"></div>
				<div class="text">
					<h3 class="ellipsis2"><?=$row["title"]?></h3>
					<div class="pay"><?=number_format( $row["priceWe"] )?>원</div>
					<div class="count"><?=$batge ?></div>
					<!--div class="delivery">무료배송</div-->
				</div>
			</a></li>		
		<?}?>
		</ul>
	</div>
	<? include ("../include/quick_btn.php"); ?>
</section>
<?
include ("../include/footer.php");
?>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$(".blind").hide();
	$(".sub_cate_list").click(function(){
		var cid = $(this).data("cateid");
		$("#sub_cate_list_"+cid).slideToggle(300);
		$(".blind").not("#sub_cate_list_"+$(this).data("cateid")).slideUp(300);
		return false;
	});
	
	<?
	if ( $cate1 != "" ) {
	?>
	$("#sub_cate_list_<?= substr( $cate1 , 0 , 2 )?>").show();
	<?}?>
});

$(document).on( "click" , ".search-button" , function(){
	if ( $("#skey").val() == "" ) {
		alert("검색어를 입력해주세요");
		return false;
	}else {
		$("#search_frm").submit();
	}
});

$(document).on( "click" , ".submit_condition" , function(){
	$("#cate1").val("<?=$cate1?>");
	$("#cate2").val("<?=$cate2?>");
	$("#sort").val($("input[type=radio][name=sort_rdo]:checked").val());
	$("#search_frm").submit();
});

$(document).on( "click" , ".submit_condition2" , function(){
	var cate1 = $(this).data("cateid1");
	var cate2 = $(this).data("cateid2");
	$("#cate1").val(cate1);
	$("#cate2").val(cate2);
	$("#sort").val($("input[type=radio][name=sort_rdo]:checked").val());
	$("#search_frm").submit();
});

$(document).on( "click" , "#search" , function(){
	var chk_free = "";
	if ( $("#chk_free").is(":checked") ) {
		var chk_free = "1";
	}else {
		var chk_free = "0";
	}

	if ( $("#skey").val() == "" ) {
		alert("검색어를 입력해주세요");
		return false;
	}else {
		$("#search_frm").action("./category_list.php");
		$("#search_frm").submit();
	}
});

$(document).on("change" , "#cate_1" , function(){
	var val = $("#cate_1 option:selected").val();
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: '../proc/_product.php',
		data: 'mode=sel_cate&dep=2&skey=<?=$skey?>&val='+val,
		success:function (data) {
			//alert(data);
				console.log(data);
			if (data.rst != "1" ) {
				//alert("문제가 발생하여 정상적으로 반영되지 않았습니다.");
				//location.reload();
			}else {
				$("#cate2_list").html(data.content);
				$('#cate2_list').touchMoveMenu();
			}
		},
		error : function (data) {
			console.log(data);
		} 
	});
});

var swich_sort = false;
$(document).on( "click" , "#sort_layer" , function(){
	if ( swich_sort == false ) {
		$(".sort-area").show();
		$(this).removeClass("arrow-up");
		$(this).addClass("arrow-down");
		swich_sort = true;
	}else {
		$(".sort-area").hide();
		$(this).removeClass("arrow-down");
		$(this).addClass("arrow-up");
		swich_sort = false;
	}
	
});

var page = <?=$page?>;
var page_ex = true;
var paging_ing = false;

$(document).ready(function(){
	show_loading();
});

$(window).load(function(){
	<? if ( $paging != false ) {?>
	$(window).scroll(function(){
		if($(window).scrollTop() >= $(document).height() - $(window).height() -200 ){
			if ( paging_ing == false ) {
				paging();
				paging_ing = true;
			}
			return false;
		}
	});
	<?}?>

	$('#div_ajax_load_image').hide();  
	function paging(){
		if ( page_ex == true ) {
			var params = { mode:'paging10' , page:page , cate1:"<?=$cate1?>" , cate2:"<?=$cate2?>" , skey:"<?=$skey?>", sort:"<?=$sort?>" }
			$.ajax({
				url:"../product/_proc_json.php",
				data:params,
				type:"GET",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.content != "" ) {
						$("#listOfBoard").append( data.content);
						paging_ing = false;
						page++;
					}else {
						page_ex = false;
					}
				}, error:function(jqXHR,textStatus,errorThrown){
					console.log(errorThrown);
					alert("페이지 로딩 도중 에러가 발생하였습니다.");
				}, beforeSend: function () {
					show_loading();
				}, complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
	}

});

$(document).on( "change" , "#cate_1" , function(){
	location.href="./category_list.php?cate1="+$(this).val()+"&skey=<?=$skey?>";
});

//-->
</script>
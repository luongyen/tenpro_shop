<?
$menu_id = "12";
$page_id = "3";
include "../include/header.php"; 
$code = mysql_real_escape_string( $code );
$row = getdata("select * from request where idx='$code'");
if ($row["idx"] == "" ) {
	echo "<script>window.alert(' 신청정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
	exit;
}
?>
			<!-- leftmneu -->
			<? include "../include/left_request.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				<!-- title -->
				<div class="titbox">
					<h2 class="title">회원관리</h2>
				</div>
				<!-- //title -->				
				<div class="contbox">
					<h3>&gt; 기본정보</h3>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="page" value="<?=$page?>">
					<input type="hidden" name="code" value="<?=$code?>">
					<input type="hidden" name="mode" id="mode" value="edit">
					<div class="table_typeB m10">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width="326px"><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>신청자명</strong></th>
									<td><input type="text" name="wname" value="<?=$row["wname"]?>" class="ip1"  /></td>
									<th><strong>연락처</strong></th>
									<td><input type="text" name="phone" value="<?=$row["phone"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>자녀명</strong></th>
									<td><input type="text" name="pname" value="<?=$row["pname"]?>" class="ip1"  /></td>
									<th><strong>자녀나이</strong></th>
									<td><input type="text" name="age" value="<?=$row["age"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>지역, 구, 동</strong></th>
									<td colspan="3"><input type="text" name="addr" value="<?=$row["addr"]?>" class="ip1" style="width:80%;"  /></td>
								</tr>
								<tr>
									<th><strong>통화가능시간</strong></th>
									<td colspan="3"><input type="text" name="ptime" value="<?=$row["ptime"]?>" class="ip1" style="width:80%;"  /></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="m30">&gt; 부가정보</h3>
					<div class="table_typeB m10">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>관리자메모</strong></th>
									<td><textarea type="text" name="memo" class="ip1" style="width:80%;height:100px;"  /><?=$row["memo"]?></textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback" onclick=""><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120bk" onclick="javascript:del_submit();"><span>삭제</span></a>
							<a href="#" class="btn_120b" onclick="javascript:comp_submit();"><span>수정</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>				
			</div>
			
			<script type="text/javascript">
			<!--
				$(function () {
					$('a.goback').click(function () {
						history.back();
						return false;
					});
				});

				function closeDaumPostcode(){
					$("#sddr_layer").css( 'display' , 'none' );
				}
				function comp_submit() {
					if ($("input[name=wname]").val() == "" ) {
						alert("신청자명을 입력하세요.");
						return false;
					}
					else if ($("input[name=phone]").val() == "" ) {
						alert("연락처를 입력하세요.");
						return false;
					}
					else if ($("input[name=pname]").val() == "" ) {
						alert("자녀명을 입력하세요.");
						return false;
					}
					else if ($("input[name=age]").val() == "" ) {
						alert("자녀나이(개월)를 입력하세요.");
						return false;
					}
					else if ($("input[name=addr]").val() == "" ) {
						alert("지역, 구, 동을 입력하세요.");
						return false;
					}
					$("#regi_form").submit();
					return false;
				}
				function del_submit(){
					var con = confirm("삭제하시겠습니까?");
					if ( con ) {
						$("#mode").val("delete");
						$("#regi_form").submit();
					}
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
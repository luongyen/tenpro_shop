<?
$menu_id = "6";
$page_id="2";
include "../include/header.php"; 

$sdate = mysql_real_escape_string($sdate);
$edate = mysql_real_escape_string($edate);
$gcode = mysql_real_escape_string($gcode);
$media = mysql_real_escape_string($media);
$where = "";

if ( $sdate == "" ) { $sdate = date( "Y-m-d" , strtotime( "-7 day" ) ); }
if ( $edate == "" ) { $edate = date( "Y-m-d" ); }

$ssdate = str_replace( "-" , "" , $sdate );
$sedate = str_replace( "-" , "" , $edate );

if ( $gcode != "" ) { $where .= " and gcode = '$gcode' "; }
if ( $media != "" ) { $where .= " and shop_id = '$media' "; }
if ( $channel != "" ) { $where .= " and channel = '$channel' "; }


$stat_sum = getdata( "select sum(cnt) as cnt, sum(price_real) as price_real, sum(price_sales) as price_sales , sum(price_supply) as price_supply from status_goods where sdate >= '$ssdate' and sdate <= '$sedate' ".$where." " );
$sql =  "SELECT gcode, sum( vcnt ) AS vcnt, sum( cnt ) AS cnt, sum( price_real ) AS price_real, sum( price_sales ) AS price_sales, sum( price_supply ) AS price_supply, sum( price_delivery ) AS price_delivery, sum( price_point ) AS price_point, sum( price_media ) AS price_media
FROM status_goods 
WHERE sdate >= '$ssdate'
AND sdate <= '$sedate'
 ".$where."
GROUP BY gcode
ORDER BY sdate DESC ";
$no_where_sql = "SELECT gcode
FROM status_goods 
WHERE sdate >= '$ssdate'
AND sdate <= '$sedate'
GROUP BY gcode
ORDER BY sdate DESC ";
$rst = mysql_query( $sql );
$no_rst = mysql_query( $no_where_sql );
?>
			<!-- leftmneu -->
			<? include "../include/left_stat.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">상품별통계</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox" style="width:1300px;">
					
					<!-- search -->
					<div class="board_search">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<input type="hidden" name="mid" value="<?=$mid?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="95px"><col width="400px"><col width="95px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>적용매체</strong></th>
									<td>
										<select name="media">
											<option value="">전체</option>
											<?
											$rst_media = mysql_query( " select * from media where del_ok='0' " );
											while ( $row_media = mysql_fetch_array( $rst_media ) ) {
											?>
												<option value="<?=$row_media["mid"]?>" <?= ( $media == $row_media["mid"] ) ? " selected" : "" ?>><?=$row_media["mname"]?></option>
											<?}?>
										</select>
									</td>
									<th><strong>채널선택</strong></th>
									<td>
										<select name="channel" class="sel2" id="channel" style="width:150px;">
											<option value=""> -- 선택 -- </option>
											<option value="hotdeal_1" <?= ( $channel == "hotdeal_1" ) ? " selected" : "" ?>> 공동구매핫딜</option>
											<option value="hotdeal_2" <?= ( $channel == "hotdeal_2" ) ? " selected" : "" ?>> 육아홀릭쇼핑홀릭 </option>
											<option value="hotdeal_3" <?= ( $channel == "hotdeal_3" ) ? " selected" : "" ?>> 긴급땡처리 </option>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>주문일</strong></th>
									<td>
										<input type="text" name="sdate" class="ip1" style="width:100px;" id="sdate" value="<?=$sdate ?>" data-date="<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>"  /> ~ 
										<input type="text" name="edate" class="ip1" style="width:100px;" id="edate" value="<?=$edate ?>" data-date="<?=date("Y-m-d")?>" />
										<button class="btn btn-default btn-xs sel_today" type="button" >오늘</button> 
										<button class="btn btn-default btn-xs sel_week" type="button" >일주일</button> 
										<button class="btn btn-default btn-xs sel_month" type="button" >한달</button>
									</td>
									<th><strong>상품명</strong></th>
									<td>
										<select name="gcode" class="sel2" id="mem_type" style="width:200px;">
											<option value=""> -- 선택 -- </option>

											<? while ($no_row = mysql_fetch_array($no_rst)) {
												$goods_info = getdata("select * from goods where gcode='" . $no_row["gcode"] . "'");?>		
											<option value="<?=$no_row["gcode"]?>" <?= ( $no_row["gcode"] == $gcode ) ? " selected" : "" ?>> <?=$goods_info["pname"]?> </option>
											<?}?>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
						</form>						
						<div class="btn_search"><a href="#">검색</a></div>
					</div>
					<!-- //search -->
					
			<div style="padding-top:20px;">
				<div class="table_typeA">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col width=""><col width=""><col width=""><col width=""></colgroup>
						<thead>
							<tr>
								<th class="text-center active vertical_50">총 결재 금액</th>
								<th class="text-center active vertical_50">총 판매 이익</th>
								<th class="text-center active vertical_50">총 구매건수</th>
								<th class="text-center active vertical_50">선택날짜</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong><?=number_format( $stat_sum["price_real"] )?> 원</strong></td>
								<td><strong><?=number_format( $stat_sum["price_sales"] - $stat_sum["price_supply"] )?> 원</strong></td>
								<td><strong><?=number_format( $stat_sum["cnt"] )?> 건</strong></td>
								<td><strong><?= $sdate . " ~ " . $edate ?></strong></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="gubun" value="<?=$gubun?>">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="mode" value="">
					<div class="table_typeA table_bg m20">
						<table cellpadding="0" cellspacing="0" border="1" summary="">						
							<colgroup><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""></colgroup>
							<thead>											
								<tr>
									<th>상품명</th>
									<th>노출수</th>
									<th>결재건수</th>
									<th>공급가</th>
									<th>배송비</th>
									<th>적립금</th>
									<th>매체사수익</th>
									<th>실결재금액</th>
									<th>판매금액</th>
									<th>판매이익</th>
								</tr>
							</thead>
							<tbody>
								<? 
								while ($row = mysql_fetch_array($rst)) { 
									$goods_info = getdata("select * from goods where gcode='" . $row["gcode"] . "'");
									$sales_margin = $row["price_real"] - $row["price_delivery"] - $row["price_supply"];
									?>
								<tr style="background-color:#ffffff;">
									<td><?=$goods_info["pname"]?></td>
									<td><?=number_format( $row["vcnt"] ) ?></td>
									<td><?=number_format( $row["cnt"] ) ?></td>
									<td><?=number_format( $row["price_supply"] ) ?></td>
									<td><?=number_format( $row["price_delivery"] ) ?></td>
									<td><?=number_format( $row["price_point"] ) ?></td>
									<td><?=number_format( $row["price_media"] ) ?></td>
									<td><?=number_format( $row["price_real"] ) ?></td>
									<td><?=number_format( $row["price_sales"] ) ?></td>
									<td><?=number_format( $sales_margin ) ?></td>
								</tr>						
								<?
								$sum_1 += $row["vcnt"];
								$sum_2 += $row["cnt"];
								$sum_3 += $row["price_supply"];
								$sum_4 += $row["price_delivery"];
								$sum_5 += $row["price_point"];
								$sum_6 += $row["price_media"];
								$sum_7 += $row["price_real"];
								$sum_8 += $row["price_sales"];
								$sum_9 += $sales_margin;
								}?>		
							</tbody>
							<tfoot>							
								<tr style="background-color:#ffffff;">
									<th>합계</th>
									<th><?=number_format( $sum_1 ) ?></th>
									<th><?=number_format( $sum_2 ) ?></th>
									<th><?=number_format( $sum_3 ) ?></th>
									<th><?=number_format( $sum_4 ) ?></th>
									<th><?=number_format( $sum_5 ) ?></th>
									<th><?=number_format( $sum_6 ) ?></th>
									<th><?=number_format( $sum_7 ) ?></th>
									<th><?=number_format( $sum_8 ) ?></th>
									<th><?=number_format( $sum_9 ) ?></th>
								</tr>	
							</tfoot>
						</table>
					</div>
				</div>
				
			</div>
			
			<script src='/manager/js/raphael-min.js'></script>
			<script src='/manager/js/morris-0.4.3.min.js'></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#sdate').datepicker().on('changeDate', function(e) {
						$("#sdate").datepicker('hide');
					});
					$('#edate').datepicker().on('changeDate', function(e) {
						$("#edate").datepicker('hide');
					});

					var today_val = "<?=date("Y-m-d")?>";
					var week_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>";
					var mon_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")-1  , date("d"), date("Y")))?>";

					$(".sel_today").on("click",function(){
						$("#sdate").val(today_val);
						$("#edate").val(today_val);
					});
					$(".sel_week").click(function(){
						$("#sdate").val(week_val);
						$("#edate").val(today_val);
					});
					$(".sel_month").click(function(){
						$("#sdate").val(mon_val);
						$("#edate").val(today_val);
					});
						
				});

				$(function() {

					var f = document.getElementById('regi_form');
					$('.btn_search').click(function () {
						document.getElementById('search_frm').submit();
						return false;
					});
				});
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
$menu_id = "6";
$page_id="4";
include "../include/header.php"; 

$sdate = mysql_real_escape_string( $sdate );
$edate = mysql_real_escape_string( $edate );
$media = mysql_real_escape_string( $media );
$stat_pay_rst = mysql_real_escape_string($stat_pay_rst);
$where = "";

if ( $sdate == "" ) { $sdate = date( "Y-m-d" , strtotime( "-7 day" ) ); }
if ( $edate == "" ) { $edate = date( "Y-m-d" ); }

$ssdate = str_replace( "-" , "" , $sdate );
$sedate = str_replace( "-" , "" , $edate );

if ( $stat_pay_rst != "" ) { $where .= " and stat_pay_rst = '$stat_pay_rst' "; }
if ( $media != "" ) { $where .= " and shop_id='$media' "; }
$media_info = getdata("SELECT * FROM member WHERE id = '".$_SESSION['yi_id']."' ");

$sql = "SELECT  
	sdate, 
	SUM(IF( stat_pay = '1', cnt, 0)) AS pay1_cnt, 
	SUM(IF( stat_pay = '2', cnt, 0)) AS pay2_cnt, 
	SUM(IF( stat_pay = '3', cnt, 0)) AS pay3_cnt,
	SUM(IF( stat_pay = '1', price_real, 0)) AS pay1_price_real, 
	SUM(IF( stat_pay = '2', price_real, 0)) AS pay2_price_real, 
	SUM(IF( stat_pay = '3', price_real, 0)) AS pay3_price_real,  
	SUM(IF( stat_pay = '1', price_sales, 0)) AS pay1_price_sales, 
	SUM(IF( stat_pay = '2', price_sales, 0)) AS pay2_price_sales, 
	SUM(IF( stat_pay = '3', price_sales, 0)) AS pay3_price_sales,  
	SUM(IF( stat_pay = '1', price_supply, 0)) AS pay1_price_supply, 
	SUM(IF( stat_pay = '2', price_supply, 0)) AS pay2_price_supply, 
	SUM(IF( stat_pay = '3', price_supply, 0)) AS pay3_price_supply
FROM status_date 
WHERE  sdate >= '$ssdate'
AND sdate <= '$sedate'
 ".$where."
GROUP BY sdate
ORDER BY sdate DESC ";

$no_sql = "select mid, mname from media";
$rst = mysql_query( $sql );
$no_rst = mysql_query( $no_sql );
?>
			<!-- leftmneu -->
			<? include "../include/left_stat.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">정산내역</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox" style="width:1300px;">
					
					<!-- search -->
					<div class="board_search">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="95px"><col width="400px"><col width="95px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>주문일</strong></th>
									<td colspan="3">
										<input type="text" name="sdate" class="ip1" style="width:100px;" id="sdate" value="<?=$sdate ?>" data-date="<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>"  /> ~ 
										<input type="text" name="edate" class="ip1" style="width:100px;" id="edate" value="<?=$edate ?>" data-date="<?=date("Y-m-d")?>" />
										<button class="btn btn-default btn-xs sel_today" type="button" >오늘</button> 
										<button class="btn btn-default btn-xs sel_week" type="button" >일주일</button> 
										<button class="btn btn-default btn-xs sel_month" type="button" >한달</button>
									</td>
								</tr>
								<tr>
									<th><strong>매체사</strong></th>
									<td colspan="3">
										<select name="media" class="sel2" id="media" style="width:200px;">
											<option value=""> -- 선택 -- </option>

											<? while ($no_row = mysql_fetch_array($no_rst)) {?>
											<option value="<?=$no_row["mid"]?>" <?= ( $no_row["mid"] == $media ) ? " selected" : "" ?>> <?=$no_row["mname"]?> </option>
											<?}?>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
						</form>						
						<div class="btn_search"><a href="#">검색</a></div>
					</div>
					<!-- //search -->
					
					<div style="padding-top:20px;">
						<div class="table_typeA">
							<form action="_proc.php" method="post" role="form" class="form-horizontal" name="chg_form">
							<input type="hidden" name="mode" value="chg_com">
							<table cellpadding="0" cellspacing="0" border="1" summary="" style="width:60%;">
							<colgroup><col width=""><col width=""><col width=""><col width=""><col width="8%"><col width="15%"></colgroup>
								<tr>
									<th style="background-color:#f8f8f8;color:#333;">신용카드결재수수료</th>
									<td class="text-center vertical-middle"><input type="text" name="pay1_com" value="<?=$media_info["pay1_com"]?>" class="form-control" style="width:100px;display:inline-block;"> %</td>
									<th style="background-color:#f8f8f8;color:#333;">휴대폰결재 수수료</th>
									<td class="text-center vertical-middle"><input type="text" name="pay2_com" value="<?=$media_info["pay2_com"]?>" class="form-control" style="width:100px;display:inline-block;"> %</td>
									<td class="text-center vertical-middle" style="background-color:#fff;vertical-align:middle;"><button type="submit" class="btn btn-primary btn-sm"> 적 용 </button></td>
									<td class="text-center vertical-middle" style="background-color:#fff;vertical-align:middle;"><button type="" class="btn btn-success " id="down_excel"> 엑셀 다운로드 </button></td>
								</tr>
							</table>
							</form>
						</div>
					</div>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="gubun" value="<?=$gubun?>">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="mode" value="">
					<div class="table_typeA table_bg m20">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup>
								<col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style=""><col style="">
							</colgroup>
							<thead>
							<tr>
								<th>주문일</th>
								<th>총매출</th>
								<th>총매입</th>
								<th>카드<br />총매출</th>
								<th>핸드폰<br />총매출</th>
								<th>무통장<br />총매출</th>
								<th>카드<br />수수료<br /><?=$media_info["pay1_com"]?>%</th>
								<th>핸드폰<br />수수료<br /><?=$media_info["pay2_com"]?>%</th>
								<th>수수료<br />차감후<br />마진</th>
								<th>부가세</th>
								<th>SMS충전</th>
								<th>최종마진</th>
								<th>마진율<br />%</th>
							</tr>
							<tr>
								<th id="row_1">합계</th>
								<th id="row_2"><?=number_format( $sum_1 ) ?></th>
								<th id="row_3"><?=number_format( $sum_2 ) ?></th>
								<th id="row_4"><?=number_format( $sum_3 ) ?></th>
								<th id="row_5"><?=number_format( $sum_4 ) ?></th>
								<th id="row_6"><?=number_format( $sum_5 ) ?></th>
								<th id="row_7"><?=number_format( $sum_6 ) ?></th>
								<th id="row_8"><?=number_format( $sum_7 ) ?></th>
								<th id="row_9"><?=number_format( $sum_8 ) ?></th>
								<th id="row_10"><?=number_format( $sum_9 ) ?></th>
								<th id="row_11">&nbsp;</th>
								<th id="row_12"><?=number_format( $sum_10 ) ?></th>
								<th id="row_13"><?=number_format( $sum_10 / $sum_1 * 100 , 1 ) ?>%</th>
							</tr>	
							</thead>		
							<? while ($row = mysql_fetch_array($rst)) { 
								$total_sales = $row["pay1_price_real"] + $row["pay2_price_real"] + $row["pay3_price_real"];
								$total_supply = $row["pay1_price_supply"] + $row["pay2_price_supply"] + $row["pay3_price_supply"];
								$total_margin = $total_sales - $total_supply - ( $row["pay1_price_real"] * $media_info["pay1_com"] / 100 ) - ( $row["pay2_price_real"] * $media_info["pay2_com"] / 100 ) ;
								$final_margin = $total_margin / 1.1;
								$vat = $total_margin - $final_margin;
								$magin_rate = $final_margin / $total_sales * 100;
								$pay1_comm = $row["pay1_price_real"] * $media_info["pay1_com"] / 100;
								$pay2_comm = $row["pay1_price_real"] * $media_info["pay2_com"] / 100;
							?>						
							<tr style="background-color:#ffffff;">
								<td class="text-center vertical-middle"><?=substr( $row["sdate"] , 0 , 4 ) . "-" . substr( $row["sdate"] , 4 , 2 ) . "-" . substr( $row["sdate"] , 6 , 2) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $total_sales ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $total_supply ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $row["pay1_price_real"] ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $row["pay2_price_real"] ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $row["pay3_price_real"] ) ?></td>

								<td class="text-center vertical-middle"><?=number_format( $pay1_comm ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $pay2_comm ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $total_margin ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $vat )?></td>
								<td class="text-center vertical-middle">&nbsp;</td>
								<td class="text-center vertical-middle"><?=number_format( $final_margin ) ?></td>
								<td class="text-center vertical-middle"><?=number_format( $magin_rate , 1 ) ?>%</td>
							</tr>						
							<?
								$sum_1 += $total_sales;
								$sum_2 += $total_supply;
								$sum_3 += $row["pay1_price_real"];
								$sum_4 += $row["pay2_price_real"];
								$sum_5 += $row["pay3_price_real"];
								$sum_6 += $pay1_comm;
								$sum_7 += $pay2_comm;
								$sum_8 += $total_margin;
								$sum_9 += $vat;
								$sum_10 += $final_margin;
							}?>
							<tfoot>
							<tr style="background-color:#ffffff;">
								<th id="sum_1">합계</th>
								<th id="sum_2"><?=number_format( $sum_1 ) ?></th>
								<th id="sum_3"><?=number_format( $sum_2 ) ?></th>
								<th id="sum_4"><?=number_format( $sum_3 ) ?></th>
								<th id="sum_5"><?=number_format( $sum_4 ) ?></th>
								<th id="sum_6"><?=number_format( $sum_5 ) ?></th>
								<th id="sum_7"><?=number_format( $sum_6 ) ?></th>
								<th id="sum_8"><?=number_format( $sum_7 ) ?></th>
								<th id="sum_9"><?=number_format( $sum_8 ) ?></th>
								<th id="sum_10"><?=number_format( $sum_9 ) ?></th>
								<th id="sum_11">&nbsp;</th>
								<th id="sum_12"><?=number_format( $sum_10 ) ?></th>
								<th id="sum_13"><?=number_format( $sum_10 / $sum_1 * 100 , 1 ) ?>%</th>
							</tr>	
							</tfoot>		

						</table>
					</div>
				</div>
				
			</div>
			
			<script src='/manager/js/raphael-min.js'></script>
			<script src='/manager/js/morris-0.4.3.min.js'></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#sdate').datepicker().on('changeDate', function(e) {
						$("#sdate").datepicker('hide');
					});
					$('#edate').datepicker().on('changeDate', function(e) {
						$("#edate").datepicker('hide');
					});

					var today_val = "<?=date("Y-m-d")?>";
					var week_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>";
					var mon_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")-1  , date("d"), date("Y")))?>";

					$(".sel_today").on("click",function(){
						$("#sdate").val(today_val);
						$("#edate").val(today_val);
					});
					$(".sel_week").click(function(){
						$("#sdate").val(week_val);
						$("#edate").val(today_val);
					});
					$(".sel_month").click(function(){
						$("#sdate").val(mon_val);
						$("#edate").val(today_val);
					});
						
				});

				$(function() {
					var f = document.getElementById('regi_form');
					$('.btn_search').click(function () {
						document.getElementById('search_frm').submit();
						return false;
					});
				});

				$("#down_excel").on( "click" , function(){
					location.href='_excel.php?stat_pay_rst=<?=$stat_pay_rst?>&sdate=<?=$sdate?>&edate=<?=$edate?>&media=<?=$media?>';
					//alert("excel down!!");
					return false;
				});
				for ( i = 2 ; i < 14 ; i++ ) {
					$("#row_"+i).html($("#sum_"+i).html());
				}
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
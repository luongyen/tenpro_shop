<?
$menu_id = "3";
include "../include/header.php"; 
$mid = mysql_real_escape_string( $mid );
if ( $mid == "" ) {
	$tmp_lm = getdata( "select mid from media where del_ok='0' order by idx desc limit 0 , 1 " );
	if ( $tmp_lm["mid"] != "" ) {
		$mid = $tmp_lm["mid"];	
	}else {
		BACK__("잘못된 접근입니다.");
	}
}
$gubun = mysql_real_escape_string( $gubun );
$code = mysql_real_escape_string( $code );
	$row = getdata("select * from users where idx='$code'");	
if ($row["idx"] == "" ) {
	echo "<script>window.alert(' 회원정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
	exit;
}
$email = explode( "@" , $row["email"] );

$rm_1 = getdata("select count(*) as cnt from recommend where uid='".$row["idx"]."' and depth='1' ");
$rm_2 = getdata("select count(*) as cnt from recommend where uid='".$row["idx"]."' and depth='2' ");
$rm_cnt_1 = ($rm_1["cnt"] > 0 ) ? "<strong style='color:#1ea6b8;'>" . $rm_1["cnt"] . "</strong>" : $rm_1["cnt"] ;
$rm_cnt_2 = ($rm_2["cnt"] > 0 ) ? "<strong style='color:#1ea6b8;'>" . $rm_2["cnt"] . "</strong>" : $rm_2["cnt"] ;

$rm = getdata("select * from recommend where rid='".$row["idx"]."' and depth='1'");
if ($rm["uid"]!= "") {
	$rm_info = getdata("select uname , id from users where idx='" . $rm["uid"] . "' ");
}
?>
			<!-- leftmneu -->
			<? include "../include/left_account.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<div id="sddr_layer" style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer" style="cursor:pointer;position:absolute;right:-3px;top:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer1" style="cursor:pointer;position:absolute;right:-3px;bottom:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
				</div>
				<!-- title -->
				<div class="titbox">
					<h2 class="title">회원관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					<h3>&gt; 기본정보</h3>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="page" value="<?=$page?>">
					<input type="hidden" name="code" value="<?=$code?>">
					<input type="hidden" name="gubun" value="<?=$gubun?>">
					<input type="hidden" name="mode" value="edit">
					<div class="table_typeB m10">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width="326px"><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>아이디</strong></th>
									<td><?=$row["id"]?></td>
									<th><strong>이메일</strong></th>
									<td><?=$row["email"]?></td>
								</tr>
								<tr>
									<th><strong>닉네임</strong></th>
									<td><?=$row["uname"]?></td>
									<th><strong>이름</strong></th>
									<td><?=$row["rname"]?></td>
								</tr>
								<tr>
									<th><strong>로그인타입</strong></th>
									<td><?=$row["login_type"]?></td>
									<th><strong>가입매체</strong></th>
									<td><?=$row["shop_id"]?></td>
								</tr>
								<tr>
									<th><strong>가입일</strong></th>
									<td><?=$row["reg_date"]?></td>
									<th><strong>최종로그인</strong></th>
									<td><?=$row["last_login"]?></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="m30">&gt; 부가정보</h3>
					<div class="table_typeB m10">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
							<!--
								<tr>
									<th><strong>상위추천인</strong></th>
									<td><?=$rm_info["id"]?><?=( $rm_info["uname"] != "" ) ? " / " . $rm_info["uname"] : "&nbsp;" ?></td>
								</tr>
								<tr>
									<th><strong>하위추천인</strong></th>
									<td><?=$rm_cnt_1?> <a title="새창" class="btn_70" style="margin-left:15px;" onclick="javascript:pop_recommend(<?=$row["idx"]?>);return false;" href="#"><span>상세보기</span></a></td>
								</tr>
							-->
								<tr>
									<th><strong>보유포인트</strong></th>
									<td><?=number_format( $row["point"] )?> P <a title="새창" class="btn_70" style="margin-left:15px;" onclick="javascript:pop_point_admin(<?=$row["idx"]?>);return false;" href="#"><span>포인트수정</span></a></td>
								</tr>
								<tr>
									<th><strong>상태</strong></th>
									<td>
										<select name="status" class="sel1">
											<option value="1" <?= ( $row["status"] == "1" ) ? "selected" : "" ?>>사용중</option>
											<option value="0" <?= ( $row["status"] == "0" ) ? "selected" : "" ?>>미사용</option>
										</select> <?= ( $row["del_force"] != "0" ) ? "[강제탈퇴]" : "" ?> <?= ( $row["del_ok"] != "0" ) ? $row["del_reason"] : "" ?>
									</td>
								</tr>
								<tr>
									<th><strong>메모</strong></th>
									<td><textarea type="text" name="comment" class="ip1" style="width:80%;height:100px;"  /><?=$row["comment"]?></textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback" onclick=""><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120bk" onclick="javascript:del_submit();"><span>삭제</span></a>
							<a href="#" class="btn_120b" onclick="javascript:comp_submit();"><span>수정</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			
			<script type="text/javascript">
			<!--
				$(function () {
					$('a.goback').click(function () {
						history.back();
						return false;
					});
				});

				function emailsel(emailv){
					if(emailv=="d"){
						document.regi_form.email2.value = "";
						document.regi_form.email2.readOnly = false;
						document.regi_form.email2.focus();
					}else{
						document.regi_form.email2.value = emailv;
					}
				}

				var element_layer = document.getElementById('sddr_layer');
				function search_addr(val){
					new daum.Postcode({
						oncomplete: function(data) {
							var fullAddr = data.address; 
							var extraAddr = '';
							if(data.addressType === 'R'){
								if(data.bname !== ''){
									extraAddr += data.bname;
								}
								if(data.buildingName !== ''){
									extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
								}
								fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
							}
							document.getElementById('zip').value = data.zonecode; //5자리 새우편번호(zonecode) 사용 , 기존 postcode
							document.getElementById('addr1').value = fullAddr;
							document.getElementById('addr2').focus();
							element_layer.style.display = 'none';
						},
						width : '100%',
						height : '100%'
					}).embed(element_layer);
				   element_layer.style.display = 'block';
				
					initLayerPosition();
				}

				function initLayerPosition(){
					var width = 330; //우편번호서비스가 들어갈 element의 width
					var height = 460; //우편번호서비스가 들어갈 element의 height
					var borderWidth = 1; //샘플에서 사용하는 border의 두께

					// 위에서 선언한 값들을 실제 element에 넣는다.
					element_layer.style.width = width + 'px';
					element_layer.style.height = height + 'px';
					element_layer.style.border = borderWidth + 'px solid';
					// 실행되는 순간의 화면 너비와 높이 값을 가져와서 중앙에 뜰 수 있도록 위치를 계산한다.
					element_layer.style.left = (((window.innerWidth || document.documentElement.clientWidth) - width)/2 - borderWidth) + 'px';
					element_layer.style.top = (((window.innerHeight || document.documentElement.clientHeight) - height)/2 - borderWidth) + 'px';
				}
				
				function closeDaumPostcode(){
					$("#sddr_layer").css( 'display' , 'none' );
				}
				function comp_submit() {
					if ($("input[name=uid]").val() == "" ) {
						alert("아이디를 입력하세요.");
						return false;
					}
					else if ($("input[name=upass]").val() == "" ) {
						alert("비밀번호를 입력하세요.");
						return false;
					}
					else if ($("input[name=uname]").val() == "" ) {
						alert("이름을 입력하세요.");
						return false;
					}
					else if ($("input[name=phone1]").val() == "" || $("input[name=phone2]").val() == "" || $("input[name=phone3]").val() == "" ) {
						alert("전화번호를 입력하세요.");
						return false;
					}
					else if ($("input[name=email1]").val() == "" || $("input[name=email2]").val() == "" ) {
						alert("이메일을 입력하세요.");
						return false;
					}
					
					$("#regi_form").submit();
					return false;
				}
				
				function pop_recommend(idx) {
					window.open("./pop_recommend.php?idx=" + idx , "pop_island" , "width=1200, height=800, top=0, left=0" );
				}
				function pop_point_admin(idx) {
					window.open("./pop_point_admin.php?idx=" + idx , "pop_admin" , "width=500, height=500, top=0, left=0" );
				}
			//-->
			</script>
			<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
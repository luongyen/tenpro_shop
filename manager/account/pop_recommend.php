<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}
$mem = getdata("select * from users where idx='" . $idx . "' ");

if ( $mem["idx"] == "" ) {
	echo "<script>alert('잘못된 회원 정보입니다.다시시도해 주세요');self.close();</script>";
}
$idx = mysql_real_escape_string( $_GET["idx"]);
$depth = mysql_real_escape_string( $_GET["depth"] );
$keyword = mysql_real_escape_string( $_GET["keyword"] );


$get_query = "idx=$idx&depth=$depth&keyword=$keyword";
if ( $keyword	!= "" ) { $where .= " and ( id like '%" . $keyword . "%' or uname like '%" . $keyword . "%' or phone like '%" . $keyword . "%' or tel like '%" . $keyword . "%' )"; }
if ( $depth		!= "" ) { $where .= " and depth = '" . $depth . "' "; } 

$gcount=getdata( "select count(*) as cnt from recommend as A left join users as B on A.rid=B.idx where uid='" . $mem["idx"] . "' ".$where );
$cnt = $gcount["cnt"];

//페이징준비처리
	$pageIdx=1;	
	if ($page>0) {
		$pageIdx=$page;
	}
	$page_set = 15; //한페이지 줄수-기본값
	$block_size = 10;
	if($pageIdx % $block_size==0) {
		$start_num=$pageIdx-$block_size+1;
	}else {
		$start_num=floor($pageIdx/$block_size)*$block_size +1;
	}
	$end_num = $start_num+$block_size-1;
	$total_page = ceil($cnt / $page_set); // 총 페이지 수

	if($pageIdx==1) {
		$limit_idx=0;
	}else {
		$limit_idx=$pageIdx*$page_set-$page_set;
	}
//페이징준비 끝

$i = 0;
$rst = mysql_query("select * from recommend as A left join users as B on A.rid=B.idx where uid='" . $mem["idx"] . "' ".$where." order by A.idx desc limit ".$limit_idx.", ".$page_set);

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>추천인 보기</title>

	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">

	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">

	<div class="popup_box">

		<div class="titbox">
			<p class="t"><?=$mem["uname"]?>님의 추천인 정보</p>
		</div>
		<div class="popbody">

					<!-- search -->
					<div class="board_search">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<input type="hidden" name="idx" value="<?=$idx?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="95px"><col width="120px"><col width="95px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>Depth</strong></th>
									<td>
										<select name="depth"  class="sel2">
											<option value="">전체</option>
											<option value="1" <?= ( $depth == "1" ) ? " selected" : "" ?>>1차 추천인</option>
											<option value="2" <?= ( $depth == "2" ) ? " selected" : "" ?>>2차 추천인</option>
										</select>
									</td>
									<th><strong>통합검색</strong></th>
									<td><input type="text" name="keyword" value="<?=$keyword?>" class="ip3" /></td>
								</tr>
							</tbody>
						</table>
						</form>
						<div class="btn_search" style="top:10px;"><a href="#" style="height:30px;line-height:30px;" onclick="javascript:document.search_frm.submit();">검색</a></div>
					</div>
					<!-- //search -->
					
			<div class="scrollbox" style="padding-top:20px;">
				<div class="table_typeA">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col width=""><col width=""><col width=""></colgroup>
						<thead>
							<tr>
								<th class="text-center active vertical_50">Depth</th>
								<th class="text-center active vertical_50">아이디</th>
								<th class="text-center active vertical_50">이름</th>
							</tr>
						</thead>
						<tbody>
							<? while ($row = mysql_fetch_array($rst)) { ?>
							<tr>
								<td><?=$row["depth"]?>차 추천인</td>
								<td><?=$row["id"]?></td>
								<td><?=$row["uname"]?></td>
							</tr>
							<?$i++;}
							if ($i == 0 ) {
								echo "<tr><td colspan='3'>등록된 추천인이 없습니다.</td></tr>";
							}?>
						</tbody>
					</table>
				</div>
				</form>
				
					<!-- paging -->
					<div class="paging">
					<? 
						if($cnt>0) {
							$prev_page=$pageIdx-1;
							$next_page=$pageIdx+1;							
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$prev_page."&".$get_query."\" class=\"prev\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a>" : "<a href=\"#\" class=\"prev\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a> ";
							if ($total_page<10) {
								$vpage=1;
							}else{
								$vpage = ( ( (int)( ($pageIdx - 1 ) / $page_set ) ) * $page_set ) + 1;
							}
							$spage = $vpage + $page_set - 1;
							if ($spage >= $total_page) $spage = $total_page;

							for($i=$vpage;$i<=$spage;$i++){ 
								if ($pageIdx==$i) {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\" class=\"current\"><span><strong>" . $i . "</strong></span></a> ";
								}else {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\"><span>" . $i . "</span></a> ";
								}
							}
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$next_page."&".$get_query."\" class=\"next\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a>" : "<a href=\"#\" class=\"next\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a> ";
						}?>
					</div>
					<!-- //paging -->
			</div>
			<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
			<script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
			<script>
				$(function () {
					var f = $(document.forms["regi_form"]);

					$('a#updateBtn')
					.css('cursor', 'pointer')
					.click(function () {
						if ($('#cost', f).val() == "" ) {
							alert("추가배송비를 입력하세요");
							$('#cost', f).focus();
						}else {
							f.submit();
						}
					});
				});
			</script>
		</div>

	</div>

</body>
</html>
<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}
$idx = mysql_real_escape_string( $_GET["idx"]);
$mem = getdata("select * from users where idx='" . $idx . "' ");
if ( $mem["idx"] == "" ) {
	echo "<script>alert('잘못된 회원 정보입니다. 다시 시도해 주세요');self.close();</script>";
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>POINT 등록</title>

	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">

	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">

	<div class="popup_box">

		<div class="titbox">
			<p class="t"><?=( $mem["uname"] != "" ) ? $mem["uname"] : $mem["id"]?>님의 POINT 등록</p>
		</div>
		<div class="popbody">
			<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
			<input type="hidden" name="idx" value="<?=$idx?>">
			<input type="hidden" name="mode" value="point">
			<div style="padding-top:20px;">
				<div class="table_typeB">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col width="30%"><col width=""></colgroup>						
							<tbody>
								<tr>
									<th><strong>구분</strong></th>
									<td>
										<select name="gubun" class="sel1" id="gubun">
											<option value="">선택</option>
											<option value="2">지급</option>
											<option value="1">차감</option>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>내용</strong></th>
									<td>	<input type="text" name="title" id="title" value="" style="width:280px;" /></td>
								</tr>
								<tr>
									<th><strong>포인트</strong></th>
									<td>	<input type="text" name="point" id="point" value="" style="width:100px;"  /> * 숫자만 입력하세요</td>
								</tr>
							</tbody>
					</table>
				</div>
			</div>
			<div class="btn_box m20">
				<div style="text-align:center;" >
					<a href="#" class="btn_120b" onclick="javascript:comp_submit();"><span>등록</span></a>
				</div>
			</div>
			</form>
			<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
			<script>
				function comp_submit(){
					if ( $("#gubun option:selected").val() == "" ) {
						alert("구분을 선택 해주세요");
					}else if ( $("#title").val() == "" ) {
						alert("내용을 입력 해주세요");
					}else if ( $("#point").val() == "" ) {
						alert("포인트를 입력 해주세요");
					}else {
						$("#regi_form").submit();
					}
				}
			</script>
		</div>

	</div>

</body>
</html>
<?
$menu_id = "2";
$page_id = "2";
include "../include/header.php";
if ( $_GET["v_num"] != "" ) {
	$title_str = "수정";
	$v_num = mysql_real_escape_string( $_GET["v_num"] );
	$row = getdata(" select * from provide where v_num ='$v_num'");
	if ( $row["idx"] == "" ) {
		BACK__( "등록되지 않은 공급사입니다." );
	}
}else {
	$title_str = "등록";	
	$temp = getdata("select v_num from provide order by v_num desc limit 0 , 1");
	$max_num = (int)(str_replace( "P" , "" , $temp["v_num"])) + 1;
	if ( $max_num > 100 ) {
		$v_num= "P0".$max_num;
	}else {
		if ( $max_num > 10 ) {
			$v_num= "P00".$max_num;
		}else {
			$v_num= "P000".$max_num;			
		}
	}
}
?>
			<!-- leftmneu -->
			<? include "../include/left_media.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				<!-- title -->
				<div class="titbox">
					<h2 class="title">공급사<?=$title_str?></h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="page" value="<?=$page?>">
					<input type="hidden" name="mode" value="<?= ( $title_str == "등록" ) ? "regist_p":"edit_p"?>">
					<input type="hidden" name="v_num" value="<?=$v_num?>">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>공급사 코드</strong></th>
									<td><?=$v_num?></td>
								</tr>
								<tr>
									<th><strong> * 공급사명</strong></th>
									<td><input type="text" name="p_name" value="<?=$row["p_name"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>E-mail</strong></th>
									<td><input type="text" name="p_email" value="<?=$row["p_email"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>연락처</strong></th>
									<td><input type="text" name="p_phone" value="<?=$row["p_phone"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>담당자</strong></th>
									<td><input type="text" name="p_manager" value="<?=$row["p_manager"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>관리자메모</strong></th>
									<td><textarea name="memo" id="memo" style="height:150px;width:90%;"><?=$row["memo"]?></textarea></td>
								</tr>								
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback" onclick="history.back();"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="comp_submit();"><span>저장</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					if ($("input[name=p_name]").val() == "" ) {
						alert("공급사명을 입력하세요.");
						return false;
					}
					$("#regi_form").submit();
					return false;
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
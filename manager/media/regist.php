<?
$menu_id = "2";
$page_id = "1";
include "../include/header.php";
if ( $_GET["mid"] != "" ) {
	$title_str = "수정";
	$mid = mysql_real_escape_string( $_GET["mid"] );
	$row = getdata(" select * from media where mid ='$mid'");
	if ( $row["idx"] == "" ) {
		BACK__( "등록되지 않은 업체입니다." );
	}
}else {
	$title_str = "등록";	
}
?>
			<!-- leftmneu -->
			<? include "../include/left_media.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<div id="sddr_layer" style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer" style="cursor:pointer;position:absolute;right:-3px;top:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer1" style="cursor:pointer;position:absolute;right:-3px;bottom:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
				</div>
				<!-- title -->
				<div class="titbox">
					<h2 class="title">매체<?=$title_str?></h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="page" value="<?=$page?>">
					<input type="hidden" name="mode" value="<?= ( $title_str == "등록" ) ? "regist":"edit"?>">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width="326px"><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>아이디</strong></th>
									<td><?= ( $title_str == "등록" ) ? "<input type=\"text\" name=\"mid\" value=\"\" class=\"ip1\"  />" : $row["mid"] . "<input type=\"hidden\" name=\"mid\" value=\"" . $row["mid"] . "\" class=\"ip1\"  />" ?></td>
									<th><strong>패스워드</strong></th>
									<td><input type="password" name="mpass" value="<?=$row["mpass"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>상점명</strong></th>
									<td colspan="3"><input type="text" name="mname" value="<?=$row["mname"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>대표번호</strong></th>
									<td><input type="text" name="phone_main" value="<?=$row["phone_main"]?>" class="ip1"  /></td>
									<th><strong>고객센터 전화번호<br />(SMS발신)</strong></th>
									<td><input type="text" name="phone_sms" value="<?=$row["phone_sms"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>상호명</strong></th>
									<td><input type="text" name="mallname" value="<?=$row["mallname"]?>" class="ip1"  /></td>
									<th><strong>대표자</strong></th>
									<td><input type="text" name="ceo" value="<?=$row["ceo"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>사업자등록번호</strong></th>
									<td><input type="text" name="num1" value="<?=$row["num1"]?>" class="ip1"  /></td>
									<th><strong>통신판매업신고번호</strong></th>
									<td><input type="text" name="num2" value="<?=$row["num2"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>개인정보 책임자</strong></th>
									<td><input type="text" name="manager" value="<?=$row["manager"]?>" class="ip1"  /></td>
									<th><strong>개인정보 책임자 E-mail</strong></th>
									<td><input type="text" name="manager_email" value="<?=$row["manager_email"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>정산 계좌</strong></th>
									<td colspan="3">
										<input type="text" name="bank_name" value="<?=$row["bank_name"]?>" class="ip1" placeholder="은행명" style="width:150px;" />
										<input type="text" name="bank_num" value="<?=$row["bank_num"]?>" class="ip1" placeholder="계좌번호"  style="width:250px;"  />
										<input type="text" name="bank_account" value="<?=$row["bank_account"]?>" class="ip1" placeholder="예금주"  style="width:150px;"  />
									</td>
								</tr>
								<tr>
									<th><strong>수익정산율(%)</strong></th>
									<td colspan="3">
										<input type="text" name="calculate_rate" value="<?=$row["calculate_rate"]?>" class="ip1" style="width:100px;" /> %
									</td>
								</tr>
								<tr>
									<th><strong>운영자 휴대폰번호<br />(SMS수신)</strong></th>
									<td colspan="3"><input type="text" name="manager_phone" value="<?=$row["manager_phone"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>사업장주소</strong></th>
									<td colspan="3"><input type="text" name="addr" value="<?=$row["addr"]?>" class="ip6"  /></td>
								</tr>
								<tr>
									<th><strong>관리자메모</strong></th>
									<td colspan="3"><input type="text" name="memo" value="<?=$row["memo"]?>" class="ip6"  /></td>
								</tr>								
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback" onclick=""><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="comp_submit();"><span>저장</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					if ($("input[name=mid]").val() == "" ) {
						alert("아이디를 입력하세요.");
						return false;
					}
					else if ($("input[name=mpass]").val() == "" ) {
						alert("비밀번호를 입력하세요.");
						return false;
					}
					else if ($("input[name=mname]").val() == "" ) {
						alert("이름을 입력하세요.");
						return false;
					}
					
					$("#regi_form").submit();
					return false;
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
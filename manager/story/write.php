<?
$menu_id = "16";
$page_id = "1";
include "../include/header.php"; 

//search_setting
$sql_append = "";

$p_content = mysql_real_escape_string( $_GET["p_content"] );
$shop_id = mysql_real_escape_string( $_GET["shop_id"] );
$page = mysql_real_escape_string( $_GET["page"] );
if ($sort == "" ) {
	$sort = " idx desc ";
}
$get_query = "p_content=$p_content&shop_id=$shop_id";


if ( $idx != "" ) {
	$mode = "edit";
	$row = getdata("select * from board_contents where idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 게시글 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}
	$img_arr = array();
	$rst_img = mysql_query("select * from board_contents_img where contents_idx = '$idx' order by sort asc");
	while ( $row_img = mysql_fetch_array( $rst_img ) ) {
		$img_arr["idx"][] = $row_img["idx"];
		$img_arr["file_name"][] = $row_img["file_name"];
	}
}else {
	$mode = "regist";
}
?>		
			<!-- leftmneu -->
			<? include "../include/left_story.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">컨텐츠등록</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc" enctype="multipart/form-data">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="idx" value="<?=$row["idx"]?>">
					<input type="hidden" name="mode" value="<?=$mode?>">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="120px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong><span>*</span>제목</strong></th>
									<td>
										<input type="text" name="title" class="ip6" value="<?=$row["title"]?>" title="" /> 
									</td>
								</tr>
								<tr>
									<th><strong><span>*</span>등록/예정일시</strong></th>
									<td>
										<input type="text" name="reg_date" id="reg_date" class="ip4" value="<?=substr( $row["reg_date"] , 0 , 10 )?>" title="" />
										<select name="reg_time" style="width:60px;">
										<? 
											for ( $i = 0 ; $i < 24 ; $i++ ) { 
												$j = substr( $i + 100 , -2 );
										?>
											<option value="<?=$j?>" <?= ( substr( $row["reg_date"] , -2 ) == $j ) ? "selected" : "" ?>><?=$j?></option>
										<?}?>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong><span>*</span>대표이미지</strong></th>
									<td>
											<?= ( $row["thumb"] == "" ) ? '<!-- 70*76 -->' : '<img src="' . $row["thumb"] . '" alt="" width="200" onerror="this.src=\'/img/product/img_noimg.jpg\';" /><br /><br />'?>
											<div class="filebox"><input type="file" name="thumb" class="ip_file" value="" style="border:0;" /></div>
											<div id=''>* 이미지 비율을 3:2로 편집하여 올려주세요.</div>
										</div>
									</td>
								</tr>
								<tr>
									<th><strong>내용이미지</strong></th>
									<td>
									<?
									if (count( $img_arr["idx"] ) > 0 ) { echo "<table style='border:0;'><tr>"; }
									for ( $i = 0 ; $i < count( $img_arr["idx"] ) ; $i++ ) {
										echo "<td width='150' style='text-align:center;'><img src='" . $img_arr["file_name"][$i] . "' width='120' style='margin:5px 0;'/><br /><a href='#' class='del_img' data-idx='" . $img_arr["idx"][$i] . "'>삭제</a></td>";
										if ( $i % 5 == 4  ) { echo "</tr><tr >"; }
									}
									if ( $i % 5 != 0  ) {
										for ( $j = 0 ; $j < 5-($i % 5) ; $j++ ) {
											echo "<td></td>";
										}
									}
									if (count( $img_arr["idx"] ) > 0 ) { echo "</tr></table>"; }
									?><br /> <div class="filebox"><input type="file" name="img_file[]" class="ip_file" style="border:0;" multiple></div>
									</td>
								</tr>
								<tr>
									<th><strong>관련상품</strong></th>
									<td><a href="#" onclick="javascript:pop_rst('<?=$row["idx"]?>');return false;"  class="btn_70" style="margin:10px 0 0 20px;"><span>상품등록</span></a><br />
									<?
									$arr_goods_list = array();
									if ( $row["goods_list"] != "" ) {
										echo "<table style='border:0;'><tr>";
										$arr_goods_list = explode( "|" , $row["goods_list"] );
										for ( $i = 0 ; $i < count( $arr_goods_list ) ; $i++ ) {
											$goods_info = getdata("select * from goods where no='".$arr_goods_list[$i]."' ");
											echo "<td width='150' style='text-align:center;'><a href='../product/product_detail.php?no=" . $goods_info["no"] . "' target='_blank'><img src='" . $goods_info["thumb"] . "' width='120' style='margin:5px 0;border:1px solid #ddd;'/><br />" . $goods_info["title"] . "</a><br /><b>" . number_format( $goods_info["price"] ) . "원</b><br /><button class='btn btn-default del_goods' type='button' data-goods_no='".$arr_goods_list[$i]."'>삭제</button>" ;
											if ( $i % 5 == 4  ) { echo "</tr><tr >"; }
										}
										if ( $i % 5 != 0  ) {
											for ( $j = 0 ; $j < 5-($i % 5) ; $j++ ) {
												echo "<td></td>";
											}
										}
										echo "</tr></table>";
									}
									?>
									</td>
								</tr>
								<tr>
									<th><strong>내용</strong></th>
									<td class="td_editor">
										<textarea name="content" class="summernote" id="content" ><?=$row["content"]?></textarea>
									</td>
								</tr>

							<? if ( $mode == "edit" ) { ?>
								<tr>
									<th style="height:400px;"><strong>댓글</strong></th>
									<td><iframe name="ifr_reply" src="./board_reply.php?idx=<?=$row["idx"]?>" width="100%" height="400" scrolling="auto" frameborder=0></iframe></td>
								</tr>
								<?}?>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
						<? if ( $mode == "edit" ) { ?>
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>
							<a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a>
						<?}else {?>
							<a href="#" class="btn_120b" id="updateBtn"><span>저장</span></a>							
						<?}?>
						</div>
					</div>
					<!-- //button -->

				</div>
				
			</div>
					
			<script>			
				$(document).on( "click" , ".del_goods" , function(){
					var idx = "<?=$row["idx"]?>";
					var goods_list = "<?=$row["goods_list"]?>";
					var goods_no = $(this).data("goods_no");					
					$.ajax({
						type: 'POST',
						cache: false,
						dataType: 'json',
						url: './_proc_json.php',
						data: 'mode=del_goods&idx='+idx+'&goods_no='+goods_no+'&goods_list='+goods_list,
						success:function (data) {
							console.log(data);
							if ( data == "1" ) {
								location.reload();
							}else {
								alert("관련 상품 삭제에 실패했습니다.");
							}
						},
						error : function (data) {
							console.log(data);
						} 
					});
				});
				$('#reg_date').datepicker({
					onSelect: function(dateText, datePicker) { 
						var sDate = new Date(dateText); 
						var dd = sDate.getDate(); 
						var mm = sDate.getMonth()+1; 
						if(mm<10) mm = "0" + mm; 
						if(dd <10) dd = "0" + dd; 
						$("#reg_date").val(sDate.getFullYear() +"-"+mm+"-"+dd); 
					}
				}).on('changeDate', function(e) {
					$("#reg_date").datepicker('hide');
				});

				$(".del_img").click(function(){
					document.regi_form.mode.value = "del_img";
					document.regi_form.gid.value = $(this).data("idx");
					document.regi_form.submit();
					return false;
				});

				$(function () {
					$('a.goback').click(function () {
						history.back();
						return false;
					});					
					$('#delBtn').click(function (){
						var conOk = confirm("정말 삭제 하시겠습니까?");
						if (conOk) {
							document.regi_form.mode.value = "board_del";
							document.regi_form.submit();
						}
					});
					
					$('#updateBtn').click(function (){
						if ($('input[name=title]').val() == '') {
							alert('제목을 입력하세요.');
							return false;
						}
						if ($('#reg_date').val() == '') {
							alert('등록일자를 설정 하세요.');
							return false;
						}
						<? if ( $mode != "edit" ) { ?>
						if ($('input[name=thumb]').val() == '') {
							alert('대표사진을 등록하세요.');
							return false;
						}
						<?}?>
						if ($('#content').val().length < 10 ) {
							alert('대표사진을 등록하세요.');
							return false;
						}
						document.regi_form.submit();
						return false;
					});
				});

				function CheckGroup(fr){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName(fr);     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
					} 
				}
				function pop_rst(idx) {
					window.open("./regi_goods.php?idx=" + idx , "pop_rst" , "width=1200, height=970, top=0, left=0, scrollbars=1" );
				}
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}

$idx = mysql_real_escape_string( $_GET["idx"]);
$cate1 = mysql_real_escape_string( $_GET["cate1"] );
$cate2 = mysql_real_escape_string( $_GET["cate2"] );
$cate3 = mysql_real_escape_string( $_GET["cate3"] );
$cate4 = mysql_real_escape_string( $_GET["cate4"] );
$keyword = mysql_real_escape_string( $_GET["keyword"] );

$get_query = "idx=$idx&cate1=$cate1&cate2=$cate2&cate3=$cate3&cate4=$cate4&keyword=$keyword";

if ( $keyword	!= "" ) { $sql_append .= " and ( title like '%" . $keyword . "%' or keywords like '%" . $keyword . "%' ) "; }
if ( $cate1	!= "" ) { $sql_append .= " and cateogry1='" . $cate1 . "' "; }
if ( $cate2	!= "" ) { $sql_append .= " and cateogry2='" . $cate2 . "' "; }
if ( $cate3	!= "" ) { $sql_append .= " and cateogry3='" . $cate3 . "' "; }
if ( $cate4	!= "" ) { $sql_append .= " and cateogry4='" . $cate4 . "' "; }

$board_contents = getdata( "select * from board_contents where idx = '" . $idx . "' " );
 
if ( $sql_append == "" ) { 
	$sql = "select B.* from goods_top100 as A left join goods as B on A.goods_no=B.no where A.sdate = '" . date("Ymd") . "' and A.cate_code='' and A.gubun = '' order by A.rank asc limit 0, 100 ";
	$rst = mysql_query($sql);
	$goods_total["cnt"] = mysql_num_rows($rst);
}else {
	$goods_total = getdata( "SELECT count(no) as cnt FROM `goods` where status='판매중' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . "");
	$sql = "SELECT * FROM `goods` where status = '판매중' and dateStart <= '" . date("Y-m-d H:i:s") . "' and dateEnd >= '" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' " . $sql_append . " order by  lastUpdate desc  limit 0 , 100";
	$rst = mysql_query( $sql );	
}

$cnt = $goods_total["cnt"];
//페이징준비처리
	$pageIdx=1;	
	if ($page>0) {
		$pageIdx=$page;
	}
	$page_set = 100; //한페이지 줄수-기본값
	$block_size = 10;
	if($pageIdx % $block_size==0) {
		$start_num=$pageIdx-$block_size+1;
	}else {
		$start_num=floor($pageIdx/$block_size)*$block_size +1;
	}
	$end_num = $start_num+$block_size-1;
	$total_page = ceil($cnt / $page_set); // 총 페이지 수

	if($pageIdx==1) {
		$limit_idx=0;
	}else {
		$limit_idx=$pageIdx*$page_set-$page_set;
	}
//페이징준비 끝

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title><?=$admin_title?></title>

	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">

	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">

	<div class="popup_box">

		<div class="titbox">
			<p class="t"><strong class="fc1">스토리쇼핑 관련상품 등록(<?=$cnt .$board_contents["title"]?>)</strong></p>
		</div>
		<div class="popbody">
		
			<div class="table_typeB">
				<table cellpadding="0" cellspacing="0" border="1" summary="">
					<colgroup><col width="250px"><col width=""></colgroup>
					<tbody>
						<tr>
							<th><strong>현재 등록된 관련상품</strong></th>
							<td style='padding:0;padding-left:10px;'>
							<?
							$arr_goods_list = array();
							if ( $board_contents["goods_list"] != "" ) {
								echo "<table style='border:0;'padding:0;><tr>";
								$arr_goods_list = explode( "|" , $board_contents["goods_list"] );
								for ( $i = 0 ; $i < count( $arr_goods_list ) ; $i++ ) {
									$goods_info = getdata("select * from goods where no='".$arr_goods_list[$i]."' ");
									echo "<td style='width:150px;text-align:center;padding0;padding-left:10px; overflow: hidden;text-overflow: ellipsis;white-space: nowrap;'><a href='../product/product_detail.php?no=" . $goods_info["no"] . "' target='_blank'><img src='" . $goods_info["thumb"] . "' width='90' style='margin:5px 0;border:1px solid #ddd;'/><br />" . $goods_info["title"] . "<br /><b>" . number_format( $goods_info["price"] ) . "원</b></a>" ;
									if ( $i % 5 == 4  ) { echo "</tr><tr >"; }
								}
								if ( $i % 5 != 0  ) {
									for ( $j = 0 ; $j < 5-($i % 5) ; $j++ ) {
										echo "<td></td>";
									}
								}
								echo "</tr></table>";
							}
							?>
							</td>
						</tr>						
					</tbody>
				</table>
			</div>
			<!-- search -->
			<div class="board_search" style="margin-top:20px;margin-bottom:20px;">
				<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
				<input type="hidden" name="idx" value="<?=$idx?>">
				<table cellpadding="0" cellspacing="0" border="1" summary="">
					<colgroup><col width="95px"><col width=""></colgroup>
					<tbody>
						<tr>
							<th><strong>검색어</strong></th>
							<td><input type="text" name="keyword" value="<?=$keyword?>" class="ip3" /></td>
						</tr>
						<tr>
							<th><strong>카테고리</strong></th>
							<td>
								<select name="cate1" id="cate1">
									<option value="">선택하세요</option>
								<?
								$rst_cate1 = mysql_query( "select * from  category where depth='1' and cname not like '%삭제%' order by code_id asc ");
								while ( $row_cate1 = mysql_fetch_array( $rst_cate1 ) ) {
								?>
									<option value="<?=$row_cate1["code_id"]?>" <?=( $row_cate1["code_id"] == $cate1 ) ? "selected" : "" ?>><?=$row_cate1["cname"]?></option>
								<?}?>
								</select>
								<div id='cate2_area' style="display:inline-block;">
									<? if ( $cate2 != "" ) { ?>								
										<select name="cate2" id="cate2">
										<?
										$rst_cate2 = mysql_query( "select * from  category where depth='2' and code_id like '" . substr( $cate2 , 0 , 2 ) . "%' and cname not like '%삭제%' order by code_id asc ");
										while ( $row_cate2 = mysql_fetch_array( $rst_cate2 ) ) {
										?>
											<option value="<?=$row_cate2["code_id"]?>" <?=( $row_cate2["code_id"] == $cate2 ) ? "selected" : "" ?>><?=$row_cate2["cname"]?></option>
										<?}?>
									</select>
									<?}?>
								</div>
								<div id='cate3_area' style="display:inline-block;">
									<? if ( $cate3 != "" ) { ?>								
										<select name="cate3" id="cate3">
										<?
										$rst_cate3 = mysql_query( "select * from  category where depth='3' and code_id like '" . substr( $cate3 , 0 , 5 ) . "%' and cname not like '%삭제%' order by code_id asc ");
										while ( $row_cate3 = mysql_fetch_array( $rst_cate3 ) ) {
										?>
											<option value="<?=$row_cate3["code_id"]?>" <?=( $row_cate3["code_id"] == $cate3 ) ? "selected" : "" ?>><?=$row_cate3["cname"]?></option>
										<?}?>
									</select>
									<?}?>
								</div>
								<div id='cate4_area' style="display:inline-block;">
									<? if ( $cate4 != "" ) { ?>								
										<select name="cate4" id="cate4">
										<?
										$rst_cate4 = mysql_query( "select * from  category where depth='4' and code_id like '" . substr( $cate4 , 0 , 8 ) . "%' and cname not like '%삭제%' order by code_id asc ");
										while ( $row_cate4 = mysql_fetch_array( $rst_cate4 ) ) {
										?>
											<option value="<?=$row_cate4["code_id"]?>" <?=( $row_cate4["code_id"] == $cate4 ) ? "selected" : "" ?>><?=$row_cate4["cname"]?></option>
										<?}?>
									</select>
									<?}?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				</form>
				<div class="btn_search"><a href="#"  onclick="javascript:document.search_frm.submit();">검색</a></div>
			</div>
			<!-- //search -->
			<div class="scrollbox" style="height:450px;">
				<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">				
					<input type="hidden" name="mode" value="">
					<div class="table_typeA table_bg">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="80px"><col width="90px"><col width=""><col width="80px"><col width="80px"><col width="120px"><col width="120px"><col width="80px"></colgroup>
							<thead>
								<tr>
									<th>상품코드</th>
									<th>이미지</th>
									<th>상품명</th>
									<th>가격</th>
									<th>재고수량</th>
									<th>기간</th>
									<th>최종업데이트</th>
									<th>선택</th>
								</tr>
							</thead>
							<tbody>
							<?
							
							$now = date("Y-m-d H:i:s");
							while ($row = mysql_fetch_array($rst)) { 
								$cate_this = getdata(" select * from category where code_id = '" . $row["category"] . "'");
								$cate_str = $cate_this["cname"];
								if ( $cate_this["depth"] > 1) {
									$cate_1 = getdata(" select * from category where code_id='" . substr( $row["category"] , 0 , 2 ). "_00_00_00_00' and depth='1'");
									$cate_str = $cate_1["cname"] . " &gt; " . $cate_str;								
								}
								if ( $cate_this["depth"] > 2) {
									$cate_2 = getdata(" select * from category where code_id='" . substr( $row["category"] , 0 , 5 ). "_00_00_00' and depth='2'");
									$cate_str = $cate_2["cname"] . " &gt; " . $cate_str;								
								}
								if ( $cate_this["depth"] > 3) {
									$cate_3 = getdata(" select * from category where code_id='" . substr( $row["category"] , 0 , 8 ). "_00_00' and depth='3'");
									$cate_str = $cate_3["cname"] . " &gt; " . $cate_str;								
								}
								if ( $cate_this["depth"] > 4) {
									$cate_4 = getdata(" select * from category where code_id='" . substr( $row["category"] , 0 , 11 ). "_00' and depth='4'");
									$cate_str = $cate_4["cname"] . " &gt; " . $cate_str;								
								}
							?>
								<tr>
									<td style="padding:3px;"><?=$row["no"]?></td>
									<td style="padding:3px;"><img src="<?=$row["thumb"]?>" width="70" height="76" alt="" /></td>
									<td style="padding:3px;" class="td_left"><?=$cate_str?><br /><a href="product_detail.php?no=<?=$row["no"]?>"><strong class="fc1"><?=$row["title"]?></strong></a></td>
									<td style="padding:3px;"><?=number_format( $row["price"] )?></td>
									<td style="padding:3px;"><?=number_format( $row["qtyInventory"] )?></td>
									<td style="padding:3px;"><?=str_replace( " 00:00:00" , "" , $row["dateStart"] )?> ~<br /><?=str_replace( " 23:59:00" , "" , $row["dateEnd"] )?></td>
									<td style="padding:3px;"><?=substr( $row["lastUpdate"] , 0 , 10 )?><br /><?=substr( $row["lastUpdate"] , -8 )?></td>
									<td><button class="btn btn-default sel_goods" type="button" data-goods_no="<?=$row["no"]?>">상품선택</button></td>
								</tr>
							<?}?>
							</tbody>
						</table>
					</div>
				</form>
			</div>

					<!-- paging -->
					<div class="paging">
					<? 
						if($cnt>0) {
							$prev_page=$pageIdx-1;
							$next_page=$pageIdx+1;							
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$prev_page."&".$get_query."\" class=\"prev\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a>" : "<a href=\"#\" class=\"prev\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a> ";
							if ($total_page<10) {
								$vpage=1;
							}else{
								$vpage = ( ( (int)( ($pageIdx - 1 ) / $block_size ) ) * $block_size ) + 1;
							}
							$spage = $vpage + $block_size - 1;
							if ($spage >= $total_page) $spage = $total_page;

							for($i=$vpage;$i<=$spage;$i++){ 
								if ($pageIdx==$i) {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\" class=\"current\"><span><strong>" . $i . "</strong></span></a> ";
								}else {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\"><span>" . $i . "</span></a> ";
								}
							}
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$next_page."&".$get_query."\" class=\"next\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a>" : "<a href=\"#\" class=\"next\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a> ";
						}?>
					</div>
					<!-- //paging -->

			<!-- button -->
			<div class="btn_box">
				<div class="btn_right">
					<a href="#" onclick="javascript:self.close();opener.location.reload();" class="btn_120b"><span>닫기</span></a>
				</div>
			</div>
			<!-- //button -->
			<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
		</div>

	</div>
<script type="text/javascript">
<!--
$(document).on( "click" , ".sel_goods" , function(){
	var idx = "<?=$board_contents["idx"]?>";
	var goods_list = "<?=$board_contents["goods_list"]?>";
	var goods_no = $(this).data("goods_no");
	
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: './_proc_json.php',
		data: 'mode=add_goods&idx='+idx+'&goods_no='+goods_no+'&goods_list='+goods_list,
		success:function (data) {
			console.log(data);
			if ( data == "1" ) {
				location.reload();
			}else {
				alert("관련 상품 등록에 실패했습니다.");
			}
		},
		error : function (data) {
			console.log(data);
		} 
	});
});

$(document).on( "change" , "#cate1" , function(){
	var cate = $("#cate1 option:selected").val();
	
	if ( cate == "" ) {
		$("#cate2_area").html("");
		$("#cate3_area").html("");
		$("#cate4_area").html("");
	}else {
		$.ajax({
			type: 'POST',
			cache: false,
			dataType: 'json',
			url: './_proc_json.php',
			data: 'mode=category_pop&depth=1&cate='+cate,
			success:function (data) {
				console.log(data);
				$("#cate2_area").html(data.content);
				$("#cate3_area").html("");
				$("#cate4_area").html("");
			},
			error : function (data) {
				console.log(data);
			} 
		});
	}
});

$(document).on( "change" , "#cate2" , function(){
	var cate = $("#cate2 option:selected").val();
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: './_proc_json.php',
		data: 'mode=category_pop&depth=2&cate='+cate,
		success:function (data) {
			console.log(data);
			$("#cate3_area").html(data.content);
			$("#cate4_area").html("");
		},
		error : function (data) {
			console.log(data);
		} 
	});
});

$(document).on( "change" , "#cate3" , function(){
	var cate = $("#cate3 option:selected").val();
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: './_proc_json.php',
		data: 'mode=category_pop&depth=3&cate='+cate,
		success:function (data) {
			console.log(data);
			$("#cate4_area").html(data.content);
		},
		error : function (data) {
			console.log(data);
		} 
	});
});
//-->
</script>
</body>
</html>
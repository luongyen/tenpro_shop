<?
$menu_id = "1";
$page_id = "5";
include "../include/header.php"; 

//search_setting
$sql_append = "";

$idx = mysql_real_escape_string( $_GET["idx"] );
if ( $idx != "" ) {
	$mode = "banner_edit";
	$row = getdata("select * from banner where idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}
}else {
	$mode = "banner_regist";
}
?>

			<!-- leftmneu -->
			<? include "../include/left_basis.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">배너관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<!-- tabmenu -->
					<?// include "../include/tab_basis.php"; ?>
					<!-- //tabmenu -->
					
					<form action="./_proc.php" method="post" name="regi_form" id="regi_form"  enctype="multipart/form-data" target="ifr_proc">
					<input type="hidden" name="idx" value="<?=$row["idx"]?>">
					<input type="hidden" name="gid" value="<?=$row["view_range"]?>">
					<input type="hidden" name="mode" value="<?=$mode?>">
					<div class="table_typeB m25">
						<table cellpadding="0" cellspacing="0" border="1" summary="" id="product_ko">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>						
								<tr>
									<th><strong>적용매체</strong></th>
									<td>
										<ul class="ch_list1">
											<li><label><input type="checkbox" name="chkall" value="1"  onclick="javascript:selall(this,document.regi_form.chkDel);" <?=$chkAll?> />전체</label></li>
											<?
											$rst_media = mysql_query( " select * from media where del_ok='0' " );
											while ( $row_media = mysql_fetch_array( $rst_media ) ) {
												if ( strpos( $row["view_range"] , $row_media["mid"] ) !== false ) {
													$chkDel = " checked";
												}else {
													$chkDel = "";													
												}
											?>
												<li><label><input type="checkbox" name="chkDel" value="<?=$row_media["mid"]?>" <?=$chkDel?>  /><?=$row_media["mname"]?></label></li>
											<?}?>
										</ul>
									</td>
								</tr>
								<tr>
									<th><strong> * 배너 이미지</strong></th>
									<td>
										<div class="imgbox" style="width:170px !important;" >
											<?= ( $mode == "banner" || $row["file_name"] == "" ) ? '<!-- 70*76 -->' : '<img src="/data/banner/' . $row["file_name"] . '" width="200"  alt=""  onerror="this.src=\'/img/product/img_noimg.jpg\';" /><br /><br />'?>
											<div class="filebox"><input type="file" name="file_name" class="ip_file" value="" style="width:200px;"  /></div>
										</div> * 720px * 300px 로 등록해주셔야 합니다.
									</td>
								</tr>
								<tr>
									<th><strong> * 배너명</strong></th>
									<td><input type="text" name="title" value="<?=$row["title"]?>" class="ip6" maxlength="60"  /></td>
								</tr>
								<tr>
									<th><strong> * 링크</strong></th>
									<td><input type="text" name="url" value="<?=$row["url"]?>" class="ip6" maxlength="60"  />
									<p><strong class="fc1"> * 공백 시 이미지만 노출합니다.</strong></p></td>
								</tr>
								<tr>
									<th><strong> * 사용여부</strong></th>
									<td>
										<select name="status" class="sel1">
											<option value="1">Y</option>
											<option value="0">N</option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
						<? if ( $mode == "banner_edit" ) { ?>
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>
							<a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a>
						<?}else {?>
							<a href="#" class="btn_120b" id="updateBtn"><span>저장</span></a>							
						<?}?>
						</div>
					</div>
					<!-- //button -->

				</div>
				
			</div>
					
			<script type="text/javascript" src="../js/editor.js" charset="utf-8"></script>
			<script src="../js/jquery.filestyle.mini.js"></script>
			<script>
				$(function () {
						
					$('a.goback').click(function () {
						history.back();
						return false;
					});					
					$('#delBtn').click(function (){
						var conOk = confirm("정말 삭제 하시겠습니까?");
						if (conOk) {
							document.regi_form.mode.value = "banner_del";
							document.regi_form.submit();
						}
					});
					$('#updateBtn').click(function (){
						
						CheckGroup("chkDel");
						if ($('input[name=gid]').val() == '') {
							alert('적용매체를 선택하세요.');
							return false;
						}
						<? if ( $mode == "banner_regist" ) {?>
						if ($('input[name=file_name]').val() == '') {
							alert('배너이미지를 입력하세요.');
							return false;
						}
						<?}?>
						if ($('input[name=title]').val() == '') {
							alert('배너명을 입력하세요.');
							return false;
						}
						document.regi_form.submit();
					});
				});
				
				function selall(chekboxall,checkboxsub){
					var ischeck="";
					if (chekboxall.checked) {
						ischeck="checked";
					}
					if (checkboxsub.name !=undefined){
						checkboxsub.checked=ischeck;
					}else {
						for (i=0; i<checkboxsub.length; i++) {
							checkboxsub[i].checked =ischeck;
						}
					}
				}
				function CheckGroup(fr){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName(fr);     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
					} 
				}
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
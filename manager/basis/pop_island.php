<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}

$zipcode = mysql_real_escape_string( $_GET["zipcode"]);
$island = getdata("select * from island_setting where id='" . $_SESSION["yi_id"] . "' and post='" . $zipcode . "' ");
$island_zip = getdata("select * from island_zip where zipcode='" . $zipcode . "' ");

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>지역별 배송비 수정</title>
	
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">
	
	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">
	
	<div class="popup_box">
		
		<div class="titbox">
			<p class="t">지역별 배송비 수정</p>
		</div>
		<div class="popbody">
			
			<div class="scrollbox">
				<form action="_proc.php" method="post" role="form" class="form-horizontal" name="regi_form">
				<input type="hidden" id="mode" name="mode" value="island_update">
				<input type="hidden" id="idx" name="zipcode" value="<?=$zipcode?>">
				<div class="table_typeA">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col style="width:70%;"><col style="width:30%;"></colgroup>
						<thead>
							<tr>
								<th class="text-center active vertical_50">배송지역</th>
								<th class="text-center active vertical_50">배송비</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>(<?=$island_zip["zipcode"]?>) <?=$island_zip["sido"]?> <?=$island_zip["gugun"]?> <?=$island_zip["umd"]?> <?=$island_zip["lee"]?></td>
								<td>
									<span class="input_inbox">
										<input type="text" name="cost" id="cost" class="form-control" value="<?=( $island["price"] != "" ) ? $island["price"] : "0" ?>" style="width:80px;display:inline-block;">
									</span>
									<a href="#" class="btn_70 edit_pop" data-idx="<?=$row["idx"]?>" id="updateBtn"><span>등록</span></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				</form>
			</div>
			<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
			<script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
			<script>
				$(function () {
					var f = $(document.forms["regi_form"]);

					$('a#updateBtn')
					.css('cursor', 'pointer')
					.click(function () {
						if ($('#cost', f).val() == "" ) {
							alert("추가배송비를 입력하세요");
							$('#cost', f).focus();
						}else {
							f.submit();
						}
					});
				});
			</script>
		</div>
		 
	</div>

</body>
</html>
<?
$menu_id = "1";
$page_id = "4";
include "../include/header.php";
$row = getdata("select * from member where num=". $_SESSION["yi_idx"]);
$bank = mysql_query("select * from bank order by num desc");
$rst = mysql_query("select * from media where del_ok='0' order by idx desc");
?>			
			<!-- leftmneu -->
			<? include "../include/left_basis.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">입금계좌설정</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
				<form action="_proc.php" method="post" role="form" class="form-horizontal" name="regi_form" target="ifr_proc">
				<input type="hidden" id="mode" name="mode" value="payment">
				<input type="hidden" id="id" name="id" value="10proshop">
				<input type="hidden" id="del_id" name="del_id" value="">

					<!-- search -->
					<div class="board_search" style="padding:10px;margin-bottom:15px;">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="170px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>무통장 입금 계좌 등록</strong></th>
									<td>
										<select name="bankname" class="form-control"style="width:150px;display:inline-block;" class="sel4">
											<option value="">은행선택</option>
											<option value="국민은행" >국민은행</option>
											<option value="신한은행" >신한은행</option>
											<option value="기업은행" >기업은행</option>
											<option value="우리은행" >우리은행</option>
											<option value="농협" >농협</option>												
											<option value="스탠다드차타드은행" >스탠다드차타드은행</option>
											<option value="하나은행" >하나은행</option>												
											<option value="신협은행" >신협은행</option>												
											<option value="외환은행" >외환은행</option>												
											<option value="한국씨티은행" >한국씨티은행</option>												
											<option value="우체국" >우체국</option>												
											<option value="수협중앙회" >수협중앙회</option>												
											<option value="새마을금고" >새마을금고</option>												
											<option value="경남은행" >경남은행</option>												
											<option value="광주은행" >광주은행</option>												
											<option value="대구은행" >대구은행</option>												
											<option value="전북은행" >전북은행</option>												
											<option value="제주은행" >제주은행</option>
											<option value="부산은행" >부산은행</option>
											<option value="KDB산업은행" >KDB산업은행</option>
											<option value="동양종금" >동양종금</option>											
										</select>
										<input type="text" name="account_number" maxlength="30" class="ip2" style="width:200px;display:inline-block;" id="account_number" value="" placeholder="계좌번호">
										<input type="text" name="owner" maxlength="20" class="ip2" style="width:150px;display:inline-block;" id="owner" value="" placeholder="예금주">
										<a href="#" class="btn_70g product_sort" id="createBtn"><span>등록</span></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //search -->
					
					<div class="table_typeB table_bg ">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col style="width:25%;"><col style="width:25%;"><col style="width:25%;"><col style="width:25%;"></colgroup>
							<tbody>
								<tr>
									<th class="text-center">은행</th>
									<th class="text-center">계좌번호</th>
									<th class="text-center">예금주</th>
									<th class="text-center">관리</th>
								</tr>
								
								<? while ($bank_unit = mysql_fetch_array($bank)) { 
								?>
								
								<tr style="background-color:#ffffff;">
									<td class="small text-center"><?=$bank_unit["bankname"]?></td>
									<td class="small text-center"><?=$bank_unit["account_number"]?></td>
									<td class="small text-center"><?=$bank_unit["owner"]?></td>
									<td class="small text-center"><a href="#" class="btn_70 deleteBtn" idx="<?=$bank_unit["num"]?>"><span>삭제</span></a>
								</tr>						
								<? }?>

							</tbody>
						</table>
					</div>
					
					</form>
				</div>
				
			</div>
<script type="text/javascript">
	$(function () {
		var f = $(document.forms["regi_form"]);
		$('a#createBtn')
			.css('cursor', 'pointer')
			.click(function () {
				$('#mode', f).val('mu_account_add');
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: '_proc_json.php',
					data: f.serialize(),
					success : function (data) {
						if (data.state) {
							$(location).get(0).reload();
						} else {
							alert(data.message);
						}
					},
					error : function (data) {
							console.log(data);
					}
				});
			});

		$('a.deleteBtn')
			.css('cursor', 'pointer')
			.click(function () {
				if (!confirm("삭제하시겠습니까?")) return;
				var t = $(this);
				$('#del_id', f).val(t.attr('idx'));
				$('#mode', f).val('mu_account_del');
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: '_proc_json.php',
					data: f.serialize(),
					success : function (data) {
						if (data.state) {
							$(location).get(0).reload();
						} else {
							alert(data.message);
						}
					},
					error : function (data) {
							console.log(data);
					}
				});
			});
	});
	function comm_submit() {

		document.regi_form.mode.value="payment";
		document.regi_form.submit();
		
	}
</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
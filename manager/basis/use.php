<?
$menu_id = "1";
$page_id = "3";
include "../include/header.php";
$row = getdata("select * from member where id='". $_SESSION["yi_id"]."'");
?>			
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/manager/js/se/HuskyEZCreator.js" charset="utf-8"></script>

			<!-- leftmneu -->
			<? include "../include/left_basis.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">상점운영정책</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
				<div class="contbox">
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="mode" value="info">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>현금영수증</strong></th>
									<td>
										<input type="radio" name="cash_receipt_yn" value="1" <?= ( $row["cash_receipt_yn"] == "1" ) ? "checked" : "" ?>> 사용
										<input type="radio" name="cash_receipt_yn" value="0" <?= ( $row["cash_receipt_yn"] != "1" ) ? "checked" : "" ?>> 미사용
									</td>
								</tr>
								<tr>
									<th><strong>필독사항</strong></th>
									<td>
										<input type="radio" name="must_read_yn" value="1" <?= ( $row["must_read_yn"] == "1" ) ? "checked" : "" ?>> 사용
										<input type="radio" name="must_read_yn" value="0" <?= ( $row["must_read_yn"] != "1" ) ? "checked" : "" ?>> 미사용
									</td>
								</tr>
								<tr>
									<th><strong> 필독사항 동의</strong></th>
									<td>
										<input type="radio" name="must_read_agree" value="1" <?= ( $row["must_read_agree"] == "1" ) ? "checked" : "" ?>> 사용
										<input type="radio" name="must_read_agree" value="0" <?= ( $row["must_read_agree"] != "1" ) ? "checked" : "" ?>> 미사용
									</td>
								</tr>
								<tr>
									<th><strong> 비회원 개인정보수집 동의</strong></th>
									<td>
										<input type="radio" name="non_info_agree" value="1" <?= ( $row["non_info_agree"] == "1" ) ? "checked" : "" ?>> 사용
										<input type="radio" name="non_info_agree" value="0" <?= ( $row["non_info_agree"] != "1" ) ? "checked" : "" ?>> 미사용
									</td>
								</tr>
								<tr>
									<th><strong>필독사항</strong></th>
									<td><textarea name="must_read" style="width:100%;height:100px;" class="se_editor" id="must_read"><?=$row["must_read"]?></textarea></td>
								</tr>
								<tr>
									<th><strong>비회원 개인정보수집</strong></th>
									<td><textarea name="non_info" style="width:100%;height:100px;" class="se_editor" id="non_info"><?=$row["non_info"]?></textarea></td>
								</tr>

								<tr>
									<th><strong>쇼핑몰 이용약관</strong></th>
									<td><textarea name="yak" style="width:100%;height:100px;" class="se_editor" id="yak"><?=$row["yak"]?></textarea></td>
								</tr>
								<tr>
									<th><strong>개인정보취급방침</strong></th>
									<td><textarea name="personal_data" style="width:100%;height:100px;" class="se_editor" id="personal_data"><?=$row["personal_data"]?></textarea></td>
								</tr>
								<tr>
									<th><strong>고객센터 안내 및 이용안내</strong></th>
									<td><textarea name="guide_info" style="width:100%;height:100px;" class="se_editor" id="guide_info"><?=$row["guide_info"]?></textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_right">
							<a href="#" class="btn_120b" id="updateBtn"><span>저장</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				</div>
				
			</div>
			
<script type="text/javascript" src="/manager/js/editor.js" charset="utf-8"></script>
<script type="text/javascript">
<!--
	$(function() {
		$('#updateBtn').click(function (){
			updateContentsEditor('must_read');
			updateContentsEditor('yak');
			updateContentsEditor('non_info');
			updateContentsEditor('personal_data');
			updateContentsEditor('guide_info');
			document.regi_form.submit();
			return false;
		});
	});
//-->
</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
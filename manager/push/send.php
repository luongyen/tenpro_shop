<?
$menu_id = "10";
$page_id = "3";
include "../include/header.php";
?>
			<!-- leftmneu -->
			<? include "../include/left_push.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents" style="min-height:700px;">
				<!-- title -->
				<div class="titbox">
					<h2 class="title">PUSH 메세지발송</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					<h3 style="margin-bottom:15px;"><strong style="color:#1ea6b8;">PUSH 메세지 설정</strong> (* PUSH 메세지는 200 Bytes 까지만 입력하실 수 있습니다.)</h3>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" enctype="multipart/form-data" target="ifr_proc">
					<input type="hidden" name="mode" value="pushsend">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>알림 제목</strong></th>
									<td><input type="text" name="subject" id="subject" value="10%샵" style="width:300px;"></td>
								</tr>
								<tr>
									<th><strong>짧은 내용</strong></th>
									<td><input type="text" name="short" id="short" value="" style="width:300px;"> <strong>( * 25자 내로 입력하세요.)</strong></td>
								</tr>
								<tr>
									<th><strong>링크 URL</strong></th>
									<td><input type="text" name="url" id="url" value="" style="width:500px;"></td>
								</tr>
								<tr>
									<th><strong>이미지 URL</strong></th>
									<td><input type="text" name="img_file2" id="img_file2" value="" style="width:500px;"></td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="javascript:comp_submit();return false;"><span>전송</span></a>
						</div>
					</div>
					<!-- //button -->
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					if ( $("#subject").val() == "" ) {
						alert("PUSH 제목을 입력하세요");
					}else if ( $("#short").val() == "" ) {
						alert("짧은 내용을 입력하세요");
					}else if ( $("#url").val() == "" ) {
						alert("링크URL을 입력하세요");
					}else {
						$("#regi_form").submit();
					}
					return false;
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
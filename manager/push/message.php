<?
$menu_id = "10";
$page_id = "2";
include "../include/header.php";
$row = getdata("select * from push_message where shop_id='admin'");
?>
			<!-- leftmneu -->
			<? include "../include/left_push.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<div id="sddr_layer" style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer" style="cursor:pointer;position:absolute;right:-3px;top:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer1" style="cursor:pointer;position:absolute;right:-3px;bottom:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
				</div>
				<!-- title -->
				<div class="titbox">
					<h2 class="title">메세지관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					<h3 style="margin-bottom:15px;"><strong style="color:#1ea6b8;">PUSH 메세지 설정</strong></h3>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="mode" value="message">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><input type="checkbox" name="m_yn_1" value="1" <?= ( $row["m_yn_1"] == "1" ) ? "checked" : "" ?> style="margin-left:20px;"> 포인트적립</th>
									<td><textarea name="m_con_1" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_1"] != "" )? $row["m_con_1"] : "[ORDERNAME] 상품을 구매하여  [POINT]P 적립되었습니다."?></textarea></td>
								</tr>
								<tr>
									<th><input type="checkbox" name="m_yn_2" value="1" <?= ( $row["m_yn_2"] == "1" ) ? "checked" : "" ?> style="margin-left:20px;"> 추천인 등록</th>
									<td><textarea name="m_con_2" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_2"] != "" )? $row["m_con_2"] : "[MEMBERNAME]님이 [RECOMM_DEPTH]차 추천인으로 등록 되었습니다."?></textarea></td>
								</tr>
								<tr>
									<th><input type="checkbox" name="m_yn_4" value="1" <?= ( $row["m_yn_4"] == "1" ) ? "checked" : "" ?> style="margin-left:20px;"> 찜상품 마감전 알림</th>
									<td><textarea name="m_con_4" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_4"] != "" )? $row["m_con_4"] : "찜 상품 [[PRODUCTNAME]] 마감이 얼마남지 않았습니다."?></textarea></td>
								</tr>
								<tr>
									<th><input type="checkbox" name="m_yn_5" value="1" <?= ( $row["m_yn_5"] == "1" ) ? "checked" : "" ?> style="margin-left:20px;"> 예정상품오픈시 </th>
									<td><textarea name="m_con_5" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_5"] != "" )? $row["m_con_5"] : "알림예약하신 상품 [[PRODUCTNAME]] 오픈되었습니다."?></textarea></td>
								</tr>
							</tbody>
						</table>

						
					<h3 style="margin:30px 0 15px 0;"><strong style="color:#1ea6b8;">PUSH 삽입코드 </strong></h3>
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong> 공통 삽입 코드</strong></th>
									<td>
										* 쇼핑몰이름 : [SHOPNAME]<br />
										* 쇼핑몰링크 : [SHOPURL]
									</td>
								</tr>
								<tr>
									<th><strong>주문 관련 삽입코드</strong></th>
									<td>
										* 주문자명 : [ORDERNAME] <br />
										* 포인트금액 : [POINT] <br />
										* 상품명 : [PRODUCTNAME] 
									</td>
								</tr>
								<tr>
									<th><strong>회원 관련 삽입 코드</strong></th>
									<td>
										* 회원명 : [MEMBERNAME]<br />
										* 추천인단계 : [RECOMM_DEPTH]
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="javascript:comp_submit();return false;"><span>저장</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					$("#regi_form").submit();
					return false;
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
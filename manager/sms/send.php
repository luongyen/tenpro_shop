<?
$menu_id = "7";
$page_id = "3";
include "../include/header.php";
?>
			<!-- leftmneu -->
			<? include "../include/left_sms.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents" style="min-height:700px;">
				<!-- title -->
				<div class="titbox">
					<h2 class="title">메세지발송</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					<h3 style="margin-bottom:15px;"><strong style="color:#1ea6b8;">SMS 메세지 설정</strong> (* SMS메세지는 200 Bytes 까지만 입력하실 수 있습니다.)</h3>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="mode" value="smssend">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>발송매체</strong></th>
									<td>
										<select name="callback" id="callback" style="width:200px;">
											<option value="">------- 선택하세요 -------</option>
											<?
											$rst_media = mysql_query( " select * from media where del_ok='0' " );
											while ( $row_media = mysql_fetch_array( $rst_media ) ) {
											?>
												<option value="<?=$row_media["phone_sms"]?>_<?=$row_media["mid"]?>"><?=$row_media["mname"]?> (<?=$row_media["phone_sms"]?>)</option>
											<?}?>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>수신번호</strong></th>
									<td><input type="text" name="phone" id="phone" value="" style="width:500px;"> 숫자만 입력</td>
								</tr>
								<tr>
									<th><strong>메세지 내용</strong></th>
									<td><textarea name="msg" id="msg" style="line-height: 20px; font-size: 11px;width:500px;" onkeyup="checkByte(this, 200, 'calByte')" rows="10" ></textarea><br /><div class="textCount" id="calByte">0 Bytes</div><br /><p> * SMS메세지는 최대 200 Bytes 까지만 입력하실 수 있습니다.</p><br /></td>
								</tr>
							</tbody>
						</table>

						
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="javascript:comp_submit();return false;"><span>전송</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					var regPhone = /^((01[1|6|7|8|9])[1-9]+[0-9]{6,7})|(010[1-9][0-9]{7})$/;
					if ($("#callback option:selected").val() == "" ) {
						alert("발송매체를 선택하세요");
					}else if(!regPhone.test($("#phone").val())) {
						alert("수신전화번호를 확인하세요");
					}else if($("#msg").val() == "" ) {
						alert("메세지 내용을 입력하세요");
					}else {
						$("#regi_form").submit();
					}
					return false;
				}
				function cutLength(value){
		 
					//입력받은 문자열을 escape() 를 이용하여 변환한다.
					//변환한 문자열 중 유니코드(한글 등)는 공통적으로 %uxxxx로 변환된다.
					var cutEscapeStr = escape(value);
					var cutStartIndex = 0;
					var cutEndIndex = 0;
					var cutTempStr = "";
					var cutCnt = 0;
				
					//문자열 중에서 유니코드를 찾아 제거하면서 갯수를 센다.
					while ((cutEndIndex = cutEscapeStr.indexOf("%u", cutStartIndex)) >= 0){ //제거할 문자열이 존재한다면
				  
						cutTempStr += cutEscapeStr.substring(cutStartIndex, cutEndIndex);
						cutStartIndex = cutEndIndex + 6;
						cutCnt ++;
					}
				
					cutTempStr += cutEscapeStr.substring(cutStartIndex);
					cutTempStr = unescape(cutTempStr); //원래 문자열로 바꾼다.
					
					//유니코드는 2바이트 씩 계산하고 나머지는 1바이트씩 계산한다.
					return ((cutCnt * 2) + cutTempStr.length) + "";
				} 
	
				function checkByte(str, maxLength, title){
					
					var cbStr = str.value; //이벤트가 일어난 컨트롤의 value 값
					var cbTotalLength = cbStr.length; //전체길이
					var max = maxLength; //제한할 글자수 크기
					var i = 0;  //for문에 사용
					var cbCount = 0;  //한글일경우는 2 그밗에는 1을 더함
					var cbLength = 0;  //substring하기 위해서 사용
					var cbOneChar = ""; //한글자씩 검사한다
					var cbResizingStr = ""; //글자수를 초과하면 제한할수 글자전까지만 보여준다.
				   
					for(i=0; i< cbTotalLength; i++){
					  
						cbOneChar = cbStr.charAt(i); //한글자추출
					  
						if (escape(cbOneChar).length > 4){ //한글이면 2를 더한다.
							cbCount = cbCount + 2;
						}else{ //그외의 경우는 1을 더한다.
							cbCount = cbCount + 1;
						}
					  
						if(cbCount <= max){ //전체 크기가 max를 넘지않으면
							cbLength = i + 1;
						}
					}   
				   
					if(cbCount > max){ //전체길이를 초과하면
						
						alert( max + " Bytes를 초과 입력할수 없습니다. \n 초과된 내용은 자동으로 삭제 됩니다. ");
						cbResizingStr  = cbStr.substr(0, cbLength);
						str.value = cbResizingStr ; 
						document.getElementById(title).innerHTML = cutLength(str.value)+" Bytes";
					}
					
					str.focus();   
					document.getElementById(title).innerHTML = cutLength(str.value)+" Bytes";
				}

			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
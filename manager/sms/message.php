<?
$menu_id = "7";
$page_id = "2";
include "../include/header.php";
$row = getdata("select * from sms_message where shop_id='admin'");
?>
			<!-- leftmneu -->
			<? include "../include/left_sms.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<div id="sddr_layer" style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer" style="cursor:pointer;position:absolute;right:-3px;top:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer1" style="cursor:pointer;position:absolute;right:-3px;bottom:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
				</div>
				<!-- title -->
				<div class="titbox">
					<h2 class="title">메세지관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					<h3 style="margin-bottom:15px;"><strong style="color:#1ea6b8;">SMS 메세지 설정</strong> (* SMS메세지는 200 Bytes 까지만 입력하실 수 있습니다.)</h3>
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="mode" value="message">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th rowspan="6"><strong>회원에게 발송</strong></th>
									<td><input type="checkbox" name="m_yn_1" value="1" <?= ( $row["m_yn_1"] == "1" ) ? "checked" : "" ?>> 1. 무통장주문</td>
									<td><textarea name="m_con_1" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_1"] != "" )? $row["m_con_1"] : "[SHOPNAME] 주문되었습니다. [ORDERBANK] [ORDERACCOUNT] [ORDERACCOUNTNAME] / 금액 : [ORDERPRICE] 입금확인후 상품이 배송됩니다. 주문번호 : [ORDERNO]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_2" value="1" <?= ( $row["m_yn_2"] == "1" ) ? "checked" : "" ?>> 2. 주문완료시(무통장 제외)</td>
									<td><textarea name="m_con_2" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_2"] != "" )? $row["m_con_2"] : "[SHOPNAME] 주문이 완료되었습니다. 주문번호 : [ORDERNO]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_3" value="1" <?= ( $row["m_yn_3"] == "1" ) ? "checked" : "" ?>> 3. 입금확인시(무통장) </td>
									<td><textarea name="m_con_3" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_3"] != "" )? $row["m_con_3"] : "[SHOPNAME] 입금이 확인되었습니다. 주문번호 : [ORDERNO]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_4" value="1" <?= ( $row["m_yn_4"] == "1" ) ? "checked" : "" ?>> 4. 상품배송시 </td>
									<td><textarea name="m_con_4" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_4"] != "" )? $row["m_con_4"] : "[SHOPNAME] 주문하신 상품 배송출발되었습니다. [CARRYCOMP] / [CARRYNO]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_5" value="1" <?= ( $row["m_yn_5"] == "1" ) ? "checked" : "" ?>> 5. 주문취소시 </td>
									<td><textarea name="m_con_5" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_5"] != "" )? $row["m_con_5"] : "[SHOPNAME] 주문이 취소 되었습니다. 주문번호 : [ORDERNO]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_6" value="1" <?= ( $row["m_yn_6"] == "1" ) ? "checked" : "" ?>> 6. 1:1문의 답변시 </td>
									<td><textarea name="m_con_6" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_6"] != "" )? $row["m_con_6"] : "[SHOPNAME] [WRITENAME]님 문의글에 답변이 완료되었습니다."?></textarea></td>
								</tr>
								<tr>
									<th rowspan="3"><strong>판매관리자에게 발송 </strong></th>
									<td><input type="checkbox" name="m_yn_7" value="1" <?= ( $row["m_yn_7"] == "1" ) ? "checked" : "" ?>> 7. 무통장 입금확인시 </td>
									<td><textarea name="m_con_7" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_7"] != "" )? $row["m_con_7"] : "무통장 주문 등록되었습니다."?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_8" value="1" <?= ( $row["m_yn_8"] == "1" ) ? "checked" : "" ?>> 8.주문완료시(무통장 제외)</td>
									<td><textarea name="m_con_8" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_8"] != "" )? $row["m_con_8"] : "주문이 접수 되었습니다."?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_9" value="1" <?= ( $row["m_yn_9"] == "1" ) ? "checked" : "" ?>> 9. 1:1문의접수시 </td>
									<td><textarea name="m_con_9" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_9"] != "" )? $row["m_con_9"] : "1:1문의가 등록되었습니다."?></textarea></td>
								</tr>
								<tr>
									<th rowspan="2"><strong>수동발송 항목</strong></th>
									<td><input type="checkbox" name="m_yn_10" value="1" <?= ( $row["m_yn_10"] == "1" ) ? "checked" : "" ?>> 10. 미입금확인문자</td>
									<td><textarea name="m_con_10" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_10"] != "" )? $row["m_con_10"] : "[SHOPNAME] [ORDERDATE]에 주문하신 상품 입금이 확인되지 않고 있습니다. 확인부탁드립니다."?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_12" value="1" <?= ( $row["m_yn_12"] == "1" ) ? "checked" : "" ?>> 12. 이벤트 당첨 안내<br />(출석이벤트제외) </td>
									<td><textarea name="m_con_12" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_12"] != "" )? $row["m_con_12"] : "축하드립니다.[[EVENTNAME]] 이벤트에 당첨되셨습니다."?></textarea></td>
								</tr>
								<tr>
									<th rowspan="3"><strong>자동발송 항목 </strong></th>
									<td><input type="checkbox" name="m_yn_11" value="1" <?= ( $row["m_yn_11"] == "1" ) ? "checked" : "" ?>> 11. 예정상품 오픈시 </td>
									<td><textarea name="m_con_11" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_11"] != "" )? $row["m_con_11"] : "[SHOPNAME] [PRODUCTNAME] 상품의 판매가 시작되었습니다. [SHOPURL]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_13" value="1" <?= ( $row["m_yn_13"] == "1" ) ? "checked" : "" ?>> 13. 앵콜상품 오픈시 </td>
									<td><textarea name="m_con_13" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_13"] != "" )? $row["m_con_13"] : "[SHOPNAME] [PRODUCTNAME] 상품의 판매가 시작되었습니다. [SHOPURL]"?></textarea></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="m_yn_14" value="1" <?= ( $row["m_yn_14"] == "1" ) ? "checked" : "" ?>> 14. 찜 상품 마감임박 시 </td>
									<td><textarea name="m_con_14" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_14"] != "" )? $row["m_con_14"] : "회원님 장바구니에 담으신 상품 [[PRODUCTNAME]]의 마감이 얼마남지 않았습니다. [SHOPURL]"?></textarea></td>
								</tr>
								<!--tr>
									<td><input type="checkbox" name="m_yn_15" value="1" <?= ( $row["m_yn_15"] == "1" ) ? "checked" : "" ?>> 무통장 입금확인시 </td>
									<td><textarea name="m_con_15" class="form-control" style="height:50px;width:100%"><?=( $row["m_con_15"] != "" )? $row["m_con_15"] : "무통장 주문 등록되었습니다."?></textarea></td>
								</tr-->
							</tbody>
						</table>

						
					<h3 style="margin:30px 0 15px 0;"><strong style="color:#1ea6b8;">SMS 삽입코드 </strong></h3>
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong> 공통 삽입 코드</strong></th>
									<td>
										* 쇼핑몰이름 : [SHOPNAME]<br />
										* 쇼핑몰링크 : [SHOPURL]
									</td>
								</tr>
								<tr>
									<th><strong>주문 관련 삽입코드</strong></th>
									<td>
										* 주문자명 : [ORDERNAME] <br />
										* 주문자 연락처 : [ORDERPHONE] <br />
										* 상품명 : [PRODUCTNAME] <br />
										* 주문번호 : [ORDERNO]<br />
										* 주문수량 : [ORDERQTY]<br />
										* 택배사 : [CARRYCOMP]<br />
										* 송장번호 : [CARRYNO]<br />
										* 주문일시 : [ORDERDATE]<br />
										* 결제금액 : [ORDERPRICE]<br />
										* 은행 : [ORDERBANK]<br />
										* 계좌번호 : [ORDERACCOUNT]<br />
										* 예금주 : [ORDERACCOUNTNAME]
									</td>
								</tr>
								<tr>
									<th><strong> 1:1문의 관련 삽입 코드</strong></th>
									<td>* 작성자 이름 : [WRITENAME]</td>
								</tr>
								<tr>
									<th><strong>이벤트 관련 삽입 코드</strong></th>
									<td>
										* 이벤트명 : [EVENTNAME]<br />
										* 참여자명 : [WRITENAME]<br />
										* 등수 : [RSTGRADE등] (0 이면 출력안함 )
									</td>
								</tr>
								<tr>
									<th><strong>자동발송 관련 삽입 코드</strong></th>
									<td>* 상품명 : [PRODUCTNAME]</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="javascript:comp_submit();return false;"><span>저장</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					$("#regi_form").submit();
					return false;
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
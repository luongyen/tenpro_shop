<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}

include("/home/qzone/poq/PLPayComEXECV2.php");    // exec 방식

$o_num = mysql_real_escape_string( $_GET["o_num"]);
$gubun = mysql_real_escape_string( $_GET["gubun"]);
$row = getdata("SELECT * FROM `orders` where o_num = '".$o_num."'");
$goods = getdata("SELECT * FROM `goods` where gcode= '".$row["gcode"]."'");
$amt = $row["total_price"];
//부가가치세, 공급가액 계산
$comm=(int)($amt / 1.1);
$clvt = $amt - $comm;
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>현금영수증 발행</title>
	
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">
	
	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">
	
	<div class="popup_box">
		
		<div class="titbox">
			<p class="t">현금영수증 발행</p>
		</div>
		<div class="popbody">
			
			<div class="scrollbox" style="height:400px;">
				<form action="_proc.php" method="post" role="form" class="form-horizontal" name="regi_form" target="ifr_proc">
				<input type="hidden" id="mode" name="mode" value="cash_receipt">
				<input type="hidden" id="gubun" name="gubun" value="<?=$gubun?>">
				<input type="hidden" id="o_num" name="o_num" value="<?=$o_num?>">
				<input type="hidden" id="cid" name="cid" value="<?=$row["cash_authno"]?>">
				<div class="table_typeB">
					<table cellpadding="0" cellspacing="0" border="1" summary="" style="border-bottom:0;">
						<colgroup><col style="width:30%;"><col style="width:70%;"></colgroup>
						<tbody>
							<tr>
								<th class="text-center active vertical_50">주문번호</th>
								<td><?=$row["o_num"]?></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">상품명</th>
								<td><input type="text" name="pname" id="pname" value="<?=$goods["pname"]?>" style="width: 80%;"></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">결재금액</th>
								<td><input type="text" name="amt" id="amt" value="<?=$amt?>" style="width: 30%;"> 원 * 숫자만</td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">공급가액</th>
								<td><input type="text" name="comm" id="comm" value="<?=$comm?>" style="width: 30%;"> 원 * 숫자만</td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">부가가치세</th>
								<td><input type="text" name="clvt" id="clvt" value="<?=$clvt?>" style="width: 30%;"> 원 * 숫자만</td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">발급구분</th>
								<td><input type="radio" name="receipt_gubun" value="1" <?=$row["receipt_gubun"]== "1" ? "checked" : "" ?> /> 개인 소득공제용&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="receipt_gubun" value="2" <?=$row["receipt_gubun"]== "2" ? "checked" : "" ?> /> 사업자지출증빙용</td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">발급번호</th>
								<td><input type="text" name="receipt_number" id="receipt_number" value="<?=$row["receipt_number"]?>" style="width: 40%;"> * 숫자만, 띄어쓰기,(-) 등 금지</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:center;">
									<a href="#" class="btn_70 edit_pop" id="updateBtn"><span>등록</span></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				</form>
			</div>
			<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
			<script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
			<script>
				$(function () {
					var f = $(document.forms["regi_form"]);

					<? if ( $gubun == "1" ) {?>
					$('a#updateBtn')
					.css('cursor', 'pointer')
					.click(function () {
						if ($('#pname', f).val() == "" ) {
							alert("상품명을 입력하세요");
							$('#pname', f).focus();
						}else if ($('#amt', f).val() == "" ) {
							alert("결재금액을 입력하세요");
							$('#amt', f).focus();
						}else if ($('#comm', f).val() == "" ) {
							alert("공급가액을 입력하세요");
							$('#comm', f).focus();
						}else if ($('#clvt', f).val() == "" ) {
							alert("부가가치세를 입력하세요");
							$('#clvt', f).focus();
						}else if ($('#receipt_number', f).val() == "" ) {
							alert("발급번호를 입력하세요");
							$('#receipt_number', f).focus();
						}else {
							f.submit();
						}
					});
					<?}else {?>
						f.submit();
					<?}?>
				});
			</script>
		</div>
		 
	</div>

</body>
</html>
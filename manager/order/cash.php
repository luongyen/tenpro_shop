<?
$menu_id = "11";
$page_id = "18";
include "../include/header.php"; 

	$sdate = mysql_real_escape_string( $sdate );
	$edate = mysql_real_escape_string( $edate );
	$stat = mysql_real_escape_string( $stat );
	$gubun = mysql_real_escape_string( $gubun );
	$searchK = mysql_real_escape_string( $searchK );
	$page = mysql_real_escape_string($page);

	$get_query = "sdate=$sdate&edate=$edate&searchK=$searchK&gubun=$gubun&stat=$stat";
	$where = "";
	if ( $searchK != "" ) { $where .= "  ( o_num like '%$searchK%' or uname like '%$searchK%' or uid like '%$searchK%' or cnum like '%$searchK%' ) and "; }
	if ( $stat != "" ) { $where .= "  stat = '$stat' and "; }
	if ( $gubun != "" ) { $where .= "  gubun = '$gubun' and "; }
	if ( $sdate != "" ) { $where .= "  reg_date >= '$sdate 00:00:00' and "; }
	if ( $edate != "" ) { $where .= "  reg_date <= '$edate 23:59:59' and "; }
		
	$goods_count=getdata( "select count(*) as cnt from cash_receipt where ".$where . " 1=1" );
	$cnt = $goods_count["cnt"];
	//페이징준비처리
		$pageIdx=1;
		if ($page>0) {
			$pageIdx=$page;
		}
		if ($list_count != "" ) {
			if ($list_count == "all" ) {
				$page_set = 100000;			
			}else {
				$page_set = $list_count;
			}
		}else {
			$page_set = 20; //한페이지 줄수-기본값			
		}
		$block_size = 10;
		if($pageIdx % $block_size==0) {
			$start_num=$pageIdx-$block_size+1;
		}else {
			$start_num=floor($pageIdx/$block_size)*$block_size +1;
		}
		$end_num = $start_num+$block_size-1;
		$total_page = ceil($cnt / $page_set); // 총 페이지 수

		if($pageIdx==1) {
			$limit_idx=0;
		}else {
			$limit_idx=$pageIdx*$page_set-$page_set;
		}
	//페이징준비 끝

		$sql = "select * from cash_receipt where ".$where . " 1=1 order by reg_date desc  limit ".$limit_idx.", ".$page_set;
		$rst = mysql_query( $sql );
?>
			
			<!-- leftmneu -->
			<? include "../include/left_order_new.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">현금영수증 발행</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox" style="width:1300px;">
					
					<!-- search -->
					<div class="board_search" style="margin-bottom:20px;">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<input type="hidden" name="S" value="<?=$S?>">
						<input type="hidden" name="sort" value="<?=$sort?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="100px"><col width=""><col width="100px"><col width="200px"><col width="100px"><col width="200px"></colgroup>
							<tbody>
								<tr>
									<th><strong>기간</strong></th>
									<td>
										<input type="text" name="sdate" class="form-control" style="width:90px;display:inline-block;" id="sdate" value="<?=$sdate?>" data-date="<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>" data-date-format="yyyy-mm-dd" /> ~ 
										<input type="text" name="edate" class="form-control" style="width:90px;display:inline-block;" id="edate" value="<?=$edate?>" data-date="<?=date("Y-m-d")?>" data-date-format="yyyy-mm-dd" />
										<button class="btn btn-default btn-xs sel_today" type="button" style="margin:0;" >오늘</button> 
										<button class="btn btn-default btn-xs sel_week" type="button" style="margin:0;" >일주일</button> 
										<button class="btn btn-default btn-xs sel_month" type="button" style="margin:0;" >한달</button>
									</td>
									<th><strong>상태구분</strong></th>
									<td>
										<select name="stat" id="stat" class="sel1">
											<option value=""> -- 선택 -- </option>
											<option value="0" <?= ( $stat == "0" ) ? " selected" : "" ?>> 신청</option>
											<option value="1" <?= ( $stat == "1" ) ? " selected" : "" ?>> 처리완료 </option>
										</select>
									</td>
									<th><strong>발행구분</strong></th>
									<td>
										<select name="gubun"  id="gubun"  class="sel1">
											<option value=""> -- 선택 -- </option>
											<option value="0" <?= ( $gubun == "0" ) ? " selected" : "" ?>>발행</option>
											<option value="1" <?= ( $gubun == "1" ) ? " selected" : "" ?>>취소</option>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>통합검색</strong></th>
									<td colspan="5">
										<input type="text" name="searchK" class="form-control" style="display:inline-block;width:80%;" id="searchK" value="<?=$searchK?>" OnKeyDown="EnterCheck();" placeholder="주문번호, 주문자명, 신청번호, 아이디 " /> 
									</td>
								</tr>
							</tbody>
						</table>
						</form>
						<div class="btn_search"><a href="#" onclick="javascript:search_frm.submit();return false;">검색</a></div>
					</div>
					<!-- //search -->
					<div>					
						<span class="col-md-12 text-left b" style="font-size:1.3em;"><strong>총 발행건수 : <span class="text-danger"><?=number_format( $cnt )?></span>개</strong>
						</span>
					</div>
					<form action="_proc.php" method="post" role="form" class="form-horizontal" name="regi_form">
					<input type="hidden" name="sdate" value="<?=$sdate?>">
					<input type="hidden" name="edate" value="<?=$edate?>">
					<input type="hidden" name="stat" value="<?=$stat?>">
					<input type="hidden" name="searchK" value="<?=$searchK?>">
					<input type="hidden" name="gubun" value="<?=$gubun?>">
					<input type="hidden" name="page" value="<?=$page?>">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="mode" value="">
					<div class="table_typeA table_bg m10">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="3%"><col width="5%"><col width="8%"><col width="10%"><col width="8%"><col width=""><col width="10%"><col width="8%"><col width="10%"><col width="10%"><col width="10%"></colgroup>
							<thead>
								<tr>
									<th><input type="checkbox" name="chkall" value="1" onclick="javascript:selall(this,document.regi_form.chkDel);"></th>
									<th>번호</th>
									<th>발행구분</th>
									<th>주문번호</th>
									<th>주문자명<br />(아이디)</th>
									<th>발급유형:번호</th>
									<th>발행금액</th>
									<th>상태구분</th>
									<th>신청일</th>
									<th>처리일</th>
									<th>관리</th>
								</tr>
							</thead>
							<tbody>
							<?
							$i=0;
							while ($row = mysql_fetch_array($rst)) { 
								$rNumber=$cnt-$limit_idx-$i;
								$stat_str = "";
								$gubun_str = "";
								$type_str = "";
								$admin_str = "";
								$admin_str1 = "";
								$l = 0;
								//회원정보 url
								if ( $row["uid"] != "" ) {
									$row_mem = getdata( " select idx from users where id='".$row["uid"]."'" );								
								}
								
								$media = getdata( " select * from media where mid='" . $row["shop_id"] . "' " );
								if			( $row["stat"] == "0" ) { $stat_str = "<span class='dev_stat_red'>신청</span>"; }
								elseif	( $row["stat"] == "1" ) { $stat_str = "<span class='dev_stat_green'>처리완료</span>"; }
								
								if			( $row["gubun"] == "0" ) { $gubun_str = "신청"; }
								elseif	( $row["gubun"] == "1" ) { $gubun_str = "취소"; }

								if	( $row["type"] == "1" ) { $type_str = "휴대폰"; }
								elseif	( $row["type"] == "1" ) { $type_str = "사업자번호"; }
									/*
									if			( $row["status"] == "0" ) { $stat_str = "<span class='dev_stat_red'>미입금</span>"; $admin_str = "<button class=\"btn  btn-warning btn-xs\" onclick=\"javascript:sel_check('".$row["o_num"]."','2');\" type=\"button\"><i class=\"fa fa-check\"></i> 입금확인</button>"; }
									elseif	( $row["status"] == "2" ) { $stat_str = "<span class='dev_stat_yellow'>신규주문</span>"; $admin_str = "<button class=\"btn  btn-warning btn-xs\" onclick=\"javascript:sel_check('".$row["o_num"]."','3');\" type=\"button\"><i class=\"fa fa-check\"></i> 주문확인</button>"; }
									elseif	( $row["status"] == "3" ) { $stat_str = "<span class='dev_stat_green'>배송준비</span>"; }
									elseif	( $row["status"] == "4" ) { $stat_str = "<span class='dev_stat_green'>배송중</span>"; }
									elseif	( $row["status"] == "5" ) { $stat_str = "<span class='dev_stat_green'>배송완료</span>"; }
									elseif	( $row["status"] == "6" ) { $stat_str = "<span class='dev_stat_blue'>결제실패</span>"; }
									elseif	( $row["status"] == "10" ) { $stat_str = "<span class='dev_stat_blue'>배송예약</span>"; }
									elseif	( $row["status"] == "60" ) { $stat_str = "<span class='dev_stat_blue'>취소접수</span>"; }
									elseif	( $row["status"] == "61" ) { $stat_str = "<span class='dev_stat_blue'>취소완료</span>"; $admin_str1 = "<button class=\"btn  btn-normal btn-xs\" onclick=\"javascript:sel_del('".$row["o_num"]."');\" type=\"button\"><i class=\"fa fa-times\"></i>  완전삭제</button>";}
									elseif	( $row["status"] == "70" ) { $stat_str = "<span class='dev_stat_blue'>반품중</span>"; $admin_str = "<button class=\"btn  btn-warning btn-xs\" onclick=\"javascript:sel_check('".$row["o_num"]."','71');\" type=\"button\"><i class=\"fa fa-check\"></i> 반품완료</button>";}
									elseif	( $row["status"] == "71" ) { $stat_str = "<span class='dev_stat_blue'>반품완료</span>"; }
									elseif	( $row["status"] == "80" ) { $stat_str = "<span class='dev_stat_blue'>교환중</span>";$admin_str = "<button class=\"btn  btn-warning btn-xs\" onclick=\"javascript:sel_check('".$row["o_num"]."','81');\" type=\"button\"><i class=\"fa fa-check\"></i> 교환완료</button>"; }
									elseif	( $row["status"] == "81" ) { $stat_str = "<span class='dev_stat_blue'>교환완료</span>"; }
									if ( $row["deliver_name"] != "" && $row["deliver_num"] != "" ) {
										$deli_com = getdata( " select * from delivery where idx='" . $row["deliver_name"] . "' " );
										$deli_str = "<button class=\"btn  btn-success btn-xs\"  onclick=\"javascript:window.open('" . $deli_com["url"] . str_replace( "-" , "" , $row["deliver_num"] ) . "','','');\" type=\"button\"><i class=\"fa fa-truck\"></i> 배송조회</button>";
									}*/
						?>
						<tr>
							<td><input type="checkbox" name="chkDel" value="<?=$row["idx"]?>"></td>
							<td><?=$rNumber?></td>
							<td><?=$gubun_str?></td>
							<td><a href="./detail.php?S=1&c=<?=$row["o_num"]?>" target="_blank"><?=$row["o_num"]?></a></td>
							<td><?= ( $row["uid"] != "" ) ? "<br /><a href=\"../account/detail.php?code=".$row_mem["idx"]. "\" target=\"_blank\">".$row["uid"]."</a>" : "" ?></td>
							<td><?=$row["cnum"] . "(" . $type_str . ")"?></td>
							<td><?=number_format( $row["price"] )?> 원</td>
							<td><?=$stat_str?></td>
							<td><?=$row["reg_date"]?></td>
							<td><?=$row["c_date"]?></td>

							<!--
							<td><?=$row["order_phone"]?></td>
							<td><img src='<?=$goods["files"]?>' border='0' alt='' style="max-width:80px;margin:5px;"><br /><?=$goods["pname"]?> <?= $option_l ?></td>
							<td><?=number_format( $row["price"] + $row["delivery_fee"] )?> 원</td>
							<td><?=number_format( $row["use_point"] )?></td>
							<td>
								<?=$admin_str?>
								<button class="btn btn-primary btn-xs" onclick="javascript:location.href='./detail.php?S=<?=$S?>&c=<?=$row["o_num"]?>';" type="button"><i class="fa fa-pencil"></i> 상세보기</button>
								<button class="btn btn-danger btn-xs"  onclick="javascript:sel_check('<?=$row["o_num"]?>','61');" type="button"><i class="fa fa-times"></i> 주문취소</button>
								<?=$admin_str1?>
								<?=$deli_str?>
							</td-->
						</tr>						
						<?$i++; }?>
							</tbody>
						</table>
					</div>
					</form>
								
					<!-- paging -->
					<div class="paging">
					<? 
						if($cnt>0) {
							$prev_page=$pageIdx-1;
							$next_page=$pageIdx+1;							
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$prev_page."&".$get_query."\" class=\"prev\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a>" : "<a href=\"#\" class=\"prev\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a> ";
							if ($total_page<10) {
								$vpage=1;
							}else{
								$vpage = ( ( (int)( ($pageIdx - 1 ) / $page_set ) ) * $page_set ) + 1;
							}
							$spage = $vpage + $page_set - 1;
							if ($spage >= $total_page) $spage = $total_page;

							for($i=$vpage;$i<=$spage;$i++){ 
								if ($pageIdx==$i) {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\" class=\"current\"><span><strong>" . $i . "</strong></span></a> ";
								}else {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\"><span>" . $i . "</span></a> ";
								}
							}
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$next_page."&".$get_query."\" class=\"next\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a>" : "<a href=\"#\" class=\"next\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a> ";
						}?>
					</div>
					<!-- //paging -->

				</div>
				
			</div>
<script src="../js/plugins/jquery.zeroclipboard.min.js"></script>
<script type="text/javascript">

function selall(chekboxall,checkboxsub){
   var ischeck="";
	if (chekboxall.checked) {
		 ischeck="checked";
	}
   if (checkboxsub.name !=undefined){
	  checkboxsub.checked=ischeck;
   }else {
		for (i=0; i<checkboxsub.length; i++) {
		checkboxsub[i].checked =ischeck;
		}
   }
}

function CheckGroup(val){
 var form=document.regi_form;
 var i; 
 var nChk = document.getElementsByName("chkDel");     //체크되어있는 박스 value값
 form.gid.value ='';
 if(nChk){
  for(i=0;i<nChk.length;i++) { 
   if(nChk[i].checked){                                                            //체크되어 있을경우 
    if(form.gid.value ==''){
     form.gid.value = nChk[i].value; 
    }else{
     form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
    }
   } 
  }
  if(form.gid.value ==''){                                                 //선택이 하나도 안되어있을 경우
   alert("1건 이상 선택해 주세요.");       
   return false; 
  }
  if ( val == "1" ) { // SMS발송
      var con = confirm("선택된 주문에대해서 미입금 확인 SMS를 발송하시겠습니까?");
      if (con) {
        form.status_update.value =  "";
        form.mode.value =  "sms10";
	  } else {
        return false;
      }
  }else if ( val == "2" ) { // 일괄취소
      var con = confirm("선택된 내역을 일괄 취소하시겠습니까?");
      if (con) {
        form.status_update.value =  "61";
        form.mode.value =  "save_money";
	  } else {
        return false;
      }
  }else if ( val == "3" ) { // 일괄엑셀
	  form.mode.value =  "excel_down";
  }else if ( val == "4" ) { // 일괄 수정
      var con = confirm("선택된 내역을 일괄 변경하시겠습니까?");
      if (con) {
		  form.status_update.value = $('#status_update').val();
		  if (form.status_update.value == "" ) {
			alert("변경하실 주문상태를 선택해주세요");
			return false;
		  }else {
		    form.mode.value =  "save_money";
		  }
	  } else {
        return false;
      }
  }
  document.regi_form.submit();
 } 
}
function selUp(val , m) {
	document.regi_form.gid.value = val;
	if (m == "1" ) {
		document.regi_form.mode.value =  "update_list";		
	}else {
		var con = confirm("삭제하시겠습니까? \n 삭제하시면 복구 할 수 없습니다.");
		if (con) {
			document.regi_form.mode.value =  "delete_list";
		}
	}
	document.regi_form.submit();			
}

function sel_check( val , stat ) {
	var con = confirm("주문상태를 변경 하시겠습니까?");
	if (con) {
		document.regi_form.gid.value = val;
		document.regi_form.status_update.value = stat;
		document.regi_form.mode.value = "save_money";
		document.regi_form.submit();
	}else {
		return false;
	}	
}

function sel_cancel(val) {
	var con = confirm("취소 하시겠습니까?");
	if (con) {
		document.regi_form.gid.value = val;
		document.regi_form.status_update.value = "61";
		document.regi_form.mode.value = "save_money";
		document.regi_form.submit();
	}else {
		return false;
	}	
}
function sel_del(val) {
	var con = confirm("삭제하시겠습니까? \n 삭제하시면 복구 할 수 없습니다.");
	if (con) {
		document.regi_form.gid.value = val;
		document.regi_form.mode.value = "delete";
		document.regi_form.submit();
	}else {
		return false;
	}	
}

	$(document).ready(function(){
		$('#sdate').datepicker().on('changeDate', function(e) {
			$("#sdate").datepicker('hide');
		});
		$('#edate').datepicker().on('changeDate', function(e) {
			$("#edate").datepicker('hide');
		});

		var today_val = "<?=date("Y-m-d")?>";
		var week_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>";
		var mon_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")-1  , date("d"), date("Y")))?>";

		$(".sel_today").on("click",function(){
			$("#sdate").val(today_val);
			$("#edate").val(today_val);
		});
		$(".sel_week").click(function(){
			$("#sdate").val(week_val);
			$("#edate").val(today_val);
		});
		$(".sel_month").click(function(){
			$("#sdate").val(mon_val);
			$("#edate").val(today_val);
		});
			
		$("body").on("copy", ".zclip", function(/* ClipboardEvent */ e) {
			e.clipboardData.clearData();
			e.clipboardData.setData("text/plain", $(this).data("zclip-text"));
			e.preventDefault();
			alert("URL이 복사되었습니다.\n Ctrl+V로 붙여넣기 하세요");
		});

	});
	function EnterCheck() {
		if(event.keyCode==13) {
			search_frm.submit();
			return false;
		}
	}

</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
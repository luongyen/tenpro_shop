<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}
$orderNo = mysql_real_escape_string( $_GET["idx"] );
$division = mysql_real_escape_string( $_GET["division"] );

$row = getdata("select A.* , B.orderItemInfo from orders_change_req as A left join orders as B ON A. orderNo = B.orderNo where A.idx = '$idx'");
$order_change = getdata("select * from orders_change_req where orderNo = '" . $orderNo . "'");

$option_arr = explode("@" , $row["selectOpt"] );
for ( $j = 0 ; $j < count( $option_arr ) ; $j++ ) {
	$option_data = explode("|" , $option_arr[$j] );
	$row_option = getdata("select oname from goods_option_data where goods_no = '" . $orders[$i]->no . "' and okey = '" . $option_data[0] . "' ");
	if ( $row_option["oname"] != "" ) {
		$options .= "<br />" . $row_option["oname"] . " " . $option_data[1] . "개";
	}else {
		$options .= "<br />" . $option_data[1] . "개";
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>교환/반품 정보 확인</title>
	
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">
	
	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">
	
	<div class="popup_box">
		
		<div class="titbox">
			<p class="t">교환/반품 정보 확인</p>
		</div>
		<div class="popbody">
			
			<div class="scrollbox" style="height:700px;">
				<div class="table_typeB">
				<form name='frm_form' method="POST" action="./_proc.php" enctype="multipart/form-data">
				<input type="hidden" name="mode" value="req_deny" />
				<input type="hidden" name="orderNo" value="<?=$orderNo?>" />
					<table cellpadding="0" cellspacing="0" border="1" summary="" style="border-bottom:0;">
						<colgroup><col style="width:15%;"><col style="width:35%;"><col style="width:15%;"><col style="width:35%;"></colgroup>
						<tbody>
							<tr>
								<th class="text-center active vertical_50">주문번호</th>
								<td><?=$res -> order -> no?></td>
								<th class="text-center active vertical_50">주문상태</th>
								<td><?=$res -> order -> statusName?></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">주문금액</th>
								<td><?=$res -> order -> orderAmount?></td>
								<th class="text-center active vertical_50">총주문수량</th>
								<td><?=$res -> order -> orderQty?></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">결재한 배송비</th>
								<td><?=$res -> order -> paidDeli?></td>
								<th class="text-center active vertical_50">예상 반품배송비</th>
								<td><?=$res -> order -> sellDeliFee?></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">반품배송비</th>
								<td><?=$res -> order -> returnDeliAmt?></td>
								<th class="text-center active vertical_50">무료배송 2배 부과 여부</th>
								<td><?= ( $res -> order -> returnDeliAmtDouble == "1" ) ? "Y" : "N" ?></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">판매자ID</th>
								<td><?=$res -> order -> statusName?></td>
								<th class="text-center active vertical_50">반품/교환 연락처 </th>
								<td><?=$res -> order -> returnSellPhone ?></td>
							</tr>
							<tr>
								<th class="text-center active vertical_50">반품/교환 주소지</th>
								<td colspan="3"><?=$res -> order -> returnSellAddress?></td>
							</tr>							
							<tr>
								<th class="bg_mint text-center" style="vertical-align:middle;"> 교환/반품 신청</th>
								<td colspan="3">
									<select name="req_deny_gubun" id="req_deny_gubun" style="width:auto;height:auto;padding:5px;">
										<option value="">교환/반품 구분</option>
										<option value="RETURN_ALL">반품[전체]</option>
										<option value="CHANGE_ALL">교환[전체]</option>
										<option value="RETURN_UNIT">반품[부분]</option>
										<option value="CHANGE_UNIT">교환[부분]</option>
									</select>
									<span id="req_deny_reason_box" style="display:none;">
									<select name="req_deny_reason" id="req_deny_reason" style="width:auto;height:auto;padding:5px;">
										<option value="">반품신청사유</option>
										<option value="NOREASON">단순변심</option>
										<option value="BROKEN">제품불량/파손</option>
										<option value="OTHERITEM">판매내용과 다른 상품수령</option>
										<option value="NOARRIVAL">상품미도착</option>
									</select>
									</span>
									<span id="req_change_reason_box" style="display:none;">
									<select name="req_change_reason" id="req_change_reason" style="width:auto;height:auto;padding:5px;">
										<option value="">교환신청사유</option>
										<option value="BADCHOICE">상품/옵션 잘못선택</option>
										<option value="BROKEN">상품 불량/파손</option>
										<option value="OTHERITEM">판매내용과 다른 상품수령</option>
										<option value="SOLDOUT">품절사유로 판매자와 협의 후 교환</option>
									</select>
									</span>
									<input type="file" name="uploadAttachFiles[]" value="" class="" multiple='multiple' style="width:260px;border:0;">
									<span id="req_deny_otp_box" style="display:none;">
									<?
									$opt_none = "0";
									if ( count( $option_arr ) == 1 ) {
										$option_data = explode("|" , $option_arr[0] );
										$row_tmp_option = getdata("select oname from goods_option_data where goods_no = '" . $row["orderItemInfo"] . "' and okey = '" . $option_data[0] . "' ");
										if ( $row_tmp_option["oname"] == "" ) {
											$opt_none = "1";
										}								
									}
									if ( $opt_none == "0" ) {
									?>
									<table style="margin:10px;width:80%;">
										<?
										for ( $j = 0 ; $j < count( $option_arr ) ; $j++ ) {
											$option_data = explode("|" , $option_arr[$j] );
											$row_option = getdata("select oname from goods_option_data where goods_no = '" . $row["orderItemInfo"] . "' and okey = '" . $option_data[0] . "' ");
											if ( $row_option["oname"] != "" ) {
												echo "<tr><td><input type=\"hidden\" name=\"opt_code[]\" value=\"" . $option_data[0] . "\"> " . $row_option["oname"] . " <b style='color:#ff0000;'>" . $option_data[1] . "개</b>를 </td><td><input type=\"text\" name=\"opt_qty[]\" style=\"width:50px;\"value=\"\"> 개로 변경합니다.</td></tr>";
											}
										}										
										?>
										</table>
									<?}else {?>
									<table style="margin:10px;width:80%;">
										<?
											$option_data = explode("|" , $option_arr[0] );
											//echo "<tr><td><input type=\"hidden\" name=\"opt_code[]\" value=\"" . $option_data[0] . "\"> <b style='color:#ff0000;'>" . $option_data[1] . "개</b>를 </td><td><input type=\"text\" name=\"opt_code\" style=\"width:50px;\"value=\"\"> 개로 변경합니다.</td></tr>";
											echo "<tr><td><input type=\"hidden\" name=\"opt_code[]\" value=\"\"> <b style='color:#ff0000;'>" . $option_data[1] . "개</b>를 </td><td><input type=\"text\" name=\"opt_qty[]\" style=\"width:50px;\"value=\"\"> 개로 변경합니다.</td></tr>";
										?>
										</table>
									<?}?>
									</span>
									<input type="hidden" name="opt_none" value="<?=$opt_none?>" />
									<p style="margin-top:15px;padding:10px;border-top:1px solid #ededed;">반품교환 추가요청사항 (최대 180자)</p><p><input type="text" name="buyMemo" id="buyMemo" value="" class="" style="width:100%;" maxlength="180" /></p>
								</td>
							</tr>
							<tr>
								<td colspan="4" style="text-align:center;">
									<a href="#" class="btn_120w edit_pop" id="updateBtn" onclick="javascript:self.close();" style="margin-left:20px;"><span>닫기</span></a>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
				</div>
			</div>
		</div>		 
	</div>
	
<script type="text/javascript">
<!--
$(document).on( "change", "#req_deny_gubun" , function(){
	if ( $(this).val() == "RETURN_UNIT" || $(this).val() == "CHANGE_UNIT" ) {
		$("#req_deny_otp_box").show();
	}else {
		$("#req_deny_otp_box").hide();
	}
	if ( $(this).val() == "RETURN_ALL" || $(this).val() == "RETURN_UNIT" ) {
		$("#req_deny_reason_box").show();
	}else {
		$("#req_deny_reason_box").hide();
	}
	if ( $(this).val() == "CHANGE_ALL" || $(this).val() == "CHANGE_UNIT" ) {
		$("#req_change_reason_box").show();
	}else {
		$("#req_change_reason_box").hide();
	}
});

//-->
</script>
</body>
</html>
<?
$menu_id = "4";
$page_id = "2";
include "../include/header.php";
if ( $_GET["code"] != "" ) {
	$title_str = "수정";
	$code = mysql_real_escape_string( $_GET["code"] );
	$row = getdata(" select * from category where ccode ='$code'");
	if ( $row["ccode"] == "" ) {
		BACK__( "등록되지 않은 카테고리입니다." );
	}
	$ccode = $row["ccode"];
}else {
	//ccode 생성
	$row_code = getdata("select max( ccode ) as ccode from category ");
	if ($row_code["ccode"] == "" ) {
		$ccode = "1000";
	}else {
		$ccode = $row_code["ccode"] + 10;
	}
	$title_str = "등록";	
}
?>
			<!-- leftmneu -->
			<? include "../include/left_product.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<div id="sddr_layer" style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer" style="cursor:pointer;position:absolute;right:-3px;top:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
					<img src="//i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer1" style="cursor:pointer;position:absolute;right:-3px;bottom:-3px;z-index:1" onclick="closeDaumPostcode()" alt="닫기 버튼">
				</div>
				<!-- title -->
				<div class="titbox">
					<h2 class="title"> 카테고리 <?=$title_str?></h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="mode" value="<?= ( $title_str == "등록" ) ? "regist_category":"edit_category"?>">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="150px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>CODE</strong></th>
									<td><input type="hidden" name="ccode" value="<?=$ccode?>"><?=$ccode?></td>
								</tr>
								<tr>
									<th><strong>카테고리명</strong></th>
									<td><input type="text" name="cname" value="<?=$row["cname"]?>" class="ip1"  /></td>
								</tr>
								<tr>
									<th><strong>상태</strong></th>
									<td>
										<select name="status" style="width:200px;">
											<option value="1" <?=$row["status"] == "1" ? " selected" : ""?>>사용</option>
											<option value="0" <?=$row["status"] == "0" ? " selected" : ""?>>미사용</option>
										</select>
									</td>
								</tr>								
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="category.php" class="btn_120w"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120b" onclick="comp_submit();"><span>저장</span></a>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
				
			</div>
			<script type="text/javascript">
			<!--
				function comp_submit() {
					if ($("input[name=cname]").val() == "" ) {
						alert("카테고리명을 입력하세요.");
						return false;
					}
					$("#regi_form").submit();
					return false;
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
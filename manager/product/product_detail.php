<?
$menu_id = "4";
$page_id = "3";
include "../include/header.php"; 
$no = mysql_real_escape_string( $no );
$mode = "good_edit";
$row = getdata("select * from goods as A left join goods_detail as B on A.no = B.no where A.no ='$no' ");
if ( $row["no"] == "" ) {
	echo "<script>window.alert(' 상품정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
	exit;	
}
//echo "select * from goods as A left join goods_detail as B on A.no = B.no where A.no ='$no' ";
$cate_this = getdata(" select * from cateWe where code_id = '" . $row["cateWe"] . "'");
$cate_1 = getdata(" select * from cateWe where code_id='" . substr( $row["cateWe"] , 0 , 2 ). "_00' and depth='1'");
$cate_str = $cate_1["cname"] . " &gt; " . $cate_this["cname"];

$tax_str = str_replace( "상품" , "" , $row["tax"] );
if ( $tax_str == "과세" ) {
	$tax_str = "<span class='dev_stat_green'>과세</span>";
}elseif ( $tax_str == "면세" ) {
	$tax_str = "<span class='dev_stat_blue'>면세</span>";
}elseif ( $tax_str == "영세율" ) {
	$tax_str = "<span class='dev_stat_red'>영세율</span>";
}

$status_str = $row["status"];
if ( $status_str == "판매중" ) {
	$status_str = "<span class='dev_stat_green'>판매중</span>";
}elseif ( $status_str == "기간미지정" ) {
	$status_str = "<span class='dev_stat_blue'>기간미지정</span>";
}else {
	$status_str = "<span class='dev_stat_red'>" . $status_str . "</span>";
}

if ( $row["type"] == "" || $row["type"] == "미입력" || $row["pay"] == "무료배송" ) { //무료배송
	$delivery_str = "무료배송";
}elseif ( $row["type"] == "고정배송비" ) {
	$delivery_str = $row["pay"] . " " . number_format( $row["fee"] ) . "원";//구매자(선불,착불)선택, 무료배송, 선결제, 착불
}elseif ( $row["type"] == "수량별비례" || $row["type"] == "수량별차등" ) {
	$arr_deli_tbl = explode( "|" , $row["tbl"] );
	$arr_deli_fee = explode( "+" , $arr_deli_tbl[0] );
	$delivery_str = $row["pay"] . " " . number_format( $arr_deli_fee[1] ) . "원";
}
if ( $row["infoDutyContent"] != "" ) {
	$duty_content = explode( "|^|" , $row["infoDutyContent"] );
}
$seller_info = getdata("select * from seller where id='" . $row["seller_id"] . "' ");
$review_info = getdata("select count(*) as cnt, avg(score) as score from goods_review where no='" . $row["no"] . "' and del_ok='0' ");
$inquiry_info = getdata("select count(*) as cnt from goods_qna where no='" . $row["no"] . "' and del_ok='0' ");
$rst_review = mysql_query("select * from goods_review where no='" . $row["no"] . "' and del_ok='0' order by idx desc limit 0 , 5 ");
$rst_inquiry = mysql_query("select * from goods_qna where no='" . $row["no"] . "' and del_ok='0' order by idx desc limit 0 , 5 ");

?>
<style type="text/css" title="">
.pcontents img{max-width:100%;}
</style>
			<!-- leftmneu -->
			<? include "../include/left_product.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">상품관리</h2>
				</div>
				<!-- //title -->				
				<div class="contbox" style="width:1100px;">
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="no" value="<?=$no?>">
					<input type="hidden" name="mode" value="chgAgree">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width="326px"><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>상품코드</strong></th>
									<td><?=$row["no"]?></td>
									<th><strong>최종 업데이트</strong></th>
									<td><?=$row["lastUpdate"]?></td>
								</tr>
								<tr>
									<th><strong>카테고리</strong></th>
									<td colspan="3"><?=$cate_str?></td>
								</tr>
								<tr>
									<th><strong>상품명</strong></th>
									<td colspan="3"><b><?=$row["title"]?></b></td>
								</tr>
								<tr>
									<th rowspan="6"><strong>썸네일<br />이미지</strong></th>
									<td rowspan="6"><img src='<?=$row["thumb"]?>' border='0' alt='' style="max-width:300px;"></td>
									<th><strong>제조사</strong></th>
									<td><?=$row["manufacturer"]?></td>
								</tr>
								<tr>
									<th><strong>키워드</strong></th>
									<td><?=$row["keywords"]?></td>
								</tr>
								<tr>
									<th><strong>모델</strong></th>
									<td><?=$row["model"]?></td>
								</tr>
								<tr>
									<th><strong>사이즈</strong></th>
									<td><?=$row["size"]?></td>
								</tr>
								<tr>
									<th><strong>무게</strong></th>
									<td><?=$row["weight"]?></td>
								</tr>
								<tr>
									<th><strong>원산지</strong></th>
									<td><?=$row["country"]?></td>
								</tr>
								<tr>
									<th><strong>판매가</strong></th>
									<td><b><?=number_format( $row["priceWe"] )?>원</b></td>
									<th><strong>공급가</strong></th>
									<td><?=number_format( $row["price"] )?>원</td>
								</tr>
								<tr>
									<th><strong>재고</strong></th>
									<td><?=$row["qtyInventory"]?></td>
									<th><strong>과세여부</strong></th>
									<td><?=$tax_str?></td>
								</tr>
								<tr>
									<th><strong>판매기간</strong></th>
									<td><b><?=$row["dateStart"]?> ~ <?=$row["dateEnd"]?></b></td>
									<th><strong>상태</strong></th>
									<td><?=$status_str?></td>
								</tr>
								<tr>
									<th><strong>BEST 노출</strong></th>
									<td><?=( $row["best_name"] != "" ) ? $row["best_name"] : "N" ?></td>
									<th><strong>묶음배송 노출</strong></th>
									<td><?=( $row["packed"] == "1" ) ? "Y" : "N" ?></td>
								</tr>
								<tr>
									<th><strong>땡처리 노출</strong></th>
									<td><?=( $row["ttang"] == "1" ) ? "Y" : "N" ?></td>
									<th><strong>해외직배송 노출</strong></th>
									<td><?=( $row["fromOversea"] == "1" ) ? "Y" : "N" ?></td>
								</tr>
								<tr>
									<th><strong>배송비 부담주체</strong></th>
									<td><?=$row["pay"]?></td>
									<th><strong>배송비 책정방식</strong></th>
									<td><?=$row["type"]?></td>
								</tr>
								<tr>
									<th><strong>배송방법</strong></th>
									<td><?=$row["method"] ?></td>
									<th><strong>해외배송가능여부</strong></th>
									<td><?=( $row["oversea"] == "1" ) ? "Y" : "N" ?></td>
								</tr>
								<tr>
									<th><strong>배송 금액</strong></th>
									<td><?=$delivery_str?></td>
									<th><strong>반품 배송비</strong></th>
									<td><?=number_format( $row["return_deliAmt"] )?></td>
								</tr>
								<tr>
									<th><strong>배송비 추가(제주)</strong></th>
									<td><?=number_format( $row["jeju"] )?></td>
									<th><strong>배송비 추가(기타)</strong></th>
									<td><?=number_format( $row["islands"] )?></td>
								</tr>
								<tr>
									<th><strong>빠른배송 상품 여부</strong></th>
									<td><?=( $row["fastDeli"] == "1" ) ? "Y" : "N" ?></td>
									<th><strong>평균발송일</strong></th>
									<td><?=$row["sendAvg"]?></td>
								</tr>
								<tr>
									<th><strong>옵션보기</strong></th>
									<td colspan="3"><a title="삭제" class="btn_70" onclick="popOption('<?=$row["no"]?>');"><span>옵션보기</span></a></td>
								</tr>
								<tr>
									<th><strong>상품설명</strong></th>
									<td colspan="3" class="td_editor" style="text-align:center;">
									<div style='width:97%;height:500px;overflow-y:scroll;border:1px solid #ddd;' class="pcontents"><?=$row["contents"]?></div>
									</td>
								</tr>
								<? if ( $row["contentsRenewal"] != "" ) { ?>
								<tr>
									<th><strong>상품설명<br />(보정이미지)</strong></th>
									<td colspan="3" class="td_editor">
										<textarea name="contentsRenewal" class="summernote" id="goods_contentsRenewal" ><?=$row["contentsRenewal"]?></textarea>
									</td>
								</tr>
								<?}?>
								<tr>
									<th><strong>라이센스</strong></th>
									<td><?=$row["descLicenseMsg"]?></td>
									<th><strong>KC인증면제정보여부</strong></th>
									<td><?=$row["safetyCert_exem"]?></td>
								</tr>
								<tr>
									<th><strong>상품정보제공고시<br />품목명 : <?=$row["infoDutyType"]?></strong></th>
									<td colspan="3">									
									<? if ( strpos( $row["infoDutyContent"] , "item|@|" ) !== false ) {?>
									<h3 style="padding:15px 0 5px 10px">전자상거래 등에서의 상품정보제공고시</h3>
									<div class="group info">
										<table cellspacing="0" cellpadding="0">
											<colgroup><col width="33%"><col width=""></colgroup>
											<? for ( $i = 0 ; $i < count( $duty_content ) ; $i++ ) {
												$duty_item = explode( "|@|" , $duty_content[$i] );
												if ( $duty_item[0] == "item" ) {
													echo "<tr><th style='padding-left:10px;border-top:1px solid #ddd;'>" . $duty_item[1] . "</th><td>" . $duty_item[2] . "</td></tr>";
												}
											}
											?>
										</table>
									</div>
									<?}?>
									<? if ( strpos( $row["infoDutyContent"] , "transaction|@|" ) !== false ) {?>
									<h3 style="padding:15px 0 5px 10px">거래조건에 관한 정보</h3>
									<div class="group info">
										<table cellspacing="0" cellpadding="0">
											<colgroup><col width="33%"><col width=""></colgroup>
											<? for ( $i = 0 ; $i < count( $duty_content ) ; $i++ ) {
												$duty_item = explode( "|@|" , $duty_content[$i] );
												if ( $duty_item[0] == "transaction" ) {
													echo "<tr><th style='padding-left:10px;border-top:1px solid #ddd;'>" . $duty_item[1] . "</th><td>" . $duty_item[2] . "</td></tr>";
												}
											}
											?>
										</table>
									</div>
									<?}?>
									</td>
								</tr>
								<tr>
									<th><strong>공급사명</strong></th>
									<td><?=$seller_info["nick"]?></td>
									<th><strong>공급사 랭크</strong></th>
									<td><?=$seller_info["rank"]?></td>
								</tr>
								<tr>
									<th><strong>공급사 우수판매자 여부</strong></th>
									<td><?= ( $seller_info["good"] == "1" ) ? "Y" : "N" ?></td>
									<th><strong>공급사 사업자유형</strong></th>
									<td><?=$seller_info["type"]?></td>
								</tr>
								<tr>
									<th><strong>공급사 사업자정보</strong></th>
									<td><?=$seller_info["company_name"]?> (대표 : <?=$seller_info["company_boss"]?>)</td>
									<th><strong>공급사 사업자번호</strong></th>
									<td><?=$seller_info["company_cno"]?></td>
								</tr>
								<tr>
									<th><strong>공급사 연락처</strong></th>
									<td><?=$seller_info["company_phone"]?></td>
									<th><strong>공급사 사업자번호</strong></th>
									<td><?=$seller_info["company_email"]?></td>
								</tr>
								<tr>
									<th><strong>공급사 사업자주소</strong></th>
									<td colspan="3"><?=$seller_info["company_addr"]?></td>
								</tr>
								<tr>
									<th><strong>노출승인</strong></th>
									<td colspan="3">
										<select name="adminAgree" id="adminAgree" style="width:100px;">
											<option value="1" <?= ( $row["adminAgree"] == "1" ) ? "selected" : "" ?>>노출</option>
											<option value="0" <?= ( $row["adminAgree"] == "0" ) ? "selected" : "" ?>>중단</option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>	
						</div>
					</div>
					<!-- //button -->
				</div>				
			</div>
		
			<script>
				$(function () {
					var f = $(document.forms["regi_form"]);
					$('a.goback').click(function () {
						history.back();
						return false;
					});
				});
				function popOption( no ) {
					window.open("./pop_option.php?no=" + no , "pop_option" , "width=1000, height=800, top=0, left=0,scrollbars=1" );
				}
				
				$('#updateBtn').click(function (){
					if ($('#adminAgree option:selected').val() == '') {
						alert('상태를 선택하세요.');
						return false;
					}
					document.regi_form.submit();
				});
				
			</script>
			
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
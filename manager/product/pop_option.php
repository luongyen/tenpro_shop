<?
include ("../config.php");
if( $_SESSION['yi_idx'] == "" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}
$no = mysql_real_escape_string( $_GET["no"] );
$goods_info = getdata("select * from goods where no= '" . $no . "' ");
$rst = mysql_query("select * from goods_option_data where goods_no= '" . $no . "' order by okey asc ");
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>[<?=$goods_info["title"]?>] 옵션 전체보기</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">

	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">
	<div class="popup_box">
		<div class="titbox">
			<p class="t">[<?=$goods_info["no"]?>][<?=$goods_info["title"]?>] 옵션 전체보기</p>
		</div>
		<div class="popbody">
			<div style="padding-top:20px;">
				<div class="table_typeA">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col width="100px"><col width=""><col width="100px"><col width="100px"><col width="80px"><col width="80px"></colgroup>
						<thead>
							<tr>
								<th class="text-center active vertical_50">코드</th>
								<th class="text-center active vertical_50">옵션내용</th>
								<th class="text-center active vertical_50">추가금액(공급)</th>
								<th class="text-center active vertical_50">추가금액(판매)</th>
								<th class="text-center active vertical_50">재고</th>
								<th class="text-center active vertical_50">노출</th>
							</tr>
						</thead>
						<tbody>
						<?
						while ( $row = mysql_fetch_array( $rst ) ) {
							if ( $row["hid"] == "0" ) {
								$hid_str = "<span class='dev_stat_green'>노출</span>";
							}elseif ( $row["hid"] == "1" ) {
								$hid_str = "<span class='dev_stat_gray'>품절</span>";
							}elseif ( $row["hid"] == "2" ) {
								$hid_str = "<span class='dev_stat_gray'>숨김</span>";
							}
						?>
							<tr>
								<td><?=$row["okey"]?></td>
								<td style="text-align:left;"><?=$row["oname"]?></td>
								<td><?= number_format( $row["supPrice"] ) ?></td>
								<td><?= number_format( $row["priceWe"] ) ?></td>
								<td><?= number_format( $row["qty"] ) ?></td>
								<td><?=$hid_str?></td>
							</tr>
						<?}?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="btn_box m20">
				<div style="text-align:center;" >
					<a href="#" class="btn_120b" onclick="javascript:self.close();"><span>닫기</span></a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
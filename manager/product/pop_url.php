<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	echo "로그인 이후 사용하시기 바랍니다.";
	exit;
}

include "../../lib/googleUrlApi.php";
$gcode = mysql_real_escape_string( $_GET["gcode"] );
$row = getdata("select * from goods where gcode= '" . $gcode . "' ");
//$googer = new GoogleURLAPI($gkey);
$share_url_0 = "http://shockingdiscount.co.kr/order/order.php?gcode=".$gcode;
//$short_url_0 = $googer->shorten($share_url_0);
$share_url_1 = "http://shockingdiscount.co.kr/order/order.php?channel=hotdeal_1&gcode=".$gcode;
//$short_url_1 = $googer->shorten($share_url_1);
$share_url_2 = "http://shockingdiscount.co.kr/order/order.php?channel=hotdeal_2&gcode=".$gcode;
//$short_url_2 = $googer->shorten($share_url_2);
$share_url_3 = "http://shockingdiscount.co.kr/order/order.php?channel=hotdeal_3&gcode=".$gcode;
//$short_url_3 = $googer->shorten($share_url_3);
$share_url_4 = "http://shockingdiscount.co.kr/order/order.php?channel=hotdeal_4&gcode=".$gcode;
//$short_url_4 = $googer->shorten($share_url_4);


$short_url_0 = "http://shockingdiscount.co.kr/?c=".$gcode;
$short_url_1 = "http://shockingdiscount.co.kr/?channel=hotdeal_1&c=".$gcode;
$short_url_2 = "http://shockingdiscount.co.kr/?channel=hotdeal_2&c=".$gcode;
$short_url_3 = "http://shockingdiscount.co.kr/?channel=hotdeal_3&c=".$gcode;
$short_url_4 = "http://shockingdiscount.co.kr/?channel=hotdeal_4&c=".$gcode;
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>단축 URL</title>

	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">

	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">
	<div class="popup_box">
		<div class="titbox">
			<p class="t"><?=$row["pname"]?> 단축 URL 목록</p>
		</div>
		<div class="popbody">
			<div style="padding-top:20px;">
				<div class="table_typeA">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col width=""><col width=""><col width=""></colgroup>
						<thead>
							<tr>
								<th class="text-center active vertical_50">채널명</th>
								<th class="text-center active vertical_50">URL</th>
								<th class="text-center active vertical_50">단축 URL</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong>기본</strong></td>
								<td><strong><a href="<?=$share_url_0?>" target="_blank"><?=$share_url_0?></a></strong></td>
								<td><strong><a href="<?=$short_url_0?>" target="_blank"><?=$short_url_0?></a></strong></td>
							</tr>
							<tr>
								<td><strong>공동구매핫딜</strong></td>
								<td><strong><a href="<?=$share_url_1?>" target="_blank"><?=$share_url_1?></a></strong></td>
								<td><strong><a href="<?=$short_url_1?>" target="_blank"><?=$short_url_1?></a></strong></td>
							</tr>
							<tr>
								<td><strong>육아홀릭쇼핑홀릭</strong></td>
								<td><strong><a href="<?=$share_url_2?>" target="_blank"><?=$share_url_2?></a></strong></td>
								<td><strong><a href="<?=$short_url_2?>" target="_blank"><?=$short_url_2?></a></strong></td>
							</tr>
							<tr>
								<td><strong>긴급땡처리</strong></td>
								<td><strong><a href="<?=$share_url_3?>" target="_blank"><?=$share_url_3?></a></strong></td>
								<td><strong><a href="<?=$short_url_3?>" target="_blank"><?=$short_url_3?></a></strong></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="btn_box m20">
				<div style="text-align:center;" >
					<a href="#" class="btn_120b" onclick="javascript:self.close();"><span>닫기</span></a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
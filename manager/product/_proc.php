<?
include ("../config.php");
include "../../lib/googleUrlApi.php";
if( $_SESSION['yi_idx'] == "" ) {
	echo "<script>window.alert('관리자 로그인 후에 이용이 가능합니다.');top.location.href='/admin/';</script>";
	exit;
}
if ( $mode == "chg_notify" ) {
	$val = mysql_real_escape_string( $_POST["val"] );
	$ori = mysql_real_escape_string( $_POST["ori"] );
	$gcode = mysql_real_escape_string( $_POST["gcode"] );
	$rst_notify = mysql_query(" select * from goods_notify where group_id='$val' order by idx asc");
	$tbl = "<table class=\"table table-bordered\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:10px;max-width:750px;\">";
	while ( $row = mysql_fetch_array( $rst_notify ) ) {
		$notify_val = "";
		$comment = "";
		if ( $val == $ori ) { // 기존내용 불러오기
			$row_ori_data = getdata( " select * from goods_notify_data where gcode='$gcode' and notify_id='".$row["idx"]."'" );
			$notify_val = $row_ori_data["notify_val"];
		}else {
			$notify_val = "상품상세내용 참조";
		}
		if ($row["notify_comment"] != "" ) {
			$comment = "<br />(" . $row["notify_comment"] . ")";
		}
		$tbl .= "<tr><th style=\"padding-left:10px;word-break:break-all;\">".$row["notify_subject"]."</th><td><input type=\"hidden\" name=\"notify_id[]\" value=\"" . $row["idx"] . "\"><input type=\"text\" name=\"notify_val[]\" value=\"" . $notify_val . "\" style=\"width:98%;\">$comment</td></tr>";
	}
	$tbl .= "</table>";
	echo $tbl;
	exit;
}

if ( $mode == "sel_cate" ) {
	$dep = mysql_real_escape_string( $_POST["dep"] );
	$cate = mysql_real_escape_string( $_POST["val"] );
	$i = 0;
	$sql = "select * from cateWe where depth='2' and code_id like '" . $cate . "%'  order by code_id ";
	$rst = mysql_query( $sql );

	$content = "<select name=\"cate_" . $dep . "\" id=\"cate_" . $dep . "\" style=\"width:auto;\">";
	$content .= "<option value=\"\">전체</option>";
	while ( $row = mysql_fetch_array( $rst ) ) {
		$content .= "<option value=\"" . $row["code_id"] . "\">" . $row["cname"] ."</option>";
		$i++;
	}
	$content .= "</select>";
	if ( $i > 0 ) {
		$data["rst"] = "1";
	}else {
		$data["rst"] = "0";
	}
	$data["content"] = $content;
	$data["sql"] = $sql;
	echo json_encode($data);
	exit;
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
<?
if ( $mode == "delivery_regist" ) {
	$gcode = mysql_real_escape_string( $_POST["gcode"] );
	$cnt_s = mysql_real_escape_string( $_POST["cnt_s_in"] );
	$cnt_e = mysql_real_escape_string( $_POST["cnt_e_in"] );
	$price = mysql_real_escape_string( $_POST["price_in"] );

	$sql_set = " set gcode = '$gcode' , cnt_s = '$cnt_s' , cnt_e = '$cnt_e' , price = '$price' ";
	$query = mysql_query("insert into goods_delivery_setting $sql_set ");
	$msg = "저장";
}

if ( $mode == "delivery_update" ) {
	$gcode = mysql_real_escape_string( $_POST["gcode"] );
	$gid = mysql_real_escape_string( $_POST["gid"] );
	$idx_arr = explode( "," , $gid );
	for ( $i = 0 ; $i < count( $idx_arr ) ; $i++ ) {
		$cnt_s = mysql_real_escape_string( $_POST["cnt_s_" . $idx_arr[$i] ] );
		$cnt_e = mysql_real_escape_string( $_POST["cnt_e_" . $idx_arr[$i] ] );
		$price = mysql_real_escape_string( $_POST["price_" . $idx_arr[$i] ] );
		$sql_set = " set cnt_s = '$cnt_s' , cnt_e = '$cnt_e' , price = '$price' ";
		$query = mysql_query("update goods_delivery_setting $sql_set where idx='" . $idx_arr[$i] . "' and gcode = '$gcode'");
	}
	$msg = "수정";
}

if ( $mode == "delivery_del" ) {
	$gcode = mysql_real_escape_string( $_POST["gcode"] );
	$gid = mysql_real_escape_string( $_POST["gid"] );
	$idx_arr = explode( "," , $gid );
	for ( $i = 0 ; $i < count( $idx_arr ) ; $i++ ) {
		$query = mysql_query("delete from goods_delivery_setting where idx='" . $idx_arr[$i] . "' and gcode = '$gcode'");
	}
	$msg = "삭제";
}

if ( $mode == "goods_delete" ) {
	$gcode = mysql_real_escape_string( $_POST["gcode"] );
	$query = mysql_query("update goods set del_ok='1' where gcode = '$gcode'");
	$msg = "삭제";
	$re_url = "./product_list.php";
}

if ($mode == "good_regist" ) {	
	//gcode 생성
	$goods = getdata("select max(code) as c from goods_code");
	$gcode = strtoupper( dechex( $goods["c"] + 1 ) );
	$gidx = (int)$goods["c"] + 1;
	$sql = " update goods_code set code='$gidx'";
	mysql_query( $sql );

	$googer = new GoogleURLAPI($gkey);
	$share_url = "http://shockingdiscount.co.kr/order/order.php?gcode=".$gcode;
	$short_url = $googer->shorten($share_url);

	$gid = mysql_real_escape_string($_POST["gid"]);
	$pay1 = mysql_real_escape_string($_POST["pay1"]);
	$pay2 = mysql_real_escape_string($_POST["pay2"]);
	$pay3 = mysql_real_escape_string($_POST["pay3"]);
	$category = mysql_real_escape_string($_POST["category"]);
	
	$pname = mysql_real_escape_string($_POST["pname"]);
	$inventory = mysql_real_escape_string($_POST["inventory"]);
	$orign = mysql_real_escape_string($_POST["orign"]);
	$brand = mysql_real_escape_string($_POST["brand"]);
	$prov_id = mysql_real_escape_string($_POST["prov_id"]);
	$prov_email = mysql_real_escape_string($_POST["prov_email"]);
	$price_supply = mysql_real_escape_string($_POST["price_supply"]);
	$price_sales = mysql_real_escape_string($_POST["price_sales"]);
	$price_basic = mysql_real_escape_string($_POST["price_basic"]);
	$price_member = mysql_real_escape_string($_POST["price_member"]);

	$hotsale = mysql_real_escape_string($_POST["hotsale"]);
	$tax = mysql_real_escape_string($_POST["tax"]);
	$notify = mysql_real_escape_string($_POST["notify"]);

	$per_0 = mysql_real_escape_string($_POST["per_0"]);
	$per_1 = mysql_real_escape_string($_POST["per_1"]);
	$per_2 = mysql_real_escape_string($_POST["per_2"]);
	
	$won_0 = mysql_real_escape_string($_POST["won_0"]);
	$won_1 = mysql_real_escape_string($_POST["won_1"]);
	$won_2 = mysql_real_escape_string($_POST["won_2"]);
	$share_point = mysql_real_escape_string($_POST["share_point"]);

	$sdate = mysql_real_escape_string($_POST["sdate"]);
	$stime = mysql_real_escape_string($_POST["stime"]);
	$smin = mysql_real_escape_string($_POST["smin"]);
	$edate = mysql_real_escape_string($_POST["edate"]);
	$etime = mysql_real_escape_string($_POST["etime"]);
	$emin = mysql_real_escape_string($_POST["emin"]);

	$content = $_POST["content"];

	$delivery_yn = mysql_real_escape_string($_POST["delivery_yn"]);
	$delivery_pay_default = mysql_real_escape_string($_POST["delivery_pay_default"]); //기본 배송료
	$delivery_pay_free = mysql_real_escape_string($_POST["delivery_pay_free"]); //기본 무료배송
	$delivery_default = mysql_real_escape_string($_POST["delivery_default"]); // 개별 배송료
	$delivery_free = mysql_real_escape_string($_POST["delivery_free"]); // 개별 무료배송

	$stuats = mysql_real_escape_string($_POST["stuats"]);
	/*
	$short_url = mysql_real_escape_string($_POST["short_url"]);
	$short_qna = mysql_real_escape_string($_POST["short_qna"]);
	$short_deli = mysql_real_escape_string($_POST["short_deli"]);
	$short_shop = mysql_real_escape_string($_POST["short_shop"]);
	*/
	$option_inven = mysql_real_escape_string($_POST["option_inven"]);
	$optionTitle1 = mysql_real_escape_string($_POST["optionTitle1"]);
	$optionTitle2 = mysql_real_escape_string($_POST["optionTitle2"]);
	$optionTitle3 = mysql_real_escape_string($_POST["optionTitle3"]);
	$optionTitle4 = mysql_real_escape_string($_POST["optionTitle4"]);
	$invenShow = mysql_real_escape_string($_POST["invenShow"]);
	$priceState = mysql_real_escape_string($_POST["priceState"]);
	$priceState_mem = mysql_real_escape_string($_POST["priceState_mem"]);
	$option_user = mysql_real_escape_string($_POST["option_user"]);
	$option_value1 = $_POST["option_value1"];
	$option_value2 = $_POST["option_value2"];
	$option_value3 = $_POST["option_value3"];
	$option_value4 = $_POST["option_value4"];
	$option_normal = mysql_real_escape_string($_POST["option_normal"]);
	$option_normal_all = mysql_real_escape_string($_POST["option_normal_all"]);
	$inven_count = $_POST["inven_count"];
	$option_user_value = $_POST["option_user_value"];
	$option_user_value2 = $_POST["option_user_value2"];
	$sec = mysql_real_escape_string ( $_POST["sec"] );
	$view_yn = $_POST["view_yn"];

	if ( $pay1 == "" ) { $pay1 = "0"; }
	if ( $pay2 == "" ) { $pay2 = "0"; }
	if ( $pay3 == "" ) { $pay3 = "0"; }
	if ( $view_yn == "" ) { $view_yn = "0"; }
	if ( $view_main == "" ) { $view_main = "0"; }
	$sql_append = "";
	
	$provide = getdata("select v_num from provide where p_name='$prov_id' ");
	
	if($_FILES["thumb"]["name"]!="") {
		$rst_upload = SSH_UPLOAD_THUMB( $_FILES["thumb"] );
		if ( $rst_upload == false ) {
			$sql_append = "";
		}else {
			$sql_append = ", files='".$rst_upload."'";
		}
	}

	//배송료
	if ($delivery_yn == "1" ) { //기본배송료 선택시
		$d_pay = $delivery_pay_default;
		$d_free = $delivery_pay_free;
	}else {
		$d_pay = $delivery_default;
		$d_free = $delivery_free;		
	}

	//상품정보고시
	if ( $notify != "" ) {
		for ( $i = 0 ; $i < count( $_POST["notify_id"] ) ; $i++ ) {			
			$sql_notify = "INSERT INTO goods_notify_data SET 
			gcode = '" . $gcode . "' ,
			notify_id = '" . mysql_real_escape_string( $_POST["notify_id"][$i] ) . "' ,
			notify_val = '" . mysql_real_escape_string( $_POST["notify_val"][$i] ) . "' ";
			mysql_query( $sql_notify );
		}
	}

	//재고옵션
	if ( $option_inven == "1" ) {
		 $use_option1 = false;
		 $use_option2 = false;
		 $use_option3 = false;
		 $use_option4 = false;
		$sql_append .= ", optionTitle1='".$optionTitle1."' ,  optionTitle2='".$optionTitle2."' ,  optionTitle3='".$optionTitle3."' ,  optionTitle4='".$optionTitle4."' ,	invenShow = '" . $invenShow . "' , priceState = '" . $priceState . "' ";
		if ( $optionTitle1 != "" ) { $use_option1 = true; }
		if ( $optionTitle2 != "" ) { $use_option2 = true; }
		if ( $optionTitle3 != "" ) { $use_option3 = true; }
		if ( $optionTitle4 != "" ) { $use_option4 = true; }
		//echo $use_option1 . "...".$use_option2 . "...".$use_option3 . "...".$use_option4 ;
		for ( $i = 0 ; $i < count( $option_value1 ) ; $i++ ) {
			if ($use_option1 == false && $use_option2 == false && $use_option3 == false && $use_option4 == false ) {
				continue;			
			}else {
				$sql_append_option_inven = "";
				if ( $inven_count[$i] != ""  && ( $sales_price[$i] != "" ||  $sales_price_mem[$i] != "" ) ) {
					if ( $use_option1 == true && $option_value1[$i] != "" ) { //1번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value1 = '" . $option_value1[$i] . "'";
					}else if ($use_option1 == true && $option_value1[$i] == "") {
						continue;
					}
					if ( $use_option2 == true && $option_value2[$i] != "" ) { //2번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value2 = '" . $option_value2[$i] . "'";
					}else if ($use_option2 == true && $option_value2[$i] == "") {
						continue;
					}
					if ( $use_option3 == true && $option_value3[$i] != "" ) { //3번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value3 = '" . $option_value3[$i] . "'";
					}else if ($use_option3 == true && $option_value3[$i] == "") {
						continue;
					}
					if ( $use_option4 == true && $option_value4[$i] != "" ) { //4번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value4 = '" . $option_value4[$i] . "'";
					}else if ($use_option4 == true && $option_value4[$i] == "") {
						continue;
					}
					if ($basic_price[$i]=="") {
						$basic_price[$i] = 0;
					}
					$sql_option_inven = "INSERT INTO goods_option_inven SET 
					gcode = '" . $gcode . "' ,
					inven_count = '" . str_replace( "," , "" , $inven_count[$i] ) . "' ,
					basic_price = '" . str_replace( "," , "" , $basic_price[$i] ) . "' ,
					sales_price = '" . str_replace( "," , "" , $sales_price[$i] ) . "' ,
					sales_price_mem = '" . str_replace( "," , "" , $sales_price_mem[$i] ) . "' 
					" . $sql_append_option_inven . "";
					mysql_query( $sql_option_inven );
					//echo $sql_option_inven;
				}else {
					continue;
				}				
			}
		}
	}
	
	//일반옵션
	if ( $option_normal == "1" ) {
		$on_list1 = explode( "|_|" , $option_normal_all );
		if ( count( $on_list1 ) > 0 ) {
		//echo count( $on_list1 );
			for ( $on_list_i = 1 ; $on_list_i < count( $on_list1 ) ; $on_list_i++ ) {
				if ($on_list1[$on_list_i] != "" ) {
					$on_list2 = explode("|^|" , $on_list1[$on_list_i] );
					if ( count( $on_list2 ) > 0 ) {
						for ( $on_list2_i = 1 ; $on_list2_i < count( $on_list2 ) ; $on_list2_i++ ) {
							if ($on_list2[$on_list2_i] != "" ) {
								$on_list3 = explode("|:|" , $on_list2[$on_list2_i] );
								if ( count( $on_list3 ) > 0 ) {
									if ( $on_list3[1] != "" && $on_list3[0] != "undefined" ) {	
										
										$on_list4 = explode("|||" , $on_list3[1] );
										if ( count( $on_list4 ) > 0 ) {
											if ( $on_list4[0] != "" && $on_list4[1] != "" && $on_list4[0] != "undefined" ) {	
												$on_list5 = explode("|@|" , $on_list4[1] );
												if ( count( $on_list5 ) > 0 ) {
													if ( $on_list5[0] != "" && $on_list5[1] != "" && $on_list5[0] != "undefined" ) {	
														$on_list6 = explode("|-|" , $on_list5[1] );
														if ( count( $on_list6 ) > 0 ) {
															if ( $on_list6[0] != "" && $on_list6[1] != "" && $on_list6[0] != "undefined" ) {
																$sql_option_normal = "INSERT INTO goods_option_normal SET 
																gcode = '" . $gcode . "' ,
																option_name = '" . $on_list2[0] . "' ,
																option_value1 = '" . $on_list3[0] . "' ,
																option_value2 = '" . $on_list4[0] . "' ,
																option_value3 = '" . $on_list5[1] . "' ,
																option_value4 = '" . $on_list6[1] . "' ,
																cnt = '" . $on_list5[0] . "'";
																mysql_query( $sql_option_normal );
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}			
					}
				}
			}
		}
	}

	//사용자옵션
	if ( $option_user == "1" ) {
		for ( $i = 0 ; $i < count( $option_user_value ) ; $i++ ) {
			if ($option_user_value[$i] != "" && $option_user_value2[$i] != "" ) {				
				$sql_option_user = "INSERT INTO goods_option_user SET 
				gcode = '" . $gcode . "' ,
				option_value1 = '" . $option_user_value[$i] . "' ,
				option_value2 = '" . $option_user_value2[$i] . "'";
				mysql_query( $sql_option_user );	
			}else {
				continue;		
			}
		}
	}

	$option_inven = ( $option_inven == "" ) ? "0" : $option_inven;
	$option_normal = ( $option_normal == "" ) ? "0" : $option_normal;
	$option_user = ( $option_user == "" ) ? "0" : $option_user;

	$sql = "INSERT INTO goods SET
	gcode = '" . $gcode . "' ,  
	id = '" . $_SESSION['yi_id'] . "' ,  
	pname = '" . $pname . "' ,  
	short_url = '" . $short_url . "' ,  
	short_qna = '" . $short_qna . "' ,  
	short_deli = '" . $short_deli . "' ,  
	short_shop = '" . $short_shop . "' ,  
	price_supply = '" . $price_supply . "' ,  
	price_sales = '" . $price_sales . "' ,  
	price_basic = '" . $price_basic . "' ,  
	price_member = '" . $price_member . "' ,
	inventory = '" . $inventory . "' ,  
	orign = '" . $orign . "' ,  
	brand = '" . $brand . "' ,  
	prov_id = '" . $prov_id . "' ,  
	prov_num = '" . $provide["v_num"] . "' ,  
	prov_email = '" . $prov_email . "' ,  
	delivery_default = '" . $d_pay . "' ,  
	delivery_free = '" . $d_free . "' , 
	category = '" . $category . "' ,   
	delivery_yn = '" . $delivery_yn . "' ,   
	view_yn = '" . $view_yn . "' ,  
	sec = '" . $sec . "' ,  
	tax = '" . $tax . "' ,  
	notify = '" . $notify . "' ,  
	hotsale = '" . $hotsale . "' ,  
	pay1 = '" . $pay1 . "' ,  
	pay2 = '" . $pay2 . "' ,  
	pay3 = '" . $pay3 . "' ,  
	per_0 = '" . $per_0 . "' ,  
	per_1 = '" . $per_1 . "' ,  
	per_2 = '" . $per_2 . "' ,  
	won_0 = '" . $won_0 . "' ,  
	won_1 = '" . $won_1 . "' ,  
	won_2 = '" . $won_2 . "' ,  
	share_point = '" . $share_point . "' ,  
	sdate = '" . $sdate . "' ,  
	stime = '" . $stime . "' ,  
	smin = '" . $smin . "' ,  
	edate = '" . $edate . "' ,  
	etime = '" . $etime . "' ,  
	emin = '" . $emin . "' ,  
	start_date = '" . $sdate . " " . $stime . ":" . $smin . ":00' , 
	end_date = '" . $edate . " " . $etime . ":" . $emin . ":59' , 
	view_range = '" . $gid . "' ,
	option_inven = '" . $option_inven . "' ,  
	option_user = '" . $option_user . "' ,  
	option_normal = '" . $option_normal . "' ,  
	content = '" . $content . "' ,  
	status = '" . $status . "' ,  
	reg_date = '" . date("Y-m-d H:i:s") . "' 
	" . $sql_append . "";
	$query = mysql_query( $sql );
	@mysql_query("INSERT INTO `query_history` set gubun='goods', q='".str_replace( "'" , "" , $sql )."', reg_date='".date("Y-m-d H:i:s")."' ");
	//exit;
	if ($query) {		
		$redirect_url = "./product_detail.php?gcode=$gcode";
		//print_r($_POST);
		//echo $sql;
		PAGE_MOVE__("등록되었습니다." , $redirect_url);
	}else {
		$redirect_url = "./product_detail.php";
		PAGE_MOVE__("등록도중 오류가 발생하였습니다. 다시 등록해주시기 바랍니다." , $redirect_url);
	}
	exit;
}

if ($mode == "good_edit" ) {	

	$gcode = mysql_real_escape_string($_POST["gcode"]);
	$gid = mysql_real_escape_string($_POST["gid"]);
	$pay1 = mysql_real_escape_string($_POST["pay1"]);
	$pay2 = mysql_real_escape_string($_POST["pay2"]);
	$pay3 = mysql_real_escape_string($_POST["pay3"]);
	$category = mysql_real_escape_string($_POST["category"]);
	
	$pname = mysql_real_escape_string($_POST["pname"]);
	$inventory = mysql_real_escape_string($_POST["inventory"]);
	$orign = mysql_real_escape_string($_POST["orign"]);
	$brand = mysql_real_escape_string($_POST["brand"]);
	$prov_id = mysql_real_escape_string($_POST["prov_id"]);
	$prov_email = mysql_real_escape_string($_POST["prov_email"]);
	$price_supply = mysql_real_escape_string($_POST["price_supply"]);
	$price_sales = mysql_real_escape_string($_POST["price_sales"]);
	$price_basic = mysql_real_escape_string($_POST["price_basic"]);
	$price_member = mysql_real_escape_string($_POST["price_member"]);
	
	$hotsale = mysql_real_escape_string($_POST["hotsale"]);
	$tax = mysql_real_escape_string($_POST["tax"]);
	$notify = mysql_real_escape_string($_POST["notify"]);

	$per_0 = mysql_real_escape_string($_POST["per_0"]);
	$per_1 = mysql_real_escape_string($_POST["per_1"]);
	$per_2 = mysql_real_escape_string($_POST["per_2"]);

	$won_0 = mysql_real_escape_string($_POST["won_0"]);
	$won_1 = mysql_real_escape_string($_POST["won_1"]);
	$won_2 = mysql_real_escape_string($_POST["won_2"]);

	$share_point = mysql_real_escape_string($_POST["share_point"]);
	
	$sdate = mysql_real_escape_string($_POST["sdate"]);
	$stime = mysql_real_escape_string($_POST["stime"]);
	$smin = mysql_real_escape_string($_POST["smin"]);
	$edate = mysql_real_escape_string($_POST["edate"]);
	$etime = mysql_real_escape_string($_POST["etime"]);
	$emin = mysql_real_escape_string($_POST["emin"]);

	$content = $_POST["content"];

	$delivery_yn = mysql_real_escape_string($_POST["delivery_yn"]);
	$delivery_pay_default = mysql_real_escape_string($_POST["delivery_pay_default"]); //기본 배송료
	$delivery_pay_free = mysql_real_escape_string($_POST["delivery_pay_free"]); //기본 무료배송
	$delivery_default = mysql_real_escape_string($_POST["delivery_default"]); // 개별 배송료
	$delivery_free = mysql_real_escape_string($_POST["delivery_free"]); // 개별 무료배송

	//배송료
	if ($delivery_yn == "1" ) { //기본배송료 선택시
		$d_pay = $delivery_pay_default;
		$d_free = $delivery_pay_free;
	}else {
		$d_pay = $delivery_default;
		$d_free = $delivery_free;		
	}

	$stuats = mysql_real_escape_string($_POST["stuats"]);


	$option_inven = mysql_real_escape_string($_POST["option_inven"]);
	$optionTitle1 = mysql_real_escape_string($_POST["optionTitle1"]);
	$optionTitle2 = mysql_real_escape_string($_POST["optionTitle2"]);
	$optionTitle3 = mysql_real_escape_string($_POST["optionTitle3"]);
	$optionTitle4 = mysql_real_escape_string($_POST["optionTitle4"]);
	$invenShow = mysql_real_escape_string($_POST["invenShow"]);
	$priceState = mysql_real_escape_string($_POST["priceState"]);
	$option_user = mysql_real_escape_string($_POST["option_user"]);
	$option_value1 = $_POST["option_value1"];
	$option_value2 = $_POST["option_value2"];
	$option_value3 = $_POST["option_value3"];
	$option_value4 = $_POST["option_value4"];
	$option_normal = mysql_real_escape_string($_POST["option_normal"]);
	$option_normal_all = mysql_real_escape_string($_POST["option_normal_all"]);
	$inven_count = $_POST["inven_count"];
	$option_user_value = $_POST["option_user_value"];
	$option_user_value2 = $_POST["option_user_value2"];
	$view_yn = $_POST["view_yn"];
	$sec = mysql_real_escape_string ( $_POST["sec"] );

	if ( $pay1 == "" ) { $pay1 = "0"; }
	if ( $pay2 == "" ) { $pay2 = "0"; }
	if ( $pay3 == "" ) { $pay3 = "0"; }
	if ( $view_yn == "" ) { $view_yn = "0"; }
	if ( $view_main == "" ) { $view_main = "0"; }
	$sql_append = "";
	
	if($_FILES["thumb"]["name"]!="") {
		$rst_upload = SSH_UPLOAD_THUMB( $_FILES["thumb"] );
		if ( $rst_upload == false ) {
			$sql_append = "";
		}else {
			$sql_append = ", files='".$rst_upload."'";
		}
	}
	//상품정보고시
	mysql_query( "DELETE FROM goods_notify_data WHERE gcode = '" . $gcode . "' ");
	if ( $notify != "" ) {
		for ( $i = 0 ; $i < count( $_POST["notify_id"] ) ; $i++ ) {			
			$sql_notify = "INSERT INTO goods_notify_data SET 
			gcode = '" . $gcode . "' ,
			notify_id = '" . mysql_real_escape_string( $_POST["notify_id"][$i] ) . "' ,
			notify_val = '" . mysql_real_escape_string( $_POST["notify_val"][$i] ) . "' ";
			mysql_query( $sql_notify );
		}
	}

	//재고옵션
	mysql_query( "DELETE FROM goods_option_inven WHERE gcode = '" . $gcode . "' ");
	if ( $option_inven == "1" ) {
		 $use_option1 = false;
		 $use_option2 = false;
		 $use_option3 = false;
		 $use_option4 = false;
		$sql_append .= ", optionTitle1='".$optionTitle1."' ,  optionTitle2='".$optionTitle2."' ,  optionTitle3='".$optionTitle3."' ,  optionTitle4='".$optionTitle4."' ,	invenShow = '" . $invenShow . "' , priceState = '" . $priceState . "' ";
		if ( $optionTitle1 != "" ) { $use_option1 = true; }
		if ( $optionTitle2 != "" ) { $use_option2 = true; }
		if ( $optionTitle3 != "" ) { $use_option3 = true; }
		if ( $optionTitle4 != "" ) { $use_option4 = true; }
		//echo $use_option1 . "...".$use_option2 . "...".$use_option3 . "...".$use_option4 ;
		for ( $i = 0 ; $i < count( $option_value1 ) ; $i++ ) {
			if ($use_option1 == false && $use_option2 == false && $use_option3 == false && $use_option4 == false ) {
				continue;			
			}else {
				$sql_append_option_inven = "";
				//echo $inven_count[$i]."<br />";
				//echo $sales_price[$i]."<br />";
				//echo $sales_price_mem[$i]."<br />";
				if ( $inven_count[$i] != ""  &&  ( $sales_price[$i] != "" ||  $sales_price_mem[$i] != "" ) ) {
					if ( $use_option1 == true && $option_value1[$i] != "" ) { //1번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value1 = '" . $option_value1[$i] . "'";
					}else if ($use_option1 == true && $option_value1[$i] == "") {
						continue;
					}
					if ( $use_option2 == true && $option_value2[$i] != "" ) { //2번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value2 = '" . $option_value2[$i] . "'";
					}else if ($use_option2 == true && $option_value2[$i] == "") {
						continue;
					}
					if ( $use_option3 == true && $option_value3[$i] != "" ) { //3번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value3 = '" . $option_value3[$i] . "'";
					}else if ($use_option3 == true && $option_value3[$i] == "") {
						continue;
					}
					if ( $use_option4 == true && $option_value4[$i] != "" ) { //4번옵션 사용 + 값 있음
						$sql_append_option_inven .= " , option_value4 = '" . $option_value4[$i] . "'";
					}else if ($use_option4 == true && $option_value4[$i] == "") {
						continue;
					}
					if ($basic_price[$i]=="") {
						$basic_price[$i] = 0;
					}
					$sql_option_inven = "INSERT INTO goods_option_inven SET 
					gcode = '" . $gcode . "' ,
					inven_count = '" . $inven_count[$i] . "' ,
					basic_price = '" . $basic_price[$i] . "' ,
					sales_price = '" . $sales_price[$i] . "' ,
					sales_price_mem = '" . $sales_price_mem[$i] . "' 
					" . $sql_append_option_inven . "";
					mysql_query( $sql_option_inven );	
					//echo $sql_option_inven ;
				}else {
					continue;
				}				
			}
		}
	}
	
	//일반옵션
	mysql_query( "DELETE FROM goods_option_normal WHERE gcode = '" . $gcode . "' ");
	if ( $option_normal == "1" ) {
		$on_list1 = explode( "|_|" , $option_normal_all );
		if ( count( $on_list1 ) > 0 ) {
		//echo count( $on_list1 );
			for ( $on_list_i = 1 ; $on_list_i < count( $on_list1 ) ; $on_list_i++ ) {
				if ($on_list1[$on_list_i] != "" ) {
					$on_list2 = explode("|^|" , $on_list1[$on_list_i] );
					if ( count( $on_list2 ) > 0 ) {
						for ( $on_list2_i = 1 ; $on_list2_i < count( $on_list2 ) ; $on_list2_i++ ) {
							if ($on_list2[$on_list2_i] != "" ) {
								$on_list3 = explode("|:|" , $on_list2[$on_list2_i] );
								if ( count( $on_list3 ) > 0 ) {
									if ( $on_list3[1] != "" && $on_list3[0] != "undefined" ) {	
										
										$on_list4 = explode("|||" , $on_list3[1] );
										if ( count( $on_list4 ) > 0 ) {
											if ( $on_list4[0] != "" && $on_list4[1] != "" && $on_list4[0] != "undefined" ) {	
												$on_list5 = explode("|@|" , $on_list4[1] );
												if ( count( $on_list5 ) > 0 ) {
													if ( $on_list5[0] != "" && $on_list5[1] != "" && $on_list5[0] != "undefined" ) {
														$on_list6 = explode("|-|" , $on_list5[1] );
														if ( count( $on_list6 ) > 0 ) {
															if ( $on_list6[0] != "" && $on_list6[1] != "" && $on_list6[0] != "undefined" ) {

																$sql_option_normal = "INSERT INTO goods_option_normal SET 
																gcode = '" . $gcode . "' ,
																option_name = '" . $on_list2[0] . "' ,
																option_value1 = '" . $on_list3[0] . "' ,
																option_value2 = '" . $on_list4[0] . "' ,
																option_value3 = '" . $on_list6[0] . "' ,
																option_value4 = '" . $on_list6[1] . "' ,
																cnt = '" . $on_list5[0] . "'";
																mysql_query( $sql_option_normal );
															}														
														}
													}
												}
											}
										}
									}
								}
							}
						}			
					}
				}
			}
		}
	}

	//사용자옵션
	mysql_query( "DELETE FROM goods_option_user WHERE gcode = '" . $gcode . "' ");
	if ( $option_user == "1" ) {
		for ( $i = 0 ; $i < count( $option_user_value ) ; $i++ ) {
			if ($option_user_value[$i] != "" && $option_user_value2[$i] != "" ) {				
				$sql_option_user = "INSERT INTO goods_option_user SET 
				gcode = '" . $gcode . "' ,
				option_value1 = '" . $option_user_value[$i] . "' ,
				option_value2 = '" . $option_user_value2[$i] . "'";
				mysql_query( $sql_option_user );	
			}else {
				continue;		
			}
		}
	}

	$option_inven = ( $option_inven == "" ) ? "0" : $option_inven;
	$option_normal = ( $option_normal == "" ) ? "0" : $option_normal;
	$option_user = ( $option_user == "" ) ? "0" : $option_user;



	$sql = "UPDATE goods SET
	pname = '" . $pname . "' ,  
	price_supply = '" . $price_supply . "' ,  
	price_sales = '" . $price_sales . "' ,  
	price_basic = '" . $price_basic . "' ,  
	price_member = '" . $price_member . "' ,
	inventory = '" . $inventory . "' ,  
	orign = '" . $orign . "' ,  
	brand = '" . $brand . "' ,  
	prov_id = '" . $prov_id . "' ,  
	prov_email = '" . $prov_email . "' ,  
	delivery_default = '" . $d_pay . "' ,  
	delivery_free = '" . $d_free . "' , 
	category = '" . $category . "' ,   
	delivery_yn = '" . $delivery_yn . "' ,   
	view_yn = '" . $view_yn . "' ,  
	sec = '" . $sec . "' ,  
	tax = '" . $tax . "' ,  
	notify = '" . $notify . "' ,  
	hotsale = '" . $hotsale . "' ,  
	pay1 = '" . $pay1 . "' ,  
	pay2 = '" . $pay2 . "' ,  
	pay3 = '" . $pay3 . "' ,  
	won_0 = '" . $won_0 . "' ,  
	won_1 = '" . $won_1 . "' ,  
	won_2 = '" . $won_2 . "' ,
	per_0 = '" . $per_0 . "' ,  
	per_1 = '" . $per_1 . "' ,  
	per_2 = '" . $per_2 . "' ,  
	share_point = '" . $share_point . "' ,  
	sdate = '" . $sdate . "' ,  
	stime = '" . $stime . "' ,  
	smin = '" . $smin . "' ,  
	edate = '" . $edate . "' ,  
	etime = '" . $etime . "' ,  
	emin = '" . $emin . "' ,  
	start_date = '" . $sdate . " " . $stime . ":" . $smin . ":00' , 
	end_date = '" . $edate . " " . $etime . ":" . $emin . ":59' , 
	view_range = '" . $gid . "' ,
	option_inven = '" . $option_inven . "' ,  
	option_user = '" . $option_user . "' ,  
	option_normal = '" . $option_normal . "' ,  
	status = '" . $status . "' ,  
	content = '" . $content . "' 
	" . $sql_append . "
	where gcode='$gcode'";
	$query = mysql_query( $sql );
	@mysql_query("INSERT INTO `query_history` set gubun='goods', q='".str_replace( "'" , "" , $sql )."', reg_date='".date("Y-m-d H:i:s")."' ");
	if ($query) {		
		$redirect_url = "./product_detail.php?gcode=$gcode";
		//print_r($_POST);
		//echo $sql;
		//exit;
		PAGE_MOVE__("수정되었습니다." , $redirect_url);
	}else {
		$redirect_url = "./product_detail.php?gcode=$gcode";
		PAGE_MOVE__("수정도중 오류가 발생하였습니다. 다시 수정해주시기 바랍니다." , $redirect_url);
	}
	exit;
}

if ( $mode == "product_sort" ) {	
	$idx = mysql_real_escape_string( $_POST["gid"] );
	$sort = mysql_real_escape_string( $_POST["rank_".$idx] );
	$tbl = "goods";
	$set_update = " sort = '$sort' ";
	$query = mysql_query("update $tbl set $set_update where gcode='" . $idx . "'");
	$msg = "순서가 적용";
}

if ( $mode == "status_proc" ) {
	$multi_proc = mysql_real_escape_string( $_POST["multi_proc"] );
	$gid = mysql_real_escape_string( $_POST["gid"] );
	$idx_arr = explode( "," , $gid );
	for ( $i = 0 ; $i < count( $idx_arr ) ; $i++ ) {
		if ( $multi_proc == "4" ) {
			$query = mysql_query("update goods set del_ok='1' where gcode = '" . $idx_arr[$i] . "'");
			$msg = "삭제";		
		}else {
			$sql_set = " set status = '$multi_proc' ";
			$query = mysql_query("update goods $sql_set where gcode = '" . $idx_arr[$i] . "'");
			$msg = "수정";			
		}
	}
}

if ($mode == "group_proc" ) {
	$gid = mysql_real_escape_string( $_POST["gid"] );

	if ( $multi_proc == "3" ) {
		$status = "1";
		$set_sql = "update category set status = '$status' where ccode=";
		$msg = "수정";
	}elseif ( $multi_proc == "4" ) {
		$status = "0";	
		$set_sql = "update category set status = '$status' where ccode=";
		$msg = "수정";
	}elseif ( $multi_proc == "2" ) {
		$set_sql = "delete from category where ccode=";
		$msg = "삭제";
	}
	$idx_arr = explode( "," , $gid );
	for ( $i = 0 ; $i < count( $idx_arr ) ; $i++ ) {
		$query = mysql_query(" $set_sql '" . $idx_arr[$i] . "'");
	}
}

if ( $mode == "regist_category" ) {
	$ccode = mysql_real_escape_string( $_POST["ccode"] );
	$cname = mysql_real_escape_string( $_POST["cname"] );
	$status = mysql_real_escape_string( $_POST["status"] );

	$sql_set = " set ccode = '$ccode' , cname = '$cname' , status = '$status' ";
	$query = mysql_query("insert into category $sql_set ");
	$msg = "저장";
}

if ( $mode == "edit_category" ) {
	$ccode = mysql_real_escape_string( $_POST["ccode"] );
	$cname = mysql_real_escape_string( $_POST["cname"] );
	$status = mysql_real_escape_string( $_POST["status"] );

	$sql_set = " set cname = '$cname' , status = '$status' ";
	$query = mysql_query("update category $sql_set where ccode='$ccode'");
	$msg = "수정";
}

if($mode == "goods_excel_upload" ) {
	$upData = "";
	if($_FILES["upFile"]["name"] != "") {
	//	UpLoad start
		$s_uploadDir = "../order/excel/" ;
		$s_uploadFileAllowExt = " xls "; //allow image
		$AllowSize=1024*1024*10; //1Mb
		$uploadProcessResult = uploadProcess2($_FILES["upFile"], $s_uploadDir, $s_uploadFileAllowExt,$AllowSize);
		if ($uploadProcessResult=="201") {?>
			<script>alert('예기치 못한 오류로 등록이 취소 됐습니다. 확인 후 다시 등록 해주시기 바랍니다.');history.back();</script>
			<? exit();
		}elseif ($uploadProcessResult=="202") {?>
			<script>alert('파일의 확장자는 xls 파일만 등록 가능합니다. \n확인 후 다시 등록 해주시기 바랍니다.');history.back();</script>
			<?exit();
		}elseif ($uploadProcessResult=="203") {?>
			<script>alert('파일의 크기는 10MB 이하만 등록 가능합니다. \n확인 후 다시 등록 해주시기 바랍니다.');history.back();</script>
			<?exit();
		}else {
			$upData = $uploadProcessResult;
		}
	}

	if ( $upData == "" ) {
		echo "<script> alert('xls 이외에는 올릴수 없습니다.'); history.go(-1);</script>";
	} else {
		$tt2 = trim( "../order/excel/$upData" );
		if ( !file_exists( $tt2 ) ) { 
			echo "<script> alert('업로드 안되었습니다.다시 업로드해주세요'); history.go(-1);</script>";
		} else {
			require_once '../order/excel/reader.php';
			$data = new Spreadsheet_Excel_Reader();

			$data->setOutputEncoding('CP949');
			$data->read($tt2);
			//echo "<br />end<br />";
			
			error_reporting(E_ALL ^ E_NOTICE);
			$in_ok = 0;
			$in_no = 0;
			$skip = 0;
			//echo $data->sheets[0]['numRows'];

			$mem_info = getdata("select * from member where num=". $_SESSION["yi_idx"]);
			$row_cnt = $data->sheets[0]['numRows'];
			for ($i = 3; $i <= $row_cnt; $i++) {
				if ( $data->sheets[0]['cells'][$i][3] != "" ) {

					//gcode 생성
					$goods = getdata("select max(code) as c from goods_code");
					$gcode = strtoupper( dechex( $goods["c"] + 1 ) );
					$gidx = (int)$goods["c"] + 1;
					$sql = " update goods_code set code='$gidx'";
					mysql_query( $sql );					

					$category = $data->sheets[0]['cells'][$i][1]; //카테고리
					$gid = $data->sheets[0]['cells'][$i][2]; //적용매체
					$pname = $data->sheets[0]['cells'][$i][3]; //상품명
					$thumb = $data->sheets[0]['cells'][$i][4]; //상품이미지 파일명
					$inventory = $data->sheets[0]['cells'][$i][5]; //재고수량
					$orign = $data->sheets[0]['cells'][$i][6]; //원산지
					$brand = $data->sheets[0]['cells'][$i][7]; //제조사
					$pay1 = $data->sheets[0]['cells'][$i][8]; //신용카드
					$pay2 = $data->sheets[0]['cells'][$i][9]; //휴대폰
					$pay3 = $data->sheets[0]['cells'][$i][10]; //무통장
					$tax = $data->sheets[0]['cells'][$i][11]; //과세여부
					$sec = $data->sheets[0]['cells'][$i][12]; //회원전용상품
					$hotsale = $data->sheets[0]['cells'][$i][13]; //땡처리상품
					$content = $data->sheets[0]['cells'][$i][14]; //상품설명
					$price_supply = $data->sheets[0]['cells'][$i][15]; //공급가
					$price_basic = $data->sheets[0]['cells'][$i][16]; //이전판매가
					$price_sales = $data->sheets[0]['cells'][$i][17]; //비회원판매가
					$price_member = $data->sheets[0]['cells'][$i][18]; //회원판매가
					$won_0 = $data->sheets[0]['cells'][$i][19]; //구매자적립율(원)
					$per_0 = $data->sheets[0]['cells'][$i][20]; //구매자적립율(%)
					$won_1 = $data->sheets[0]['cells'][$i][21]; //1차추천인적립율(원)
					$per_1 = $data->sheets[0]['cells'][$i][22]; //1차추천인적립율(%)
					$won_2 = $data->sheets[0]['cells'][$i][23]; //2차추천인적립율(원)
					$per_2 = $data->sheets[0]['cells'][$i][24]; //2차추천인적립율(%)
					$share_point = $data->sheets[0]['cells'][$i][25]; //공유적립율(P)
					$start_date = $data->sheets[0]['cells'][$i][26]; //시작
					$end_date = $data->sheets[0]['cells'][$i][27]; //종료
					$delivery_yn = $data->sheets[0]['cells'][$i][28]; //기본배송비 사용
					$delivery_pay_default = $data->sheets[0]['cells'][$i][29]; //배송비
					$delivery_pay_free = $data->sheets[0]['cells'][$i][30]; //무료배송비

					if ( $pay1 == "" ) { $pay1 = "0"; }
					if ( $pay2 == "" ) { $pay2 = "0"; }
					if ( $pay3 == "" ) { $pay3 = "0"; }
					
					//배송료
					if ($delivery_yn == "1" ) { //기본배송료 선택시
						$d_pay = $mem_info["delivery_pay_default"];
						$d_free = $mem_info["delivery_pay_free"];
					}else {
						$d_pay = $delivery_pay_default;
						$d_free = $delivery_pay_free;		
					}
					$sdate = substr($start_date , 0 , 10);
					$edate = substr($end_date , 0 , 10);
					$stime = substr($start_date , 11 , 2);
					$etime = substr($start_date , 11 , 2);
					$smin = substr($start_date , 14 , 2);
					$emin = substr($start_date , 14 , 2);

					$sql = "INSERT INTO goods SET
					gcode = '" . $gcode . "' ,  
					id = '" . $_SESSION['yi_id'] . "' ,  
					pname = '" . iconv("euc-kr", "utf-8", $pname ) . "' ,  
					short_url = '" . $short_url . "' ,  
					short_qna = '" . $short_qna . "' ,  
					short_deli = '" . $short_deli . "' ,  
					short_shop = '" . $short_shop . "' ,  
					price_supply = '" . $price_supply . "' ,  
					price_sales = '" . $price_sales . "' ,  
					price_basic = '" . $price_basic . "' ,  
					price_member = '" . $price_member . "' ,
					inventory = '" . $inventory . "' ,  
					orign = '" . iconv("euc-kr", "utf-8", $orign ) . "' ,  
					brand = '" . iconv("euc-kr", "utf-8", $brand ) . "' ,  
					delivery_default = '" . $d_pay . "' ,  
					delivery_free = '" . $d_free . "' , 
					category = '" . $category . "' ,   
					delivery_yn = '" . $delivery_yn . "' ,   
					view_yn = '1' ,
					sec = '" . $sec . "' ,  
					share_point = '" . $share_point . "' ,  
					tax = '" . $tax . "' ,  
					notify = '' ,  
					hotsale = '" . $hotsale . "' ,  
					pay1 = '" . $pay1 . "' ,  
					pay2 = '" . $pay2 . "' ,  
					pay3 = '" . $pay3 . "' ,  
					per_0 = '" . $per_0 . "' ,  
					per_1 = '" . $per_1 . "' ,  
					per_2 = '" . $per_2 . "' ,  
					won_0 = '" . $won_0 . "' ,  
					won_1 = '" . $won_1 . "' ,  
					won_2 = '" . $won_2 . "' ,  
					sdate = '" . $sdate . "' ,  
					stime = '" . $stime . "' ,  
					smin = '" . $smin . "' ,  
					edate = '" . $edate . "' ,  
					etime = '" . $etime . "' ,  
					emin = '" . $emin . "' ,  
					start_date = '" . $sdate . " " . $stime . ":" . $smin . ":00' , 
					end_date = '" . $edate . " " . $etime . ":" . $emin . ":59' , 
					view_range = '" . $gid . "' ,
					option_inven = '0' ,  
					option_user = '0' ,  
					option_normal = '0' ,  
					content = '" . iconv("euc-kr", "utf-8", $content ) . "' ,  
					status = '0' ,  
					reg_date = '" . date("Y-m-d H:i:s") . "' ,
					files='" . iconv( "euc-kr", "utf-8", $thumb )."'";
					$query = mysql_query( $sql );
					if ($query) {
						$in_ok++;
					}else {
						$in_no++;
					}
				}else {
					$skip++;
					continue;
				}
			}

			if($tt2 != ""){
				$x=$tt2;
				$file1=$x;
				@chmod($file1, 0775);
				@unlink ("$file1");
			}
			$row_cnt = $row_cnt - 2;
			echo "<script>alert('총 $row_cnt 개의 데이터 중 정상입력($in_ok)개 , 오류($in_no)개 , 무시($skip)개 처리되었습니다.');top.parent.opener.location.reload();top.self.close();</script>";
			exit;
		}
	}
}

if ($mode == "goods_copy" ) {	
	$gcode_ori = mysql_real_escape_string($_POST["gid"]);
	$row_ori = getdata(" select * from goods where gcode='$gcode_ori' ");
	$goods_notify_data = array();
	$rst_notify = mysql_query(" select * from goods_notify_data where gcode='$gcode_ori' ");
	while ( $row_notify = mysql_fetch_array( $rst_notify ) ) {
		$goods_notify_data[] = $row_notify;
	}

	if ( $row_ori["option_inven"] == "1" ) {
		$option_inven = array();
		$rst_inven = mysql_query(" select * from goods_option_inven where gcode='$gcode_ori' order by idx asc ");
		while ( $row_inven = mysql_fetch_array( $rst_inven ) ) {
			$option_inven[] = $row_inven;
		}
	}
	if ( $row_ori["option_normal"] == "1" ) {
		$option_normal = array();
		$rst_normal = mysql_query(" select * from goods_option_normal where gcode='$gcode_ori'  order by idx asc ");	
		while ( $row_normal = mysql_fetch_array( $rst_normal ) ) {
			$option_normal[] = $row_normal;
		}
	}
	if ( $row_ori["option_user"] == "1" ) {
		$option_user = array();
		$rst_user = mysql_query(" select * from goods_option_user where gcode='$gcode_ori'  order by idx asc ");	
		while ( $row_user = mysql_fetch_array( $rst_user ) ) {
			$option_user[] = $row_user;
		}
	}

	$db_momsday__ = DB__($db_user,$db_pass1,"momsday");
	mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $db_momsday__);
	mysql_set_charset("utf8"); 

	//gcode 생성
	$goods = getdata("select max(code) as c from goods_code");
	$gcode = strtoupper( dechex( $goods["c"] + 1 ) );
	$gidx = (int)$goods["c"] + 1;
	$sql = " update goods_code set code='$gidx'";
	mysql_query( $sql );

	$googer = new GoogleURLAPI($gkey);
	$share_url = "http://momsday.kr/order/order.php?gcode=".$gcode;
	$short_url = $googer->shorten($share_url);

	for ( $i = 0 ; $i < count( $goods_notify_data ) ; $i++ ) {
		$sql_notify = "INSERT INTO goods_notify_data SET 
		gcode = '" . $gcode . "' ,
		notify_id = '" . $goods_notify_data[$i]["notify_id"] . "' ,
		notify_val = '" . $goods_notify_data[$i]["notify_val"] . "' ";
		mysql_query( $sql_notify );
		//echo $sql_notify."<br /><br />";
	}

	if ( count( $option_inven ) > 0 ) {
		for ( $i = 0 ; $i < count( $option_inven ) ; $i++ ) {
			$sql_option_inven = "INSERT INTO goods_option_inven SET 
			gcode = '" . $gcode . "' ,
			inven_count = '" . $option_inven[$i]["inven_count"] . "' ,
			basic_price = '" . $option_inven[$i]["basic_price"] . "' ,
			sales_price = '" . $option_inven[$i]["sales_price"] . "' ,
			sales_price_mem = '" . $option_inven[$i]["sales_price_mem"] . "' ,
			option_value1 = '" . $option_inven[$i]["option_value1"] . "',
			option_value2 = '" . $option_inven[$i]["option_value2"] . "',
			option_value3 = '" . $option_inven[$i]["option_value3"] . "',
			option_value4 = '" . $option_inven[$i]["option_value4"] . "'";
			mysql_query( $sql_option_inven );
			//echo $sql_option_inven."<br /><br />";		
		}
	}
	if ( count( $option_normal ) > 0 ) {
		for ( $i = 0 ; $i < count( $option_normal ) ; $i++ ) {
			$sql_option_normal = "INSERT INTO goods_option_normal SET 
			gcode = '" . $gcode . "' ,
			option_name = '" . $option_normal[$i]["option_name"] . "' ,
			cnt = '" . $option_normal[$i]["cnt"] . "' ,
			option_value1 = '" . $option_normal[$i]["option_value1"] . "',
			option_value2 = '" . $option_normal[$i]["option_value2"] . "',
			option_value3 = '" . $option_normal[$i]["option_value3"] . "',
			option_value4 = '" . $option_normal[$i]["option_value4"] . "'";
			mysql_query( $sql_option_normal );
			//echo $sql_option_normal."<br /><br />";		
		}
	}
	if ( count( $option_user ) > 0 ) {
		for ( $i = 0 ; $i < count( $option_user ) ; $i++ ) {
			$sql_option_user = "INSERT INTO goods_option_normal SET 
			gcode = '" . $gcode . "' ,
			option_value1 = '" . $option_user[$i]["option_value1"] . "',
			option_value2 = '" . $option_user[$i]["option_value2"] . "'";
			mysql_query( $sql_option_user );
			//echo $sql_option_user."<br /><br />";		
		}
	}
	$sql = "INSERT INTO goods SET
		gcode = '" . $gcode . "' ,  
		id = '" . $row_ori["id"] . "' ,  
		pname = '" . $row_ori["pname"] . "' ,  
		short_url = '" . $short_url . "' ,  
		short_qna = '" . $short_url . "' ,  
		short_deli = '" . $short_url . "' ,  
		short_shop = '" . $short_url . "' ,  
		price_supply = '" . $row_ori["price_supply"] . "' ,  
		price_sales = '" . $row_ori["price_sales"] . "' ,  
		price_basic = '" . $row_ori["price_basic"] . "' ,  
		price_member = '" . $row_ori["price_member"] . "' ,
		inventory = '" . $row_ori["inventory"] . "' ,  
		orign = '" . $row_ori["orign"] . "' ,  
		brand = '" . $row_ori["brand"] . "' ,  
		delivery_yn = '" . $row_ori["delivery_yn"] . "' ,   
		delivery_default = '" . $row_ori["delivery_default"] . "' ,  
		delivery_free = '" . $row_ori["delivery_free"] . "' , 
		category = '' ,   
		view_yn = '" . $row_ori["view_yn"] . "' ,  
		view_range = 'momsday' ,
		view_main = '" . $row_ori["view_main"] . "' ,
		share_point = '" . $row_ori["share_point"] . "' ,  
		sec = '" . $row_ori["sec"] . "' ,  
		tax = '" . $row_ori["tax"] . "' ,  
		notify = '" . $row_ori["notify"] . "' ,  
		hotsale = '" . $row_ori["hotsale"] . "' ,  
		pay1 = '" . $row_ori["pay1"] . "' ,  
		pay2 = '" . $row_ori["pay2"] . "' ,  
		pay3 = '" . $row_ori["pay3"] . "' ,  
		pay4 = '" . $row_ori["pay4"] . "' ,  
		pay5 = '" . $row_ori["pay5"] . "' ,  
		pay6 = '" . $row_ori["pay6"] . "' ,  
		per_0 = '" . $row_ori["per_0"] . "' ,  
		per_1 = '" . $row_ori["per_1"] . "' ,  
		per_2 = '" . $row_ori["per_2"] . "' ,  
		won_0 = '" . $row_ori["won_0"] . "' ,  
		won_1 = '" . $row_ori["won_1"] . "' ,  
		won_2 = '" . $row_ori["won_2"] . "' ,  
		sdate = '" . $row_ori["sdate"] . "' ,  
		stime = '" . $row_ori["stime"] . "' ,  
		smin = '" . $row_ori["smin"] . "' ,  
		edate = '" . $row_ori["edate"] . "' ,  
		etime = '" . $row_ori["etime"] . "' ,  
		emin = '" . $row_ori["emin"] . "' ,  
		start_date = '" . $row_ori["start_date"] . "' , 
		end_date = '" . $row_ori["end_date"] . "' , 
		option_inven = '" . $row_ori["option_inven"] . "' ,  
		optionTitle1 = '" . $row_ori["optionTitle1"] . "' ,  
		optionTitle2 = '" . $row_ori["optionTitle2"] . "' ,  
		optionTitle3 = '" . $row_ori["optionTitle3"] . "' ,  
		optionTitle4 = '" . $row_ori["optionTitle4"] . "' ,   
		invenShow = '" . $row_ori["invenShow"] . "' ,  
		priceState = '" . $row_ori["priceState"] . "' ,  
		option_user = '" . $row_ori["option_user"] . "' , 
		option_normal = '" . $row_ori["option_normal"] . "' ,  
		status = '0' ,  
		content = '" . $row_ori["content"] . "' ,  
		comment = '" . $row_ori["comment"] . "' ,  
		files = '" . $row_ori["files"] . "' ,  
		prov_num = '" . $row_ori["prov_num"] . "' ,  
		prov_id = '" . $row_ori["prov_id"] . "' ,  
		prov_email = '" . $row_ori["prov_email"] . "' ,  
		reg_date = '" . date("Y-m-d H:i:s") . "' ,
		gp_yn = '" . $row_ori["gp_yn"] . "' ,  
		jh_yn = '" . $row_ori["jh_yn"] . "' 
		" . $sql_append . "";
	$query = mysql_query( $sql );
	//echo $sql."<br /><br />";
	//exit;
	if ($query) {
		echo "<script>window.alert(' 등록되었습니다.');top.location.reload();</script>";		
	}else {
		echo "<script>window.alert(' 등록도중 오류가 발생하였습니다. 다시 등록해주시기 바랍니다.');top.location.reload();</script>";		
	}
	exit;
}

if ( $mode == "chgAgree" ) {
	$no = mysql_real_escape_string( $_POST["no"] );
	$adminAgree = mysql_real_escape_string( $_POST["adminAgree"] );
	$query = mysql_query("update goods set adminAgree = '" . $adminAgree . "' where no = '" . $no . "' ");
	$msg = "수정";
}
if ($query) {
	if ( $re_url != "" ) {
		echo "<script>window.alert(' $msg 되었습니다.');top.location.href='$re_url';</script>";	
	}else {
		echo "<script>window.alert(' $msg 되었습니다.');top.location.reload();</script>";		
	}
}else {
	echo "<script>window.alert('오류가 발생 하였습니다.');top.location.reload();</script>";
}
?>
</body>
</html>
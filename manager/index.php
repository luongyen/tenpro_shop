<?
$none_login_agree="yes";
include ("./config.php");
if( $_SESSION['yi_level'] == "99" ) {
	header ("location:./order/payment.php?status=2");
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title><?=$admin_title?></title>
	
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	
	<script src="./js/jquery-1.10.2.min.js"></script>
	<script src="./js/common.js"></script>
	<script src="./js/gnb.js"></script>
	<script src="../assets/js/common.js"></script>
	
	<script type="text/javascript" src="./js/jquery-ui.js"></script>
	<script src="/manager/js/summernote-lite.js"></script>
<script type="text/javascript">
<!--
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
});
//-->
</script>
	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
	<script type="text/javascript">
	<!--
	function SgValidator() {
		var frm1 = document.form1.sgId;
		var frm3 = document.form1.passwd;

		if (_isEmpty(frm1)) {
				alert("아이디를 입력하세요.");
				frm1.focus();
				return false;
			}else if (_isLen(frm1, 4,15)) {
				alert("아이디는 4~15자입니다.");
				frm1.focus();
				return false;
			}else if (_isEmpty(frm3)) {
				alert("비밀번호를 입력하세요.");
				frm3.focus();
				return false;
			}else if (_isLen(frm3, 4,15)) {
				alert("비밀번호는 4~15자입니다.");
				frm3.focus();
				return false;
			}else {
				document.form1.submit();
			}
	}
	//-->
	</script>
</head>
<body class="bg_loign">
	<form name="form1" method="post" action="login_ok.php">
	
	<div class="loginbox">
		<div class="box_cont">
			<p class="tit"><img src="img/common/txt_logo.gif" alt="Admin Login" /></p>
			<div class="txt"><img src="img/common/txt_logo_s.gif" alt="사이트 관리자 로그인" /></div>
			<div class="imgbox"><img src="img/common/img_imgbox.jpg" alt="" /></div>
			
			<div class="login_ip">
				<div class="ipbox">
					<ul>
						<li><label><img src="img/common/txt_id.gif" alt="아이디" /></label><input type="text" name="sgId" value="" title="" /></li>
						<li><label><img src="img/common/txt_pw.gif" alt="비밀번호" /></label><input type="password" name="passwd" value="" title="" /></li>
					</ul>
					<div class="btn_login"><input type="image" src="img/common/btn_login.gif" value="" alt="로그인" onclick="SgValidator();return false;" /></div>
				</div>
				<div class="txtbox">
					이 페이지는 <strong>(주)큐존 관리자</strong>를 위한 페이지입니다.<br />
					각 권한에 따른 승인된 관리자만 접속이 가능합니다.<br />
					관리 작업을 마치신 후 반드시 로그아웃 하시기 바랍니다.
				</div>
				<div class="stiehome"><a href="#"><img src="img/common/btn_stiehome.gif" alt="사이트 바로가기" /></a></div>
			</div>
			
			<div class="txt_copyright">copyright <strong>QZONE CORP.</strong> All Right Reserved.</div>
			
		</div>
	</div>
	
	</form>
</body>
</html>
var oEditors = [];
$(function() {
    $.datepicker.setDefaults($.datepicker.regional['ko']);
    $('.jqueryui-date').each(function(){
        $(this).datepicker({
            changeYear:$(this).attr('changeYear')=='true'?true:false,
            changeMonth:$(this).attr('changeMonth')=='true'?true:false
        });
    });

    $('.se_editor').each(function(){
        var editorID = this.id;
        nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: this,
            sSkinURI: "/manager/js/SmartEditor2Skin.html",
            fCreator: "createSEditor2",
            htParams : {
                bUseToolbar: true,
                fOnBeforeUnload: function(){
                    oEditors.getById[editorID].exec("UPDATE_CONTENTS_FIELD", [])
                }
            }
			,
				fOnAppLoad: function(){
					}
        });
    });
    $('.se_editor2').each(function(){
        var editorID = this.id;
        nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: this,
            sSkinURI: "/manager/js/smart_editor2/Editor2Skin.html",
            fCreator: "createSEditor2",
            htParams : {
                bUseToolbar: true,
                fOnBeforeUnload: function(){
                    oEditors.getById[editorID].exec("UPDATE_CONTENTS_FIELD", [])
                }
            }
			,
				fOnAppLoad: function(){
					}
        });
    });
});
function updateContentsEditor(oEditorsID) {
    oEditors.getById[oEditorsID].exec("UPDATE_CONTENTS_FIELD", []);
}

var numformat = {};
numformat.format = function(num){
	var nStr = num+'';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.'+x[1]: '';
	x1 = x1.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	return x1 + x2;
};
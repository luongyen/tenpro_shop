<?php
include ("../config.php");
 	$sFileInfo = '';
	$headers = array();
	 
	//echo "<script>alert('5in');</script>";
	foreach($_SERVER as $k => $v) {
		if(substr($k, 0, 9) == "HTTP_FILE") {
			$k = substr(strtolower($k), 5);
			$headers[$k] = $v;
		} 
	}
	$file = new stdClass;
	$file->name = date("Ymdhis").rawurldecode($headers['file_name']);
	$file->size = $headers['file_size'];
	$file->content = file_get_contents("php://input");
	//print_r($file);
	$filename_ext = strtolower(array_pop(explode('.',$file->name)));
	$allow_file = array("jpg", "png", "bmp", "gif", "jpeg"); 
	
	if(!in_array($filename_ext, $allow_file)) {
		echo "NOTALLOW_".$file->name;
	} else {
		$uploadDir = '../../data/event/';
		$newFile = md5($file->name) . "." . $filename_ext;
		$newPath = $uploadDir . $newFile;
		
		if(file_put_contents($newPath, $file->content)) {
			$sFileInfo .= "&bNewLine=true";
			$sFileInfo .= "&sFileName=" . $newFile;
			$sFileInfo .= "&sFileURL=/data/event/" . $newFile;
		}
		//echo "<script>alert('".$sFileInfo."');</script>";
		echo $sFileInfo;
	}
?>
var navi = {
  $box: null,
  $one: null,
  $twobox: [],
  $two: [],
  active: null,
  overed: null,
  resettimer: null,
  anioption: [{
    queue: false,
    duration: 100,
    easing: 'easeInQuint'
  }, {
    queue: false,
    duration: 100,
    easing: 'easeInQuint'
  }],
  over: {
    one: function (noani) {
      navi.clearreset();

      var $one = navi.$one,
        $twobox = navi.$twobox,
        i = 0,
        max = $one.length;

      navi.overed = this.index;

      for (; i < max; i++) {
        if (i == this.index) {
          navi.over.two(i);
          $one[i].addClass('on');
          if ($twobox[i]) {
            if (noani === true) {
              $twobox[i].stop().fadeTo('fast', 1).show().css({
                height: 50
              });
            } else {
              $twobox[i].stop().fadeTo('fast', 1).show().animate({
                height: 50
              }, navi.anioption[0]);
            }
          }
        } else {
          $one[i].removeClass('on');
          if ($twobox[i]) {
            $twobox[i].stop().fadeTo('fast', 0).show().animate({
              height: 0
            }, navi.anioption[1]);
          }
        }
      }

    },

    two: function (index) {
      navi.clearreset();
      var $two = navi.$two[(index != undefined && !isNaN(index)) ? index : navi.overed],
        i = 0,
        max = $two.length;
      for (; i < max; i++) {
        $two[i][((((index == undefined || isNaN(index)) && i == this.index) || (index != undefined && index == navi.active[0] && i == navi.active[1])) ? 'add' : 'remove') + 'Class']('on');
      }
    }

  },

  hidesub: function () {
    this.style.display = 'none';
  },

  clearreset: function () {
    clearTimeout(navi.resettimer);
  },

  reset: function () {
    navi.resettimer = setTimeout(navi.resetaction, 100);
  },

  resetaction: function (noani) {
    if (navi.active[0] > -1 && navi.$one.length > navi.active[0]) {
      navi.over.one.call({
        index: navi.active[0],
        fromreset: true
      }, noani);
      navi.over.two.call({
        index: navi.active[1]
      });
    } else {
      navi.over.one.call({
        index: -1,
        fromreset: true
      }, noani);
    }
  },

  init: function () {

    var i, max, j, jmax;

    this.$box = $('#gnb>ul');
    this.$one = this.$box.find('li:not(li>ul>li)>a');

    for (i = 0, max = this.$one.length; i < max; i++) {

      this.$one[i] = $(this.$one[i]).mouseenter(this.over.one).focus(this.over.one).blur(this.reset);
      this.$one[i][0].index = i;
      this.$twobox[i] = this.$one[i].parent().find('ul');
      if (this.$twobox[i].length) {
        this.$two[i] = this.$twobox[i].find('a');
        for (j = 0, jmax = this.$two[i].length; j < jmax; j++) {
          this.$two[i][j] = $(this.$two[i][j]).mouseover(this.over.two).focus(this.over.two).blur(this.reset);
          this.$two[i][j][0].index = j;
        }
      }

    }

    this.active = this.$box.parent()[0].className.match(/active-([0-9]+)-([0-9]+)/);
    this.active = (this.active) ? [parseInt(this.active[1] || 0) - 1, parseInt(this.active[2] || 0) - 1] : [-1, -1];
    this.anioption[1].complete = this.hidesub;
    this.$box.mouseenter(this.clearreset).mouseleave(this.reset);
    this.resetaction(true);

  }

}


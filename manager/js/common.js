
$(document).ready(function() {
 	/* rollover  */
  //image
  $('img.rollover').hover(

  function () {
    this.src = this.src.replace('_off', '_on');
  }, function () {
    this.src = this.src.replace('_on', '_off');
  });
  
  $("#gnb li").each(function () {
    var link = $(this).children("a");
    var image = $(link).children("img");
    var imgsrc = $(image).attr("src");

    // add mouseover
    $(link).mouseover(function () {
      var on = imgsrc.replace(/_off.gif$/gi, "_on.gif");
      $(image).attr("src", on).fadeTo();
    });

    // add mouse out
    $(link).mouseout(function () {
      $(image).attr("src", imgsrc);
    //$(this).stop().fadeTo('', 2)
    });
  });
  
  $(".jcarousel-skin li").each(function (i) {
    if (i % 2 < 1) {
      $(this).addClass("left_cont");
    } else {
      $(this).addClass("right_cont");
    }
  });
    
  $('.table_bg tr').hover(
      function () { $(this).addClass('over'); },
      function () { $(this).removeClass('over'); }
  );

});


/* img over */
function over_img(img, n) {

  if (n == "on") {
    var hover = "_on";
  } else {
    var hover = "_off";
  }
  if (img.parent().hasClass("no") == false && img.parent().hasClass("on") == false && img.hasClass("on") == false) {
    menuimg = img.find("img");
    if (menuimg.attr("src").indexOf(".jpg") > 0) {
      menuimg_type = ".jpg";
    } else if (menuimg.attr("src").indexOf(".gif") > 0) {
      menuimg_type = ".gif";
    } else if (menuimg.attr("src").indexOf(".png") > 0) {
      menuimg_type = ".png";
    }


    menuimg_src = menuimg.attr("src").split("_off")[0];
    menuimg_src = menuimg_src.split("_on")[0];
    menuimg.attr("src", menuimg_src + hover + menuimg_type);
  }
}

function on_img(img, n) {
  if (n == "on") {
    var hover = "_on";
  } else {
    var hover = "_off";
  }
  menuimg = img.find("img");
  menuimg_src = menuimg.attr("src").split("_off.gif")[0];
  menuimg_src = menuimg_src.split("_on")[0];
  menuimg.attr("src", menuimg_src + hover + ".gif");
}


/*
$(document).ready(function () {
  $("#gnb li").click(function () {
    $("a.dep1").each(function () {
      on_img($(this), "off");
    })
    on_img($(this), "on");
    $(this).parent().addClass("on");
  })
});

jQuery(function(){
	
	var article = $('.on_off .article');
	article.addClass('hide');
	article.find('.onlist_view').slideUp(0);
	article.eq(0).removeClass('hide').addClass('show'); // 첫 번째 항목을 열어 둡니다
	article.eq(0).find('.onlist_view').slideDown(0); // 첫 번째 항목을 열어 둡니다
	
	$('.on_off .article .trigger').click(function(){
		var myArticle = $(this).parents('.article:first');
		if(myArticle.hasClass('hide')){
			myArticle.removeClass('hide').addClass('show');
			myArticle.find('.onlist_view').slideDown(200);
		} else {
			myArticle.removeClass('show').addClass('hide');
			myArticle.find('.onlist_view').slideUp(200);
		}
	});
});
*/


/* quickmenu */
$(document).ready(function () {
  var maxTop = 0;
  var $quickmenu = $(".l_cont");

  if ($quickmenu.length < 1) return false;


  var $cnt = $("#container");
  var pTop = 0;

  maxTop = $cnt.offset().top - pTop;

  $(window).scroll(function () {
    var top = $(document).scrollTop() - maxTop;
    var bb = $cnt.height() - $quickmenu.height();

    if (top > bb) top = bb;

    if (top < pTop) top = pTop;


    $quickmenu.stop().animate({
      top: top
    }, 800);
  });
  
});


// 팝업창 띄우기

/*
function popOpen(url,name,width,height,top,left,option) {
	window.open(url,name,'width='+width+',height='+height+',top='+top+',left='+left+","+option);
}
*/
function popOpen(url,description,width,height){
	var screenPosX = screen.availWidth/2 - width/2;
	var screenPosY = screen.availHeight/2 - height/2;
	var features = "'location=no,directories=no,status=no,menubar=no,top="+screenPosY+",left="+screenPosX+",width="+width+",height="+height+"'";
	window.open(url,description,features);
}
// 팝업창 닫기
function popClose() {
	window.close();
}

// 팝업창 리사이즈
function popResize(x,y) {
	var thisX = parseInt(x);
	var thisY = parseInt(y);
	var maxThisX = screen.width - 50;
	var maxThisY = screen.height - 50;
	var marginY = 0;

	if (navigator.userAgent.indexOf("MSIE 6") > 0) marginY = 45;		// IE 6.x
	else if(navigator.userAgent.indexOf("MSIE 7") > 0) marginY = 75;	// IE 7.x
	else if(navigator.userAgent.indexOf("MSIE 8") > 0) marginY = 27;	// IE 8.x
	else if(navigator.userAgent.indexOf("MSIE 9") > 0) marginY = 80;	// IE 9.x
	else if(navigator.userAgent.indexOf("Firefox") > 0) marginY = 50;	// FF
	else if(navigator.userAgent.indexOf("Opera") > 0) marginY = 30;		// Opera
	else if(navigator.userAgent.indexOf("Chrome") > 0) marginY = 70;	// Chrome
	else if(navigator.userAgent.indexOf("Netscape") > 0) marginY = -2;	// Netscape

	if (thisX > maxThisX) {
		window.document.body.scroll = "yes";
		thisX = maxThisX;
	}
	if (thisY > maxThisY - marginY) {
		window.document.body.scroll = "yes";
		thisX += 19;
		thisY = maxThisY - marginY;
	}
	window.resizeTo(thisX+10, thisY+marginY);

	//var windowX = (screen.width - (thisX+10))/2;
	//var windowY = (screen.height - (thisY+marginY))/2 - 20;
	//window.moveTo(windowX,windowY);
}

function addComma(n) {
 var reg = /(^[+-]?\d+)(\d{3})/;
 n += '';

 while (reg.test(n)) {
  n = n.replace(reg, '$1' + ',' + '$2');
 }

 return n;
}

/*
$(document).ready(function() {
  $('.summernote').summernote({
		height: 500,   //set editable area's height
		callbacks: {
			onImageUpload: function(image) {
				uploadImage(image[0]);
			}
		},
		lang : 'ko-KR',
		placeholder: '이제 본문에 #을 이용한 태그 입력도 가능해요! URL을 통해, 사진 및 youtube를 등록할 수도 있어요!',
		tabsize: 2
	});
});

function uploadImage(image) {
	var data = new FormData();
	data.append("image", image);
	$.ajax({
		type: "post",
		cache: false,
		contentType:false,
		processData: false,
		url: '/manager/data/upload.php',
		data: data,
		success: function(data) {
			var image = $('<img>').attr('src', data);
			$('.summernote').summernote("insertNode", image[0]);
		},
		error: function(data) {
			//console.log('error : ' +data);
			var str = "";
			for(key in data) {
				str += key+"="+data[key]+"\n";
			}
			console.log(str);
			alert('error : ' +data);
		}
	});
} 
*/
<?
$menu_id = "9";
$page_id = "1";
include "../include/header.php"; 
$idx = mysql_real_escape_string( $idx );
if ( $idx != "" ) {
	$row = getdata("select * from event_attendance where idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 이벤트 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}
}

?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/se/HuskyEZCreator.js" charset="utf-8"></script>

			<!-- leftmneu -->
			<? include "../include/left_event.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">출석이벤트 관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox" style="width:1000px;">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" enctype="multipart/form-data">
					<input type="hidden" name="idx" value="<?=$idx?>">
					<input type="hidden" name="gid" value=""><!--적용매체-->
					<input type="hidden" name="mode" value="event_regist_attend">
					<input type="hidden" name="option_normal_all" value="">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="120px"><col width=""></colgroup>
							<tbody>	
								<tr>
									<th><strong>월</strong></th>
									<td>
										<select name="sdate" id="sdate" class="sel4">
										<?
										for ( $i = 0 ; $i < 12 ; $i++ ) {
											$now_mon = date("Ym" , strtotime("+".$i . " month"));
											if ($row["sdate"] == $now_mon ) {
												$mon_sel = " selected";
											}else {
												$mon_sel = " ";
											}
											echo "<option value='$now_mon' $mon_sel>" . substr( $now_mon , 0 , 4 ) . "년 " . substr( $now_mon , - 2 ) . "월</option>";
										}?>											
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>상태</strong></th>
									<td>
										<select name="status" class="sel4" id="status">
											<option value="">선택</option>
											<option value="0" <?= ( $row["status"] == "0" ) ? "selected" : "" ?>>진행중</option>
											<option value="1" <?= ( $row["status"] == "1" ) ? "selected" : "" ?>>중지</option>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>당첨조건 1</strong></th>
									<td>연속 <input type="text" name="gift_day_1" value="<?=$row["gift_day_1"]?>" class="ip1" style="width:70px;"> 일</td>
									<th><strong>지급포인트 1</strong></th>
									<td>연속 <input type="text" name="gift_point_1" value="<?=$row["gift_point_1"]?>" class="ip1" style="width:70px;"></td>
								</tr>
								<tr>
									<th><strong>당첨조건 2</strong></th>
									<td>연속 <input type="text" name="gift_day_2" value="<?=$row["gift_day_2"]?>" class="ip1" style="width:70px;"> 일</td>
									<th><strong>지급포인트 2</strong></th>
									<td>연속 <input type="text" name="gift_point_2" value="<?=$row["gift_point_2"]?>" class="ip1" style="width:70px;"></td>
								</tr>
								<tr>
									<th><strong>당첨조건 3</strong></th>
									<td>연속 <input type="text" name="gift_day_3" value="<?=$row["gift_day_3"]?>" class="ip1" style="width:70px;"> 일</td>
									<th><strong>지급포인트 3</strong></th>
									<td>연속 <input type="text" name="gift_point_3" value="<?=$row["gift_point_3"]?>" class="ip1" style="width:70px;"></td>
								</tr>
								<tr>
									<th><strong>당첨조건 4</strong></th>
									<td>연속 <input type="text" name="gift_day_4" value="<?=$row["gift_day_4"]?>" class="ip1" style="width:70px;"> 일</td>
									<th><strong>지급포인트 4</strong></th>
									<td>연속 <input type="text" name="gift_point_4" value="<?=$row["gift_point_4"]?>" class="ip1" style="width:70px;"></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<!--a href="#" class="btn_120bk"><span>삭제</span></a>
							<a href="#" class="btn_120w"><span>수정</span></a-->
							<?if ($mode != "event_regist" ) {?>
							<!--a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a-->
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>							
							<?}else {?>
							<a href="#" class="btn_120b" id="updateBtn"><span>등록</span></a>								
							<?}?>
						</div>
					</div>
					<!-- //button -->
					
				</div>
				
			</div>
		
			<script src="../js/jquery.filestyle.mini.js"></script>
			<script>
				$(function () {
					$("input.ip_file").filestyle({
						image: "../img/board/btn_search_f.gif",
						imageheight: 30,
						imagewidth: 80,
						marginleft: 0,
						width: 88
					});
					$('a.goback').click(function () {
						history.back();
						return false;
					});
					$('#updateBtn').click(function (){
					
						/*
						if ($('#view_range option:selected').val() == '') {
							alert('적용매체를 선택하세요.');
							return false;
						}
						*/
						if ($('input[name=gift_day_1]').val() == ''&& $('input[name=gift_day_2]').val() == ''&& $('input[name=gift_day_3]').val() == ''&& $('input[name=gift_day_4]').val() == '') {
							alert('당첨조건을 적어도 1개 이상 입력하세요.');
							return false;
						}
						
						if ( $('input[name=gift_day_1]').val() != '' ) {
							if ( $('input[name=gift_point_1]').val() == '' ) {
								alert('당첨조건1을 정확히 입력하세요.');
							}
						}
						if ( $('input[name=gift_day_2]').val() != '' ) {
							if ( $('input[name=gift_point_2]').val() == '' ) {
								alert('당첨조건2을 정확히 입력하세요.');
							}
						}
						if ( $('input[name=gift_day_3]').val() != '' ) {
							if ( $('input[name=gift_point_3]').val() == '' ) {
								alert('당첨조건3을 정확히 입력하세요.');
							}
						}
						if ( $('input[name=gift_day_4]').val() != '' ) {
							if ( $('input[name=gift_point_4]').val() == '' ) {
								alert('당첨조건4을 정확히 입력하세요.');
							}
						}
						if ($('#status option:selected').val() == '') {
							alert('상태를 선택하세요.');
							return false;
						}
						document.regi_form.submit();
					});
				});
				
			</script>
			
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
$menu_id = "9";
$page_id = "1";
include "../include/header.php"; 
$gubun = mysql_real_escape_string( $gubun );

//search_setting
$sql_append = "";

$etitle = mysql_real_escape_string( $_GET["etitle"] );
$view_range = mysql_real_escape_string( $_GET["view_range"] );
$status = mysql_real_escape_string( $_GET["status"] );
if ($sort == "" ) {
	$sort = " reg_date desc ";
}
$get_query = "view_range=$view_range&sdate=$sdate";

//공통 
if ( $sdate		!= "" ) { $where .= " and sdate = '" . $sdate . "' "; } 
if ( $media	!= "" ) { $where .= " and view_range like '%$media%' "; }

$ocount=getdata( "select count(*) as cnt from event_attendance where del_ok='0' ".$where." " );
$cnt = $ocount["cnt"];

//페이징준비처리
	$pageIdx=1;	
	if ($page>0) {
		$pageIdx=$page;
	}
	$page_set = 15; //한페이지 줄수-기본값
	$block_size = 10;
	if($pageIdx % $block_size==0) {
		$start_num=$pageIdx-$block_size+1;
	}else {
		$start_num=floor($pageIdx/$block_size)*$block_size +1;
	}
	$end_num = $start_num+$block_size-1;
	$total_page = ceil($cnt / $page_set); // 총 페이지 수

	if($pageIdx==1) {
		$limit_idx=0;
	}else {
		$limit_idx=$pageIdx*$page_set-$page_set;
	}
//페이징준비 끝

$rst = mysql_query("select * from event_attendance where del_ok='0' ".$where." order by idx desc limit ".$limit_idx.", ".$page_set);
?>
			
			<!-- leftmneu -->
			<? include "../include/left_event.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents" >
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">출석이벤트관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<!-- search -->
					<div class="board_search">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="100px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>월선택</strong></th>
									<td>
										<select name="sdate"  class="sel1">
											<option value="">전체</option>
											<?
											$rst_mon = mysql_query( " select sdate from event_attendance group by sdate order by sdate desc" );
											while ( $row_mon = mysql_fetch_array( $rst_mon ) ) {
											?>
												<option value="<?=$row_mon["sdate"]?>" <?= ( $sdate == $row_mon["sdate"] ) ? " selected" : "" ?>><?=substr( $row_mon["sdate"] , 0 , 4 ) ?>년 <?=substr( $row_mon["sdate"] , -2 ) ?>월</option>
											<?}?>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
						</form>						
						<div class="btn_search" style="top:10px;"><a href="#" style="height:30px;line-height:30px;">검색</a></div>
					</div>
					<!-- //search -->
					
					<!-- button -->
					<div class="btn_box">
						<div class="btn_right"><a href="attend.php" class="btn_120b"><span>등록</span></a></div>
					</div>
					<!-- //button -->
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="gubun" value="<?=$gubun?>">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="mode" value="">
					<div class="table_typeA table_bg m20">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="60px"><col width="120px"><col width=""><col width="100px"><col width="100px"><col width=""><col width="100px"><col width="100px"></colgroup>
							<thead>
								<tr>
									<th><input type="checkbox" name="chkall" value="1" onclick="javascript:selall(this,document.regi_form.chkDel);"></th>
									<th>월</th>
									<th>제목</th>
									<th>총 참여자</th>
									<th>총 출석 횟수</th>
									<th>당첨자</th>
									<th>상태</th>
									<th>복사</th>
								</tr>
							</thead>
							<tbody>
							<?
							$z = 0;
							while ($row = mysql_fetch_array($rst)) {
								$rst_list = "";
								//총참여자
								$_row_1 = mysql_query("select count( distinct uid) as cnt  from event_attendance_data where event_idx='" . $row["idx"] . "' group by uid");
								$row_1["cnt"] = mysql_num_rows($_row_1);
								//$row_1 = getdata("select sum(count( distinct uid) ) as cnt  from event_attendance_data where event_idx='" . $row["idx"] . "' group by uid");
								//총 출석횟수
								$row_2 = getdata("select count(*) as cnt  from event_attendance_data where event_idx='" . $row["idx"] . "'");
								//당첨자
								for ( $i = 1; $i < 5 ; $i++ ) {
									if ($row["gift_day_".$i] != "" ) {
										$rst_list .= "<span style='line-height:20px;'><a href=\"#\" onclick='javascript:pop_rst(" . $row["idx"] . "," . $i . ");return false;'>".$row["gift_day_".$i]."일 출석 : <strong class=\"fc1\">" . $row["gift_cnt_".$i] . "</strong> 명</a></span> <br />";
									}
								}
								if ( $rst_list != "" ) {
									$rst_list = substr( $rst_list , 0 , -6 );
								}
								if ( $row["status"] == "0" && $row["sdate"] == date("Ym") ) {
										$stat_str="<span class='dev_stat_green'>진행중</span>";
								}else {
										$stat_str="<span class='dev_stat_red'>중지</span>";
								}
							?>
								<tr>
									<td><input type="checkbox" name="chkDel" value="<?=$row["idx"]?>"></td>
									<td><?=substr( $row["sdate"] , 0 , 4 ) ?>년 <?=substr( $row["sdate"] , -2 ) ?>월</td>
									<td><a href="./attend.php?idx=<?=$row["idx"]?>"><strong class="fc1"><?=substr( $row["sdate"] , 0 , 4 ) ?>년 <?=substr( $row["sdate"] , -2 ) ?>월 출석이벤트</strong></a></td>
									<td><?=number_format( $row_1["cnt"] )?></td>
									<td><?=number_format( $row_2["cnt"] )?></td>
									<td><?=$rst_list?></td>
									<td><?=$stat_str?></td>
									<td><a title="복사" class="btn_70" onclick="javascript:copy_data(<?=$row["idx"]?>);return false;" href="#"><span>복사</span></a></td>
								</tr>
							<?$z++;}
							
							if ($z == 0 ) {
								echo "<tr><td colspan='8'> 등록된 이벤트가 없습니다. </td></tr>";
							}?>
							</tbody>
						</table>
					</div>
					<div id='' style="padding-top:20px;line-height:25px;">
						<p> * <strong style="color:#1ea6b8;">해당 월이 아니면 진행중 상태여도 진행되지 않습니다.</strong></p>
						<p> * <strong style="color:#1ea6b8;">진행중지는 이벤트에 이상이 있거나 긴급한 사항이 있을때만 사용하시기 바랍니다.</p>
					</div>
					</form>
					<!-- paging -->
					<div class="paging">
					<? 
						if($cnt>0) {
							$prev_page=$pageIdx-1;
							$next_page=$pageIdx+1;							
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$prev_page."&".$get_query."\" class=\"prev\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a>" : "<a href=\"#\" class=\"prev\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a> ";
							if ($total_page<10) {
								$vpage=1;
							}else{
								$vpage = ( ( (int)( ($pageIdx - 1 ) / $page_set ) ) * $page_set ) + 1;
							}
							$spage = $vpage + $page_set - 1;
							if ($spage >= $total_page) $spage = $total_page;

							for($i=$vpage;$i<=$spage;$i++){ 
								if ($pageIdx==$i) {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\" class=\"current\"><span><strong>" . $i . "</strong></span></a> ";
								}else {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\"><span>" . $i . "</span></a> ";
								}
							}
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$next_page."&".$get_query."\" class=\"next\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a>" : "<a href=\"#\" class=\"next\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a> ";
						}?>
					</div>
					<!-- //paging -->

				</div>
				
			</div>
			
			<script type="text/javascript">
			<!--					
				$(function () {
					var f = document.getElementById('regi_form');
					$('.btn_search').click(function () {
						document.getElementById('search_frm').submit();
						return false;
					});
				});
				
				function selall(chekboxall,checkboxsub){
					var ischeck="";
					if (chekboxall.checked) {
						ischeck="checked";
					}
					if (checkboxsub.name !=undefined){
						checkboxsub.checked=ischeck;
					}else {
						for (i=0; i<checkboxsub.length; i++) {
							checkboxsub[i].checked =ischeck;
						}
					}
				}

				function CheckGroup(val){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName("chkDel");     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
						if(form.gid.value ==''){                                                 //선택이 하나도 안되어있을 경우
							alert("1건 이상 선택해 주세요.");       
							return false; 
						}
						if ( val != "" ) {
							form.mode.value =  "status_proc_attend";
						}else {
							alert("처리방법을 선택해 주세요.");
							form.multi_proc.focus(); 
							return false; 
						}
						document.regi_form.submit();
					} 
				}
				function copy_data(val) {
					var form=document.regi_form;
					if ( val != "" ) {
						form.gid.value =  val;
						form.mode.value =  "copy_attend";
					}else {
						alert("오류입니다. 새로고침 후 다시 시도 해주세요.");
						return false; 
					}
					document.regi_form.submit();					
				}
				
				function pop_rst(idx , val) {
					window.open("./pop_rst.php?idx=" + idx + "&val=" + val , "pop_rst" , "width=1200, height=800, top=0, left=0" );
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
$menu_id = "9";
$page_id = "4";
include "../include/header.php"; 

//search_setting
$sql_append = "";

$p_content = mysql_real_escape_string( $_GET["p_content"] );
$shop_id = mysql_real_escape_string( $_GET["shop_id"] );
$page = mysql_real_escape_string( $_GET["page"] );
if ($sort == "" ) {
	$sort = " idx desc ";
}
$get_query = "p_content=$p_content&shop_id=$shop_id";


if ( $idx != "" ) {
	$mode = "edit";
	$row = getdata("select * from board_event where idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 게시글 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}
}else {
	$mode = "regist";
}
?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/se/HuskyEZCreator.js" charset="utf-8"></script>
		
			<!-- leftmneu -->
			<? include "../include/left_event.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">이벤트 당첨자 발표</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc" enctype="multipart/form-data">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="idx" value="<?=$row["idx"]?>">
					<input type="hidden" name="mode" value="edit">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong><span>*</span>제목</strong></th>
									<td>
										<input type="text" name="title" class="ip5" value="<?=$row["title"]?>" title="" /> 
									</td>
								</tr>
								<tr>
									<th><strong>내용</strong></th>
									<td class="td_editor">
										<textarea name="content" class="se_editor" id="content" ><?=$row["content"]?></textarea>
									</td>
								</tr>
								<tr>
									<th><strong><span>*</span>상태</strong></th>
									<td>
										<select name="status">
											<option value="1" <?= ( $row["status"] == "1" ) ? " selected" : "" ?>>노출</option>
											<option value="0" <?= ( $row["status"] == "1" ) ? " selected" : "" ?>>미노출</option>
										</select>
									</td>
								</tr>
								<tr>
									<th style="height:400px;"><strong>댓글</strong></th>
									<td><iframe name="ifr_reply" src="./board_reply.php?idx=<?=$row["idx"]?>" width="100%" height="400" scrolling="auto" frameborder=0></iframe></td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
						<? if ( $mode == "edit" ) { ?>
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>
							<a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a>
						<?}else {?>
							<a href="#" class="btn_120b" id="updateBtn"><span>저장</span></a>							
						<?}?>
						</div>
					</div>
					<!-- //button -->

				</div>
				
			</div>
					
			<script type="text/javascript" src="../js/editor.js" charset="utf-8"></script>
			<script src="../js/jquery.filestyle.mini.js"></script>
			<script>
				$(function () {
					$("input.ip_file").filestyle({
						image: "../img/board/btn_search_f.gif",
						imageheight: 30,
						imagewidth: 80,
						marginleft: 0,
						width: 88
					});
					$('a.goback').click(function () {
						history.back();
						return false;
					});					
					$('#delBtn').click(function (){
						var conOk = confirm("정말 삭제 하시겠습니까?");
						if (conOk) {
							document.regi_form.mode.value = "board_del";
							document.regi_form.submit();
						}
					});
					$('#updateBtn').click(function (){
						updateContentsEditor('content');
						if ($('input[name=title]').val() == '') {
							alert('제목을 입력하세요.');
							return false;
						}
						if ($('textarea[name=content]').val() == '') {
							alert('내용을 입력하세요.');
							return false;
						}
						document.regi_form.submit();
					});
				});
				
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
$menu_id = "9";
$page_id = "2";
include "../include/header.php"; 

//search_setting
$sql_append = "";

$keyword = mysql_real_escape_string( $_GET["keyword"] );
$status = mysql_real_escape_string( $_GET["status"] );
$page = mysql_real_escape_string( $_GET["page"] );
$get_query = "keyword=$keyword&status=$status";

//공통
if ( $status			!= "" ) { $where .= " and status = '" . $status . "' "; }
if ( $keyword			!= "" ) { $where .= " and ( etitle like '%" . $keyword . "%' or econtent like '%" . $keyword . "%' ) "; }

$gcount=getdata( "select count(*) as cnt from event where del_ok='0' ".$where." " );
$cnt = $gcount["cnt"];

//페이징준비처리
	$pageIdx=1;	
	if ($page>0) {
		$pageIdx=$page;
	}
	$page_set = 15; //한페이지 줄수-기본값
	$block_size = 10;
	if($pageIdx % $block_size==0) {
		$start_num=$pageIdx-$block_size+1;
	}else {
		$start_num=floor($pageIdx/$block_size)*$block_size +1;
	}
	$end_num = $start_num+$block_size-1;
	$total_page = ceil($cnt / $page_set); // 총 페이지 수

	if($pageIdx==1) {
		$limit_idx=0;
	}else {
		$limit_idx=$pageIdx*$page_set-$page_set;
	}
//페이징준비 끝

$rst = mysql_query("select * from event where del_ok='0' ".$where." order by sort asc , idx desc limit ".$limit_idx.", ".$page_set);
?>			
			<!-- leftmneu -->
			<? include "../include/left_event.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">이벤트 관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox" style="width:1400px;">
					
					<!-- search -->
					<div class="board_search">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<input type="hidden" name="type" value="<?=$type?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="100px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>통합검색</strong></th>
									<td>
										<input type="text" name="keyword" id="keyword" value="<?=$keyword?>" class="" style="width:500px;" />
									</td>
								</tr>
								<tr>
									<th><strong>상태</strong></th>
									<td>
										<select  class="sel1" name="status">
											<option value=""<?= ( "" == $status ) ? " selected" : "" ?>>전체</option>
											<option value="0"<?= ( "0" == $status ) ? " selected" : "" ?>>진행</option>
											<option value="1"<?= ( "1" == $status ) ? " selected" : "" ?>>중지</option>
											<option value="2"<?= ( "2" == $status ) ? " selected" : "" ?>>마감</option>
									</td>
								</tr>
							</tbody>
						</table>
						</form>
						<div class="btn_search"><a href="#">검색</a></div>
					</div>
					<!-- //search -->
					
					<!-- button -->
					<div class="btn_box">
						<div class="btn_right"><a href="event_detail.php?page=<?=$page?>&<?=$get_query?>" class="btn_120b"><span>등록</span></a></div>
					</div>
					<!-- //button -->
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="mode" value="">
					<div class="table_typeA table_bg m20">
						<table cellpadding="0" cellspacing="0" border="1" summary="">

							<colgroup><col width="60px"><col width="120px"><col width=""><col width="120px"><col width="80px"><col width="80px"><col width="80px"><col width="80px"><col width="100px"><col width="100px"><col width="100px"></colgroup>
							<thead>
								<tr>
									<th><input type="checkbox" name="chkall" value="1" onclick="javascript:selall(this,document.regi_form.chkDel);"></th>
									<th>이미지</th>
									<th>이벤트 제목</th>
									<th>기간</th>
									<th>덧글</th>
									<th>좋아요</th>
									<th>공유</th>
									<th>상태</th>
									<th>우선순위</th>
									<th>등록일</th>
									<th>조회</th>
								</tr>
							</thead>
							<tbody>
							<? $i = 0;
								while ( $row = mysql_fetch_array( $rst ) ) {
									if ( $row["status"] == "0" ) {
										$stat_str = "<span class='dev_stat_green'>진행중</span>";
									}elseif ( $row["status"] == "1" ) {
										$stat_str = "<span class='dev_stat_red'>중지</span>";
									}elseif ( $row["status"] == "2" ) {
										$stat_str = "<span class='dev_stat_gray'>마감</span>";
									}
									$reply = getdata( " select count(*) as cnt from event_reply where event_idx='".$row["idx"]."' and del_ok='0' " );
							?>	
								<tr>
									<td><input type="checkbox" name="chkDel" value="<?=$row["idx"]?>"></td>
									<td><img src='/data/event/<?=$row["tumb"]?>' border='0' alt='' width="100"></td>
									<td class="td_left"><a href="./event_detail.php?idx=<?=$row["idx"]?>&page=<?=$page?>&<?=$get_query?>"><strong class="fc1"><?=$row["etitle"]?></strong></a></td>
									<td><?=substr( $row["sdate"] , 0 , 4 ) . "-" . substr( $row["sdate"] , 4 , 2 ) . "-" . substr( $row["sdate"] , 6 , 2 )?> ~ <?=substr( $row["edate"] , 0 , 4 ) . "-" . substr( $row["edate"] , 4 , 2 ) . "-" . substr( $row["edate"] , 6 , 2 )?></td>
									<td><?=$reply["cnt"]?></td>
									<td><?=$row["likeit"]?></td>
									<td><?=$row["share"]?></td>
									<td><?=$stat_str?></td>
									<td><?=$row["sort"]?></td>
									<td><?=str_replace( "-" , "." , substr( $row["reg_date"] , 0 , 10 ) ) ."<br />" . substr( $row["reg_date"] , 10 , 10 ) ?></td>
									<td><?=$row["visited"]?></td>
								</tr>
							<?$i++;}
							if ($i == 0 ) {
								echo "<tr><td colspan='11'>등록된 이벤트가 없습니다.</td></tr>";
							}
							?>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<div class="btn_process">
								<select name="multi_proc">
									<option value="">선택</option>
									<option value="0">진행중</option>
									<option value="1">중지</option>
									<option value="2">마감</option>
									<option value="del">삭제</option>
								</select>
								<a href="#" onclick="javascript:CheckGroup(document.regi_form.multi_proc.value);"><span>선택항목 처리</span></a>
							</div>
						</div>
						<div class="btn_right"><a href="event_detail.php?page=<?=$page?>&<?=$get_query?>" class="btn_120b"><span>등록</span></a></div>
					</div>
					<!-- //button -->
					</form>
								
					<!-- paging -->
					<div class="paging">
					<? 
						if($cnt>0) {
							$prev_page=$pageIdx-1;
							$next_page=$pageIdx+1;							
							echo ($pageIdx>1)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$prev_page."&".$get_query."\" class=\"prev\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a>" : "<a href=\"#\" class=\"prev\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_prev.gif\" alt=\"이전\" /></a> ";
							if ($total_page<10) {
								$vpage=1;
							}else{
								$vpage = ( ( (int)( ($pageIdx - 1 ) / $block_size ) ) * $block_size ) + 1;
							}
							$spage = $vpage + $block_size - 1;
							if ($spage >= $total_page) $spage = $total_page;

							for($i=$vpage;$i<=$spage;$i++){ 
								if ($pageIdx==$i) {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\" class=\"current\"><span><strong>" . $i . "</strong></span></a> ";
								}else {
									echo "<a href=\"" . $_SERVER["PHP_SELF"] . "?page=" . $i . "&" . $get_query . "\"><span>" . $i . "</span></a> ";
								}
							}
							echo ($i <= $total_page)? "<a href=\"".$_SERVER["PHP_SELF"]."?page=".$next_page."&".$get_query."\" class=\"next\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a>" : "<a href=\"#\" class=\"next\" onclick=\"javascript:return false;\"><img src=\"../img/board/btn_next.gif\" alt=\"다음\" /></a> ";
						}?>
					</div>
					<!-- //paging -->

				</div>
				
			</div>
			
			<script type="text/javascript">
			<!--					
				$(function () {
					var f = document.getElementById('regi_form');
					$('.btn_search').click(function () {
						document.getElementById('search_frm').submit();
						return false;
					});
				});
				
				function selall(chekboxall,checkboxsub){
					var ischeck="";
					if (chekboxall.checked) {
						ischeck="checked";
					}
					if (checkboxsub.name !=undefined){
						checkboxsub.checked=ischeck;
					}else {
						for (i=0; i<checkboxsub.length; i++) {
							checkboxsub[i].checked =ischeck;
						}
					}
				}

				function CheckGroup(val){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName("chkDel");     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
						if(form.gid.value ==''){                                                 //선택이 하나도 안되어있을 경우
							alert("1건 이상 선택해 주세요.");       
							return false; 
						}
						if ( val != "" ) {
							form.mode.value =  "status_proc";
						}else {
							alert("처리방법을 선택해 주세요.");
							form.multi_proc.focus(); 
							return false; 
						}
						if ( val == "del") {
							var con = confirm("정말로 삭제하시겠습니까? \n삭제후 복구가 불가능합니다.");
							if ( con ) {
								document.regi_form.submit();
							}
						}else {
							document.regi_form.submit();
						}
					} 
				}
			//-->
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
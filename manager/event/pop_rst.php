<?
include ("../config.php");

$idx = mysql_real_escape_string( $_GET["idx"]);
$val = mysql_real_escape_string( $_GET["val"] );
$keyword = mysql_real_escape_string( $_GET["keyword"] );
$event = getdata(" select * from event_attendance where idx='$idx'");

if ( $keyword	!= "" ) { $where .= " and ( B.id like '%" . $keyword . "%' or B.uname like '%" . $keyword . "%' or B.phone like '%" . $keyword . "%' or B.email like '%" . $keyword . "%' )"; }

$rst = mysql_query("select A.* , B.uname, B.id, B.phone, B.email from event_attendance_data as A left join users as B on A.uid=B.idx where A.event_idx='$idx' and A.rst='$val' ".$where." order by A.idx desc");

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title>이벤트 당첨자 보기</title>

	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" type="text/css" href="../css/ui/jquery-ui-1.10.1.css">

	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<script type="text/javascript" src="/js/validation.js"></script>

	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
</head>
<body class="bgNO">

	<div class="popup_box">

		<div class="titbox">
			<p class="t"><strong class="fc1"><?=substr( $event["sdate"] , 0 , 4 ) ?>년 <?=substr( $event["sdate"] , -2 ) ?>월 출석이벤트(<?=$event["gift_day_".$val]?>일 연속 출석 )</strong></p>
		</div>
		<div class="popbody">
					<!-- search -->
					<div class="board_search">
						<form name='search_frm' id="search_frm" method="get" action="<?=$_SERVER['PHP_SELF']?>">
						<input type="hidden" name="idx" value="<?=$idx?>">
						<input type="hidden" name="val" value="<?=$val?>">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="95px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>통합검색</strong></th>
									<td><input type="text" name="keyword" value="<?=$keyword?>" class="ip3" /></td>
								</tr>
							</tbody>
						</table>
						</form>
						<div class="btn_search" style="top:10px;"><a href="#" style="height:30px;line-height:30px;" onclick="javascript:document.search_frm.submit();">검색</a></div>
					</div>
					<!-- //search -->
					
			<div class="scrollbox" style="padding-top:20px;height:450px;">
				<div class="table_typeA">
					<table cellpadding="0" cellspacing="0" border="1" summary="">
						<colgroup><col width=""><col width=""><col width=""><col width=""><col width=""></colgroup>
						<thead>
							<tr>
								<th class="text-center active vertical_50">회원ID</th>
								<th class="text-center active vertical_50">회원명</th>
								<th class="text-center active vertical_50">연락처</th>
								<th class="text-center active vertical_50">이메일</th>
								<th class="text-center active vertical_50">출석완료일</th>
							</tr>
						</thead>
						<tbody>
							<? while ($row = mysql_fetch_array($rst)) { 
							
								if ( $row["gift_rst"] == "1" ) {
										$stat_str="<a href='./_proc.php?mode=gift_rst&stat=0&idx=".$row["idx"]."&eidx=$idx&val=$val'><span class='dev_stat_green'>지급완료</span></a>";
								}else {
										$stat_str="<a href='./_proc.php?mode=gift_rst&stat=1&idx=".$row["idx"]."&eidx=$idx&val=$val'><span class='dev_stat_red'>미지급</span></a>";
								}
							?>
							<tr>
								<td><?=str_replace( $keyword , "<strong class=\"fc1\">$keyword</strong>" , $row["id"] )?></td>
								<td><?=str_replace( $keyword , "<strong class=\"fc1\">$keyword</strong>" , $row["uname"] )?></td>
								<td><?=str_replace( $keyword , "<strong class=\"fc1\">$keyword</strong>" , $row["phone"] )?></td>
								<td><?=str_replace( $keyword , "<strong class=\"fc1\">$keyword</strong>" , $row["email"] )?></td>
								<td><?=$row["sdate"]?></td>
							</tr>
							<?$i++;}
							if ($i == 0 ) {
								echo "<tr><td colspan='5'>당첨자가 없습니다.</td></tr>";
							}?>
						</tbody>
					</table>
				</div>
				</form>
				
			</div>

			<!-- button -->
			<div class="btn_box">
				<div class="btn_right"><a href="#" onclick="javascript:self.close();" class="btn_120b"><span>닫기</span></a></div>
			</div>
			<!-- //button -->
			<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
			<script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
			<script>
				$(function () {
					var f = $(document.forms["regi_form"]);

					$('a#updateBtn')
					.css('cursor', 'pointer')
					.click(function () {
						if ($('#cost', f).val() == "" ) {
							alert("추가배송비를 입력하세요");
							$('#cost', f).focus();
						}else {
							f.submit();
						}
					});
				});
			</script>
		</div>

	</div>

</body>
</html>
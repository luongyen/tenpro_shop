<?
$menu_id = "9";
$page_id = "2";
include "../include/header.php"; 

$idx = mysql_real_escape_string( $idx );
$keyword = mysql_real_escape_string( $_GET["keyword"] );
$status = mysql_real_escape_string( $_GET["status"] );
$page = mysql_real_escape_string( $_GET["page"] );

if ( $idx != "" ) {
	$row = getdata("select * from event where idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 이벤트 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}else {
		$sdate = substr( $row["sdate"] , 0 , 4 ) . "-" . substr( $row["sdate"] , 4 , 2 ) . "-" . substr( $row["sdate"] , 6 , 2 );
		$edate = substr( $row["edate"] , 0 , 4 ) . "-" . substr( $row["edate"] , 4 , 2 ) . "-" . substr( $row["edate"] , 6 , 2 );
	}
}

if ( $sdate == "" ) {
	$sdate = date("Y-m-d");
}
if ( $edate == "" ) {
	$edate = date("Y-m-d" , strtotime("+7 day"));
}
?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/se/HuskyEZCreator.js" charset="utf-8"></script>

			<!-- leftmneu -->
			<? include "../include/left_event.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">이벤트 관리</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox" style="width:1000px;">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" enctype="multipart/form-data">
					<input type="hidden" name="idx" value="<?=$idx?>">
					<input type="hidden" name="gid" value=""><!--적용매체-->
					<input type="hidden" name="mode" value="event_regist">
					<input type="hidden" name="skeyword" value="<?=$keyword?>">
					<input type="hidden" name="sstatus" value="<?=$status?>">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="120px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong>제목</strong></th>
									<td><input type="text" name="etitle" value="<?=$row["etitle"]?>" class="ip1" style="width:90%;" maxlength="20"></td>
								</tr>
								<tr>
									<th><strong>기간</strong></th>
									<td>
										<input type="text" name="sdate" class="ip1" style="width:100px;" id="sdate" value="<?=$sdate ?>" data-date="<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>"  /> ~ 
										<input type="text" name="edate" class="ip1" style="width:100px;" id="edate" value="<?=$edate ?>" data-date="<?=date("Y-m-d")?>" />
									</td>
								</tr>
								<tr>
									<th><strong>상태</strong></th>
									<td>
										<select name="status" class="sel4" id="status">
											<option value="">선택</option>
											<option value="0" <?= ( $row["status"] == "0" ) ? "selected" : "" ?>>진행중</option>
											<option value="1" <?= ( $row["status"] == "1" ) ? "selected" : "" ?>>중지</option>
											<option value="2" <?= ( $row["status"] == "2" ) ? "selected" : "" ?>>마감</option>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>우선순위</strong></th>
									<td>
										<select name="sort" class="sel4" id="sort">
											<? for ( $i = 1 ; $i < 100 ; $i++ ) { ?>
											<option value="<?=$i?>" <?= ( ( $row["sort"] == "" && $i == 99 ) || $row["sort"] == $i ) ? "selected" : "" ?>><?=$i?></option>
											<?}?>
										</select>
									</td>
								</tr>
								<tr>
									<th><strong>썸네일 이미지</strong></th>
									<td><?=( $row["tumb" ] != "" ) ? "<img src='/data/event/".$row["tumb"] . "' width='300' />" : "" ?> <div class="filebox"><input type="file" name="tumb" class="ip_file"></div></td>
								</tr>
								<tr>
									<th><strong>내용</strong></th>
									<td class="td_editor">
										<textarea name="econtent" class="se_editor" id="econtent" ><?=$row["econtent"]?></textarea>
									</td>
								</tr>
								<tr>
									<th style="height:400px;"><strong>댓글</strong></th>
									<td><iframe name="ifr_reply" src="./reply.php?idx=<?=$row["idx"]?>" width="100%" height="400" scrolling="auto" frameborder=0></iframe></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<!--a href="#" class="btn_120bk"><span>삭제</span></a>
							<a href="#" class="btn_120w"><span>수정</span></a-->
							<?if ($mode != "event_regist" ) {?>
							<a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a>
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>							
							<?}else {?>
							<a href="#" class="btn_120b" id="updateBtn"><span>등록</span></a>								
							<?}?>
						</div>
					</div>
					<!-- //button -->
					</form>
				</div>
			</div>
				
		
			<script type="text/javascript" src="../js/editor.js" charset="utf-8"></script>
			<script src="../js/jquery.filestyle.mini.js"></script>
			<script>
				$(function () {
					$('a.goback').click(function () {
						history.back();
						return false;
					});					
					$('#delBtn').click(function (){
						var conOk = confirm("정말 삭제 하시겠습니까?");
						if (conOk) {
							document.regi_form.mode.value = "event_delete_attend";
							document.regi_form.submit();
						}
					});			

					
					$('#updateBtn').click(function (){
						updateContentsEditor('econtent');
						if ($('input[name=sdate]').val() == '' || $('input[name=edate]').val() == '') {
							alert('기간을 입력하세요.');
							return false;
						}
						if ($('#status option:selected').val() == '') {
							alert('상태를 선택하세요.');
							return false;
						}
						<? if ($idx == "" ) {?>
						if ($('input[name=tumb]').val() == '') {
							alert('썸네일 이미지를 입력하세요.');
							return false;
						}
						<?}?>
						if ($('input[name=etitle]').val() == '') {
							alert('제목을 입력하세요.');
							return false;
						}
						if ($('input[name=econtent]').val() == '') {
							alert('내용을 입력하세요.');
							return false;
						}
						document.regi_form.submit();
					});
				});
				
				function selall(chekboxall,checkboxsub){
					var ischeck="";
					if (chekboxall.checked) {
						ischeck="checked";
					}
					if (checkboxsub.name !=undefined){
						checkboxsub.checked=ischeck;
					}else {
						for (i=0; i<checkboxsub.length; i++) {
							checkboxsub[i].checked =ischeck;
						}
					}
				}

				function CheckGroup(fr){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName(fr);     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
					} 
				}
				$(document).ready(function(){
					$('#sdate').datepicker().on('changeDate', function(e) {
						$("#sdate").datepicker('hide');
					});
					$('#edate').datepicker().on('changeDate', function(e) {
						$("#edate").datepicker('hide');
					});

					var today_val = "<?=date("Y-m-d")?>";
					var week_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")  , date("d")-7, date("Y")))?>";
					var mon_val = "<?=date("Y-m-d" , mktime (0,0,0,date("m")-1  , date("d"), date("Y")))?>";

					$(".sel_today").on("click",function(){
						$("#sdate").val(today_val);
						$("#edate").val(today_val);
					});
					$(".sel_week").click(function(){
						$("#sdate").val(week_val);
						$("#edate").val(today_val);
					});
					$(".sel_month").click(function(){
						$("#sdate").val(mon_val);
						$("#edate").val(today_val);
					});
						
				});

			</script>
			
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
$menu_id = "8";
$page_id = "4";
include "../include/header.php"; 

//search_setting
$sql_append = "";

$p_content = mysql_real_escape_string( $_GET["p_content"] );
$status = mysql_real_escape_string( $_GET["status"] );
$page = mysql_real_escape_string( $_GET["page"] );
$get_query = "p_content=$p_content&status=$status";

if ( $idx != "" ) {
	$mode = "edit";
	$row = getdata("select * from board_cs where idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 게시글 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}
	@mysql_query("update board_cs set visited=visited+1 where idx='$idx'");
}else {
	$mode = "regist";
}
?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/se/HuskyEZCreator.js" charset="utf-8"></script>
		
			<!-- leftmneu -->
			<? include "../include/left_community.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">CS게시판</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc" enctype="multipart/form-data">
					<input type="hidden" name="gid" value="">
					<input type="hidden" name="idx" value="<?=$row["idx"]?>">
					<input type="hidden" name="mode" value="cs_regist">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""></colgroup>
							<tbody>
								<tr>
									<th><strong><span>*</span>제목</strong></th>
									<td>
										<input type="text" name="title" class="ip5" value="<?=$row["title"]?>" title="" /> 
									</td>
								</tr>
								<tr>
									<th><strong>내용</strong></th>
									<td class="td_editor">
										<textarea name="content" class="se_editor" id="content" ><?=$row["content"]?></textarea>
									</td>
								</tr>
								<tr>
									<th><strong>상태</strong></th>
									<td>
										<select name="status" id="status" class="sel1">
											<option value="0" <?= ( $row["status"] == "0" ) ? " selected" : "" ?>> 접수 </option>
											<option value="1" <?= ( $row["status"] == "1" ) ? " selected" : "" ?>> 처리중 </option>
											<option value="2" <?= ( $row["status"] == "2" ) ? " selected" : "" ?>> 처리완료 </option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="#" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
						<? if ( $mode == "edit" ) { ?>
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>
							<a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a>
						<?}else {?>
							<a href="#" class="btn_120b" id="updateBtn"><span>저장</span></a>							
						<?}?>
						</div>
					</div>
					<!-- //button -->

				</div>
				
			</div>
					
			<script type="text/javascript" src="../js/editor.js" charset="utf-8"></script>
			<script src="../js/jquery.filestyle.mini.js"></script>
			<script>
				$(function () {
					$("input.ip_file").filestyle({
						image: "../img/board/btn_search_f.gif",
						imageheight: 30,
						imagewidth: 80,
						marginleft: 0,
						width: 88
					});
					$('a.goback').click(function () {
						history.back();
						return false;
					});					
					$('#delBtn').click(function (){
						var conOk = confirm("정말 삭제 하시겠습니까?");
						if (conOk) {
							document.regi_form.mode.value = "board_del";
							document.regi_form.submit();
						}
					});
					$('#updateBtn').click(function (){
						<?if ($category == "QA" ) {?>
						if ($('input[name=title_ans]').val() == '') {
							alert('답변 제목을 입력하세요.');
							return false;
						}
						if ($('textarea[name=content_ans]').val().length < 10 ) {
							alert('답변내용을 입력하세요.');
							return false;
						}
						<?}else {?>
						updateContentsEditor('content');
						if ($('select[name=shop_id]').val() == '') {
							alert('매체를 선택하세요.');
							return false;
						}
						if ($('input[name=title]').val() == '') {
							alert('제목을 입력하세요.');
							return false;
						}
						if ($('textarea[name=content]').val() == '') {
							alert('내용을 입력하세요.');
							return false;
						}
						<?}?>
						document.regi_form.submit();
					});
				});
				
				function selall(chekboxall,checkboxsub){
					var ischeck="";
					if (chekboxall.checked) {
						ischeck="checked";
					}
					if (checkboxsub.name !=undefined){
						checkboxsub.checked=ischeck;
					}else {
						for (i=0; i<checkboxsub.length; i++) {
							checkboxsub[i].checked =ischeck;
						}
					}
				}

				function CheckGroup(){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName("chkDel");     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
					} 
				}
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
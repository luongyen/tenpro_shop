<?
$menu_id = "8";
$page_id = "5";
include "../include/header.php"; 

//search_setting
$sql_append = "";

$p_content = mysql_real_escape_string( $_GET["p_content"] );
$wname = mysql_real_escape_string( $_GET["wname"] );
$Btitle = mysql_real_escape_string( $_GET["Btitle"] );
$uid = mysql_real_escape_string( $_GET["uid"] );
$page = mysql_real_escape_string( $_GET["page"] );
if ($sort == "" ) {
	$sort = " idx desc ";
}
$get_query = "p_content=$p_content&wname=$wname&Btitle=$Btitle&uid=$uid";

if ( $idx != "" ) {
	$mode = "edit";
	$row = getdata("select A.*, B.title as Btitle, B.thumb from goods_qna as A left join goods as B ON A.no=B.no where A.idx ='$idx' ");
	if ( $row["idx"] == "" ) {
		echo "<script>window.alert(' 게시글 정보 오류입니다. 다시 시도 해주세요. 오류가 계속 될 시 관리자에게 문의 하세요');history.back();</script>";
		exit;	
	}
}else {
	$mode = "regist";
}
?>
<!--
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/se/HuskyEZCreator.js" charset="utf-8"></script>
-->
		
			<!-- leftmneu -->
			<? include "../include/left_community.php"; ?>
			<!-- //leftmneu -->
			
			<div id="contents">
				
				<!-- title -->
				<div class="titbox">
					<h2 class="title">상품문의</h2>
				</div>
				<!-- //title -->
				
				<div class="contbox">
					
					<form action="_proc.php" method="post" name="regi_form" id="regi_form" target="ifr_proc" enctype="multipart/form-data">
					<input type="hidden" name="idx" value="<?=$row["idx"]?>">
					<input type="hidden" name="mode" value="product_edit">
					<div class="table_typeB">
						<table cellpadding="0" cellspacing="0" border="1" summary="">
							<colgroup><col width="180px"><col width=""><col width="180px"><col width="325px"></colgroup>
							<tbody>
								<tr>
									<th><strong>상품정보 </strong></th>
									<td><a href="../product/product_detail.php?no=<?=$row["no"]?>" target="_blank"><img src='<?=$row["thumb"]?>' border='0' alt='' style="width:200px;"><br /><br /><b><?=$row["Btitle"]?></b></a></td>
								</tr>
								<tr>
									<th><strong>제목</strong></th>
									<td><?=$row["title"]?><?= ( $row["sec"] == "1" ) ? "&nbsp;&nbsp;<img src=\"../../assets/images/icon_sec1.png\" border=\"0\" alt=\"\" style=\"width:12px;vertical-align:bottom;\" />" : "" ?></td>
								</tr>
								<tr>
									<th><strong>작성자</strong></th>
									<td><?=$row["wname"]?></td>
								</tr>
								<tr>
									<th><strong>내용</strong></th>
									<td class="td_editor"><?=nl2br( $row["content"] )?></td>
								</tr>								
								<tr>
									<th><strong><span>*</span>답변</strong></th>
									<td>
										<textarea name="ans_cont" id="ans_cont" class="text txt inputplus_s" cols="" rows="80" style="width:350px;height:195px;"><?=$row["ans_content"]?></textarea>
									</td>
								</tr>								
								<tr>
									<th><strong>답변완료 시각</strong></th>
									<td><?=$row["ans_date"]?></td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<!-- button -->
					<div class="btn_box m20">
						<div class="btn_left">
							<a href="./product.php?page=<?=$page?>&<?=$get_query?>" class="btn_120w goback"><span class="list">목록</span></a>
						</div>
						<div class="btn_right">
							<a href="#" class="btn_120b" id="updateBtn"><span>수정</span></a>
							<a href="#" class="btn_120bk" id="delBtn"><span>삭제</span></a>
						</div>
					</div>
					<!-- //button -->

				</div>
				
			</div>
					
			<script type="text/javascript" src="../js/editor.js" charset="utf-8"></script>
			<script src="../js/jquery.filestyle.mini.js"></script>
			<script>
				$(function () {
					$("input.ip_file").filestyle({
						image: "../img/board/btn_search_f.gif",
						imageheight: 30,
						imagewidth: 80,
						marginleft: 0,
						width: 88
					});
					$('a.goback').click(function () {
						history.back();
						return false;
					});					
					$('#delBtn').click(function (){
						var conOk = confirm("정말 삭제 하시겠습니까?");
						if (conOk) {
							document.regi_form.mode.value = "goods_qna_del";
							document.regi_form.submit();
						}
					});
					$('#updateBtn').click(function (){
						if ($('#ans_cont').val() == '') {
							alert('답변내용을 입력하세요.');
							return false;
						}else {
							document.regi_form.submit();
						}
					});
				});
				
				function selall(chekboxall,checkboxsub){
					var ischeck="";
					if (chekboxall.checked) {
						ischeck="checked";
					}
					if (checkboxsub.name !=undefined){
						checkboxsub.checked=ischeck;
					}else {
						for (i=0; i<checkboxsub.length; i++) {
							checkboxsub[i].checked =ischeck;
						}
					}
				}

				function CheckGroup(){
					var form=document.regi_form;
					var i; 
					var nChk = document.getElementsByName("chkDel");     //체크되어있는 박스 value값
					form.gid.value ='';
					if(nChk){
						for(i=0;i<nChk.length;i++) { 
							if(nChk[i].checked){                                                            //체크되어 있을경우 
								if(form.gid.value ==''){
									form.gid.value = nChk[i].value; 
								}else{
									form.gid.value =  form.gid.value+ ',' +nChk[i].value;   //구분자로 +=
								}
							} 
						}
					} 
				}
			</script>
			<!-- footer -->
			<? include "../include/footer.php"; ?>
			<!-- //footer -->
<?
include ("../config.php");
$allowedExts = array("jpg", "png", "gif", "jpeg");
$temp = explode(".", $_FILES["image"]["name"]);
$extension = end($temp);
if ( ( ($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/png" ) ) && in_array($extension, $allowedExts) ) {
    if ($_FILES["image"]["error"] > 0) {
        $err = "Error en Upload. Return Code: " . $_FILES["image"]["error"] . "<br>";
    } else {
		$weburl = SSH_UPLOAD_( $_FILES["image"] );
    }
    echo $weburl;
} else {
	header("HTTP/1.1 500 Internal Server Error");
}

?>

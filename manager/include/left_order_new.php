			
			<div id="leftcont">
				<h3 class="tit">주문관리</h3>
				<ul>
					<li>=======발주전=======</li>
					<li><a href="../order/payment.php" <?= ( strpos( $_SERVER["PHP_SELF"] , "payment.php" ) !== false && $status != "1" && $status != "2" ) ? " class=\"on\"" : "" ?>>결재관리</a></li>
					<li><a href="../order/payment.php?status=1" <?= ( strpos( $_SERVER["PHP_SELF"] , "payment.php" ) !== false && $status == "1" ) ? " class=\"on\"" : "" ?>>미입금확인</a></li>
					<li><a href="../order/payment.php?status=2" <?= ( strpos( $_SERVER["PHP_SELF"] , "payment.php" ) !== false && $status == "2" ) ? " class=\"on\"" : "" ?>>신규주문</a></li>
					<li></li>
					<li>=======발주후=======</li>
					<li><a href="../order/" <?= ( strpos( $_SERVER["PHP_SELF"] , "index.php" ) !== false ) ? " class=\"on\"" : "" ?>>전체주문조회</a></li>
					<li><a href="../order/?status=WAITPAY" <?= ( $status == "WAITPAY" ) ? " class=\"on\"" : "" ?>>입금예정</a></li>
					<li><a href="../order/?status=WAITCONFIRM" <?= ( $status == "WAITCONFIRM" ) ? " class=\"on\"" : "" ?>>승인대기</a></li>
					<li><a href="../order/?status=WAITDELI" <?= ( $status == "WAITDELI" ) ? " class=\"on\"" : "" ?>>발송예정</a></li>
					<li><a href="../order/?status=WAITOK" <?= ( $status == "WAITOK" ) ? " class=\"on\"" : "" ?>>배송중</a></li>
					<li><a href="../order/?status=DONE" <?= ( $status == "DONE" ) ? " class=\"on\"" : "" ?>>구매종료</a></li>
					<li><a href="../order/?status=DENYCONFIRM" <?= ( $status == "DENYCONFIRM" ) ? " class=\"on\"" : "" ?>>승인취소</a></li>
					<li><a href="../order/?status=DENYBUY" <?= ( $status == "DENYBUY" ) ? " class=\"on\"" : "" ?>>구매취소</a></li>
					<li><a href="../order/?status=DENYSELL" <?= ( $status == "DENYSELL" ) ? " class=\"on\"" : "" ?>>판매취소</a></li>
					<li></li>
					<li>=======기타=======</li>
					<li><a href="../order/cash_new.php" <?= ( $page_id == "18" ) ? " class=\"on\"" : "" ?>>현금영수증</a></li>
					<li><a href="../order/old.php" <?= ( strpos( $_SERVER["PHP_SELF"] , "old.php" ) !== false ) ? " class=\"on\"" : "" ?>>이전주문조회</a></li>
				</ul>
			</div>
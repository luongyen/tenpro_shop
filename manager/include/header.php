<?
include ("../config.php");
if( $_SESSION['yi_level'] != "99" ) {
	header ("location: ../index.php");
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1460">
	<title><?=$admin_title?></title>	
	<link rel="stylesheet" type="text/css" href="../css/style.css?v=0.14" />
	<link rel="stylesheet" type="text/css" href="../css/filestyle.css?v=0.14" />
	<script src="https://use.fontawesome.com/609a5eb453.js"></script> 
	<link rel='stylesheet' href='//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'>
	<link href="/manager/js/summernote-master/dist/summernote-lite.css" rel="stylesheet">
	  <style type="text/css" title="">
			.mypageBtn {top: -10px; left:50px; width: 27%; height: 40px; line-height: 40px;position:absolute;}
			.mypageBtn a {background:#41b6c4;  font-size: 14px; color: #fff;}
			.toppointcountB{position:absolute; display:inline-block; background:#ffffff; border-radius:50%; width:20px; height:20px; line-height:22px; text-align:center; color:#41b6c4; font-weight:bold; font-size:14px;}
	  </style>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
	<script src="https://use.fontawesome.com/609a5eb453.js"></script> 
	<script src="../js/jquery-ui.js"></script>
	<script src="../js/common.js"></script>
	<!--[if lt IE 9]><script src="../js/html5shiv.js"></script><![endif]-->
	<!-- script src="/manager/js/summernote-lite.js"></script-->
</head>
<body>	
	<div class="skipmenu">
		<dl>
			<dt class="hidden">바로가기 메뉴</dt>
			<dd><a href="#gnb" class="skip_link">메뉴바로가기</a></dd>
			<dd><a href="#container" class="skip_link">본문바로가기</a></dd>
		</dl>
	</div>	
	<!-- wrap -->
	<div id="wrap">		
		<!-- header -->
		<div id="header">			
			<div class="topbox">
				<h1><a href="#"><img src="../img/common/img_logo.gif" alt="Website Management System" /></a></h1>			
				<div class="topmenu">
					<ul>
						<li><a href="/" class="sitehome">사이트 바로가기</a></li>
						<li><a href="../logout.php" class="logout">로그아웃</a></li>
					</ul>
				</div>
			</div>				
			<!-- gnb -->
			<div id="gnb">
				<ul>
					<li><a href="../basis/" <?= ( $menu_id == "1" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb1.png" alt="" />기본관리</a></li>
					<li><a href="../account/" <?= ( $menu_id == "3" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb2.png"  alt="" />회원관리</a></li>
					<!--<li><a href="../story/" <?= ( $menu_id == "16" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb8.png" alt="" />MD추천</a></li>
					<li><a href="../account/reseller_list.php" <?= ( $menu_id == "12" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb6.png" alt="" />리셀러신청</a></li-->
					<li><a href="../product/product_list.php" <?= ( $menu_id == "4" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb9.png" alt="" />상품관리</a></li>
					<li><a href="../order/payment.php?status=2" <?= ( $menu_id == "11" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb5.png" alt="" />주문관리</a></li>
					<li><a href="../event/" <?= ( $menu_id == "9" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb3.png" alt="" />이벤트관리</a></li>
					<li><a href="../stat/" <?= ( $menu_id == "6" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb4.png" alt="" />통계</a></li>
					<li><a href="../push/" <?= ( $menu_id == "7" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb7.png" alt="" />PUSH관리</a></li>
					<!--li><a href="../push/" <?= ( $menu_id == "10" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb7.png" alt="" />PUSH관리</a></li-->
					<li><a href="../community/product.php" <?= ( $menu_id == "8" ) ? " class=\"on\"" : "" ?>><img src="../img/common/icon_gnb6.png" alt="" />고객센터</a></li>
				</ul>
			</div>
			<!-- //gnb -->			
		</div>
		<!-- //header -->		
		<!-- container -->
		<div id="container">
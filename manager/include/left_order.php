<?
if ( strpos( $_SERVER["PHP_SELF"] , "/order/payment.php" ) !== false || strpos( $_SERVER["PHP_SELF"] , "/order/payment_detail.php" ) !== false ) {
	$submenuId = "1";
}elseif ( strpos( $_SERVER["PHP_SELF"] , "/order/index.php" ) !== false || strpos( $_SERVER["PHP_SELF"] , "/order/detail.php" ) !== false ) {
	$submenuId = "2";
}else {
	$submenuId = "3";	
}
?>
			<div id="leftcont">
				<h3 class="tit">주문관리</h3>
				<ul>
					<li>===== 발주전 =====</li>
					<li><a href="../order/payment.php" <?= ( $submenuId == "1" && $status == "" ) ? " class=\"on\"" : "" ?>>전체주문</a></li>
					<li><a href="../order/payment.php?status=1" <?= ( $submenuId == "1" && $status == "1" ) ? " class=\"on\"" : "" ?>>미입금확인</a></li>
					<li><a href="../order/payment.php?status=2" <?= ( $submenuId == "1" && $status == "2" ) ? " class=\"on\"" : "" ?>>신규주문</a></li>
					<li><a href="../order/payment.php?status=3" <?= ( $submenuId == "1" && $status == "3" ) ? " class=\"on\"" : "" ?>>발주완료</a></li>
					<li><a href="../order/payment.php?status=-1" <?= ( $submenuId == "1" && $status == "-1" ) ? " class=\"on\"" : "" ?>>발주실패</a></li>
					<li><a href="../order/payment.php?status=61" <?= ( $submenuId == "1" && $status == "61" ) ? " class=\"on\"" : "" ?>>주문취소</a></li>
					<li>===== 발주후 =====</li>
					<li><a href="../order/" <?= ( $submenuId == "2" && $status == "" ) ? " class=\"on\"" : "" ?>>전체주문조회</a></li>
					<li><a href="../order/?status=WAITCHK" <?= ( $submenuId == "2" && $status == "WAITCHK" ) ? " class=\"on\"" : "" ?>>발주완료</a></li>
					<li><a href="../order/?status=WAITDELI" <?= ( $submenuId == "2" && $status == "WAITDELI" ) ? " class=\"on\"" : "" ?>>발송예정</a></li>
					<li><a href="../order/?status=WAITOK" <?= ( $submenuId == "2" && $status == "WAITOK" ) ? " class=\"on\"" : "" ?>>배송중</a></li>
					<li><a href="../order/?status=WAITRCPT" <?= ( $submenuId == "2" && $status == "WAITRCPT" ) ? " class=\"on\"" : "" ?>>배송완료</a></li>
					<li><a href="../order/?status=DONE" <?= ( $submenuId == "2" && $status == "DONE" ) ? " class=\"on\"" : "" ?>>구매종료</a></li>
					<li><a href="../order/?status=DENYBUY" <?= ( $submenuId == "2" && $status == "DENYBUY" ) ? " class=\"on\"" : "" ?>>구매취소</a></li>
					<!--
					<li>===== 기 타 =====</li>
					<li><a href="../order/cash_new.php" <?= ( $submenuId == "3" && $page_id == "18" ) ? " class=\"on\"" : "" ?>>현금영수증</a></li>
					<li style="border:0;"><a href="../order/old.php" <?= ( $submenuId == "2" && strpos( $_SERVER["PHP_SELF"] , "old.php" ) !== false ) ? " class=\"on\"" : "" ?>>이전주문조회</a></li>
					-->
				</ul>
			</div>
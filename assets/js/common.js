
/*
* 자바스크립트 유효성 검사 함수
* 작성자 : 김신웅 sin5842@nate.com
*/var GLOBAL_CONSOLE = true;
var GLOBAL_SCROLL_NO = 0;
var GLOBAL_SCROLL_ON = false;
var GLOBAL_IS_MOBILE = false;
var GLOBAL_MOBILE_SIZE = 640
$(function(){
	$( document ).on( "click", "nav.menu-mobile", function( e ) {
		set_menu_toggle();
		return false;
	});
	$( document ).on( "click", ".layer-background, button.search-close, .layer-close", function( e ) {
		set_menu_toggle('false');
		return false;
	});
	$( document ).on( "click", ".menu-close, .pop-close", function( e ) {
		set_menu_toggle('false');
	});
	$( document ).on( "click", ".menu-search", function( e ) {
		set_menu_toggle('search');
		return false;
	});

	$( document ).on( "click", ".pop-open", function( e ) {
		var title = $(this).attr("title");
		var msg = $(this).attr("msg");
		$("#global-popup").find("header > span.title").html(title);
		$("#global-popup").find("section").html(msg);
		$("#global-popup").removeClass("hide");
		$("#background-popup").removeClass("hide");
		$('body').addClass("fixed");
	});


	$( document ).on( "click", "a.layer-open, button.layer-open", function( e ) {
		set_menu_toggle('false');
		$('body').addClass("fixed");
		$("#background-popup").removeClass("hide");
		$(".layer-box").addClass("open");
		$(".layer-box > #" + $(this).attr("id")).removeClass("hide");
	});

	$( document ).on( "change", "label.agree_checkbox > input[type=checkbox]", function( e ) {
		if($(this).prop("checked")) { 
			$(this).parent("label").addClass("active").find("i").removeClass("fa-circle").addClass("fa-check-circle");
		} else {
			$(this).parent("label").removeClass("active").find("i").addClass("fa-circle").removeClass("fa-check-circle");
		}
	});
	$( document ).on( "change", "label.agree_checkbox > input[type=radio]", function( e ) {
		//if($(this).prop("checked")) { 
			$(this).parents().find("label").removeClass("active").find("i").addClass("fa-circle").removeClass("fa-check-circle");
			$(this).parent().parents().find(".checkbox > label").removeClass("active").find("i").addClass("fa-circle").removeClass("fa-check-circle");
			$(this).parent("label").addClass("active").find("i").removeClass("fa-circle").addClass("fa-check-circle");
		//} 
	});
	$( document ).on( "click", "section.box_sub > .box_center > .box_menu", function( e ) {
			$(this).find(".menu").toggleClass("open");
	});

});

function set_menu_toggle(chk){
	var chk = chk ? chk : 'toggle';
	if( chk == 'toggle' ) {
		$('body').toggleClass("fixed");
		$('header.menu').toggleClass("open");
		$('.layer-background').toggleClass("open");
		$('nav.menu-mobile').toggleClass("active");
	} else if( chk == 'true' || chk == 'open' ) {
		$('body').addClass("fixed");
		$('header.menu').addClass("open");
		$('.layer-background').addClass("open");
		$('nav.menu-mobile').addClass("active");
	} else if( chk == 'search' ) {
		$('body').addClass("fixed");
		$('header.menu').removeClass("open");
		$('.search-box').addClass("open");
		$('.layer-background').addClass("open");
		$('.search-box').find('input[type=text]').val("").focus();
	} else {
		$('body').removeClass("fixed");
		$('nav.menu-mobile').removeClass("active");
		$('header.menu').removeClass("open");
		$('.search-box').removeClass("open");
		$('.layer-background').removeClass("open");
		$(".layer-box").removeClass("open");
		$(".layer-popup").addClass("hide");
		$("#background-popup").addClass("hide");
	}
}
	// TAB
	$(function() {
		$('.tab a').click(function() {
			var activeTab = $(this).attr('data-tab');
			$('.tab a').removeClass('current');
			$('.tabcontent').removeClass('current');
			$(this).addClass('current');
			$('#' + activeTab).addClass('current');
		})
	});

	/*상품 정보
	$(function() {

		var topBar = $("#topBar").offset();

		$(window).scroll(function(){
			
			var docScrollY = $(document).scrollTop()
			var barThis = $("#topBar")
			var fixNext = $("#fixNextTag")

			if( docScrollY > topBar.top ) {
				barThis.addClass("top_bar_fix");
				fixNext.addClass("pd_top_80");
			}else{
				barThis.removeClass("top_bar_fix");
				fixNext.removeClass("pd_top_80");
			}

		});
	})
	*/
	//파일첨부 
	$(function() {

		var fileTarget = $('.filebox .upload-hidden'); 
		
		fileTarget.on('change', function(){ // 값이 변경되면 

		if(window.FileReader){ // modern browser 
		var filename = $(this)[0].files[0].name; 
		} else { // old IE 

		var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출 
		} 

		// 추출한 파일명 삽입 
		$(this).siblings('.upload-name').val(filename); 
	
		}); 

	})

function set_console(msg){
	if( GLOBAL_CONSOLE ) console.log(msg);
}

function show_loading(){
	var width = 0;
	var height = 0;
	var left = 0;
	var top = 0;
	width = 50;
	height = 50;
	top = ( $(window).height() - height ) / 2 + $(window).scrollTop();
	left = ( $(window).width() - width ) / 2 + $(window).scrollLeft();
	if($("#div_ajax_load_image").length != 0) {
		$("#div_ajax_load_image").css({
				"top": top+"px",
				"left": left+"px"
		});
		$("#div_ajax_load_image").show();
	}
	else {
		$('body').append('<div  id="div_ajax_load_image" style="position:fixed;top:0;left:0;bottom:0;right:0;background-color:#666; opacity:0.5;z-index:99999;"><div style="position:absolute; top:' + top + 'px; left:' + left + 'px;width:' + width + 'px; height:' + height + 'px; z-index:9999;  opacity:alpha*0.5; margin:auto; padding:0; "><img src="/assets/css/ajax_loader.gif" style="width:50px; height:50px;"></div></div>');
	}
}

function addComma(n) {
	var reg = /(^[+-]?\d+)(\d{3})/;
	n += '';
	while (reg.test(n)) {
		n = n.replace(reg, '$1' + ',' + '$2');
	}
	return n;
}

/* 입력값이 NULL인지 체크*/
function _isNull(input) {
    if (input.value == null || input.value == "") {
        return true;
    }
    return false;
}

/*입력값에 스페이스 이외의 의미있는 값이 있는지 체크

* ex) if (isEmpty(form.keyword)) {
* alert("검색조건을 입력하세요.");
* }
*/
function _isEmpty(input) {
    if (input.value == null || input.value.replace(/ /gi,"") == "") {
        return true;
    }
    return false;
}

/**
* 입력값의 길이가 조건에 맞는지 체크

* ex) if (isLen(form.keyword, minLen,maxLen)) {
* alert("입력하신 글자의 수가 3~12사이여야 합니다.");
* }
*/
function _isLen(input, minLen, maxLen) {
    if (input.value.length < minLen || input.value.length >maxLen) {
        return true;
    }
    return false;
}

/**
* 입력값에 특정 문자(chars)가 있는지 체크
* 특정 문자를 허용하지 않으려 할 때 사용
* ex) if (containsChars(form.name,"!,*&^%$#@~;")) {
* alert("이름 필드에는 특수 문자를 사용할 수 없습니다.");
* }
*/
function _containsChars(input,chars) {
    for (var inx = 0; inx < input.value.length; inx++) {
        if (chars.indexOf(input.value.charAt(inx)) != -1)
            return true;
    }
    return false;
}

/**
* 입력값이 특정 문자(chars)만으로 되어있는지 체크

* 특정 문자만 허용하려 할 때 사용
* ex) if (!containsCharsOnly(form.blood,"ABO")) {
* alert("혈액형 필드에는 A,B,O 문자만 사용할 수 있습니다.");
* }
*/
function containsCharsOnly(input,chars) {
    for (var inx = 0; inx < input.value.length; inx++) {
        if (chars.indexOf(input.value.charAt(inx)) == -1)
            return true;
    }
    return false;
}

function containsCharsFirst(input) {
	var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	if (chars.indexOf(input.value.charAt(0))== -1) {
			return true;
	}
	return false;
}
/**
* 입력값이 알파벳인지 체크

* 아래 isAlphabet() 부터 isNumComma()까지의 메소드가
* 자주 쓰이는 경우에는 var chars 변수를 
* global 변수로 선언하고 사용하도록 한다.
* ex) var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
* var lowercase = "abcdefghijklmnopqrstuvwxyz"; 
* var number = "0123456789";
* function isAlphaNum(input) {
* var chars = uppercase + lowercase + number;
* return containsCharsOnly(input,chars);
* }
*/
function _isAlphabet(input) {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    return containsCharsOnly(input,chars);
}

/**
* 입력값이 알파벳 대문자인지 체크
*/
function _isUpperCase(input) {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return containsCharsOnly(input,chars);
}

/**
* 입력값이 알파벳 소문자인지 체크
*/
function _isLowerCase(input) {
    var chars = "abcdefghijklmnopqrstuvwxyz";
    return containsCharsOnly(input,chars);
}

/**
* 입력값에 숫자만 있는지 체크
*/
function _isNumber(input) {
    var chars = "0123456789";
    return containsCharsOnly(input,chars);
}

/**
* 입력값이 알파벳,숫자로 되어있는지 체크
*/
function _isAlphaNum(input) {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@.";
    return containsCharsOnly(input,chars);
}

/**
* 입력값이 숫자,대시(-)로 되어있는지 체크
*/
function _isNumDash(input) {
    var chars = "-0123456789";
    return containsCharsOnly(input,chars);
}

/**
* 입력값이 숫자,콤마(,)로 되어있는지 체크
*/
function _isNumComma(input) {
    var chars = ",0123456789";
    return containsCharsOnly(input,chars);
}

function isValidFormat(input, format) {
	var val=input.value;
	var re=/^[a-z A-Z 0-9\-_]+@[a-z A-Z 0-9\-]+(\.[a-z A-Z 0-9 \-]+)+$/;
   var st=re.test(val); 
   return st;
}
/**
* 입력값이 이메일 형식인지 체크
* ex) if (!isValidEmail(form.email)) {
* alert("올바른 이메일 주소가 아닙니다.");
* }
*/
function _isValidEmail(input) {
    // var format = /^(S+)@(S+).([A-Za-z]+)$/;
    var format = /^((w|[-.])+)@((w|[-.])+).([A-Za-z]+)$/;
    return isValidFormat(input,format);
}

/**
* 입력값이 전화번호 형식(숫자-숫자-숫자)인지 체크
*/
function _isValidPhone(input) {
    var format = /^(d+)-(d+)-(d+)$/;
    return isValidFormat(input,format);
}

/**
* 입력값의 바이트 길이를 리턴

* ex) if (getByteLength(form.title) > 100) {
* alert("제목은 한글 50자(영문 100자) 이상 입력할 수 없습니다.");
* }
* Author : Wonyoung Lee
*/
function _getByteLength(input) {
    var byteLength = 0;
    for (var inx = 0; inx < input.value.length; inx++) {
        var oneChar = escape(input.value.charAt(inx));
        if ( oneChar.length == 1 ) {
            byteLength ++;
        } else if (oneChar.indexOf("%u") != -1) {
            byteLength += 2;
        } else if (oneChar.indexOf("%") != -1) {
            byteLength += oneChar.length/3;
        }
    }
    return byteLength;
}

/**
* 입력값에서 콤마를 없앤다.
*/
function _removeComma(input) {
    return input.value.replace(/,/gi,"");
}

/**
* 선택된 라디오버튼이 있는지 체크
*/
function _hasCheckedRadio(input) {
    if (input.length > 1) {
        for (var inx = 0; inx < input.length; inx++) {
            if (input[inx].checked) return true;
        }
    } else {
        if (input.checked) return true;
    }
    return false;
}

/**
* 선택된 체크박스가 있는지 체크
*/
function _hasCheckedBox(input) {
    return hasCheckedRadio(input);
}

function hangul_chk(word) {
	var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-";
	
	for (i=0; i< word.length; i++)
	{
		idcheck = word.charAt(i);
		
		for ( j = 0 ;  j < str.length ; j++){
		
			if (idcheck == str.charAt(j)) break;
				
     			if (j+1 == str.length){
   				return false;
     			}
     		}
     	}
     	return true;
}


function toggle() {
	var ele = document.getElementById("loginpoplayer");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
				ele.style.display = "none";
		text.innerHTML = "로그인 ▼";
		}
	else {
		ele.style.display = "block";
		text.innerHTML = "<span style='color:#999'>로그인 ▲</span>";
	}
}

function toggle_n() {
	var ele = document.getElementById("loginpoplayer");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
				ele.style.display = "none";
		text.innerHTML = "닉네임 ▼";
		}
	else {
		ele.style.display = "block";
		text.innerHTML = "<span style='color:#999'>닉네임 ▲</span>";
	}
}

function urlgo(url) {
	location.href=url;
}

// ==================================
// 숫자 체크 Function
// ==================================
function NUMCHECK( CHECKINPUT ){

	if( !isNaN( CHECKINPUT ) ){

		return true;

	}else{

		return false;

	}

}	

function checkRadio(form) {
        for(var i=0;i<form.length;        i++) { // 라디오버튼의 갯수-1 만큼 루프를 돌립니다
                if (form[i].checked == true) { // 체크된 버튼을 찾으면
                return true;
                }
        }
        if(i==form.length) { 
                return false;
        }
}

function show_loading(){
	var width = 0;
	var height = 0;
	var left = 0;
	var top = 0;
	width = 50;
	height = 50;
	//top = ( $(window).height() - height ) / 2 + $(window).scrollTop();
	top = ( $(window).height() - height ) / 2;
	left = ( $(window).width() - width ) / 2 + $(window).scrollLeft();
	if($("#div_ajax_load_image").length != 0) {
		$("#div_ajax_load_image div").css({
				"top": top+"px",
				"left": left+"px"
		});
		$("#div_ajax_load_image").show();
	}
	else {
		$('body').append('<div  id="div_ajax_load_image" style="position:fixed;top:0;left:0;bottom:0;right:0;background-color:#666; opacity:0.5;z-index:99999;"><div style="position:absolute; top:' + top + 'px; left:' + left + 'px;width:' + width + 'px; height:' + height + 'px; z-index:999999;  opacity:alpha*0.5; margin:auto; padding:0; "><img src="/assets/css/ajax_loader.gif" style="width:50px; height:50px;"></div></div>');
	}
}

function getParams() {
    // 파라미터가 담길 배열
    var param = new Array();
 
    // 현재 페이지의 url
    var url = decodeURIComponent(location.href);
    // url이 encodeURIComponent 로 인코딩 되었을때는 다시 디코딩 해준다.
    url = decodeURIComponent(url);
 
    var params;
    // url에서 '?' 문자 이후의 파라미터 문자열까지 자르기
    params = url.substring( url.indexOf('?')+1, url.length );
    // 파라미터 구분자("&") 로 분리
    params = params.split("&");

    // params 배열을 다시 "=" 구분자로 분리하여 param 배열에 key = value 로 담는다.
    var size = params.length;
    var key, value;
    for(var i=0 ; i < size ; i++) {
        key = params[i].split("=")[0];
        value = params[i].split("=")[1];

        param[key] = value;
    }

    return param;
	// 현재 url: http://[domain]/exam.jsp?page=1&title=antop&sid=orcl
	//사용법
	/*
	var p = getParams();
	alert(p["title"]);    // antop
	alert(p["page"])   // 1
	*/
}
// End of Function


$( document ).on( "click", "#btn_partner", function() {
	set_menu_toggle('false');	
	var isApp = false;
	var url = "http://partner.10proshop.com/";
	try{
		isApp = window.Android.isApp();
		if ( isApp ) {
			window.Android.gotoRunLink(url);
		}else {
			window.open(url);
		}
	}catch(e){
		window.open(url);
	}
	return false;
});
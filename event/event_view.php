 <?
$page_title = "이벤트";
include ("../include/header.php");
$eid = mysql_escape_string( $_GET["eid"] );
$row = getdata(" select * from event where idx='" . $eid . "' and del_ok='0' and status='0'");
$_rep = getdata(" select count( event_idx ) as rcnt from event_reply where event_idx='" . $row["idx"] . "' and del_ok='0' ");
$sql_rep = " select * from event_reply where event_idx='" . $row["idx"] . "' and del_ok='0' order by idx desc limit 0 , 10";
$rst_rep = mysql_query($sql_rep);
if ( $row["sdate"] >= date("Ymd") || $row["edate"] <= date("Ymd") ) {
	BACK__("이벤트 기간이 종료 되었습니다.");
}
if ( $row["status"] == "1" ) {
	BACK__("이벤트가 마감 되었습니다.");
}
if ( $row["status"] == "2" || $row["idx"] == "" ) {
	BACK__("잘못된 이벤트 입니다.");
}
mysql_query("update event set visited = visited + 1 where idx = '" . $eid . "' ");
$imgSize = getimagesize("../data/event/" . $row["tumb"]) ;

?>
<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">이벤트 내용</h2>
	<div class="view">
		<div class="event-view"><img src="/data/event/<?=$row["tumb"]?>"><div>
		<div style="padding:20px;"><?=$row["econtent"]?><div>
	</div>
	<div class="view-footer" style="position:relative;">
		<ul>
			<li><a href="#" class="btn-like like <?=($_COOKIE["likeit_event_".$eid] !="" ) ? "on" : "" ?>" data-idx="<?=$row["idx"]?>" data-likeit="<?=$row["likeit"]?>"><span class="hide">좋아요</span><span class="sub-point likeit_cnt"><?=number_format( $row["likeit"] )?></span></a></li>
			<li><a href="#" class="btn-share"><span class="hide">공유</span><span><?= number_format( $row["share"] ) ?></span></a>
					<!--공유팝업 추가-->
					<div class="share-pop" style="display:none;">
						<h3>이 인사이트를 sns에 공유하세요</h3>
						<div class="share-area">
							<!--<a href="https://twitter.com/intent/tweet?text=<?=$row["title"]?>&url=<?="http://" . $SITE_DOMAIN__ . "/product/product_view.php?no=" . $no?>" class="share_icon1" target="_blank"><span class="hide">인스타</span></a>-->
							<a href="http://www.facebook.com/sharer/sharer.php?u=<?="http://" . $SITE_DOMAIN__ . "/event/event_view.php?eid=" . $eid?>" class="share_icon2" target="_blank"><span class="hide">페이스북</span></a>
							<a href="#" class="share_icon3 btn_kakaolink"><span class="hide">카카오톡</span></a>
							<a href="#" class="share_icon4 copyContent" data-clipboard-text ="http://<?=$SITE_DOMAIN__?>/event/event_view.php?eid=<?=$eid?>"><span class="hide">링크</span></a>
						</div>
					</div></li>
			<li><a href="#comment-list" class="btn-ripple"><span class="hide">댓글</span><span class="sub-point"><?= number_format( $_rep["rcnt"] ) ?></span></a></li>
			<li><a href="#" class="btn-views"><span class="hide">조회수</span><span><?= number_format( $row["visited"] ) ?></span></a></li>
		</ul>
	</div>
	<!-- comment -->
	<div class="comment">
		<div class="bookmark"><a href="#" onclick="return false;"><span class="hide">찜</span></a></div>
		<div class="input"><input type="text" name="rep_insert" id="rep_insert" value="" placeholder="댓글을 입력해주세요"></div>
		<button type="button" class="btn-ripple" id="btn_reply_in">입력</button>
	</div>
	<div class="comment-list" id="comment-list">
		<h2>댓글 <span class="point"><?= number_format( $_rep["rcnt"] ) ?></span></h2>
		<ul id="reply_list">
			<?
			while ( $row_rep = mysql_fetch_array( $rst_rep ) ) {
				$row_pro = getdata("select profile_img, uname from users where id='" . $row_rep["uid"] . "' and del_ok='0' and status='1' and del_force='0' ");
				if ( $row_rep["uid"] == "admin" ) {
					$row_pro["profile_img"] = "/assets/images/icon_def.png";
					$row_rep["writer"] = "10PROSHOP";
				}
				
			?>
			<li  id="div_reply_list_<?=$row_rep["idx"]?>">
				<div class="profile"><img src="<?=$row_pro['profile_img']?>" onerror="this.src='/assets/images/icon_noimg_m.png'"></div>
				<div class="text"><strong><?=$row_rep["writer"]?></strong><span><?=str_replace( "-" , "." , substr( $row_rep["reg_date"] , 0 , 10 ) ) ?></span>
				<? if ( $_SESSION["mstsp_id"] == $row_rep["uid"] ) {?>
					<span class="inlineR "><a href="#" class="del_reply" id="del_<?=$row_rep["idx"]?>" data-idx="<?=$row_rep["idx"]?>">x 삭제</a></span>
				<?}?>
				<br><?=$row_rep["content"]?></div>
			</li>
			<?$i++;}?>
		</ul>
	</div>
	<? if ( $_rep["rcnt"] > 10 ) {?>		
	<div id="more_box" style="clear: both;height:70px;background-color:#fff;">
		<a id="page_more" style="margin: 0px auto; text-align: center;" href="#"><span class="_1bt_more">더보기</span></a>
	</div>
	<?}?>
	
	<!-- //comment -->
</section>

<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<script type="text/javascript" src="../assets/js/clipboard.min.js"></script>
<script type="text/javascript">
<!--
var clipboard = new ClipboardJS('.copyContent');
clipboard.on('success', function(e) {
	console.log(e);
	alert( "URL 이 복사 되었습니다." );
});
clipboard.on('error', function(e) {
	console.log(e);
});
$(document).on("click",".btn-views" , function(){
	return false;
});

$(document).ready(function(){
	$(".sub-title").html("<?=$row["etitle"]?>");
});
var rep_page = 1;
$("#page_more").on("click", function(){
	$.ajax({
		type: "POST",
		url: "./_proc_json.php",
		data: { ref_idx: "<?=$eid?>" , mode: "event_rep_more" , page: rep_page },
		success:function( rst ) {
			//alert(res);
			console.log(rst);
			rep_page++;
			var data = jQuery.parseJSON(rst);
			$("#reply_list").append(data.content);
			if ( data.last ) {
				$("#more_box").hide();
			}
		}
	});
	return false;
});

	$(document).on("click",".del_reply", function(){
		var idx = $(this).data("idx");
		var params = { idx:idx, mode:'event_rep_del' }
		$.ajax({
			url:"./_proc_json.php",
			data:params,
			type:"POST",
			dataType:"json",
			success:function(data){
				console.log(data);
				$("#div_reply_list_"+idx).hide();
				alert("삭제되었습니다.");
			},error:function(){
				alert("삭제 도중 에러가 발생하였습니다." );
				location.reload();
			}
		});
		return false;
	});

	$(document).on( "click" , "#btn_reply_in" , function() {
		if ( $("#rep_insert").val() == "" ) {
			alert("내용을 입력하세요.");
			return false;
		}
		$.ajax({
			type: "POST",
			url: "./_proc_json.php",
			data: { ref_idx: "<?=$row["idx"]?>" , mode: "event_rep_insert" , msg: $("#rep_insert").val() },
			success:function( rst ) {
				var data = jQuery.parseJSON(rst);
				if ( data.content== "login fail" ) {
					$("#rep_insert").val("");
					alert("로그인 후 이용 가능합니다.");
				}else if ( data.content== "error" ) {
					$("#rep_insert").val("");
					alert("등록 도중 오류가 발생하였습니다.");
				} else {
					alert("등록 되었습니다.");
					location.reload();
				}
			}
		});
		return false;
	});

	$(document).on( "click" , ".like" , function() {
		var rep_idx = $(this).data("idx");
		var rep_likeit = $(this).data("likeit");
		$.ajax({
			type: "POST",
			url: "./_proc_json.php",
			data: { idx: rep_idx , mode: "likeit_contents" },
			success:function( rst ) {
				console.log(rst);
				var data = jQuery.parseJSON(rst);
				//alert(data.content );
				if ( data.content == "1" ) {
					$(".likeit_cnt").text( (rep_likeit + 1) );
					$(".btn-like").addClass("on");
					alert( '등록완료\n추천 하었습니다.' );
				}else if ( data.content== "0" ) {
					alert( '등록오류\n작업 도중 오류가 발생하였습니다.' );
				}else if ( data.content== "2" ) {
					alert( '등록오류\n이미 추천하였습니다.' );
				}
			}
		});
		$(this).blur();
		return false;
	});

    Kakao.init('ec3c611f8d0d6f8a767e5e24bd2a6484');
	$( document ).on( "click", ".btn_kakaolink", function( e ) {
		Kakao.Link.sendDefault({
			objectType: 'feed',
			content: {
				title: '<?=$row["etitle"]?>',
				description: '',
				imageUrl: 'http://<?=$SITE_DOMAIN__?>/data/event/<?=$row["tumb"] ?>',
				imageWidth :	<?=$imgSize[0]?>,
				imageHeight :	<?=$imgSize[1]?>,
				link: {
					mobileWebUrl: 'http://<?=$SITE_DOMAIN__?>/event/event_view.php?eid=<?=$eid?>',
					webUrl: 'http://<?=$SITE_DOMAIN__?>/event/event_view.php?eid=<?=$eid?>'
				}
			},
			buttons: [
				{
					title: '웹으로 보기',
					link: {
						mobileWebUrl: 'http://<?=$SITE_DOMAIN__?>/event/event_view.php?eid=<?=$eid?>',
						webUrl: 'http://<?=$SITE_DOMAIN__?>/event/event_view.php?eid=<?=$eid?>'
					}
				}
				<?
				if ( stripos( $_SERVER['HTTP_USER_AGENT'] , "Android") ) {
				?>
				,
				{
					title: '앱으로 보기',
					link: {
						mobileWebUrl: '<?=$APPLINK_AND?>',
						webUrl: '<?=$APPLINK_AND?>'
					}
				}
				<?}?>
			]
		});
		return false;
	});

	$( document ).on( "click", ".btn-share", function( e ) {
		if ( $(".share-pop").css("display") == "none" ) {
			$(".share-pop").show(); 
		} else { 
			$(".share-pop").hide();
		}
		return false;
	});
//-->
</script>
<? include ("../include/footer.php");?>
 <?
$page_title = "출석이벤트";
include ("../include/header.php");
$event = getdata(" select * from event_attendance where  sdate='".date("Ym")."' and del_ok='0' and status='0' and gift_day_1 <> '' order by reg_date desc limit 0 , 1");

$s=date("Y-m-d");
$x=explode("-",$s);
$s_Y=$x[0]; 
$s_m=$x[1];
$s_d=(int)($x[2] ); 

$s_t=date("t",mktime(0,0,0,$s_m,$s_d,$s_Y)); 
$s_n=date("N",mktime(0,0,0,$s_m,1,$s_Y));
$l = $s_n % 7; // 지정된 달 1일 앞의 공백 숫자.
$ra = ( $s_t + $l ) / 7; 
$ra=ceil($ra); 
$ra=$ra-1; //달력의 열수

$relay_cnt = 0;
$j = "";
//연속 출석일 체크
for ( $i = $s_d ; $i > 0 ; $i-- ) {
	if ($i <10 && strpos( $i , "0") === false ) {
		$j = "0".$i;
	}else {
		$j = $i;
	}
	$relay = getdata(" select count(*) as cnt from event_attendance_data where event_idx = '" . $event["idx"] . "' and uid='" . $_SESSION["mstsp_idx"] . "' and sdate='" . date("Y-m-") . $j . "'");
	if ($relay["cnt"] > 0 ) {
		$relay_cnt++;
	}elseif ( date("Y-m-") . $j != date("Y-m-d") ) {
		break;
	}
}
$ecount = 0;
$event_msg = "";
if ($event["gift_day_1"] != "" ) {
	$event_msg .= $event["gift_day_1"] . "일, ";
	$ecount ++;
}
if ($event["gift_day_2"] != "" ) {
	$event_msg .= $event["gift_day_2"] . "일, ";	
	$ecount ++;
}
if ($event["gift_day_3"] != "" ) {
	$event_msg .= $event["gift_day_3"] . "일, ";
	$ecount ++;
}
if ($event["gift_day_4"] != "" ) {
	$event_msg .= $event["gift_day_4"] . "일, ";
	$ecount ++;
}
?>
<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">출석체크</h2>
	<div class="event-attend">
		<div class="day"><?=substr( $event["sdate"] , 0 , 4 ) ?>. <?=substr( $event["sdate"] , -2 ) ?></div>
		<div class="text">
			<p>회원님께서는 현재 <span class="point"><?=$relay_cnt?>번</span> 연속 출석체크 하셧습니다</p>
			<button class="btn-point">출석하기</button>
		</div>
		<div class="calendar">
			<table>
				<thead>
					<tr>
						<th>SUN</th><th>MON</th><th>TUE</th><th>WED</th><th>THU</th><th>FRI</th><th>SAT</th>
					</tr>
				</thead>
				<tbody>
				<?
				for ( $r = 0 ; $r <= $ra ; $r++ ) {
					echo "<tr>";
					for ( $z = 1 ; $z <= 7 ; $z++ ) {
						$rv = ( 7 * $r ) + $z; 
						$ru = $rv - $l; 
						echo "<td";
						if ( $ru > 0 && $ru <= $s_t ) {
							$now_date = date("Y-m-d" , mktime(0,0,0,$s_m,$ru,$s_Y)); // 현재칸의 날짜
							//echo $now_date;
							$row_data = getdata(" select count(*) as cnt from event_attendance_data where event_idx='".$event["idx"]."' and uid='".$_SESSION["mstsp_idx"]."' and sdate='" . $now_date . "'");
							if ( $row_data["cnt"] == "1" ) { //출석이 있으면
								echo " class='check'>";
							}else{ 
								echo ">";
							}
							echo "$ru"; 
						}else {
							echo ">";
						}
						echo "</td>";
					}
					echo "</tr>";
				}
				?>
				</tbody>
			</table>
		</div>
		<div class="group">
			<ul class="guide-point">
				<li>
					<p><span class="point"><?=$event["gift_day_1"]?>일</span> 연속 출석</p>
					<p class="box"><span class="point">GIFT</span><br><span class="point size"><?=number_format( $event["gift_point_1"] )?>P</span><br>전원지급</p>
				</li>
				<li>
					<p><span class="point"><?=$event["gift_day_2"]?>일</span> 연속 출석</p>
					<p class="box"><span class="point">GIFT</span><br><span class="point size"><?=number_format( $event["gift_point_2"] )?>P</span><br>전원지급</p>
				</li>
				<li>
					<p><span class="point"><?=$event["gift_day_3"]?>일</span> 연속 출석</p>
					<p class="box"><span class="point">GIFT</span><br><span class="point size"><?=number_format( $event["gift_point_3"] )?>P</span><br>전원지급</p>
				</li>
			</ul>
			<ul class="guide-text">
				<li>· 누적연속 출석일수 도달 시 해당 포인트 자동지급</li>
				<li>· 누적연속 <?= ( $event_msg != "" ) ? substr( $event_msg , 0 , -2 ) : "" ?> <span class="point">최대 <?=$ecount?>번</span>의<span class="point">100% 당첨</span>가능</li>
				<li>· 회원이 아니거나, 부정한 방법으로 참여 시 무효</li>
			</ul>
		</div>
	</div>
</section>

<script type="text/javascript">
<!--
	$(".btn-point").on("click" , function(){
		$.ajax({
			type: 'POST',
			cache: false,
			dataType: 'json',
			url: './_proc_json.php',
			data: 'mode=event_attendance&idx=<?=$event["idx"]?>',
			success:function (data) {
				console.log(data);
				if (data == "1" ) {
					alert("출석하였습니다.");
					location.reload();
				}else if( data == "2" ) {
					alert("오류가 발생하였습니다.");
				}else if( data == "3" ) {
					alert("로그인 후 이용 가능합니다.");
					location.href="../main/login.php?reurl=<?=urlencode($_SERVER["REQUEST_URI"])?>";
				}else if( data == "4" ) {
					alert("이미 참여하셨습니다.");
				}else if( data == "5" ) {
					alert("저장 도중 오류가 발생하였습니다.");
				}
			},
			error : function (data) {
				console.log(data);
			},
			beforeSend: function () {
				show_loading();
			}
			,complete: function () {
				$("#div_ajax_load_image").hide();
			}
		});
		return false;
	});
//-->
</script>
<? include ("../include/footer.php");?>
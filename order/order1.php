<?
$PAGE_TITLE = "주문서작성";
include ("../include/header.php");

$no = mysql_real_escape_string( $_GET["no"] );
$row = getdata("SELECT A.*, B.* FROM `goods` as A left join goods_detail as B ON A.no=B.no where A.no = '" . $no . "' ");
if ( $row["status"] == "" ) {
	BACK__("상품을 찾을 수 없습니다.");
}elseif ( $row["status"] != "판매중" ) {
	BACK__("판매 중지 중인 상품입니다.");
}
if ( $row["type"] == "" || $row["type"] == "미입력" || $row["pay"] == "무료배송" ) { //무료배송
	$delivery_str = "무료배송";
	$delivery = "0";
	$delivery_gubun = "s";
}elseif ( $row["type"] == "고정배송비" ) {
	$delivery_str = $row["pay"] . " " . number_format( $row["fee"] ) . "원";//구매자(선불,착불)선택, 무료배송, 선결제, 착불
	$delivery = $row["fee"];
	$delivery_gubun = "s";
}elseif ( $row["type"] == "수량별비례") {
	$arr_deli_tbl = explode( "|" , $row["tbl"] );
	$arr_a = explode("+",$arr_deli_tbl[0]);
	$arr_b = explode("+",$arr_deli_tbl[1]);
	$arr_deli_fee = explode( "+" , $arr_deli_tbl[0] );
	$delivery_str = "수량별비례 / " . $row["pay"] . "<br />";
	$delivery_str_step = $arr_a[0] . "개까지 " . number_format($arr_a[1] ) . "원 / 이후 " . $arr_b[0] . "개마다 " . number_format( $arr_b[1] ) . "원씩 추가";
	$delivery_str .= $delivery_str_step;
	$delivery = "0";
	$delivery_gubun = "m";
}elseif ( $row["type"] == "수량별차등" ) {
	$arr_deli_tbl = explode( "|" , $row["tbl"] );
	$arr_deli_fee = explode( "+" , $arr_deli_tbl[0] );
	$delivery_str = $row["tbl"] . "수량별차등 적용 / " . $row["pay"] . "<br />";
	for ( $i = count( $arr_deli_tbl ) -1; $i >= 0 ; $i-- ) {
		$arr_deli_fee = explode( "+" , $arr_deli_tbl[$i] );
		if ( $i == count( $arr_deli_tbl ) -1 ) {
			$delivery_str_step = $arr_deli_fee[0] . "개 이상 " . number_format( $arr_deli_fee[1] );
		}else {
			$delivery_str_step = $arr_deli_fee[0] . "~" . ( $pre_cnt -1 ) . "개 " . number_format( $arr_deli_fee[1] ) . "<br />" . $delivery_str_step;
		}
		$pre_cnt = $arr_deli_fee[0];
	}
	$delivery_str .= $delivery_str_step;
	$delivery = "0";
	$delivery_gubun = "m";
}

if ( strpos( $delivery_str , "구매자(선불,착불)선택" ) !== false ) {
	$delivery_str = str_replace("구매자(선불,착불)선택" , "선결제" , $delivery_str);
}
?>

<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">주문</h2>
	<div class="order-area">
		<ul class="order-list">
			<li>
				<div class="product">
					<div class="img">
						<a href="../product/product_view.php?no=<?=$row["no"]?>" class="product-view"><img src="<?=$row["thumb"]?>"></a>
					</div>
					<div class="text">
						<a href="../product/product_view.php?no=<?=$row["no"]?>" class="product-view">
							<h3><?=$row["title"]?></h3>
						</a>
							<p class="pay"><?=number_format( $row["priceWe"] )?>원</p>
							<p class="gray"><!--배송비 조건부 무료-->
							<?=$row["method"]?> : <?=$delivery_str?><br><?=$row["wating"]?>(평균출고일 <?=$row["sendAvg"]+1?>일 <span><?= ( $row["fastDeli"] == "1" ) ? "빠른배송" : "" ?></span>)<br>추가배송비 : 제주지역 + <?=number_format( $row["jeju"] )?>원 /도서산간 + <?=number_format( $row["islands"] )?>원</p>
					</div>
					<? if ( $row["pay"] == "구매자(선불,착불)선택" ) {?>
						<input type="hidden"  id="option_deli_pay" name="option_deli_pay" value="P" class="">
						<!--
						<div class="select-box" id="option_box_deli">
							<select name="option_deli_pay" id="option_deli_pay">
								<option value="">배송비 선택</option>
								<option value="P">선불(주문시결제)</option>
								<option value="B">착불(상품수령시결제)</option>
							</select>
						</div>
						-->
					<?}else{?>
							<input type="hidden"  id="option_deli_pay" name="option_deli_pay" value="<?=( $row["pay"] == "무료배송" || $row["type"] == "" || $row["type"] == "미입력" ) ? "S" : "P" ?>" class="">
							<!--
							<select name="option_deli_pay" id="option_deli_pay" style="display:none;">
								<option value="S" <?=( $row["pay"] == "무료배송" || $row["type"] == "" || $row["type"] == "미입력" ) ? "selected" : "" ?>>무료배송</option>
								<option value="P" <?=( $row["pay"] == "선결제" ) ? "selected" : "" ?>>선결제</option>
								<option value="B"<?=( $row["pay"] == "착불" ) ? "selected" : "" ?>>착불</option>
							</select>
							-->
					<?}?>
					<?
					if ( $row["optType"] == "c" ) {
						if ( $row["optDep1"] != "") {
							echo "<div class=\"select-box\"><select name=\"option_dep_1\" id=\"option_dep_1\"><option value=\"\">" . $row["optDep1"] . "</option>";
							$i_op = 0;
							$rst_option = mysql_query("select * from goods_option where goods_no='" . $row["no"] . "' and depth='1' ");	
							while ( $row_option = mysql_fetch_array( $rst_option ) ) {
								//print_r($row_option );
								$op_key = $i_op;
								$op_dsb = "";
								if ( strlen( $op_key ) == 1 ) {
									$op_key = "0" . $op_key;
								}
								$row_option_data = getdata("select * from goods_option_data where goods_no='" . $row["no"] . "' and okey like '" . $op_key . "%' and hid<>'2' and sup='1' and o_status='1' order by okey asc limit 0 , 1");
								//echo "select * from goods_option_data where goods_no='" . $row["no"] . "' and okey like '" . $op_key . "%' ";
								//print_r($row_option_data );
								if ( $row_option_data["goods_no"] != "" ) {
									if ( $row_option_data["hid"] == "2" || $row_option_data["sup"] != "1" ) {
										continue;
									}elseif ( $row_option_data["hid"] == "1" ) {
										$hid_str = "(판매완료)";
										$op_dsb = " disabled ";
									}elseif ( $row_option_data["hid"] == "0" ) {
										$hid_str = "";
									}
									if ( $row_option_data["supPrice"] > "0" ) {
										$price_opt = " ( + " . number_format( $row_option_data["priceWe"]  )."원)";
									}elseif ( $row_option_data["supPrice"] < "0" ) {
										$price_opt = " ( " . number_format( $row_option_data["priceWe"]  )."원)";
									}else {
										$price_opt = "";
									}
									echo "<option value=\"" . $op_key . "\" " . $op_dsb . ">" . $row_option["otitle"] . " " . $price_opt . " " . $hid_str . "</option>";
								}else {
									echo "<option value=\"" . $op_key . "\">" . $row_option["otitle"] . "</option>";
								}
								$i_op++;
							}
							echo "</select></div>";
						}
						
						if ( $row["optDep2"] != "" ) {
							echo "<div class=\"select-box\" id=\"option_box_2\"><select name=\"option_dep_2\" id=\"option_dep_2\" style=\"background-color:#ededed;\" disabled><option value=\"\">" . $row["optDep2"] . "</option>";
							echo "</select></div>";
						}
						if ( $row["optDep3"] != "" ) {
							echo "<div class=\"select-box\" id=\"option_box_3\"><select name=\"option_dep_3\" id=\"option_dep_3\" style=\"background-color:#ededed;\" disabled><option value=\"\">" . $row["optDep3"] . "</option>";
							echo "</select></div>";
						}
						if ( $row["optDep4"] != "" ) {
							echo "<div class=\"select-box\" id=\"option_box_4\"><select name=\"option_dep_4\" id=\"option_dep_4\" style=\"background-color:#ededed;\" disabled><option value=\"\">" . $row["optDep4"] . "</option>";
							echo "</select></div>";
						}
						if ( $row["optDep5"] != "" ) {
							echo "<div class=\"select-box\" id=\"option_box_5\"><select name=\"option_dep_5\" id=\"option_dep_5\" style=\"background-color:#ededed;\" disabled><option value=\"\">" . $row["optDep5"] . "</option>";
							echo "</select></div>";
						}
					}elseif ( $row["optType"] == "i" ) {
						$rst_option_i = mysql_query("select * from goods_option_data where goods_no='" . $row["no"] . "' and hid <> '2' and sup = '1' order by okey asc ");	
						echo "<div class=\"select-box\"><select name=\"option_dep_1\" id=\"option_dep_1\"><option value=\"\">" . $row["optDep1"] . "</option>";
						$i_op = 0;
						while ( $row_option_i = mysql_fetch_array( $rst_option_i ) ) {
							if ( $row_option_i["hid"] == "1" ) {
								$hid_str = "(판매완료)";
							}else {
								$hid_str = "";
							}
							if ( $row_option_i["supPrice"] > "0" ) {
								$price_opt = " + " . number_format( CONVERT_PRICE( $row_option_i["supPrice"] ) ) . "원";
							}elseif ( $row_option_i["supPrice"] < "0" ) {
								$price_opt = " - " . number_format( CONVERT_PRICE( $row_option_i["supPrice"] ) ) . "원";
							}else {
								$price_opt = "";
							}							

							echo "<option value=\"" . $row_option_i["okey"] . "\">" . $row_option_i["oname"] . " " . $price_opt . " " . $hid_str . "</option>";
							$i_op++;
						}
						echo "</select></div>";
					?>
						
					<?}?>
					<?
						if ( $row["optDep1"] == "" ) {
							$tmp = (int)( microtime() * 100000 );
					?>
						<div class="option-area" id="id_<?=$tmp?>" style="margin-bottom:10px;margin-top:15px;"><input type="hidden" class="opt_price" name="opt_price[]" value="<?=$row["priceWe"]?>" data-id="<?=$tmp?>"><input type="hidden" name="opt_qty[]" value="<?=$row["qtyInventory"]?>" data-id="<?=$tmp?>" id="opt_qty_<?=$tmp?>"><input type="hidden" name="opt_data[]" value="00" id="key_<?=$tmp?>" data-id="<?=$tmp?>"><div class="option"><?=$row["title"]?></div><div class="option-info"><div class="plusminus_wrap"><button type="button" class="numbtn_minus" value="-" id="minus_<?=$tmp?>" data-id="<?=$tmp?>"><span class="hide">수량감소</span></button><input type="number" name="qty[]" id="qty_<?=$tmp?>" data-id="<?=$tmp?>" class="text qty_ipt" title="수량설정" value="1"><button type="button" class="numbtn_plus" value="+" id="plus_<?=$tmp?>" data-id="<?=$tmp?>"><span class="hide">수량증가</span></button></div><div class="pay" id="pay_<?=$tmp?>"><span id="price_txt_<?=$tmp?>"><?=number_format( $data["priceWe"] )?></span>원</div></div></div>
					<?}?>
					<div id='selected_list'></div>
				</div>
			</li>
		</ul>
		
		<div class="price">
			<input type="hidden" name="deli_pay" value="" />
			<ul>
				<li>총 상품금액 <div class="point right"><span class="bold" id="total_price_txt">0</span>원</div></li>
				<li>배송비 <div class="point right"><span class="bold" id="total_deli_txt">0</span>원</div></li>
				<li class="price_all">총결제금액 <div class="point right"><span class="bold" id="total_payment_txt">0</span>원</div></li>
			</ul>
		</div>

		<div class="order-footer">
			<div class="pay">총 결제액 <span class="point" id="fanaly_payment_txt">0</span>원</div>
			<div class="btn-area">
				<a href="cart.php" class="btn-subpoint goCart">장바구니담기</a>
				<a href="order_payment.php" class="btn-point w60p goPayment">결제하기</a>	
			</div>
		</div>
	</div>
</section>
<form name='order_frm' id='order_frm' method="GET" action="_proc.php">
	<input type="hidden" name="no" id="no" value="<?=$no?>" />
	<input type="hidden" name="order_options" id="order_options" value="" />
	<input type="hidden" name="order_deli" id="order_deli" value="" />
	<input type="hidden" name="order_price" id="order_price" value="" />
	<input type="hidden" name="deli_pay" id="deli_pay" value="" />
</form>
<script type="text/javascript">
<!--
//5% + 30% 금액 추가

	var val_1 = "";
	var val_2 = "";
	var val_3 = "";
	var val_4 = "";
	var val_5 = "";

	<? if ( $row["optDep1"] != "" ){?>
		$(document).on( "change" , "#option_dep_1" , function(){
			val_1 = $(this).val();
			if ( val_1 == "" ) {
				<? if ( $row["optDep2"] != "" ){?>$('#option_dep_2 option:eq(0)').prop('selected',true);<?}?>
				<? if ( $row["optDep3"] != "" ){?>$('#option_dep_3 option:eq(0)').prop('selected',true);<?}?>
				<? if ( $row["optDep4"] != "" ){?>$('#option_dep_4 option:eq(0)').prop('selected',true);<?}?>
				<? if ( $row["optDep5"] != "" ){?>$('#option_dep_5 option:eq(0)').prop('selected',true);<?}?>
			}else {
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: './_proc_json.php',
					data: 'mode=option1&no=<?=$no?>&val_1=' + $(this).val() ,
					success:function (data) {
						console.log(data);
						if (data.content) {
							$('#option_box_2').html(data.content);
							$('#option_dep_2').focus();
							$('#option_dep_2').css("background-color","#fff;");
							$('#option_dep_2').attr("disabled",false);
							$('#option_dep_2 option:eq(0)').prop('selected',true);
						}
						if ( data.state == "commit" ) {
							$("#selected_list").append(data.option_box);
							<? if ( $row["optDep1"] != "" ){?>$('#option_dep_1 option:eq(0)').prop('selected',true);<?}?>
							select_commit();
						}
					}, error : function (data) {
						console.log(data);
					}
				});
			}
		});
	<?}?>
	<? if ( $row["optDep2"] != "" ){?>
		$(document).on( "change" , "#option_dep_2" , function(){
			val_2 = $(this).val();
			if ( val_2 == "" ) {
				<? if ( $row["optDep3"] != "" ){?>$('#option_dep_3 option:eq(0)').prop('selected',true);<?}?>
				<? if ( $row["optDep4"] != "" ){?>$('#option_dep_4 option:eq(0)').prop('selected',true);<?}?>
				<? if ( $row["optDep5"] != "" ){?>$('#option_dep_5 option:eq(0)').prop('selected',true);<?}?>
			}else {
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: './_proc_json.php',
					data: 'mode=option2&no=<?=$no?>&val_1=' + val_1 + '&val_2=' +val_2,
					success:function (data) {
						console.log(data);
						if (data.content) {
							$('#option_box_3').html(data.content);
							$('#option_dep_3').focus();
							$('#option_dep_3').css("background-color","#fff;");
							$('#option_dep_3').attr("disabled",false);
							$('#option_dep_3 option:eq(0)').prop('selected',true);
						}
						if ( data.state == "commit" ) {
							$("#selected_list").append(data.option_box);
							<? if ( $row["optDep1"] != "" ){?>$('#option_dep_1 option:eq(0)').prop('selected',true);<?}?>
							$('#option_dep_2').css("background-color","#ededed;");
							$('#option_dep_2').attr("disabled",true);
							$('#option_dep_2 option:eq(0)').prop('selected',true);
							select_commit();
						}
					}, error : function (data) {
						console.log(data);
					} 
				});
			}
		});
	<?}?>
	<? if ( $row["optDep3"] != "" ){?>
		$(document).on( "change" , "#option_dep_3" , function(){
			val_3 = $(this).val();
			if ( val_3 == "" ) {
				<? if ( $row["optDep4"] != "" ){?>$('#option_dep_4 option:eq(0)').prop('selected',true);<?}?>
				<? if ( $row["optDep5"] != "" ){?>$('#option_dep_5 option:eq(0)').prop('selected',true);<?}?>
			}else {
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: './_proc_json.php',
					data: 'mode=option3&no=<?=$no?>&val_1=' + val_1 + '&val_2=' +val_2 + '&val_3=' +val_3,
					success:function (data) {
						console.log(data);
						if (data.content) {
							$('#option_box_4').html(data.content);
							$('#option_dep_4').focus();
							$('#option_dep_4').css("background-color","#fff;");
							$('#option_dep_4').attr("disabled",false);
							$('#option_dep_4 option:eq(0)').prop('selected',true);
						}
						if ( data.state == "commit" ) {
							$("#selected_list").append(data.option_box);
							$('#option_dep_1 option:eq(0)').prop('selected',true);
							$('#option_dep_2 option:eq(0)').prop('selected',true);
							$('#option_dep_3 option:eq(0)').prop('selected',true);
							$('#option_dep_2').css("background-color","#ededed;");
							$('#option_dep_2').attr("disabled",true);
							$('#option_dep_3').css("background-color","#ededed;");
							$('#option_dep_3').attr("disabled",true);
							select_commit();
						}
					}, error : function (data) {
						console.log(data);
					} 
				});
			}
		});
	<?}?>
	<? if ( $row["optDep4"] != "" ){?>
		$(document).on( "change" , "#option_dep_4" , function(){
			val_4 = $(this).val();
			if ( val_4 == "" ) {
				<? if ( $row["optDep5"] != "" ){?>$('#option_dep_5 option:eq(0)').prop('selected',true);<?}?>
			}else {
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: './_proc_json.php',
					data: 'mode=option4&no=<?=$no?>&val_1=' + val_1 + '&val_2=' +val_2 + '&val_3=' +val_3 + '&val_4=' +val_4,
					success:function (data) {
						console.log(data);
						if (data.content) {
							$('#option_box_5').html(data.content);
							$('#option_dep_5').focus();
							$('#option_dep_5').css("background-color","#fff;");
							$('#option_dep_5').attr("disabled",false);
							$('#option_dep_5 option:eq(0)').prop('selected',true);
						}
						if ( data.state == "commit" ) {
							$("#selected_list").append(data.option_box);
							<? if ( $row["optDep1"] != "" ){?>$('#option_dep_1 option:eq(0)').prop('selected',true);<?}?>
							$('#option_dep_2').css("background-color","#ededed;");
							$('#option_dep_2').attr("disabled",true);
							$('#option_dep_2 option:eq(0)').prop('selected',true);
							$('#option_dep_3').css("background-color","#ededed;");
							$('#option_dep_3').attr("disabled",true);
							$('#option_dep_3 option:eq(0)').prop('selected',true);
							$('#option_dep_4').css("background-color","#ededed;");
							$('#option_dep_4').attr("disabled",true);
							$('#option_dep_4 option:eq(0)').prop('selected',true);
							select_commit();
						}
					}, error : function (data) {
						console.log(data);
					}
				});
			}
		});
	<?}?>
	<? if ( $row["optDep5"] != "" ){?>
		$(document).on( "change" , "#option_dep_5" , function(){
			val_5 = $(this).val();
			if ( val_5 != "" ) {
				$("#selected_list").append(data.option_box);
				<? if ( $row["optDep1"] != "" ){?>$('#option_dep_1 option:eq(0)').prop('selected',true);<?}?>
				$('#option_dep_2').css("background-color","#ededed;");
				$('#option_dep_2').attr("disabled",true);
				$('#option_dep_2 option:eq(0)').prop('selected',true);
				$('#option_dep_3').css("background-color","#ededed;");
				$('#option_dep_3').attr("disabled",true);
				$('#option_dep_3 option:eq(0)').prop('selected',true);
				$('#option_dep_4').css("background-color","#ededed;");
				$('#option_dep_4').attr("disabled",true);
				$('#option_dep_4 option:eq(0)').prop('selected',true);
				$('#option_dep_5').css("background-color","#ededed;");
				$('#option_dep_5').attr("disabled",true);
				$('#option_dep_5 option:eq(0)').prop('selected',true);
				select_commit();
			}
		});
	<?}?>
		
	<? if ( $row["optDep1"] == "" ){?>
		select_commit();
	<?}?>

	$(document).on( "click" , ".goCart" , function(){
		if ( $("#order_options").val() == "" ) {
			alert("장바구니에 담으실 아이템을 선택해주세요");
		}else {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=save_order&no=<?=$no?>&order_options=' + $("#order_options").val() + '&order_deli=' + $("#order_deli").val() + '&order_price=' + $("#order_price").val() + '&deli_pay=' + $("#option_deli_pay").val(),
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						var con = confirm("장바구니에 상품을 담았습니다.\n장바구니로 이동하시겠습니까?");
						if ( con ) {
							location.href="../order/cart.php";
						}
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}
		return false;
	});

	$(document).on( "click" , ".goPayment" , function(){
		if ( $("#order_options").val() == "" ) {
			alert("구매하실 아이템을 선택해주세요");
		}else {			
			//alert( $("#order_options").val() + '&order_deli=' + $("#order_deli").val() + '&order_price=' + $("#order_price").val() + '&deli_pay=' + $("#option_deli_pay option:selected").val());
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=payment_regi&no=<?=$no?>&order_options=' + $("#order_options").val() + '&order_deli=' + $("#order_deli").val() + '&order_price=' + $("#order_price").val() + '&deli_pay=' + $("#option_deli_pay").val(),
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						location.href="../order/order_payment.php?oidx="+data.oidx;
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}
		return false;
	});

	$(document).on( "click" , ".btn-delete" , function(){
		var val = $(this).data("id");
		$("#id_"+val).remove();
		select_commit();
	});

	$(document).on( "click" , ".numbtn_minus" , function(){
		var val = $(this).data("id");
		var nowval = parseInt( $("#qty_"+val).val() );
		if ( nowval <= 1 ) {
			alert("수량은 1개 이상 입니다.");
		}else {
			$("#qty_"+val).val((nowval-1));
			select_commit();
		}
		return false;
	});

	$(document).on( "click" , ".numbtn_plus" , function(){
		var val = $(this).data("id");
		var nowval = parseInt( $("#qty_"+val).val() );
		var qty =  parseInt( $("#opt_qty_"+val).val() );
		if ( nowval >= qty ) {
			alert("재고수량을 초과하였습니다.");
		}else {
			$("#qty_"+val).val((nowval+1));
			select_commit();
		}
		return false;
	});

	$(document).on("change",".qty_ipt",function(e){
		var val = $(this).data("id");
		var nowval = parseInt( $(this).val() );
		var qty =  parseInt( $("#opt_qty_"+val).val() );

		if ( nowval >= qty ) {
			alert("재고수량을 초과하였습니다.");
		}else {
			select_commit();
		}
		return false;
	});

	$(document).on("keydown",".qty_ipt",function(e){
		var keycode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
		if ((keycode <= 57 && keycode >= 48) || (keycode <= 105 && keycode >= 96) || keycode < 33 || keycode == 46 || keycode == 37 || keycode == 39) {
			if(keycode == 13){
				e.target.onchange();
				return false;
			}
			return true;
		} else {
			if (!e.preventDefault) {
				e.returnValue = false;
			} else {
				e.preventDefault();
				return false;
			}
			return false;
		}
	});

	function select_commit() {
		var cnt = $(".opt_price").length;
		var total_price = 0;
		var total_deli = <?=$delivery?>;
		var deli_gubun = "<?=$delivery_gubun?>";
		var total_payment = 0;
		var total_qty = 0;		
		var total_deli_txt = "";
		for ( i = 0 ; i < cnt ; i++ ) {
			var price = parseInt( $(".opt_price").eq(i).val() );
			var id = $(".opt_price").eq(i).data("id");
			var qty = parseInt( $(".qty_ipt").eq(i).val() );
			var opt_price = price * qty;
			total_price += opt_price;
			total_qty += qty;
			$("#price_txt_"+id).html(addComma(opt_price));
		}
		
		if ( deli_gubun == "m" ) {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=delipay&no=<?=$no?>&qty=' + total_qty ,
				success:function (data) {
					console.log(data);
					if ( parseInt( data.deliprice ) > 0 ) {
						total_deli = data.deliprice;
					}else {
						total_deli = 0;
					}
					
					total_payment = total_price + total_deli;
					$("#total_price_txt").html(addComma(total_price));
					$("#total_payment_txt").html(addComma(total_payment));
					$("#fanaly_payment_txt").html(addComma(total_payment));
					if ( total_deli == 0 ) {
						total_deli_txt = "0";
					}else {
						total_deli_txt = "+"+addComma(total_deli);
					}
					$("#total_deli_txt").html(total_deli_txt);

					var rst_options = "";
					for ( i = 0 ; i < $(".opt_price").length ; i++ ) {
						var key = $(".opt_price").eq(i).data("id");
						if ( rst_options == "" ) {
							rst_options = $("#key_"+key).val()+"|"+$("#qty_"+key).val(); //01_03|13|02_02|7
						}else {
							rst_options = rst_options + "@"+$("#key_"+key).val()+"|"+$("#qty_"+key).val();
						}
					}
					$("#order_options").val(rst_options);
					$("#order_deli").val(total_deli);
					$("#order_price").val(total_price);
					$("#deli_pay").val($("#option_deli_pay").val());
				}, error : function (data) {
					console.log(data);
				} 
			});
		}else {
			total_payment = total_price + total_deli;
			$("#total_price_txt").html(addComma(total_price));
			$("#total_payment_txt").html(addComma(total_payment));
			$("#fanaly_payment_txt").html(addComma(total_payment));
			if ( total_deli == 0 ) {
				total_deli_txt = "0";
			}else {
				total_deli_txt = "+"+addComma(total_deli);
			}
			$("#total_deli_txt").html(total_deli_txt);
			
			var rst_options = "";
			for ( i = 0 ; i < $(".opt_price").length ; i++ ) {
				var key = $(".opt_price").eq(i).data("id");
				if ( rst_options == "" ) {
					rst_options = $("#key_"+key).val()+"|"+$("#qty_"+key).val(); //01_03|13|02_02|7
				}else {
					rst_options = rst_options + "@"+$("#key_"+key).val()+"|"+$("#qty_"+key).val();
				}
			}
			$("#order_options").val(rst_options);
			$("#order_deli").val(total_deli);
			$("#order_price").val(total_price);
			$("#deli_pay").val($("#option_deli_pay").val());
		}
	}

//-->
</script>
<?include ("../include/footer.php");?>
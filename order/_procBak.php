<?
include ("../config.php");


if ( $mode == "order" ) {
	$o_num = strtoupper(dechex(date("ymdHis")))."_".rand(10000,99999);
	$_orders = getdata("select count(*) as cnt from orders where o_num = '$o_num'");
	if ($_orders["cnt"] == 0 ) {	
		$gcode = mysql_real_escape_string($_POST["gcode"]);
		$delivery_pay = mysql_real_escape_string( str_replace( "," , "" , $_POST["delivery_pay"] ));
		$island_pay = mysql_real_escape_string( str_replace( "," , "" , $_POST["island_pay"]));
		$order_name = $_POST["order_name"];
		$use_point = $_POST["use_point"];
		$order_phone = mysql_real_escape_string($_POST["order_phone"]);
		$receive_name = $_POST["receive_name"];
		$receive_tel = mysql_real_escape_string($_POST["receive_tel"]);
		$receive_phone = mysql_real_escape_string($_POST["receive_phone"]);
		$zipcode = mysql_real_escape_string($_POST["zipcode"]);
		$addr1 = $_POST["addr1"];
		$addr2 = $_POST["addr2"];
		$order_comment = $_POST["order_comment"];
		$pay = mysql_real_escape_string($_POST["pay"]);
		$mu_name = $_POST["mu_name"];
		$receipt_yn = mysql_real_escape_string($_POST["receipt_yn"]);
		$bank = $_POST["bank"];
		$receipt_gubun = mysql_real_escape_string($_POST["receipt_gubun"]);
		$receipt_number = mysql_real_escape_string($_POST["receipt_number"]);
		$price_sales = mysql_real_escape_string($_POST["price_sales"]);
		$qty = mysql_real_escape_string($_POST["qty"]);
		$channel = mysql_real_escape_string($_POST["channel"]);
		$no_option_qty = mysql_real_escape_string($_POST["no_option_qty"]);
		$d_fee = $delivery_pay + $island_pay;
		$price = 0;
		$price_supply = 0;

		$_goods = getdata("select * from goods where gcode = '$gcode'");


		$event_chk = false;
		if ( $gcode == "A054AC" || $gcode == "A054AD" || $gcode == "A054AE" || $gcode == "A054AF" || $gcode == "A054B0" || $gcode == "A054B1" || $gcode == "A054B2" || $gcode == "A054B3" || $gcode == "A054B4" || $gcode == "A054B5" || $gcode == "A054B6" || $gcode == "A054B7" || $gcode == "A0550E" || $gcode == "A05515" || $gcode == "A05514" || $gcode == "A05556" || $gcode == "A0555B" ) {
			$event_chk = true;
		}
		//이벤트 상품 한정 - 재고선차감
		if ( $event_chk == true ) { //차후 gcode변경
			$sql_inven_update = "";
			$inven_empty = false;
			if ( $no_option_qty > 0 ) { //옵션 x , 전체 재고 차감
				$inven_arr = getdata("select inventory from goods where gcode ='" . $gcode . "'");
				if ( $inven_arr["inventory"] - $no_option_qty < 0 ) {
					$inven_empty = true;				
				}else {
					$sql_inven_update = " update goods set inventory = inventory - " . $no_option_qty . " where gcode='" . $gcode . "'" ;	
					@mysql_query($sql_inven_update);
				}
			}else { //옵션 차감
				$sql_i="select * from orders_options where o_num='".$o_num."' ";
				$result_i=mysql_query( $sql_i ) OR die(__FILE__." : Line ".__LINE__."<p>".mysql_error());
				while ( $row_i = mysql_fetch_array($result_i ) ) {
					if ( substr( $row_i["oidx"] , 0 , 1 ) == "u" && $row_i["qty"] > 0  ) { // 사용자옵션만 있는 경우
						$inven_arr = getdata("select inventory from goods where gcode ='" . $gcode . "'");
						if ( $inven_arr["inventory"] - $row_i["qty"] < 0 ) {
							$inven_empty = true;
						}else {
							$sql_inven_update = " update goods set inventory = inventory - " . $row_i["qty"] . " where gcode='" . $gcode . "'" ;
							@mysql_query($sql_inven_update);
						}
					}else {
						$list_i = explode("_" , $row_i["oidx"] );
						for ( $i = 0 ; $i < count( $list_i ) ; $i++ ) {
							if ( substr( $list_i[$i] , 0 , 1 ) == "o" && $row_i["qty"] > 0  ) {
								$inven_arr = getdata("select inven_count from goods_option_inven where idx ='" . substr( $list_i[$i] , 1 ) . "'");
								if ( $inven_arr["inven_count"] - $row_i["qty"] < 0 ) {
									$inven_empty = true;				
								}else {
									$sql_inven_update = " update goods_option_inven set inven_count = inven_count - " . $row_i["qty"] . " where idx='" . substr( $list_i[$i] , 1 ) . "'" ;
									@mysql_query($sql_inven_update);
								}
							}elseif ( substr( $list_i[$i] , 0 , 1 ) == "n" && $row_i["qty"] > 0 ) {
								$inven_arr = getdata("select cnt from goods_option_normal where idx ='" . substr( $list_i[$i] , 1 ) . "'");
								if ( $inven_arr["cnt"] - $row_i["qty"] < 0 ) {
									$inven_empty = true;				
								}else {
									$sql_inven_update = " update goods_option_normal set cnt = cnt - " . $row_i["qty"] . " where idx='" . substr( $list_i[$i] , 1 ) . "'" ;
									@mysql_query($sql_inven_update);
								}
							}
						}
					}
				}
			}
			if ( $inven_empty == true ) {
				echo "<script>alert('죄송합니다. 수량 초과로 인해 마감되었습니다');window.open('about:blank','_self'); opener=window; window.close(); </script>";
				exit;
			}
		}


		//공급가액 구하기
		if ( count( $_POST["op_list"] ) > 0 ) {
			for ( $i = 0 ; $i < count( $_POST["op_list"] ) ; $i++ ) {
				$op_list = mysql_real_escape_string($_POST["op_list"][$i]);
				$op_info = explode("|^|" , $op_list );
				$psp = $op_info[1] * $op_info[3];
				$sql = "INSERT INTO orders_options SET
				o_num='$o_num' , 
				title = '" . $op_info[0] . "',
				qty = '" . $op_info[1] . "',
				price = '" . $op_info[2] . "',
				price_supply = '" . $psp . "',
				oidx = '" . $op_info[4] . "'
				";
				mysql_query( $sql );
				//echo $sql."<br /><br />";
				$price += $op_info[2];
				$price_supply += $op_info[3]*$op_info[1];
			}
		}else {
			if ( $no_option_qty > 0) {
			//if ( $no_option_qty > 0 && $price_sales > 0) {
				$price = $price_sales * $no_option_qty;
				$price_supply = $_goods["price_supply"] * $no_option_qty;
				$sql_append = ",  no_option_qty = '$no_option_qty' ";			
			}else {
				$redirect_url = "/order/goods_select.php?gcode=".$gcode;
				GOURL__( $redirect_url , $msg="수량 또는 상품가격에 오류가 발생하였습니다." );
			}
		}

		if ( $price == 0 ) {
			$price = $price_sales;
		}
		$total_price = $price + $d_fee - $use_point;
		if ( $pay == "1" ||  $pay == "2" ) {
			$mu_name = "";
			$bank = "";
			$mu_name = "";
			$receipt_yn = "0";
			$receipt_gubun = "";
			$receipt_number = "";
		}
		
		$new_point = (int)( ( $price - $use_point ) * $_goods["per_0"] / 100 );

		$sql = "INSERT INTO orders SET
		o_num = '" . $o_num . "' ,
		gcode = '" . $gcode . "' ,
		shop_id = '" . $MEDIA_CODE . "' ,
		channel = '" . $channel . "' ,
		uid = '" . $_SESSION[$MEDIA_CODE.'_id'] . "' ,
		order_name = '" . $order_name . "' ,
		order_phone = '" . $order_phone . "' ,
		rcv_name = '" . $receive_name . "' ,
		rcv_tel = '" . $receive_tel . "' ,
		rcv_phone = '" . $receive_phone . "' ,
		zip = '" . $zipcode . "' ,
		addr1 = '" . $addr1 . "' ,
		addr2 = '" . $addr2 . "' ,
		pay = '" . $pay . "' ,
		mu_name = '" . $mu_name . "' ,
		mu_bank = '" . $bank . "' ,
		price_supply = '" . $price_supply . "' ,
		receipt_yn = '" . $receipt_yn . "' ,
		receipt_gubun = '" . $receipt_gubun . "' ,
		receipt_number = '" . $receipt_number . "' ,
		status = '0' ,
		use_point = '" . $use_point . "',
		new_point = '" . $new_point . "',
		price = '" . $price . "',
		delivery_fee = '" . $d_fee . "' ,
		total_price = '" . $total_price . "' ,
		order_comment = '" . $order_comment . "' ,
		o_date = '" . date("Y-m-d H:i:s") . "'
		" . $sql_append . "";

		$query = mysql_query( $sql );	
		if ($query) {
			//비회원일 경우 주소 쿠키에 저장 30일
			if ($_SESSION[$MEDIA_CODE.'_id'] == "" ) {
				@setcookie($MEDIA_CODE.'_temp_addr' , $zipcode."|^|".$addr1."|^|".$addr2 , time()+2592000 , "/" , $_SERVER["SERVER_NAME"]);
			}
			if ($pay == "3" ) {
				_AT( "1" , $p_num );
				//SMS( "1" , $p_num );
				//SMSSEND__( $o_num , "1" );
				//완료페이지로 연결
				GOURL__( "./order_ok.php?o_num=".$o_num , $msg = "" );
			}else {
				if ( $pay == "1" ) {
					//카드 결재 모듈 실행
					header("location: ./card.php?o_num=$o_num");
				}elseif ( $pay == "2" ) {
					//핸드폰 결재 모듈 실행
					header("location: ./phone.php?o_num=$o_num");
				}
			}
		}else {
			if ( $redirect_url == "" ) {
				$redirect_url = "/order/goods_select.php?gcode=".$gcode;
			}
			//echo $msg_html ;
			GOURL__( $redirect_url , $msg="오류가 발생하였습니다." );
		}
	}else {
		$redirect_url = "/order/goods_select.php?gcode=".$gcode;
		GOURL__( $redirect_url , $msg="오류가 발생하였습니다." );
	}
}

?>
<?
$PAGE_TITLE = "장바구니";
$page_name = "cart";
include ("../include/header.php");
?>

<!-- BODY -->
<section class="sub-body">

	<?include ("../include/mypage_menu.php");?>
	<div class="order-area cart">
		<h2 class="hide">장바구니리스트</h2>
		<div class="order-title">
			<div class="checkbox">
				<div class="checks"><input type="checkbox" id="order-all" name="order-list" checked><label for="order-all">전체선택/해제</label></div>
			</div>
			<div class="btn-area"><a href="#" class="btn" id="sel_delete">선택삭제</a></div>
		</div>
		
		<ul class="order-list">
		<?
		$rst = mysql_query("select * from cart as A left join goods_detail as B ON A.gcode=B.no where uid='" . $_UID . "' order by seller_id asc , order_deli desc");
		while ( $row = mysql_fetch_array( $rst ) ) {
			$row_goods = getdata("SELECT A.*, B.* FROM `goods` as A left join goods_detail as B ON A.no=B.no where A.no = '" . $row["gcode"] . "' ");
			$opt_arr = explode( "@" , $row["order_options"] );
			$totOrderPrice = 0;
			$totDeliPrice = 0;
			$optQtyAll = 0;
			$use_row = true;
			$use_row_opt = false;
			$use_arr_opt = array();
			$opt_arr_qty = array();
			$opt_arr_price = array();
			//상품 판매중인지 확인 체크박스 막음
			if ( $row_goods["dateStart"] > date("Y-m-d H:i:s") || $row_goods["dateEnd"] < date("Y-m-d H:i:s") || $row_goods["status"] <> "판매중" ) {
				$use_row = false;
			}
			for ( $i = 0 ; $i < count( $opt_arr ) ; $i++ ) {
				$opt_data = explode( "|" , $opt_arr[$i] );
				$opt_goods = getdata("select * from goods_option_data where goods_no='" . $row["gcode"] . "' and okey='" . $opt_data[0] . "' ");
				if ( $opt_goods["goods_no"] == "" ) { //무옵션일때 / 배송비 따로 계산할 필요없음
					$optTF = false;
					if ( $row_goods["qtyInventory"] < $opt_data[1] ) {
						$use_row = false;
					}else {
						$totDeliPrice = $row["order_deli"];
					}
				}else { //옵션이 있을때 하나라도 판매중인게 있다면 패스
					$optTF = true;
					if ( $opt_goods["qty"] >= $opt_data[1] && $opt_goods["hid"] == "0" ) {
						$use_row_opt = true;
						$optQtyAll += $opt_data[1];
					}
				}
			}
			if ( $use_row_opt == true ) {
				
				//배송비 계산 다시
				if ( $row["deliPay"] == "S" || $row["deliPay"] == "B" ) {
					$totDeliPrice = "0";	
				}else {
					$arr_deli_tbl = explode( "|" , $row_goods["tbl"] );
					if ( $row_goods["type"] == "" || $row_goods["type"] == "미입력" || $row_goods["pay"] == "무료배송" ) { //무료배송
						$totDeliPrice = "0";
					}elseif ( $row_goods["type"] == "고정배송비" ) {
						$totDeliPrice = $row_goods["fee"];
					}elseif ( $row_goods["type"] == "수량별비례") {
						$totDeliPrice = "0";
						$arr_a = explode("+",$arr_deli_tbl[0]);
						$arr_b = explode("+",$arr_deli_tbl[1]);
						if ( $arr_a[0] >= $optQtyAll ) {
							$totDeliPrice = $arr_a[1];
						}else {
							$tmp_qty = $optQtyAll - $arr_a[0]; // 두번째에 해당하는 물건 수
							$qty_set = ( $tmp_qty - ( $tmp_qty % $arr_b[0] ) ) / $arr_b[0]; //몫
							$qty_etc = $tmp_qty % $arr_b[0]; //나머지
							if ( $qty_set != 0 ) {
								$totDeliPrice = $qty_set * $arr_b[1];
							}
							if ( $qty_etc != 0 ) {
								$totDeliPrice = $totDeliPrice + $arr_b[1];
							}
							$totDeliPrice = $totDeliPrice + $arr_a[1];
						}
					}elseif ( $row_goods["type"] == "수량별차등" ) {
						for ( $j = count( $arr_deli_tbl )-1 ; $j >= 0 ; $j-- ) {
							$arr_deli_fee = explode( "+" , $arr_deli_tbl[$j] );
							if ( $arr_deli_fee[0] <= $optQtyAll ) {
								$totDeliPrice = $arr_deli_fee[1];
								$pickup = true;
								break;
							}
						}
					}
				}
			}
			//옵션 내부 확인하여 전부 판매중지이면 체크박스 막음
			if ( $use_row == false || ( $optTF == true && $use_row_opt == false ) ) {
				$style_row_txt_1 = "style=\"background-color:#ededed;\"";
				$chk_txt = "disabled";
			}else {
				$style_row_txt_1 = "";
				$chk_txt = "";
			}

			$packed_apply = false;
			$packed_charge = false;
			$packed_deli_price = 0;
			//묶음배송 관련... 배열에 제일높은값을담고 일치하는게 있으면 무시하고 묶음배송이라고 표시해준다.
			//적용상품이 구매가 가능한 상태인지 확인
			if ( $row_goods["shippingArea"] != "" && $style_row_txt_1 == "" ) {
				$packed_apply = true;
				//배열중에 제일 높은 값인지 확인
				if ( $packed["_".$row_goods["shippingArea"]] == "" ) {
					$packed_charge = true;
					$packed["_".$row_goods["shippingArea"]] = $row["order_deli"];
					$packed_deli_price = $row["order_deli"];
				}else {
					$totDeliPrice = 0;
				}
			}
			if ( $packed_apply == false ) {
				if ( $row["order_deli"] == "0" ) {
					$deli_str = "무료배송";
				}else {
					$deli_str = number_format( $row["order_deli"] ) . "원";	
				}
			}else {
				if ( $packed_charge == true ) {
					if ( $row["order_deli"] == "0" ) {
						$deli_str = "무료배송";
					}else {
						$deli_str = number_format( $row["order_deli"] ) . "원";	
					}				
				}else {
					$deli_str =  "<strike>" . number_format( $row["order_deli"] ) . "원</strike> (묶음배송 적용)";
				}
			}
		?>
			<li id="list_id_<?=$row["idx"]?>" <?=$style_row_txt_1?>>
				<div class="checks"><input type="checkbox" name="order_list" id="chk_id_<?=$row["idx"]?>" class="order_list" value="<?=$row["idx"]?>" <?=$chk_txt?>><label for="chk_id_<?=$row["idx"]?>"></label></div>
				<div class="product">
					<div class="img"><a href="../product/product_view.php?no=<?=$row["gcode"]?>" class="product-view"><img src="<?=$row_goods["thumb"]?>"></a></div>
					<div class="text">
						
							<a href="../product/product_view.php?no=<?=$row["gcode"]?>" class="product-view"><h3><?=$row_goods["title"]?><?=$row_goods["shippingArea"] ."...".$packed_apply?></h3></a>
							<p class="pay"></p>
							<p class="gray" id="deli_pay_txt_<?=$row["idx"]?>">배송비 <?=$deli_str?></p>
						
					</div>
					<button class="btn-delete del_row" data-id="<?=$row["idx"]?>"><span class="hide">삭제</span></button>
					<? if ( $chk_txt == "" ) { ?>
					<?
					for ( $i = 0 ; $i < count( $opt_arr ) ; $i++ ) {
						$opt_data = explode( "|" , $opt_arr[$i] );
						$use_ok = true;
						$opt_goods = getdata("select * from goods_option_data where goods_no='" . $row["gcode"] . "' and okey='" . $opt_data[0] . "' ");
						if ( $opt_goods["goods_no"] == "" ) {
							if ( $row_goods["qtyInventory"] < $opt_data[1] ) {
								$use_ok = false;
							}
							$opt_goods["qty"] = $row_goods["qtyInventory"];
							$optPrice = $row_goods["priceWe"] * $opt_data[1];
						}else {
							if ( $opt_goods["qty"] < $opt_data[1] || $opt_goods["hid"] != "0" ) {
								$use_ok = false;
							}	
							$optPrice = ( $row_goods["priceWe"] + $opt_goods["priceWe"] ) * $opt_data[1];
						}
						if ( $use_ok == false ) {
							$style_txt_1 = "style=\"background-color:#ededed;\"";
							$style_txt_2 = "style=\"color:#666;\"";
							$style_txt_3 = " - 선택 불가능한 옵션입니다.";
							$btn_qty_txt = "";
							$optPrice = 0;
						}else {
							$style_txt_1 = "";
							$style_txt_2 = "";
							$style_txt_3 = "";
							$btn_qty_txt = "<button type=\"button\" class=\"numbtn_minus\" value=\"-\" data-id=\"" . $row["idx"] . "\" data-key=\"" . $opt_data[0] . "\" ><span class=\"hide\">수량감소</span></button> <input type=\"number\" name=\"prdcAmount\" class=\"text qty_ipt\" id=\"qty_" . $row["idx"] . "_" . $opt_data[0] . "\" title=\"수량설정\" value=\"" . $opt_data[1] . "\" data-id=\"" . $row["idx"] . "\" data-key=\"" . $opt_data[0] . "\" data-qty=\"" . $opt_goods["qty"] . "\"> <button type=\"button\" class=\"numbtn_plus\" value=\"+\" data-id=\"" . $row["idx"] . "\" data-key=\"" . $opt_data[0] . "\" ><span class=\"hide\">수량증가</span></button>";
							$totOrderPrice += $optPrice;
						}
						//echo "select * from goods_option_data where goods_no='" . $row["gcode"] . "' and okey='" . $opt_data[0] . "' ";
					?>
					<div class="option-area" id="list_opt_<?=$opt_data[0]?>" <?=$style_txt_1?>>
						<?
						if ( count( $opt_arr ) > 1 ) {
						?>
						<button class="btn-delete del_one" data-id="<?=$row["idx"]?>" data-key="<?=$opt_data[0]?>"><span class="hide">삭제</span></button>
						<?}?>
						<div class="option" <?=$style_txt_2?>>&nbsp;&nbsp;<?=$opt_goods["oname"] . $style_txt_3?></div>
						<div class="option-info">
							<div class="plusminus_wrap">
								<?=$btn_qty_txt?>
				       	 	</div>
				        	<div class="pay"><span class="point" id="opt_price_txt_<?=$row["idx"]?>_<?=$opt_data[0]?>"><?=number_format( $optPrice )?></span>원</div>
						</div>
					</div>
					<?}?>					
					<input type="hidden" name="delipay" id="deli_pay_<?=$row["idx"]?>" value="<?=$totDeliPrice?>" class="delipay">
					<input type="hidden" name="cartpay" id="cart_pay_<?=$row["idx"]?>" value="<?=$totOrderPrice?>" class="cartpay">
					<?
						$order_price_txt += $totOrderPrice;
						$order_deli_txt += $totDeliPrice;
					}else{?>
					<div class="option-area" style="background-color:#ededed;">
					<p>현재 구매가 불가능한 상품입니다.</p>
					</div>
					<?}?>
				</div>
			</li>
		<?}?>
		</ul>
		
		<div class="price">
			<ul>
				<li>총 상품금액 <div class="point right"><span class="bold" id="order_options"><?=number_format( $order_price_txt )?></span>원</div></li>
				<li>배송비 <div class="point right"><span class="bold" id="order_deli"><?=( $order_deli_txt > 0 ) ? "+ " . number_format( $order_deli_txt ) : "0" ?></span>원</div></li>
				<li class="price_all">총결제금액 <div class="point right"><span class="bold" id="order_price"><?=number_format( $order_price_txt + $order_deli_txt )?></span>원</div></li>
			</ul>
		</div>
		<div class="order-footer">
			<div class="pay">총 결제액 <span class="point" id="total_price"><?=number_format( $order_price_txt + $order_deli_txt )?></span>원</div>
			<div class="btn-area">
				<a href="#" class="btn-point w100p" id="payment_regist">결제하기</a>	
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
<!--
	$(document).on("click","#order-all", function(){
		if ( $(this).is(":checked") == true ) {
			$(".order_list").not(":disabled").prop("checked", true );
		}else {
			$(".order_list").prop("checked", false );
		}
	});

	$(document).ready(function(){
		$(".order_list").not(":disabled").prop("checked", true );
		select_commit();
	});

	$(document).on("click","#payment_regist", function(){
		var nChk = document.getElementsByName("order_list");
		var gid = "";
		if(nChk){
			for(i=0;i<nChk.length;i++) { 
				if(nChk[i].checked){
					if(gid ==''){
						gid = nChk[i].value; 
					}else{
						gid =  gid+ ',' +nChk[i].value;
					}
				} 
			}
		}
		if ( gid != "" ) {				
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=payment_regi&carts='+gid,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						location.href="../order/order_payment.php?oidx="+data.oidx;
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}else {
			alert("선택하신 상품이 없습니다.");
		}
		return false;
	});

	$(document).on("click","#sel_delete", function(){
		var nChk = document.getElementsByName("order_list");
		var gid = "";
		if(nChk){
			for(i=0;i<nChk.length;i++) { 
				if(nChk[i].checked){
					if(gid ==''){
						gid = nChk[i].value; 
					}else{
						gid =  gid+ ',' +nChk[i].value;
					}
				} 
			}
		}
		if ( gid != "" ) {
			var con = confirm("선택하신 상품을 장바구니에서 삭제하시겠습니까?");
			if ( con ) {
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: './_proc_json.php',
					data: 'mode=del_row&idx=' + gid,
					success:function (data) {
						console.log(data);
						if ( data.rst == "1" ) {
							if (gid.match(",")) {
								var arr_gid = gid.split(",");
								for ( i = 0 ; i<arr_gid.length ; i++ ) {
									$("#list_id_"+arr_gid[i]).remove();
								}
							} else {
								$("#list_id_"+gid).remove();
							}
							select_commit();
						}else {
							alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
							location.reload();
						}
					}, error : function (data) {
						console.log(data);
					} 
				});
			}
		}else {
			alert("선택하신 상품이 없습니다.");
		}
		return false;
	});

	$(document).on( "click" , ".del_row" , function(){		
		var gid = $(this).data("id");
		var con = confirm("선택하신 상품을 장바구니에서 삭제하시겠습니까?");
		if ( con ) {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=del_row&idx=' + gid,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						$("#list_id_"+gid).remove();
					}else {
						alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
						location.reload();
					}
					select_commit();
				}, error : function (data) {
					console.log(data);
				} 
			});
		}
	});

	$(document).on( "click" , ".del_one" , function(){
		var val = $(this).data("id");
		var okey = $(this).data("key");
		$("#id_"+val).remove();
		$.ajax({
			type: 'POST',
			cache: false,
			dataType: 'json',
			url: './_proc_json.php',
			data: 'mode=del_one&idx=' + val + '&okey=' + okey ,
			success:function (data) {
				console.log(data);
				if ( data.rst == "1" ) {
					$("#list_opt_"+okey).remove();					
					$("#deli_pay_txt_"+val).html(data.deli_txt);
					$("#deli_pay_"+val).html(data.deli);
					$("#cart_pay_"+val).html(data.tot_price);
					select_commit();
				}else {
					alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
					location.reload();
				}
			}, error : function (data) {
				console.log(data);
			} 
		});
	});

	$(document).on( "click" , ".numbtn_minus" , function(){
		var val = $(this).data("id");
		var okey = $(this).data("key");
		var nowval = parseInt( $("#qty_"+val+"_"+okey).val() );
		var oqty = $("#qty_"+val+"_"+okey).data("qty");
		if ( nowval <= 1 ) {
			alert("수량은 1개 이상 입니다.");
		}else {			
			$("#qty_"+val+"_"+okey).val((nowval-1));
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=chg_qty&idx=' + val + '&okey=' + okey + '&qty=' + (nowval-1) ,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						$("#deli_pay_txt_"+val).html(data.deli_text);
						$("#opt_price_txt_"+val+"_"+okey).html(data.price_txt);
						$("#deli_pay_"+val).val(data.deli);
						$("#cart_pay_"+val).val(data.tot_price);
						select_commit();
					}else {
						alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
						location.reload();
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}
		return false;
	});

	$(document).on( "click" , ".numbtn_plus" , function(){
		var val = $(this).data("id");
		var okey = $(this).data("key");
		var nowval = parseInt( $("#qty_"+val+"_"+okey).val() );
		var oqty = $("#qty_"+val+"_"+okey).data("qty");
		if ( nowval >= oqty ) {
			alert("재고수량을 초과하였습니다.");
		}else {
			$("#qty_"+val+"_"+okey).val((nowval+1));
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=chg_qty&idx=' + val + '&okey=' + okey + '&qty=' + (nowval+1) ,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						$("#deli_pay_txt_"+val).html(data.deli_txt);
						$("#opt_price_txt_"+val+"_"+okey).html(data.price_txt);
						$("#deli_pay_"+val).val(data.deli);
						$("#cart_pay_"+val).val(data.tot_price);
						select_commit();
					}else {
						alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
						location.reload();
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}
		return false;
	});

	$(document).on("change",".qty_ipt",function(e){
		var val = $(this).data("id");
		var okey = $(this).data("key");
		var nowval = parseInt( $(this).val() );
		var oqty = $("#qty_"+val+"_"+okey).data("qty");
		if ( nowval >= oqty ) {
			alert("재고수량을 초과하였습니다.");
		}else {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=chg_qty&idx=' + val + '&okey=' + okey + '&qty=' + nowval ,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						$("#deli_pay_txt_"+val).html(data.deli_text);
						$("#opt_price_txt_"+val+"_"+okey).html(data.price_txt);
						$("#deli_pay_"+val).val(data.deli);
						$("#cart_pay_"+val).val(data.tot_price);
						select_commit();
					}else {
						alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
						location.reload();
					}
				}, error : function (data) {
					console.log(data);
				} 
			});

		}
		return false;
	});

	$(document).on("keydown",".qty_ipt",function(e){
		var keycode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
		if ((keycode <= 57 && keycode >= 48) || (keycode <= 105 && keycode >= 96) || keycode < 33 || keycode == 46 || keycode == 37 || keycode == 39) {
			if(keycode == 13){
				e.target.onchange();
				return false;
			}
			return true;
		} else {
			if (!e.preventDefault) {
				e.returnValue = false;
			} else {
				e.preventDefault();
				return false;
			}
			return false;
		}
	});
	
	function select_commit() {
		var tot_price = 0;
		var tot_deli = 0;
		if ( $(".delipay").length > 0 && $(".cartpay").length > 0 ) {
			$(".cartpay").each(function(index){ tot_price = tot_price + parseInt( $(this).val() ); });
			$(".delipay").each(function(index){ tot_deli = tot_deli + parseInt( $(this).val() ); });
		}
		$("#order_options").html( addComma( tot_price ) );
		$("#order_deli").html( "+ " + addComma( tot_deli ) );
		$("#order_price").html( addComma( tot_price + tot_deli ) );
		$("#total_price").html( addComma( tot_price + tot_deli ) );
		//alert("배송비 수:"+$(".delipay").length+"\n장바구니 상품 금액 수"+$(".cartpay").length+"\n배송비합:"+tot_deli+"\n상품금액합:"+tot_price);		
	}

	function addComma(n) {
		var reg = /(^[+-]?\d+)(\d{3})/;
		n += '';
		while (reg.test(n)) {
			n = n.replace(reg, '$1' + ',' + '$2');
		}
		return n;
	}

	//수량 변경 시 
	//선택 결재
	//선택 시 하단 총 결재 부분 수정
	//재고, 노출 x 옵션 표시 및 수정
	//select_commit 전체 재계산 수정
	//메일 전송 - 옵션 갯수 초과, 도매꾹 링크, 외부 링크 구분해서 a태그 막기??
//-->
</script>
<?
include ("../include/footer.php");
?>
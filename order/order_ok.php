<?
$PAGE_TITLE = "주문완료";
include ("../include/header.php");
sleep(3);
$oidx = mysql_real_escape_string( $_GET["oidx"] );
$deli_yn = false;
$row = getdata("SELECT * FROM payments WHERE num='" . $oidx . "' ");
if ( $row["num"] != "" ) {
	$order_list = json_decode( $row["order_content"] );
	//장바구니 삭제
	CUT_CART($oidx);	

	if			( $row["pay"] == "1" ) { $pay_str = "신용카드[일반]"; }
	elseif	( $row["pay"] == "2" ) { $pay_str = "휴대전화"; }
	elseif	( $row["pay"] == "3" ) { $pay_str = "무통장입금"; $mu_str = "<br />입금자명 : " . $row["mu_name"] . "<br />입금은행 : " . str_replace( "|^|", " " , $row["mu_bank"]); }
	elseif	( $row["pay"] == "4" ) { $pay_str = "신용카드[간편]"; }
	elseif	( $row["pay"] == "5" ) { $pay_str = "네이버페이"; }
	elseif	( $row["pay"] == "6" ) { $pay_str = "카카오페이"; }

	if ( $row["pay"] == "3" || $row["status"] == "2" ) {
		$pay_rst = "주문이 정상적으로 완료되었습니다.";
	}elseif ( $row["status"] == "2" ) {
		$pay_rst = $row["rst_msg"];
	}
}else {
	GOURL__( "/", "잘못된 결재정보입니다." );
}
?>
<!-- BODY -->
<section class="sub-body">
<form class="form">
	<h2 class="hide">정보입력폼</h2>
	<div class="order-form">
		<div class="msg-box">
			<span><?=$pay_rst?></span>
			<span>주문번호 : <?=$oidx?></span>
			<span>(마이페이지에서 배송확인이 가능합니다.)</span>
			<a href="../product/story_shop.php"><img src="../assets/images/sub/top_bnr01.png"></a>
		</div>
		<div class="finish-box">
			<h3>주문자정보</h3>
			<ul>
				<li><label>주문자명</label><div class="text"><?=$row["order_name"]?></div></li>
				<li><label>연락처</label><div class="text"><?=$row["order_phone"]?></div></li>
				<li><label>이메일</label><div class="text"><?=( $row["order_email"] == "@" ? "미입력" : $row["order_email"] )?></div></li>
			</ul>
		</div>
		<div class="finish-box">
			<h3>배송지 정보</h3>
			<ul>
				<li><label>수령인</label><div class="text"><?=$row["rcv_name"]?></div></li>
				<li><label>배송지</label><div class="text"> <?=$row["zip"]?><br><?=$row["addr1"]?> <?=$row["addr2"]?></div></li>
				<li><label>연락처</label><div class="text"><?=$row["rcv_phone1"]?> <?=( $row["rcv_phone2"] != "" ) ? " , " . $row["rcv_phone2"] : "" ?></div></li>
				<li><label>배송메세지</label><div class="text">부재시, 경비실에 맡겨 주세요.</div></li>
			</ul>
		</div>
		<div class="finish-box">
			<h3>결제방식</h3>
			<ul>
			

			<? if ( $row["pay"] == "1" ) { //신용카드?>
				<li><label>결제방법</label><div class="text">신용카드</div></li>
				<li><label>카드사 주문번호</label><div class="text"><?=$row["rst_id"]?></div></li>
				<li><label>카드결제 결과</label><div class="text"><?=$row["rst_msg"]?></div></li>
			<?}?>
			<? if ( $row["pay"] == "2" ) { //핸드폰?>
				<li><label>결제방법</label><div class="text">휴대폰결재</div></li>
				<li><label>승인번호</label><div class="text"><?=$row["rst_id"]?></div></li>
				<li><label>결제 결과</label><div class="text"><?=$row["rst_msg"]?></div></li>
			<?}?>
			<? if ( $row["pay"] == "3" ) { //무통장?>
				<li><label>결제방법</label><div class="text">무통장입금</div></li>
				<li><label>입금자명</label><div class="text"><?=$row["mu_name"]?></div></li>
				<li><label>입금은행</label><div class="text"><?=str_replace( "|^|" , " " , $row["mu_bank"] )?></div></li>
			<?}?>
			<? if ( $row["pay"] == "6" ) { //카카오페이?>
				<li><label>결제방법</label><div class="text">카카오페이</div></li>
				<li><label>결제번호</label><div class="text"><?=$row["rst_id"]?></div></li>
				<li><label>결제 결과</label><div class="text"><?=$row["rst_msg"]?></div></li>
			<?}?>
			</ul>
		</div>
		<div class="finish-box">
			<h3>결제정보</h3>
			<ul>
				<li><label>총 상품금액</label><div class="text"><div class="point right"><span class="bold"><?= number_format( $row["price"] )?></span>원</div></div></li>
				<li><label>배송비</label><div class="text"><div class="point right"><span class="bold">+ <?= number_format( $row["delivery_fee"] )?></span>원</div></div></li>
				<? if ( $row["delivery_add_fee"] > 0 ) {?>
				<li><label>추가배송비</label><div class="text"><div class="point right"><span class="bold">+ <?= number_format( $row["delivery_add_fee"] )?></span>원</div></div></li>
				<?}?>
				<? if ( $row["use_point"] > 0 ) {?>
				<li><label>포인트사용</label><div class="text"><div class="point right"><span class="bold">- <?= number_format( $row["use_point"] )?></span>원</div></div></li>
				<?}?>
				<li><label>총결제금액</label><div class="text"><div class="point right"><span class="bold point"><?= number_format( $row["total_price"] )?></span>원</div></div></li>
			</ul>
		</div>
		<div class="order-footer">
			<div class="btn-area">
				<a href="/" class="btn-point w100p">메인으로</a>	
			</div>
		</div>
	</div>
</form>
</section>
<script type="text/javascript">
<!--
	history.pushState(null, null, '');
	window.onpopstate = function(event) {
		history.back();
		location.href= "/";
	};
//-->
</script>
<?
include ("../include/footer.php");
?>
<?
$PAGE_TITLE = "주문서작성";
include ("../include/header.php");
$oidx = mysql_real_escape_string( $_GET["oidx"] );
$deli_yn = false;
$row = getdata("SELECT * FROM payments WHERE num='" . $oidx . "' ");

if ( $_SESSION["mstsp_id"] != "" ) {//배송지 사전입력 확인
	$_deli_info = getdata("select count( * ) as cnt from order_deli where uid='" . $_SESSION["mstsp_id"] . "' ");
	if ( $_deli_info["cnt"] > 0 ) {
		$deli_yn = true;
	}	
}
if ( $_SESSION["mstsp_phone"] != "" ) {
	$ss_phone = explode( "-" , $_SESSION["mstsp_phone"] );
}
if ( $_SESSION["mstsp_email"] != "" ) {
	$ss_email = explode( "@" , $_SESSION["mstsp_email"] );
}
$goods_name = "";
?>
<style type="text/css" title="">
	#sddr_layer {position:fixed; overflow:hidden; z-index:999; -webkit-overflow-scrolling:touch; top:0; width:100%; height:100%;max-width:900px;}
	#sddr_layer .fix_tit {background: #fff; width:100%; height:50px; line-height:50px; position:relative; text-align:center;}
	#sddr_layer .fix_tit > h1 {display:inline-block; text-align:center; color:#fff; font-size:20px;}
	#sddr_layer .fix_tit button {background:none; color:#fff; position:absolute; right:20px; line-height:55px;}
</style>
<div id="sddr_layer" style="display:none;">
	<div class="fix_tit">
		<h1>우편번호 찾기</h1>
		<button id="btnCloseLayer" onclick="closeDaumPostcode()"><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
	</div>
</div>
<!-- BODY -->
<section class="sub-body">
<form name="regi_form" id="regi_form" action="./_proc_json.php" method="post">
<input type="hidden" name="mode" id="mode" value="payment_try">
<input type="hidden" name="oidx" value="<?=$oidx?>">
<input type="hidden" name="deli_directly" id="deli_directly" value="<?= ( $deli_yn == true ) ? "0" : "1" ?>">
<input type="hidden" name="order_zip" id="order_zip" value="">
<input type="hidden" name="order_addr1" id="order_addr1" value="">
<input type="hidden" name="order_addr2" id="order_addr2" value="">
<input type="hidden" name="order_island" id="order_island" value="">

	<h2 class="hide">정보입력폼</h2>
	<div class="order-form">
		<div class="form-box delivery-form">
			<h3>배송지 정보입력</h3>
			<div class="tab n2">
				<a class="deli_tab <?= ( $deli_yn == true ) ? "current" : "" ?>"  data-tab="delivery-list-tab">배송지목록</a>
				<a class="deli_tab <?= ( $deli_yn == false ) ? "current" : "" ?>" data-tab="delivery-tab">직접입력</a>
			</div>
			<div id="delivery-list-tab" class="tabcontent <?= ( $deli_yn == true ) ? "current" : "" ?>">
				<h4 class="hide">배송지목록</h4>
				<p class="guide-text">최근 사용된 배송지 목록입니다. 배송지를 선택해주세요.<br>주문후 배송지 변경은 <?=$CSC_PHONE__?>로 문의 주세요</p>
				<? if ( $deli_yn == false ) { ?>
				<div class="guide-text2">현재 등록된 배송지가 없습니다</div>	
				<?}else{?>
				<table class="delivery_info_list">
					<thead>
						<tr>
							<th>배송지</th>
							<th>연락처</th>
							<th>주소</th>
							<th>선택</th>
						</tr>
					</thead>
					<tbody>
					<?
					$rst_deli = mysql_query("select * from order_deli where uid='" . $_SESSION["mstsp_id"] . "' order by reg_date desc limit 0 , 5");
					while ( $row_deli = mysql_fetch_array( $rst_deli ) ) {
					?>
						<tr>
							<td><?=$row_deli["dname"]?></td>
							<td><?=$row_deli["dphone1"]?>-<?=$row_deli["dphone2"]?>-<?=$row_deli["dphone3"]?></td>
							<td><span style="margin-right:10px;"><?=$row_deli["zip"]?></span><?=$row_deli["addr1"]?><br><?=$row_deli["addr2"]?></td>
							<td><button type="button" class="btn select_deli" data-zip="<?=$row_deli["zip"]?>" data-addr1="<?=$row_deli["addr1"]?>" data-addr2="<?=$row_deli["addr2"]?>" data-dname="<?=$row_deli["dname"]?>" data-dphone1="<?=$row_deli["dphone1"]?>" data-dphone2="<?=$row_deli["dphone2"]?>" data-dphone3="<?=$row_deli["dphone3"]?>" data-email1="<?=$row_deli["email1"]?>" data-email2="<?=$row_deli["email2"]?>">선택</button></td>
						</tr>
					<?}?>
					</tbody>
				</table>
				<?}?>
			</div>
			<div id="delivery-tab" class="tabcontent <?= ( $deli_yn == false ) ? "current" : "" ?>">
				<h4 class="hide">직접입력</h4>
				<p class="guide-text"><span class="point">*</span> 필수 입력사항</p>
				<ul>
					<li>
						<label>수령인<span class="point">*</span></label>
						<input type="text" name="d_dname" id="d_dname" value="" />
					</li>
					<li>
						<label>배송지<span class="point">*</span></label>
						<div class="address-area">
							<div class="zipcode">
								<div class="zipcode-input"><input type="text" name="zip-code" id="zip" readonly="readonly" maxlength="5" /></div>
								<button type="button" name="zipcode-btn" class="btn zipcode-btn" id="search_addr">우편번호검색</button>
							</div>
							<input type="text" name="d_addr1" id="d_addr1" value="" readonly="readonly" />
							<input type="text" name="d_addr2" id="d_addr2" value="" />
						</div>
					</li>
					<li>
						<label>연락처<span class="point">*</span></label>
						<select name="d_phone1" id="d_phone1" style="width:70px;display:inline;height:35px;border:1px solid #ddd;">
							<option value="010">010</option>
							<option value="011">011</option>
							<option value="017">017</option>
							<option value="018">018</option>
							<option value="019">019</option>
						</select> -
						<input type="number" name="d_phone2" id="d_phone2" class="number" style="width:70px;display:inline;" value="" /> -
						<input type="number" name="d_phone3" id="d_phone3" class="number" style="width:70px;display:inline;" value="" />
					</li>
					<?if ( $_SESSION["mstsp_id"] != "" ) {?>
					<li class="checks-box">
						<div class="checks">
							<input type="checkbox" id="delivery-list" name="delivery_list" value="1" checked> 
							<label for="delivery-list">배송지 목록에 추가하기</label>
						</div>
					</li>
					<?}?>
				</ul>
			</div>
			<div class="select-area">
				<label>배송시 전달할 메세지</label>
				<select name="deli_comment" id="deli_comment">
					<option value="">배송시 전달할 사항을 선택해주세요</option>
					<option value="배송전 연락바랍니다.">배송전 연락바랍니다.</option>
					<option value="부재시, 경비실에 맡겨 주세요.">부재시, 경비실에 맡겨 주세요.</option>
					<option value="부재시, 전화 또는 문자 연락주세요.">부재시, 전화 또는 문자 연락주세요.</option>
					<option value="택배함에 넣어주세요.">택배함에 넣어주세요.</option>
					<option value="파손 위험이 있는 상품이니 조심히 다뤄주세요.">파손 위험이 있는 상품이니 조심히 다뤄주세요.</option>
					<option value="directly">직접입력</option>
				</select>
				<input type="text" name="deli_ipt" id="deli_ipt" value="" class="" style="display:none;margin-top:5px;" placeholder="배송시 요청사항을 입력해주세요 ">
			</div>
		</div>
		<div class="form-box">
			<h3>주문자정보</h3>
			<ul>
				<li>
					<label>주문자명<span class="point">*</span></label>
					<input type="text" name="order_name" id="order_name" value="<?=$_SESSION["mstsp_name"]?>">
				</li>
				<li>
					<label>연락처<span class="point">*</span></label>
					<select name="order_phone1" id="order_phone1" style="width:70px;display:inline;height:35px;border:1px solid #ddd;">
						<option value="010" <?= ( $ss_phone[0] == "010" ) ? "selected" : "" ?>>010</option>
						<option value="011" <?= ( $ss_phone[0] == "011" ) ? "selected" : "" ?>>011</option>
						<option value="017" <?= ( $ss_phone[0] == "017" ) ? "selected" : "" ?>>017</option>
						<option value="018" <?= ( $ss_phone[0] == "018" ) ? "selected" : "" ?>>018</option>
						<option value="019" <?= ( $ss_phone[0] == "019" ) ? "selected" : "" ?>>019</option>
					</select> -
					<input type="number" name="order_phone2" id="order_phone2" class="number" style="width:70px;display:inline;" value="<?=$ss_phone[1]?>" /> -
					<input type="number" name="order_phone3" id="order_phone3" class="number" style="width:70px;display:inline;" value="<?=$ss_phone[2]?>" />
				</li>
				<li>
					<label>이메일</label>
					<input type="text" name="order_email1" id="order_email1" class="number" style="width:100px;display:inline;" value="<?=$ss_email[0]?>" />@
					<input type="text" name="order_email2" id="order_email2" class="number" style="width:100px;display:inline;" value="<?=$ss_email[1]?>" />
					<select name="email_select" style="display:inline;height:35px;border:1px solid #ddd;" onchange="emailsel(this.value);">
						<option value="">선택하세요</option>
						<option value="naver.com">naver.com (네이버)</option>
						<option value="hanmail.net">hanmail.net (한메일)</option>
						<option value="daum.net">daum.net (다음)</option>
						<option value="hotmail.com">hotmail.com (구글)</option>
						<option value="nate.com">nate.com (네이트)</option>
						<option value="d">직접입력</option>
					</select>
				</li>
			</ul>
		</div>
		<div class='form-box'>
		
		<ul class="order-list">
		<?	
		$order_info = json_decode( $row["order_content"] );
		$tot_price = 0;
		$tot_deli = 0;
		for ( $i = 0 ; $i < count($order_info) ; $i++ ) {
			$goods_info = getdata( "select * from goods as A left join goods_detail as B on A.no = B.no where A.no='" . $order_info[$i]->no . "'" );
			$goods_opt = "";
			$opt_arr = explode( "@" , $order_info[$i]->order_options);
			for ( $j = 0 ; $j < count( $opt_arr ) ; $j++ ) {
				$opt_data = explode( "|" , $opt_arr[$j] );				
				$opt_goods = getdata("select * from goods_option_data where goods_no='" . $order_info[$i]->no . "' and okey='" . $opt_data[0] . "' ");
				if ( $opt_goods["goods_no"] != "" ) {
					$goods_opt .= $opt_goods["oname"];
				}
				$goods_opt .= $opt_data[1] . "개 | ";
			}
			$tot_price += $order_info[$i]-> order_price;
			$tot_deli += $order_info[$i]-> order_deli;
			$goods_opt = substr( $goods_opt , 0 , -3 );
			
			$goods_codes .= $order_info[$i]->no . "|";
		?>
			<li id="list_id_<?=$goods_info["no"]?>">
				<div class="product">
					<div class="img" style="left:10px;"><a href="../product/product_view.php?no=<?=$goods_info["no"]?>"><img src="<?=$goods_info["thumb"]?>"></a></div>
					<div class="text" style="margin-left:100px;margin-right:10px;">						
							<a href="../product/product_view.php?no=<?=$goods_info["no"]?>"><h3 style="padding:0px;padding-bottom:5px;"><?=$goods_info["title"]?></h3></a>
							<p class="pay"><?=$goods_opt?></p>
							<p class="pay">배송비 : <?= ( $order_info[$i]->order_deli == "0" ) ? "무료배송" : number_format( $order_info[$i]->order_deli ) . "원"?><br />상품금액 : <?=number_format( $order_info[$i]-> order_price )?> 원<br />적립예정 포인트 금액 : <?= number_format( $order_info[$i]-> order_price * $NEW_ORDER_POINT_RATE )?>원</p>
					</div>
				</div>
				<div class="product">
				<p>판매자에게 전달할 내용</p>
				<textarea  id="comment_<?=$goods_info["no"]?>" name="comment_<?=$goods_info["no"]?>" style="margin:5px 0;width:100%;height:60px;border:1px solid #ddd"></textarea>
				</div>
			</li>
		<?}
			$goods_info = getdata("select * from goods where no = '" . $order_info[0] -> no . "' ");
			$goods_cnt = count( $order_info )-1;
			if ( $goods_cnt > 0 ) {
				$goods_name = $goods_info["title"] . "외 " . $goods_cnt . "건";
			}else {
				$goods_name = $goods_info["title"];	
			}
			$goods_codes = substr( $goods_codes , 0 , -1 );
		?>
		</ul>
		</div>
		<div class="price">
			<ul>
				<li>총 상품금액 <div class="point right"><span class="bold"><?=number_format( $tot_price )?></span>원</div></li>
				<li>배송비 <div class="point right"><span class="bold">+ <?=number_format( $tot_deli )?></span>원</div></li>
				<li id="deli_island_price_li" style="display:none">추가배송비 <div class="point right"><span class="bold" id="deli_island_price_txt">+ 0</span>원</div></li>
				<li id="use_point_li" style="display:<?= ( $_SESSION["mstsp_id"] != "" ) ? "" : "none" ?>">포인트사용(보유금액 : <?=number_format( $_MEM_INFO["point"] )?>원) <div class="point right"><span class="bold" id="use_point_txt">- <input type="number" name="use_point" id="use_point" class="number" style="width:70px;display:inline;text-align:right;" value="0" /></span>원 <a href="#" id="cal_point" style="display:inline;background: #574394; padding: 5px 8px; border-radius: 3px; color: #fff; font-size: 12px; font-weight: bold; margin-left: 5px;">적용</a></div></li>
				<li class="price_all">총결제금액 <div class="point right"><span class="bold point" id="tot_price"><?=number_format( $tot_price + $tot_deli )?></span>원</div></li>
			</ul>
		</div>
		<div class="form-box">
			<h3>결제정보</h3>
			<ul>
				<!--li class="checks-box"><div class="radio"><input type="radio" id="pay_rd1" name="chk_info" value="4" checked><label for="pay_rd1">신용카드 간편결제</label></div></li-->
				<li class="checks-box"><div class="radio"><input type="radio" id="pay_rd2" name="chk_info" value="1"><label for="pay_rd2">신용카드 일반결제</label></div></li>
				<li class="checks-box"><div class="radio"><input type="radio" id="pay_rd3" name="chk_info" value="3" ><label for="pay_rd3">무통장 결제</label></div></li>
				<li class="checks-box"><div class="radio"><input type="radio" id="pay_rd4" name="chk_info" value="2" ><label for="pay_rd4">휴대폰 결제</label></div></li>
				<!--li class="checks-box"><div class="radio"><input type="radio" id="pay_rd5" name="chk_info" value="5"><label for="pay_rd5">네이버페이 결제 <img src="../assets/images/naver_pay.png"></label></div></li-->
				<li class="checks-box"><div class="radio"><input type="radio" id="pay_rd6" name="chk_info" value="6"><label for="pay_rd6">카카오페이 결제 <img src="../assets/images/kakao_pay.png"></label></div></li>
			</ul>
			<div class="line" id="receipt" style="display:none;">
				<ul class="card-area">
					<li>
						<label>은행선택</label>
						<select name="mu_bank" style="width:auto;">					
						<?
							$bank_info = mysql_query( " select * from bank " );
							if (mysql_num_rows( $bank_info ) > 0 ) {
								$i=0;
								while ( $bank_unit = mysql_fetch_array( $bank_info ) ) {
									$i++;
						?>
							<option value="<?=$bank_unit["bankname"] . "|^|" . $bank_unit["account_number"] . "|^|" . $bank_unit["owner"] ?>" <?= ( $i == 1 ) ? "checked" : "" ?> ><?=$bank_unit["bankname"] . " " . $bank_unit["account_number"] . " " . $bank_unit["owner"]?></option>
						<?
								}
							}
						?>
						</select>
					</li>					
					<li>
						<label>입금자명</label>
						<input type="text" name="mu_name" id="mu_name" class="w240" value="">
						<input type="hidden" name="receipt_yn" value="1" />
					</li>
					<li>
						<label>현금영수증</label>
					</li>
					<li class="checks-box">
						<div class="radio"><input type="radio" id="receipt_gubun1" name="receipt_gubun" value="1" checked><label for="receipt_gubun1">소득공제용(일반개인용)</label></div>
					</li>
					<li class="checks-box">
						<div class="radio"><input type="radio" id="receipt_gubun2" name="receipt_gubun" value="2" ><label for="receipt_gubun2">지출증빙용(사업자용)</label></div>
					</li>
					<li>
						<label>번호입력</label>
						<input type="text" name="receipt_number" id="receipt_number" class="w240" value="">
						<input type="hidden" name="receipt_yn" value="1" />
					</li>
				</ul>
			</div>
		</div>
		
		<? if ( $_POLICY["must_read_yn"] == "1" ) {?>		
		<div class="form-box">
			<div class="accept-terms">		
				<h3>필독사항</h3>
				<ul>
					<li style="line-height:18px;"><?=$_POLICY["must_read"]?></li>
					<? if ( $_POLICY["must_read_agree"] == "1" ) {?>
					<li class="checks-box">
						<div class="checks">
							<input type="checkbox" id="must_read_agree" name="must_read_agree" >
							<label for="must_read_agree">필독사항내용을 모두 확인하였습니다.<span class="point">(필수)</span></label>
						</div>
					</li>
					<?}?>
				</ul>
			</div>
		</div>
		<?}?>
		<div class="form-box">
			<div class="accept-terms">
				<h3>약관동의</h3>
				<ul>
					<li class="checks-box">
						<div class="checks">
							<input type="checkbox" id="accept1" name="accept1" checked>
							<label for="accept1">만 14세미만은 구매가 불가능하며,만 14세이상이신 경우 체크해주세요<span class="point">(필수)</span></label>
						</div>
					</li>
					<li class="checks-box">
						<div class="checks">
							<input type="checkbox" id="accept2" name="accept2" checked>
							<label for="accept2">필독사항 확인 및 결제진행 전체동의<span class="point">(필수)</span></label>
						</div>
						<!--a href="#" class="more">내용보기</a-->
						<div class="guide-text">
							<h5>법적고지</h5>
							<p>신기한쇼핑은 통신판매중개시스템의 제공자로서, 통신판매의 당사자가 아니며 상품의 주문, 배송 및 환불 등과 관련한 의무와 책임은 각 판매자에게 있습니다.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>



		<div class="order-footer">
			<div class="btn-area">
				<a href="#" class="btn-point w100p" id="order_proc">결제하기</a>	
			</div>
		</div>
	</div>
</form>
</section>
<form name="frmConfirm" id="frmConfirm" action="" method="post" accept-charset="euc-kr" >
	<input type="hidden" name="ORDERNO" value="<?=$row["num"]?>">
	<input type="hidden" name="PRODUCTNAME" value="<?=$SITE_NAME__?> 주문결재"><!-- 주문결재-->
	<input type="hidden" name="PRODUCTTYPE" value="2">
	<input type="hidden" name="BILLTYPE" value="1">
	<input type="hidden" name="USERID" size="30" maxlength="30" value="<?=( $_SESSION["mstsp_id"] != "" ) ? $_SESSION["mstsp_id"] : "GUEST" ?>">
	<input type="hidden" name="PRODUCTCODE" size="10" value="<?=$goods_codes?>">
	<input type="hidden" name="RESERVEDINDEX2" size="20" value="">
	<input type="hidden" name="RESERVEDSTRING" size="100" value="">
</form>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
<!--
	<? if ( $_SERVER["REMOTE_ADDR"] == "220.230.7.218" ) {?>
	$("#d_dname").val("김신웅");
	$("#zip").val("12345");
	$("#d_addr1").val("주소주소");
	$("#d_addr2").val("상세주소");
	$("#d_phone2").val("4763");
	$("#d_phone3").val("6169");
	$("#order_name").val("김신웅");
	$("#order_phone2").val("4763");
	$("#order_phone3").val("6169");
	$("#order_email1").val("sin5842");
	$("#order_email2").val("nate.com");
	<?}?>
var order_price = <?=$tot_price?>;
var order_deli = <?=$tot_deli?>;
var order_island = 0;
var use_point = 0;
var tot_price = order_price + order_deli + order_island - use_point;
var mypoint = <?=( $_MEM_INFO["point"] != "" ) ? $_MEM_INFO["point"] : "0" ?>;
var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
$('a#order_proc').click(function () {
	order_proc();
	return false;
});

$('input[name=chk_info]').click(function () {
	if ( $('input[name=chk_info]:checked').val() == '3') {
		$('#receipt').show();
	} else {
		$('#receipt').hide();
	}
});

$(document).on( "change" , "#deli_comment" , function(){
	if ( $("#deli_comment option:selected").val() == "directly" ) {
		$("#deli_ipt").show();
	} else {
		$("#deli_ipt").hide();
	}
});

$(document).on( "click" , ".select_deli" , function(){
	var dphone2 = $(this).data("dphone2");
	var dphone3 = $(this).data("dphone3");
	$("#order_name").val( $(this).data("dname") );
	$("#order_phone1").val($(this).data("dphone1")).prop("selected", true);
	$("#order_phone2").val(dphone2);
	$("#order_phone3").val(dphone3);
	$("#order_zip").val( $(this).data("zip") );
	$("#order_addr1").val( $(this).data("addr1") );
	$("#order_addr2").val( $(this).data("addr2") );
	//$("#order_email1").val( $(this).data("email1") );
	//$("#order_email2").val( $(this).data("email2") );
	$(".delivery_info_list tr").css("background-color","");
	$(this).parent().parent().css("background-color","#ededed");

	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: './_proc_json.php',
		data: 'mode=island&zip=' + $(this).data("zip") + '&oidx=' + <?=$oidx?> ,
		success:function (data) {
			console.log(data);
			if (data.state=="1") {
				if ( data.island_price > 0 ) {
					order_island = parseInt(data.island_price);
					$('#order_island').val( order_island );
					$('#deli_island_price_li').show();
					$('#deli_island_price_txt').html('+ '+addComma(order_island));
				} else {
					order_island = 0;
					$('#order_island').val("0");
					$('#deli_island_price_li').hide();
				}						
				tot_price = order_price + order_deli + order_island - use_point;
				$("#tot_price").html(addComma(tot_price));
			}
		},
		error : function (data) {
			console.log(data);
		} 
	});
});


var element_layer = document.getElementById('sddr_layer');
$('#search_addr').on('click',  function() {
	new daum.Postcode({
		oncomplete: function(data) {
			var fullAddr = data.address; 
			var extraAddr = '';
			if(data.addressType === 'R'){
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			}
			document.getElementById('zip').value = data.zonecode; //5자리 새우편번호(zonecode) 사용 , 기존 postcode
			document.getElementById('d_addr1').value = fullAddr;
			document.getElementById('d_addr2').focus();
			element_layer.style.display = 'none';
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=island&zip=' + data.zonecode+'&oidx=' + <?=$oidx?> ,
				success:function (data) {
					console.log(data);
					if (data.state=="1") {
						if ( data.island_price > 0 ) {
							order_island = parseInt(data.island_price);
							$('#order_island').val( order_island );
							$('#deli_island_price_li').show();
							$('#deli_island_price_txt').html('+ '+addComma(order_island));
						} else {
							order_island = 0;
							$('#order_island').val("0");
							$('#deli_island_price_li').hide();
						}
						tot_price = order_price + order_deli + order_island - use_point;
						$("#tot_price").html(addComma(tot_price));
					}
				},
				error : function (data) {
						console.log(data);
				} 
			});
		},
		width : '100%',
		height : '100%'
	}).embed(element_layer);
   element_layer.style.display = 'block';

	/*initLayerPosition();*/
});

$(document).on("click", ".deli_tab ", function(){
	if ( $(this).data("tab") == "delivery-list-tab" ) {
		$("#deli_directly").val("0");
	}else {
		$("#deli_directly").val("1");
	}
});

function order_proc() {
	var frm = document.regi_form;
	var op_idx = 0;

	if ( $('#order_zip').val() == "" ) {
		$('#order_zip').val($('#zip').val());
	}
	if ( $('#order_addr1').val() == "" ) {
		$('#order_addr1').val($('#d_addr1').val());
	}
	if ( $('#order_addr2').val() == "" ) {
		$('#order_addr2').val($('#d_addr2').val());
	}
		
	if ( $('#order_zip').val() == '' || $('#order_addr1').val() == '' || $('#order_addr2').val() == '' ) {
		<? if ( $deli_yn == true ) {?>
		alert( '수령주소를 선택하시거나 \n새로운 주소를 입력하시려면 직접입력 탭에서 입력해 주시기 바랍니다.' );
		<?}else{?>
		alert( '주소를 입력하시려면 직접입력 탭에서 입력해 주시기 바랍니다.' );
		<?}?>
		return false;
	}

	if ($('#order_name').val() == '') {
		alert( '주문자명을 입력하세요.' );
		return false;
	}
	if ($('#order_phone').val() == '') {
		alert( '주문자 휴대폰 번호를 입력하세요.' );
		return false;
	}
	if ($('#order_email1').val() != '' || $('#order_email2').val() != '' ) {
		if( !regEmail.test( $('#order_email1').val()+"@"+$('#order_email2').val() ) ) {
			alert( '주문자 이메일을 정확하게 입력하세요.' );
			return false;
		}
	}
	if ( !$('input:radio[name=chk_info]').is(':checked') ) {
		alert('결재방법을 선택하세요.');
		return false;
	}
	if ($(':radio[name=chk_info]:checked').val()=='3') {
		if ($('#mu_name').val()=='') {
			alert('입금자명을 입력하세요.');
			return false;
		}
		if ( $("#receipt_number").val().length < 10 || $("#receipt_number").val().length > 15 ) {
			alert('현금영수증 발급 번호를 입력하세요.');
			return false;
		}
	}
	if (!$('#accept1').is(':checked')) {
		alert('만 14세 미만은 구매가 불가능합니다. 동의 버튼을 확인 해주세요.');
		$('#accept1').focus();
		return false;
	}
	if (!$('#accept2').is(':checked')) {
		alert('구매조건 및 결제진행에 동의 해주세요.');
		$('#accept2').focus();
		return false;
	}
	
	<? if ( $_POLICY["must_read_yn"] == "1" && $_POLICY["must_read_agree"] == "1" ) {?>
	if (!$('#amust_read_agree').is(':checked')) {
		alert('구매조건 및 결제진행에 동의 해주세요.');
		$('#must_read_agree').focus();
		return false;
	}
	<?}?>
	if ( $("#use_point").val() == "" ) {
		$("#use_point").val("0")
	}
	$('#order_island').val(order_island);
	var paygubun = $('input:radio[name=chk_info]:checked').val();
	var email = $("#order_email1").val()+"@"+$("#order_email2").val();
	var formData = $('#regi_form').serialize(); 
	$.ajax({
		type: 'POST',
		cache: false,
		dataType: 'json',
		url: './_proc_json.php',
		data:formData ,
		success:function (data) {
			if ( data.rst == "1" ) {
					$('#frmConfirm').append("<input type='hidden' name='USERNAME' value='" + $("#order_name").val() + "' />");
					$('#frmConfirm').append("<input type='hidden' name='AMOUNT' value='"+data.totprice+"' />");
				if ( paygubun == "3" ) {
					alert("주문이 완료되었습니다.");
					location.href="../order/order_ok.php?oidx=<?=$oidx?>";
					return false;
				}else if ( paygubun == "1" ) {
					$('#frmConfirm').append("<input type='hidden' name='CPID' value='<?=$CPID_CARD?>' />");
					$('#frmConfirm').append("<input type='hidden' name='RESERVEDINDEX1' value='1' />");
					$('#frmConfirm').append("<input type='hidden' name='CPQUOTA' value='0:2:3:4:5:6:7:8:9:10:11:12' style='IME-MODE:disabled' />");
					$('#frmConfirm').append("<input type='hidden' name='CARDLIST' value='CCLG:CCBC:CCKM:CCSS:CCDI:CCLO:CCHN:CCNH:CCCT:CCPH:CCCJ:CCMG:CCPO:CCSB:CCKD:CCCU:CCJB:CCKJ:CCSH:CCKA' />");
					$('#frmConfirm').append("<input type='hidden' name='HIDECARDLIST' value='' />");
					$('#frmConfirm').append("<input type='hidden' name='TAXFREECD' value='02' />");
					$('#frmConfirm').append("<input type='hidden' name='POPUPTYPE' value='' />");
					$('#frmConfirm').append("<input type='hidden' name='TELNO2' value='' />");
					$('#frmConfirm').append("<input type='hidden' name='EMAIL' size='100' maxlength='100' value='" + email + "' />");
					<?if ( $_SESSION["mstsp_isapp"] == "1" ) {?>
					$('#frmConfirm').append("<input type='hidden' name='HOMEURL' value='http://<?=$SITE_DOMAIN__?>/order/order_ok.php?oidx=<?=$oidx?>' />");
					$('#frmConfirm').append("<input type='hidden' name='CLOSEURL' value='http://<?=$SITE_DOMAIN__?>' />");
					$('#frmConfirm').append("<input type='hidden' name='FAILURL' value='http://<?=$SITE_DOMAIN__?>' />");
					$('#frmConfirm').append("<input type='hidden' name='APPURL' value='<?=$APPURL?>' />");
					<?}else { // 신용카드 K일 때 PC일 때만 사용됨 - 추후 공용으로 변경
					?>
					$('#frmConfirm').append("<input type='hidden' name='HOMEURL' value='' />");
					$('#frmConfirm').append("<input type='hidden' name='RETURNURL' value='http://<?=$SITE_DOMAIN__?>/order/order_ok.php?oidx=<?=$oidx?>' />");
					<?}?>
					pay();
				}else if ( paygubun == "2" ) {
					$('#frmConfirm').append("<input type='hidden' name='CPID' value='<?=$CPID_CARD?>' />");
					$('#frmConfirm').append("<input type='hidden' name='EMAIL' size='100' maxlength='100' value='" + email + "' />");
					$('#frmConfirm').append("<input type='hidden' name='RESERVEDINDEX1' value='2' />");
					$('#frmConfirm').append("<input type='hidden' name='CLOSEURL' value='http://<?=$SITE_DOMAIN__?>' />");
					$('#frmConfirm').append("<input type='hidden' name='FAILURL' value='http://<?=$SITE_DOMAIN__?>' />");

					$('#frmConfirm').append("<input type='hidden' name='MOBILECOMPANYLIST' value='' />");
					$('#frmConfirm').append("<input type='hidden' name='FIXTELNO' value='' />");
					<?if ( $_SESSION["mstsp_isapp"] != "1" ) {?>
					$('#frmConfirm').append("<input type='hidden' name='HOMEURL' value='' />");
					$('#frmConfirm').append("<input type='hidden' name='RETURNURL' value='http://<?=$SITE_DOMAIN__?>/order/order_ok.php?oidx=<?=$oidx?>' />");						
					<?}else {?>
					$('#frmConfirm').append("<input type='hidden' name='HOMEURL' value='http://<?=$SITE_DOMAIN__?>/order/order_ok.php?oidx=<?=$oidx?>' />");
					<?}?>
					pay();
				}else if ( paygubun == "4" ) {
				}else if ( paygubun == "5" ) {
				}else if ( paygubun == "6" ) {
					$('#frmConfirm').append("<input type='hidden' name='HOMEURL' value='http://<?=$SITE_DOMAIN__?>/order/order_ok.php?oidx=<?=$oidx?>' />");
					$('#frmConfirm').append("<input type='hidden' name='CPID' value='<?=$CPID_CARD?>' />");
					$('#frmConfirm').append("<input type='hidden' name='QUANTITY' value='1' />");
					$('#frmConfirm').append("<input type='hidden' name='CASHRECEIPTFLAG' value='Y' />");
					$('#frmConfirm').append("<input type='hidden' name='FREE_AMT' value='0' />");
					$('#frmConfirm').append("<input type='hidden' name='RESERVEDINDEX1' value='6' />");
					$('#frmConfirm').append("<input type='hidden' name='CLOSEURL' value='http://<?=$SITE_DOMAIN__?>' />");
					$('#frmConfirm').append("<input type='hidden' name='FAILURL' value='http://<?=$SITE_DOMAIN__?>' />");
					pay();
				}
			}else if ( data.rst == "2" ) {
				alert('이미 처리된 주문입니다.\n결재를 진행 하실 수 없습니다.');
				location.href='/';
			}else {
				alert("결재 신청 도중 오류가 발생하였습니다.");
				location.href='/';
			}
		},
		error : function (data) {
				console.log(data);
		} 
	});

	//$("#regi_form").submit();
	return false;
}

$('a#cal_point').click(function () {
	var f = document.regi_form; 
	<?if ($_SESSION['mstsp_id'] == "") {?>
		alert('로그인후 이용가능합니다.');
		return false;
	<?}else {?>
		if (_isNumber(f.use_point) || _isEmpty(f.use_point)) {
			alert('포인트를 숫자만 입력 하세요');
			return false;
		}
		if (mypoint < $("#use_point").val()) {
			alert('보유한 포인트가 부족합니다');
			$("#use_point").val(0);
		}else if ($("#use_point").val() > ( order_price / 2 ) ) {
			alert('포인트는 상품 금액의 50%까지 사용 가능합니다');
			$("#use_point").val(0);
		}
		use_point = parseInt($("#use_point").val());
		tot_price = order_price + order_deli + order_island - use_point;
		$("#tot_price").html(addComma(tot_price));
		return false;
	<?}?>
});

function emailsel(emailv){
	if(emailv=="d"){
		$("#order_email2").val("");
		$("#order_email2").focus();
	}else{
		$("#order_email2").val(emailv);
	}
}

function pay(){
	document.charset = "EUC-KR";
	var pf = document.frmConfirm;
	var paygubun = $('input:radio[name=chk_info]:checked').val();
	var fileName;
	if ( paygubun == "1" ) {
		<? if ( $_SESSION["mstsp_isapp"] == "1" ) {?>
		fileName = "https://ssl.daoupay.com/m/card_webview/DaouCardMng.jsp";	 //신용카드D
		//fileName = "https://ssl2.daoupay.com/m/creditCard_webview/DaouCreditCardMng.jsp"; //신용카드K
		pf.target = "_self";
		<?}else{?>
		var UserAgent = navigator.userAgent;	
		if (UserAgent.match(/iPhone|iPod|iPad|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || UserAgent.match(/LG|SAMSUNG|Samsung/) != null){
			fileName = "https://ssl.daoupay.com/m/card/DaouCardMng.jsp";	 //신용카드D
			//fileName = "https://ssl2.daoupay.com/m/creditCard/DaouCreditCardMng.jsp"; //신용카드K
			var DAOUPAY = window.open("about:blank", "DAOUPAY", "fullscreen");
		}else{
			fileName = "https://ssl.daoupay.com/card/DaouCardMng.jsp"; //신용카드D
			//fileName = "https://ssl.daoupay.com/creditCard/DaouCreditCardMng.jsp"; //신용카드K
			var DAOUPAY = window.open("", "DAOUPAY", "width=579,height=527");
		}
		DAOUPAY.focus();
		pf.target = "DAOUPAY";
		<?}?>
	} else if ( paygubun == "2" ) {
		<? if ( $_SESSION["mstsp_isapp"] == "1" ) {?>
		fileName = "https://ssl.daoupay.com/m/mobile_webview/DaouMobileMng.jsp";
		pf.target = "_self";
		//alert("CPID:"+pf.CPID.value+"\\n"+"ORDERNO:"+pf.ORDERNO.value+"\\n"+"PRODUCTTYPE:"+pf.PRODUCTTYPE.value+"\\n"+"BILLTYPE:"+pf.BILLTYPE.value+"\\n"+"AMOUNT:"+pf.AMOUNT.value+"\\n"+"PRODUCTNAME:"+pf.PRODUCTNAME.value+"\\n"+"HOMEURL:"+pf.HOMEURL.value+"\\n"+"CLOSEURL:"+pf.CLOSEURL.value+"\\n"+"FAILURL:"+pf.FAILURL.value);
		<?}else{?>
		var UserAgent = navigator.userAgent;	
		if (UserAgent.match(/iPhone|iPod|iPad|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || UserAgent.match(/LG|SAMSUNG|Samsung/) != null){
			fileName = "https://ssl.daoupay.com/m/mobile/DaouMobileMng.jsp";
		}else{
			fileName = "https://ssl.daoupay.com/2.0/mobile/DaouMobileMng.jsp";
		}
		DAOUPAY = window.open("", "DAOUPAY", "width=468,height=750");
		DAOUPAY.focus();
		pf.target = "DAOUPAY";
		<?}?>
	} else if ( paygubun == "4" ) {
	} else if ( paygubun == "5" ) {
	} else if ( paygubun == "6" ) {		
		<? if ( $_SESSION["mstsp_isapp"] == "1" ) {?>
			fileName = "https://ssl.daoupay.com/m/kakao_pay/DaouKakaoPayMng.jsp";
			pf.target = "_self";
		<?}else{?>
		var UserAgent = navigator.userAgent;	
		if (UserAgent.match(/iPhone|iPod|iPad|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || UserAgent.match(/LG|SAMSUNG|Samsung/) != null){
			fileName = "https://ssl.daoupay.com/m/kakao_pay/DaouKakaoPayMng.jsp";
		}else{
			fileName = "https://ssl.daoupay.com/kakao_pay/DaouKakaoPayMng.jsp";
		}
		DAOUPAY = window.open("about:blank", "DAOUPAY", "width=468,height=538");
		pf.target = "DAOUPAY";
		<?}?>
	}
	//alert(fileName +".."+ pf.AMOUNT.value);
	pf.action = fileName;
	pf.submit();
	return false;
}
//-->

</script>
<?include ("../include/footer.php");?>
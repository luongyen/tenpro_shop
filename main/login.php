<?
include ("../include/header.php");
mysql_query("insert into cron_log set msg='" . json_encode($_GET)."' ");
?>
<!-- HEAD -->
<header class="sub-header">
	<h1 class="sub-title">로그인/회원가입</h1>
	<div class="back-btn"><a href="./"><span class="hide">뒤로</span></a></div>
</header>

<!-- BODY -->
<section class="login-area">
	<h2 class="hide">로그인버튼</h2>
	<div class="logo"></div>
	<div class="btn-area">
		<button type="button" class="kakao-login ">카카오계정 로그인</button>
		<button type="button" class="naver-login " id="naverIdLogin_loginButton">네이버 로그인</button>
	</div>
</section>

<div class="lazy-group lazy-policy1" style="display: none;">
	<div class="lazy-header">
		이용약관
	</div>
	<div class="lazy-content alarm">
		<div class="inner-offset" style="">
			<div class="inner-text" style="max-height:300px;overflow-y:auto;"><?=$_POLICY["yak"]?></div>
			<div class="inner-fn"><a class="alarm" onclick="close_policy('1');return false;" href="#">닫기</a></div>
		</div>
	</div>
	<div class="lazy-close">
		<button><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
	</div>
</div>	

<div class="lazy-group lazy-policy2" style="display: none;">
	<div class="lazy-header">
		개인정보취급방침
	</div>
	<div class="lazy-content alarm">
		<div class="inner-offset">
			<div class="inner-text" style="max-height:300px;overflow-y:auto;"><?=$_POLICY["personal_data"]?></div>
			<div class="inner-fn">
				<a class="alarm" onclick="close_policy('2');return false;" href="#">닫기</a>
			</div>
		</div>
	</div>
	<div class="lazy-close">
		<button><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
	</div>
</div>

<div class="btn-agree" style="left: 0px; top: 470px; width: 100%; text-align: center; display: block; position: absolute;">
	<a href="#" onclick="javascript:pop_policy('1');return false;">이용약관</a>
	<a href="#" onclick="javascript:pop_policy('2');return false;" style="margin-left:20px;">개인정보보호정책</a>
</div>

<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="/assets/js/naverLogin.js"></script>
<script type="text/javascript">
<!--
var isApp = false;
//var loginSuccUrl = "<?=urlencode("http://" . $SITE_DOMAIN__ . "/index_app.php?referrer=succ")?>";
//var loginFailUrl ="<?=urlencode("http://" . $SITE_DOMAIN__ . "/main")?>";
var loginSuccUrl = "<?="http://" . $SITE_DOMAIN__ . "/index_app.php"?>";
var loginFailUrl ="<?="http://" . $SITE_DOMAIN__ . "/main"?>";
var link_url = "https://play.google.com/store/apps/details?id=com.10proshop";
try{
	isApp = window.Android.isApp();
	if ( isApp ) {
		var isLogin = window.Android.isLogin();
		var uuid = window.Android.getUUID();
		if ( isLogin == "none" ) {
			$(document).on("click" , ".kakao-login" , function(){
				window.Android.LoginKakao( loginSuccUrl , loginFailUrl );
				
			});
			$(document).on("click" , ".naver-login" , function(){
				window.Android.LoginNaver( loginSuccUrl , loginFailUrl );
			});
		}else if ( isLogin == "kakao" && isLogin == "naver" ) {
			//로그인정보 다시받아오기
			location.href='/index_app.php'
		}
	}
} catch(e){
	Kakao.init('<?=$KAKAO_CLIENT_ID?>');	
	var naverLogin = new naver.LoginWithNaverId(
		{
			clientId: "9PTpAxDpTlCrWQ6j_8W5",
			callbackUrl: "http://" + window.location.hostname+"/koauth/naver.php",
			isPopup: false,
			callbackHandle: true
			//http://10proshop.com/a.php#access_token=AAAAOce9_IetsHc3SYLA4B66xgvNwy55Q0rzeBOPbyY5eM06Yqk4gwMd2FprlMbYe0kvFvb5-MXQoxP-0wYAqz_9C_4&state=845c003c-030a-4fc8-83ed-e8c08e297530&token_type=bearer&expires_in=3600
		}
	);
	/* (4) 네아로 로그인 정보를 초기화하기 위하여 init을 호출 */
	naverLogin.init();
	$(document).on( "click" , ".kakao-login" , function(){		
			Kakao.Auth.login({
				success: function(authObj) {
					//console.log(authObj.access_token);
					var token = authObj.access_token;
					Kakao.API.request({
						url: '/v2/user/me',
						success: function(res) {
							//세션저장
							var params = {
								mode:'isNoneApp', 
								gubun:'kakao', 
								nickname:res.properties.nickname, 
								email:res.kakao_account.email, 
								profile:res.properties.profile_image, 
								thum:res.properties.thumnail_image, 
								id:res.id,
								token:token
							}
							$.ajax({
								url:"../proc/_setting.php",
								data:params,
								type:"POST",
								dataType:"json",
								success:function(data){
									console.log(data);
									if ( data.content == "1" ) {
										location.replace('http://<?=$SITE_DOMAIN__?>/main/?reurl=<?=urlencode($reurl)?>');
									}else if ( data.content == "3" ) {
										alert("탈퇴한 회원정보입니다.");
										location.href="";
									}else {
										alert("로그인 정보를 저장하지 못했습니다.");
									}
								},
								error:function(jqXHR,textStatus,errorThrown){	
								}
							});
						},
						fail: function(error) {
							alert("카카오 사용자 정보를 불러올 수 없습니다.");
							location.reload();
						}
					});
					
			
				},
				fail: function(err) {
					alert(JSON.stringify(err));
					alert("카카오로그인에 실패하였습니다.");
					//location.reload();
				}
			});
		});

	$(document).on("click" , ".naver-login" , function(){
		
		
		/* (4-1) 임의의 링크를 설정해줄 필요가 있는 경우 */
		//$("#gnbLogin").attr("href", naverLogin.generateAuthorizeUrl());

		/* (5) 현재 로그인 상태를 확인 */
		window.addEventListener('load', function () {
			naverLogin.getLoginStatus(function (status) {
				if (status) {
					/* (6) 로그인 상태가 "true" 인 경우 로그인 버튼을 없애고 사용자 정보를 출력합니다. */
					setLoginStatus();
				}
			});
		});

		/* (6) 로그인 상태가 "true" 인 경우 로그인 버튼을 없애고 사용자 정보를 출력합니다. */
		function setLoginStatus() {
			var profileImage = naverLogin.user.getProfileImage();
			var nickName = naverLogin.user.getNickName();
			$("#naverIdLogin_loginButton").html('<br><br><img src="' + profileImage + '" height=50 /> <p>' + nickName + '님 반갑습니다.</p>');
			$("#gnbLogin").html("Logout");
			$("#gnbLogin").attr("href", "#");
		}
	});
}

function pop_policy(val){
	$('.lazy-policy'+val).show();
	$('body').addClass('lazy-openner');

	$('.lazy-policy'+val).on('click',' .lazy-close>button',function(){
		$(this).parents('.lazy-policy'+val).fadeOut(function(){
			$('body').removeClass('lazy-openner');
		});
	});
}

function close_policy(val){
	var hide_yn = true;
	$('.lazy-policy'+val).fadeOut(function(){
		$(".lazy-group-popup").each(function(){
			if ($(this).css("display") == "block" ) {
				hide_yn = false;
			}
		});
		if ( hide_yn == true ) {
			$('body').removeClass('lazy-openner');
		}
	});
}

//-->
</script>
<?
	include ("../include/footer.php");
	urlencode
?>
<?
$PAGE_TITLE = "탈퇴하기";
include ("../include/header.php");
?>
<!-- BODY -->
<section class="sub-body">
	<div class="inquire-area">
		<p class="guide-text">10proshop.com 회원탈퇴시 탈퇴 안내 사항들을 반드시 확인해 주십시오.<br><br>
		<span> - 회원정보는 관련법령에 따라 보관 의무가 있는 경우를 제외하고는 탈퇴시 즉시 삭제됩니다.</span><br><span> - 전자상거래 이용 후 탈퇴시 주문정보는 교환/반품/환불 및 사후처리(A/S)등을 위하여 전자상거래 등에서의 소비자 보호에 관한 법률에 의거, 일반정보 보호정책에 따라 관리됩니다.</span><br><span> - 탈퇴 이후에는 어떠한 방법으로도 삭제된 회원정보를 복원할 수 없습니다.</span><br><span> - 완료되지 않은 주문/반품/교환 및 상담이력이 있는경우 탈퇴가 불가합니다.</span></p>
		<div class="form-box">
			<h3 class="hide">탈퇴사유</h3>
			<ul>
				<li>
					<label>탈퇴사유<span class="point">*</span></label>
					<input type="text" name="outreason" id="outreason">
				</li>
			</ul>
			<button type="button" class="btn btn-point" id="btn_req">탈퇴하기</button>
		</div>
	</div>
</section>
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<script type="text/javascript">
<!--

	var isApp = false;
	try{ 
		isApp = window.Android.isApp(); 
	}catch(e){
	}
	if ( isApp ) {
		$("#btn_req").on("click" , function(){
			if ( $("#outreason").val() != "" ) {
				<?if ( $_SESSION["mstsp_type"] == "naver" ) {?>
					window.Android.kickOutNaver( "http://<?=$SITE_DOMAIN__?>/logout.php" , "http://<?=$SITE_DOMAIN__?>/index_app.php" );
				<?}elseif ( $_SESSION["mstsp_type"] == "kakao" ) {?>
					window.Android.kickOutKakao( "http://<?=$SITE_DOMAIN__?>/logout.php" , "http://<?=$SITE_DOMAIN__?>/index_app.php" );
				<?}?>
			}else {
				alert("탈퇴사유를 입력해주시면 더 좋은 앱을 만드는데 소중히 이용하겠습니다.");
			}
			return false;
		});

	}else {
		Kakao.init('<?=$KAKAO_CLIENT_ID?>');
		$("#btn_req").on("click" , function(){
			if ( $("#outreason").val() != "" ) {
				<?if ( $_SESSION["mstsp_type"] == "naver" ) {?>
					//var rst = window.Android.kickOutNaver(); 앱개발 무시
					var params = {
						mode:'kickOutNaver',
						del_reason:$("#outreason").val()
					}
					$.ajax({
						url:"../proc/_setting.php",
						data:params,
						type:"POST",
						dataType:"json",
						success:function(data){
							console.log(data);
							if ( data.content == "1" ) {
								location.href = "../main/logout.php";
							}else {
										alert(data.rst);
								alert("탈퇴작업도중 오류가 발생하였습니다. [code:000"+data.content+"]고객센터로 문의 해 주시기 바랍니다.");
							}
						},
						error:function(jqXHR,textStatus,errorThrown){	
						}
					});
				<?}elseif ( $_SESSION["mstsp_type"] == "kakao" ) {?>
					
					Kakao.Auth.login({
						success: function(authObj) {
							var params = {
								mode:'kickOutKakao',
								del_reason:$("#outreason").val()
							}
							$.ajax({
								url:"../proc/_setting.php",
								data:params,
								type:"POST",
								dataType:"json",
								success:function(data){
									console.log(data);
									if ( data.content == "1" ) {
										location.href = "../main/logout.php";
									}else {
										alert(data.rst);
										alert("탈퇴작업도중 오류가 발생하였습니다. [code:000"+data.content+"]고객센터로 문의 해 주시기 바랍니다.");
									}
								},
								error:function(jqXHR,textStatus,errorThrown){	
								}
							});
						},fail: function(err) {
							//alert(JSON.stringify(err));
						}
					});
				<?}else{?>
					alert("로그인 한 상태에서 이용 가능합니다.");
				<?}?>
			}else {
				alert("탈퇴사유를 입력해주시면 더 좋은 앱을 만드는데 소중히 이용하겠습니다.");
			}
			return false;
		});
	}
//-->
</script>
<?
include ("../include/footer.php");
?>
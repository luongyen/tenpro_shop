<?
include ("../config.php");
$referrer = $_GET["referrer"];

$page = mysql_real_escape_string( $_GET["page"] );
if ( $page == "" ) {
	$page = "2";
}

$offset = ( $page - 1 ) * 20;

$paging = true;
$tot = getdata("SELECT count(no) as cnt FROM `goods` where status='판매중' and preSales = '1' and dateStart<='" . date("Y-m-d H:i:s") . "' and datePreEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' and adminAgree = '1' " . $sql_append . "");
if ( $tot["cnt"] <= 20 ) {
	$paging= false;
}
$rst = mysql_query("SELECT * FROM `goods` where status = '판매중' and descLicenseUsable = '1' and adminAgree = '1' and preSales='1' and dateStart<='" . date("Y-m-d H:i:s") . "' and datePreEnd>='" . date("Y-m-d H:i:s") . "' order by datePreEnd desc limit 0, " . $offset );

include ("../include/main_top.php");
?>
<style type="text/css" title="">
.img img { height:100% !important; }
</style>
<!-- BODY -->
<section>
	<div class="main-bnr"><img src="../assets/images/sub/top_bnr01.gif"></div>
	<h2 class="sub-title2">딱!15일간만!!</h2>
	<div class="product-list2" style="padding:10px 5px;">
		<ul id="listOfBoard">
		<?
		$script_content = "";
		while ( $row = mysql_fetch_array( $rst ) ) {
			$orders = getdata("select count(*) as cnt from orders where orderItemInfo='" . $row["no"] . "' ");
			$dateStart = str_replace( "-" , "." , substr( $row["dateStart"] , 0 , 10 ) );
			$mk_s_1 = (int)( substr( $row["dateStart"] , 0 , 4 ) ); //y
			$mk_s_2 = (int)( substr( $row["dateStart"] , 5 , 2 ) ); //m
			$mk_s_3 = (int)( substr( $row["dateStart"] , 8 , 2 ) ); //d
			$mk_s_4 = (int)( substr( $row["dateStart"] , 11 , 2 ) ); //h
			$mk_s_5 = (int)( substr( $row["dateStart"] , 14 , 2 ) ); //i
			$mk_s_6 = (int)( substr( $row["dateStart"] , 17 , 2 ) ); //s
			$start_week = date( "D" , mktime( $mk_s_4 , $mk_s_5 , $mk_s_6 , $mk_s_2 , $mk_s_3 , $mk_s_1 ) );
			$mk_e_1 = (int)( substr( $row["datePreEnd"] , 0 , 4 ) ); //y
			$mk_e_2 = (int)( substr( $row["datePreEnd"] , 5 , 2 ) ); //m
			$mk_e_3 = (int)( substr( $row["datePreEnd"] , 8 , 2 ) ); //d
			$mk_e_4 = (int)( substr( $row["datePreEnd"] , 11 , 2 ) ); //h
			$mk_e_5 = (int)( substr( $row["datePreEnd"] , 14 , 2 ) ); //i
			$mk_e_6 = (int)( substr( $row["datePreEnd"] , 17 , 2 ) ); //s
			$end_count = mktime( $mk_e_4 , $mk_e_5 , $mk_e_6 , $mk_e_2 , $mk_e_3 , $mk_e_1 ) - strtotime("now") ;
			$script_content .= "countdown('countdown_" . $row["no"] . "', " . $end_count . ");";
			if ( $end_count <= 0 || $row["status"] != "판매중" ) {
				$none_sales = true;
			}
		?>
			<li class="today-time">
				<a href="../product/product_view.php?no=<?=$row["no"]?>" <?= ( $none_sales == true ) ? "onclick=\"return false;\"" : ""?>>
				<h3 class="clear ellipsis1"><?=$row["title"]?></h3>
				<div class="img"><div class="soldout" id="soldout_<?=$row["no"]?>" style="display:<?= ( $none_sales == true ) ? "" : "none"?>;"><img src="../assets/images/sub/soldout.png"></div><img src="<?=$row["thumb"]?>"></div>
				<div class="text">
					<span>15일 간만 할인 가격</span>
					<span class="bold"><?=number_format( $row["priceWe"] )?>원</span>
					<div class="time-area">
						<div class="today"><?=$dateStart?> <?=$start_week?></div>
						<div class="time" style="margin-top:5px;color:#000"><span class="time" style="display:inline-block;font-size:20px;" id="countdown_<?=$row["no"]?>" data-id="<?=$row["no"]?>" data-countdown="<?=$mk_e_2 . "/" . $mk_e_3 . "/" . $mk_e_1 . " " . $mk_e_4 . ":" . $mk_e_5 . ":" . $mk_e_6?>"></span>
						</div>
					</div>
				</div>
				</a>
			</li>
		<?}?>
		</ul>
	</div>
	<? include ("../include/quick_btn.php"); ?>
</section>
<script type="text/javascript" src="../assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript">
<!--
var page = <?=$page?>;
var page_ex = true;

$(document).ready(function(){
	show_loading();
});

$(window).load(function(){
	<? if ( $paging != false ) {?>
	$(window).scroll(function(){
		if($(window).scrollTop() >= $(document).height() - $(window).height() -200 ){
			if ( paging_ing == false ) {
				paging();
				paging_ing = true;
			}
			return false;
		}
	});
	<?}?>

	$('#div_ajax_load_image').hide();
	function paging(){
		if ( page_ex == true ) {
			var params = { mode:'paging7' , page:page }
			$.ajax({
				url:"../product/_proc_json.php",
				data:params,
				type:"GET",
				dataType:"json",
				success:function(data){
					console.log(data);
					if ( data.content != "" ) {
						$("#listOfBoard").append( data.content);
						paging_ing = false;
						page++;
					}else {
						page_ex = false;
					}
				},
				error:function(jqXHR,textStatus,errorThrown){
					console.log(errorThrown);
					alert("페이지 로딩 도중 에러가 발생하였습니다.");
				},
				beforeSend: function () {
					show_loading();
				}
				,complete: function () {
					$("#div_ajax_load_image").hide();
				}
			});
		}
	}

});
/*
function countdown( elementId, seconds ){
  var element, endTime, hours, mins, msLeft, time;

  function updateTimer(){
    msLeft = endTime - (+new Date);
    if ( msLeft < 0 ) {
      console.log('done');
    } else {
      time = new Date( msLeft );
      hours = time.getUTCHours();
      mins = time.getUTCMinutes();
      element.innerHTML = (hours ? hours + ':' + ('0' + mins).slice(-2) : mins) + ':' + ('0' + time.getUTCSeconds()).slice(-2);
      setTimeout( updateTimer, time.getUTCMilliseconds());
      // if you want this to work when you unfocus the tab and refocus or after you sleep your computer and come back, you need to bind updateTimer to a $(window).focus event^^
    }
  }

  element = document.getElementById( elementId );
  endTime = (+new Date) + 1000 * seconds;
  updateTimer();
}

$(document).ready(function(){
	<?=$script_content?>
});
*/
$(document).ready(function(){
	$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			$this.html(event.strftime('%D일 %H:%M:%S'))}
		).on('finish.countdown', function() {
			var no = $this.data("id");
			$("#soldout_"+no).show();
		});
	});
});
//-->
</script>
<? include ("../include/main_bottom.php"); ?>
<? include ("../include/footer.php"); ?>

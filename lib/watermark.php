<?php
$HOME_PATH__ = "/home/10proshop/public_html";						//계정 홈루트 경로

function warterMark( $src_image , $new_image , $mode ) {
	global $HOME_PATH__;

	//$src_image = "image/Jellyfish.jpg";   //원본 이미지 경로
	//$duple_image = "image/ttk_img_01.png";
	$opacity = 60;    // 테스트 투과률 설정
	$font = $HOME_PATH__."/assets/NanumGothicExtraBold.ttf";   //폰트 경로
	$mark_string = "금강  공인중개사\n010-3749-8472";   //각인 문구
	 //$new_image = "image/Jellyfish2.jpg"; 각인하고 저장될 이미지 경로
	$image_org = $src_image;                //원본이미지를 다른 변수로 저장
	$read_jpg_src_image = imagecreatefromjpeg($src_image);   //원본이미지를 jpg로 읽음
	$width = imagesx($read_jpg_src_image);              //원본이미지 Width값
	$height = imagesy($read_jpg_src_image);     //원본이미지 Height 값

	$font_size = $width/8;

	if(is_file($src_image)){        //원본 이미지 파일 잇는지 확인
		if ( $mode == "1" ) { //가로 어두움
			$font_size =90;
			$text_pos_x = 80;       //폰트 위치x좌표
			$text_pos_y = $font_size+243;       //폰트 위치y좌표
			$text_color = imagecolorallocate($read_jpg_src_image,255,255,255);    //폰트 컬러
			imagettftext($read_jpg_src_image, $font_size, 0, $text_pos_x, $text_pos_y, $text_color, $font, $mark_string); // 원본이미지에 문구 각인		
		}elseif ( $mode == "2" ) {//가로 밝음
			$font_size = 90;
			$text_pos_x = 80;       //폰트 위치x좌표
			$text_pos_y = $font_size+243;       //폰트 위치y좌표
			$text_color = imagecolorallocate($read_jpg_src_image,170,170,170);    //폰트 컬러
			imagettftext($read_jpg_src_image, $font_size, 0, $text_pos_x, $text_pos_y, $text_color, $font, $mark_string); // 원본이미지에 문구 각인		
		}elseif ( $mode == "3" ) {//세로어두움
			$font_size = 60;
			$text_pos_x = $font_size+30;       //폰트 위치x좌표
			$text_pos_y = $font_size+435;       //폰트 위치y좌표
			$text_color = imagecolorallocate($read_jpg_src_image,255,255,255);    //폰트 컬러
			imagettftext($read_jpg_src_image, $font_size, 0, $text_pos_x, $text_pos_y, $text_color, $font, $mark_string); // 원본이미지에 문구 각인		
		}elseif ( $mode == "5" ) {//세로 밝음
			$font_size = 60;
			$text_pos_x = $font_size+30;       //폰트 위치x좌표
			$text_pos_y = $font_size+435;       //폰트 위치y좌표
			$text_color = imagecolorallocate($read_jpg_src_image,170,170,170);    //폰트 컬러
			imagettftext($read_jpg_src_image, $font_size, 0, $text_pos_x, $text_pos_y, $text_color, $font, $mark_string); // 원본이미지에 문구 각인		
		}elseif ( $mode == "4" ) { //세로 중상단 
			$font_size = 60;
			$text_pos_x = $font_size+30;       //폰트 위치x좌표
			$text_pos_y = $font_size+270;       //폰트 위치y좌표
			$text_color = imagecolorallocate($read_jpg_src_image,170,170,170);    //폰트 컬러
			imagettftext($read_jpg_src_image, $font_size, 0, $text_pos_x, $text_pos_y, $text_color, $font, $mark_string); // 원본이미지에 문구 각인		
		}
	}
	$image_org = imagecreatefromjpeg($image_org);    // 다른변수로 저장한 원본이미지를 jpg로 읽음
	imagecopymerge($read_jpg_src_image,$image_org,0,0,0,0,$width,$height,$opacity); 
	// 다른변수로 저장한 원본이미지와 워터마크를 찍은 이미지를 적당한 투명도로 겹치기

	imagejpeg($read_jpg_src_image, $new_image, 100); // 이미지 저장. 해상도는 90 정도

	imagedestroy($read_jpg_src_image); //사용한 이미지 변수 초기화
	imagedestroy($image_org);           //사용한 이미지 변수 초기화
}
//echo "<img src=$new_image>"; // 워터마크가 찍혀 저장된 이미지 출력

for ( $j = 5 ; $j <= 5 ; $j++ ) {
	$dir = $HOME_PATH__ . "/data/gg_new/" . $j;
	 
	// 핸들 획득
	$handle  = opendir($dir);
	 
	$files = array();
	 
	// 디렉터리에 포함된 파일을 저장한다.
	while (false !== ($filename = readdir($handle))) {
		if($filename == "." || $filename == ".."){
			continue;
		}
	 
		// 파일인 경우만 목록에 추가한다.
		if(is_file($dir . "/" . $filename)){
			$files[] = $filename;
		}
	}
	// 핸들 해제 
	closedir($handle);
	 
	// 정렬, 역순으로 정렬하려면 rsort 사용
	sort($files);
	 
	// 파일명을 출력한다.
	foreach ( $files as $f ) {
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "2");		
			/*
		if ( $f == "0003.jpg" || $f == "0011.jpg" || $f == "0008.jpg" ) { //가로어두움
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "1" );		
		}elseif ( $f == "0006.jpg" || $f == "0007.jpg" ) {//가로 밝음
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "5");
		}else { //가로 밝음
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "2");		
		}
		elseif ($f == "") { //세로어두움
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "3");		
		}
		elseif ($f == "") { //세로 밝음
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "5");		
		}
		elseif ($f == "0001.jpg") { //세로 중단
			warterMark($HOME_PATH__."/data/gg_new/" . $j . "/" . $f , $HOME_PATH__."/data/gg_new/" . $j . "/new_" . $f , "4");		
		}*/
		echo $dir . "/" . $f . "<br />";
		echo "<img src='/data/gg_new/" . $j . "/new_" . $f . "'><br />"; // 워터마크가 찍혀 저장된 이미지 출력
	}
}
?>
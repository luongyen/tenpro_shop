var GLOBAL_CONSOLE = true;
var GLOBAL_SCROLL_NO = 0;
var GLOBAL_SCROLL_ON = false;
var GLOBAL_IS_MOBILE = false;
var GLOBAL_MOBILE_SIZE = 640
$(function(){
	$( document ).on( "click", "nav.menu-mobile", function( e ) {
		set_menu_toggle();
		return false;
	});
	$( document ).on( "click", ".layer-background, button.search-close, .layer-close", function( e ) {
		set_menu_toggle('false');
		return false;
	});
	$( document ).on( "click", ".menu-close, .pop-close, .user_slide_close", function( e ) {
		set_menu_toggle('false');
	});
	$( document ).on( "click", ".menu-user", function( e ) {
		set_menu_toggle('user');
		return false;
	});

	$( document ).on( "click", ".pop-open", function( e ) {
		var title = $(this).attr("title");
		var msg = $(this).attr("msg");
		$("#global-popup").find("header > span.title").html(title);
		$("#global-popup").find("section").html(msg);
		$("#global-popup").removeClass("hide");
		$("#background-popup").removeClass("hide");
		$('body').addClass("fixed");
	});


	$( document ).on( "click", "a.layer-open, button.layer-open", function( e ) {
		set_menu_toggle('false');
		$('body').addClass("fixed");
		$("#background-popup").removeClass("hide");
		$(".layer-box").addClass("open");
		$(".layer-box > #" + $(this).attr("id")).removeClass("hide");
	});

	$( document ).on( "change", "label.agree_checkbox > input[type=checkbox]", function( e ) {
		if($(this).prop("checked")) { 
			$(this).parent("label").addClass("active").find("i").removeClass("fa-circle").addClass("fa-check-circle");
		} else {
			$(this).parent("label").removeClass("active").find("i").addClass("fa-circle").removeClass("fa-check-circle");
		}
	});
	$( document ).on( "change", "label.agree_checkbox > input[type=radio]", function( e ) {
		//if($(this).prop("checked")) { 
			$(this).parents().find("label").removeClass("active").find("i").addClass("fa-circle").removeClass("fa-check-circle");
			$(this).parent().parents().find(".checkbox > label").removeClass("active").find("i").addClass("fa-circle").removeClass("fa-check-circle");
			$(this).parent("label").addClass("active").find("i").removeClass("fa-circle").addClass("fa-check-circle");
		//} 
	});
	$( document ).on( "click", "section.box_sub > .box_center > .box_menu", function( e ) {
			$(this).find(".menu").toggleClass("open");
	});

	$( document ).on("click", "[data-hide]", function(){
		var data_hide = $(this).data("hide");
		$(data_hide).addClass("hide");
	});
});

function set_menu_toggle(chk){
	var chk = chk ? chk : 'toggle';
	if( chk == 'toggle' ) {
		if ( !$('#cate_list').hasClass("open") && $('body').hasClass("fixed") ) {
			$('body').removeClass("fixed");
		}
		$('body').toggleClass("fixed");
		$('#cate_list').toggleClass("open");
		$('#user_info').removeClass("open");
		$('.layer-background').removeClass("open");
		/*$('.layer-background').toggleClass("open");*/
		$('nav.menu-mobile').toggleClass("active");
		if ( $('nav.menu-mobile').hasClass("active") ) {
			$(".swiper-area").css("z-index","0");
		}else {
			$(".swiper-area").css("z-index","999999");
		}
	} else if( chk == 'true' || chk == 'open' ) {
		$('body').addClass("fixed");
		$('header.menu nav').addClass("open");
		$('.layer-background').addClass("open");
		$('nav.menu-mobile').addClass("active");
	} else if( chk == 'search' ) {
		$('body').addClass("fixed");
		$('header.menu nav').removeClass("open");
		$('.search-box').addClass("open");
		$('.layer-background').addClass("open");
		$('.search-box').find('input[type=text]').val("").focus();
	}  else if( chk == 'user' ) {
		$('nav.menu-mobile').removeClass("active");
		$('header.menu nav').removeClass("open");
		$('body').addClass("fixed");
		$('#user_info').toggleClass("open");
		$('nav.menu-user').toggleClass("active");
		$('.layer-background').addClass("open");
		if ( $('#user_info').hasClass("open") ) {
			$(".swiper-area").css("z-index","0");
		}else {
			$(".swiper-area").css("z-index","999999");
		}
	} else {
		$('body').removeClass("fixed");
		$('nav.menu-mobile').removeClass("active");
		$('header.menu nav').removeClass("open");
		$('.search-box').removeClass("open");
		$('.layer-background').removeClass("open");
		$(".layer-box").removeClass("open");
		$(".layer-popup").addClass("hide");
		$("#background-popup").addClass("hide");
		$(".swiper-area").css("z-index","999999");
	}
}
	// TAB
	$(function() {
		$('.tab a').click(function() {
			var activeTab = $(this).attr('data-tab');
			$('.tab a').removeClass('current');
			$('.tabcontent').removeClass('current');
			$(this).addClass('current');
			$('#' + activeTab).addClass('current');
		})
	});

	//파일첨부 
	$(function() {

		var fileTarget = $('.filebox .upload-hidden'); 
		
		fileTarget.on('change', function(){ // 값이 변경되면 

		if(window.FileReader){ // modern browser 
		var filename = $(this)[0].files[0].name; 
		} else { // old IE 

		var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출 
		} 

		// 추출한 파일명 삽입 
		$(this).siblings('.upload-name').val(filename); 
	
		}); 

	})

function set_console(msg){
	if( GLOBAL_CONSOLE ) console.log(msg);
}

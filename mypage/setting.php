<?
$PAGE_TITLE = "설정";
include ("../include/header.php");
if ( $_SESSION["mstsp_isapp"] != "1" ) {
	BACK__("앱에서만 사용이 가능합니다.");
}
?>
<!-- BODY -->
<section class="sub-body">
	<div class="setting-area">
		<h2 class="hide">설정리스트</h2>
		<ul class="list">
			<li>
				<div class="text">
					<h3>공지사항 푸쉬알림</h3>
					<p>개인정보보호 정책 등 사이트 운영에 관한 주요 정보가 있을 시 정보를 알려줍니다.</p>
				</div>
				<div class="checkbox">
					<div class="checks"><input type="checkbox" id="setting-ck" name="setting" /><label for="setting-ck"></label></div>
				</div>
			</li>
			<li>
				<div class="text">
					<h3>상품,이벤트 푸쉬알림</h3>
					<p>새로운상품,BEST상품 관련 정보를 알려줍니다.다양한 이벤트,혜택,정보를 알려줍니다.</p>
				</div>
				<div class="checkbox">
					<div class="checks"><input type="checkbox" id="setting-ck_event" name="setting_event" /><label for="setting-ck_event"></label></div>
				</div>
			</li>
		</ul>
	</div>
</section>
<script type="text/javascript">
<!--
var push_tf = window.Android.isPushUse();
var push_event_tf = window.Android.isEventPushUse();
$("#setting-ck").prop('checked',push_tf);
$("#setting-ck_event").prop('checked',push_event_tf);
$(document).on("click","#setting-ck", function(){ window.Android.setPushUse( $("#setting-ck").prop('checked') ); });
$(document).on("click","#setting-ck_event", function(){ window.Android.setPushUse( $("#setting-ck_event").prop('checked') ); });
//-->
</script>
<?include ("../include/footer.php");?>
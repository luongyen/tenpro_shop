<?
$PAGE_TITLE = "마이페이지";
include ("../include/header.php");
//$rst = mysql_query("select * from goods_top100 where sdate = '" . date("Ymd") . "' and cate_code='' and gubun = '' order by rank asc limit 0, 100 ");
$page_name = "cart";
?>
<!-- BODY -->
<section class="sub-body">
	<?include ("../include/mypage_menu.php");?>
	
	<div class="order-area cart">
		<h2 class="hide">장바구니리스트</h2>
		<div class="order-title">
			<div class="checkbox">
				<div class="checks"><input type="checkbox" id="order-all" name="order-list" checked><label for="order-all">전체선택/해제</label></div>
			</div>
			<div class="btn-area"><a href="#" class="btn">선택삭제</a></div>
		</div>
		
		<ul class="order-list">
			<li>
				<div class="checks">
					<input type="checkbox" id="order1" name="order_list">
					<label for="order1"></label>
				</div>
				<div class="product">
					<div class="img">
						<a href="product_view.html" class="product-view"><img src="assets/images/test_img/list_img01.png"></a>
					</div>
					<div class="text">
						<a href="product_view.html" class="product-view">
							<h3>나이키 언더커버 X 데이브레이크 BV4594-400</h3>
							<p class="pay"></p>
							<p class="gray">배송비 2500원</p>
						</a>
					</div>
					<button class="btn-delete"><span class="hide">삭제</span></button>
					<div class="option-area">
						<div class="option">블랙사이즈 250</div>
						<div class="option-info">
							<div class="plusminus_wrap">
					            <button type="button" class="numbtn_minus" value="-"><span class="hide">수량감소</span></button>
					            <input type="number" name="prdcAmount" class="text" title="수량설정" value="1">
					            <button type="button" class="numbtn_plus" value="+"><span class="hide">수량증가</span></button>
				       	 	</div>
				        	<div class="pay"><span class="point">28,900</span>원</div>
						</div>
						<div class="pay-delivery">배송비 <div class="point right"><span class="bold"> 2,500</span>원</div></div>
					</div>
				</div>
			</li>
			<li>
				<div class="checks">
					<input type="checkbox" id="order2" name="order_list">
					<label for="order2"></label>
				</div>
				<div class="product">
					<div class="img">
						<a href="product_view.html" class="product-view"><img src="assets/images/test_img/list_img04.png"></a>
					</div>
					<div class="text">
						<a href="product_view.html" class="product-view">
							<h3>나이키 언더커버 X 데이브레이크 BV4594-400</h3>
							<p class="pay"></p>
							<p class="gray">배송비 무료</p>
						</a>
					</div>
					<button class="btn-delete"><span class="hide">삭제</span></button>

					<div class="option-area">
						<div class="option">블랙사이즈 250</div>
						<div class="option-info">
							<div class="plusminus_wrap">
					            <button type="button" class="numbtn_minus" value="-"><span class="hide">수량감소</span></button>
					            <input type="number" name="prdcAmount" class="text" title="수량설정" value="1">
					            <button type="button" class="numbtn_plus" value="+"><span class="hide">수량증가</span></button>
				       	 	</div>
				        	<div class="pay"><span class="point">28,900</span>원</div>
						</div>
						
					</div>
				</div>
			</li>
		</ul>
		
		<div class="price">
			<ul>
				<li>총 상품금액 <div class="point right"><span class="bold">20,000</span>원</div></li>
				<li>배송비 <div class="point right"><span class="bold">+ 2,500</span>원</div></li>
				<li class="price_all">총결제금액 <div class="point right"><span class="bold">22,500</span>원</div></li>
			</ul>
		</div>

		<div class="order-footer">
			<div class="pay">총 결제액 <span class="point">22,500</span>원</div>
			<div class="btn-area">
				<a href="order_payment.html" class="btn-point w100p">결제하기</a>	
			</div>
		</div>
	</div>
</section>



<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
<script src="http://idangero.us/swiper/dist/js/swiper.min.js"></script>
<script src="assets/js/common.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>

</body>
</html>


<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
<script src="http://idangero.us/swiper/dist/js/swiper.min.js"></script>
<script src="assets/js/common.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>

</body>
</html>
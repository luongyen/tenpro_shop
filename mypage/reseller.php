<?
$PAGE_TITLE = "15%적립받기";
include ("../include/header.php");
?>
<!-- BODY -->
<section class="sub-body">
	<h2 class="hide">15% 적립</h2>
	<div class="earn-area">
		<div class="earn-box">
			<div class="earn">
				<p>15% 적립받기<br><span>포인트 적립</span></p>
			</div>
			<button type="button" name="" class="btn btn-point" id="btn_req" >리셀러 신청</button>
		</div>
		<div class="text">
		<?
		if ( $_MEM_INFO["reseller"] == "1" ) { echo "<p><strong>신청 승인이 완료되었습니다.!!</strong></p><br />";}
		if ( $_MEM_INFO["reseller"] == "2" ) { echo "<p><strong>신청이 완료되었습니다.<br />승인 후 URL 제공됩니다.</strong></p><br />";}
		if ( $_MEM_INFO["reseller"] == "3" ) {echo "<p><strong>신청이 거부되었습니다.</strong></p><br />"; }
		?>
			<p><strong>친구를 초대하고, 친구가 구매한 <br>상품금액의 15%를 적립받으세요!!</strong><br><br>
			친구에게 또는 내 블로그,카페,인스타에 상품을 등록후,<br> 링크를 통해 상품구매시 구매금액의 15%를 <br>적립받으실수 있습니다<br>적립된 포인트는 마이페이지에서 확인하실수 있습니다.</p>
			<div class="earn-form hide" >
				<div class="earn-input"><input type="text" name="" class="" value="<?=$_MEM_INFO["reseller_url"]?>" placeholder="<?= ( $_MEM_INFO["reseller"] == "2" ) ? "승인 대기중입니다." : "" ?>" readonly></div>
				<button type="button" name="" class="btn-point"  id="copyContent" data-clipboard-text ="<?=$_MEM_INFO["reseller_url"]?>" >복사</button>
			</div>
			<p class="gray  hide">※ 리셀러 신청 후 승인이 완료되면 링크주소가 나옵니다.<br>※ 승인후에 받으세요</p>		
		</div>
	</div>
</section>

<script type="text/javascript">
<!--
	$("#btn_req").on("click" , function(){
	<? if ( $_SESSION["mstsp_isapp"] != "1" ) { ?>
		window.open('about:blank').location.href='<?=$PARTNER_DOMAIN__?>';
		return false;
	<?}else{?>
		window.Android.gotoRunLink('<?=$PARTNER_DOMAIN__?>');
	<?}?>
	});
//-->
</script>
<?
include ("../include/footer.php");
?>
<?
$PAGE_TITLE = "마이페이지";
include ("../include/header.php");
$page_name = "inquiry";

$sql = "select A.*, B.thumb, B.title as Btitle from goods_qna as A left join goods as B ON A.no=B.no where uid='" . $_SESSION["mstsp_id"] . "' and A.del_ok='0' order by A.idx desc";
$rst = mysql_query( $sql );
?>
<!-- BODY -->
<section class="sub-body">
	<?include ("../include/mypage_menu.php");?>
	<h2 class="hide">MY문의글</h2>
	<div class="order-area cart">
		<h2 class="hide">리스트</h2>
		<div class="order-title">
			<div class="checkbox">
				<div class="checks"><input type="checkbox" id="order-all" name="order-list"><label for="order-all">전체선택/해제</label></div>
			</div>
			<div class="btn-area"><a href="#" class="btn" id="sel_delete">선택삭제</a></div>
		</div>
		
		<ul class="order-list">
			<?
			while ( $row = mysql_fetch_array( $rst ) ) {
			?>
			<li id="list_id_<?=$row["idx"]?>">
				<div class="checks" <?=( $row["ans_date"] != "" ) ? "style=\"display:none;\"" : "" ?>>
					<input type="checkbox" id="my-ck<?=$row["idx"]?>" class="my_list" name="my_list" value="<?=$row["idx"]?>" <?=( $row["ans_date"] != "" ) ? "disabled" : "" ?>>
					<label for="my-ck<?=$row["idx"]?>"></label>
				</div>
				<div class="product">
					<div class="profile">
						<div class="img"><img src="<?=$row["thumb"]?>"></div>
					</div>
					<div class="text">
						<p class="ellipsis1"><strong class="black"><?=$row["title"]?></strong></p>
						<p class="ellipsis2"><?=nl2br($row["content"])?></p>							
						<a href="#" class="<?=( $row["ans_date"] != "" ) ? "layer-open-inquiry" : "noAns" ?>" data-idx="<?=$row["idx"]?>"><?=( $row["ans_date"] != "" ) ? "<span class=\"btn btn-point\">답변완료" : "<span class=\"btn btn-point\">답변대기" ?></span></a>
					</div>
					<?=( $row["ans_date"] == "" ) ? "<button class=\"btn-delete\" data-idx=\"" . $row["idx"] . "\"><span class=\"hide\">삭제</span></button>" : ""?>
				</div>
			</li>
			<?}?>
		</ul>
	</div>
</section>


<!--inquiry popup/ 내 문의글 보기-->
<div id="layer-box-inquiry" class="inquiry-popup layer-box">
	<div id="layer-popup-inquiry" class="layer-popup">
		<div class="right"><a href="#" class="pop-close" onclick="return false;"><span class="hide">닫기</span></a></div>
		<div class="title">내 문의글 보기</div>
		<div class="body">
			<div class="list">
				<div class="profile">
					<div class="img"><img src="" id="pop_data1" onerror="this.src='/assets/images/icon_noimg_m.png'" ></div>
				</div>
				<div class="text">
					<div class="name"><span class="point" id="pop_data2">멍뭉이</span> <span class="gray" id="pop_data3">| 2019.09.30</span></div>
					<p><strong id="pop_data4">배송관련 문의드립니다.</strong><br><span  id="pop_data5">오늘 주문하면 배송이 얼마나 걸릴까요?</span></p>
				</div>
			</div>
			<div class="list">
				<div class="shop">
					<div class="img"><img src="/assets/images/icon_def.png"></div>
				</div>
				<div class="text">
					<div class="name"><span class="btn btn-point">답변</span> <span class="point">10PROSHOP</span> <span class="gray"  id="pop_data6">| 2019.09.30</span></div>
					<p  id="pop_data7">네 회원님 배송은 3일에서 5일정도 걸립니다.오늘 받으시면 주말이 껴있는 관계로 7일 후에 받으실 수 있습니다.</p>
				</div>
			</div>
		</div>
		<div class="footer">
        	<button type="button" class="btn btn-point w100p pop-close">확인</button>
        </div>
	</div>
</div>
<!--//inquiry popup-->
<div class="layer-background" ></div>
<script type="text/javascript">
<!--

	$(document).on("click","#order-all", function(){
		if ( $(this).is(":checked") == true ) {
			$(".my_list").not(":disabled").prop("checked", true );
		}else {
			$(".my_list").prop("checked", false );
		}
	});

	$(document).on("click","#sel_delete", function(){
		var nChk = document.getElementsByName("my_list");
		var gid = "";
		if(nChk){
			for(i=0;i<nChk.length;i++) { 
				if(nChk[i].checked){
					if(gid ==''){
						gid = nChk[i].value; 
					}else{
						gid =  gid+ ',' +nChk[i].value;
					}
				} 
			}
		}
		if ( gid != "" ) {
			var con = confirm("선택하신 문의글을 삭제하시겠습니까?\n답변이 등록된 글은 삭제하실 수 없습니다.");
			if ( con ) {
				$.ajax({
					type: 'POST',
					cache: false,
					dataType: 'json',
					url: './_proc_json.php',
					data: 'mode=del_row&idx=' + gid,
					success:function (data) {
						console.log(data);
						if ( data.rst == "1" ) {
							if (gid.match(",")) {
								var arr_gid = gid.split(",");
								for ( i = 0 ; i<arr_gid.length ; i++ ) {
									$("#list_id_"+arr_gid[i]).remove();
								}
							} else {
								$("#list_id_"+gid).remove();
							}
						}else {
							alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
							location.reload();
						}
					}, error : function (data) {
						console.log(data);
					} 
				});
			}
		}else {
			alert("선택하신 문의글이 없습니다.\n답변이 등록된 글은 삭제하실 수 없습니다.");
		}
		return false;
	});

	$(document).on( "click" , ".btn-delete" , function(){		
		var gid = $(this).data("idx");
		var con = confirm("선택하신 문의글을 삭제하시겠습니까?\n답변이 등록된 글은 삭제하실 수 없습니다.");
		if ( con ) {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=del_one&idx=' + gid,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						$("#list_id_"+gid).remove();
					}else {
						alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
						location.reload();
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}
	});

	$( document ).on( "click", ".noAns", function( e ) {
		alert("빠른시간내에 답변 드리도록 노력하겠습니다.");
		return false;
	});

	$( document ).on( "click", ".layer-open-inquiry", function( e ) {
		var idx = $(this).data("idx");
		var params = { idx:idx, mode:'show_inquiry' }
		$.ajax({
			url:"./_proc_json.php",
			data:params,
			type:"POST",
			dataType:"json",
			success:function(data){
				console.log(data);
				if ( data.rst == "1" ) {
					$("#pop_data1").html(data.cont1);
					$("#pop_data2").html(data.cont2);
					$("#pop_data3").html(data.cont3);
					$("#pop_data4").html(data.cont4);
					$("#pop_data5").html(data.cont5);
					$("#pop_data6").html(data.cont6);
					$("#pop_data7").html(data.cont7);
					$("#layer-popup-inquiry.layer-popup").removeClass("hide");
					$(".layer-background").addClass("open");
					$(".inquiry-popup").addClass("open"); 
					$("#layer-box-inquiry").show();
				}else {
					alert("정보를 불러오는 도중 오류가 발생 하였습니다. \n오류가 계속될 경우 하단 고객센터로 전화 부탁드립니다.\nTEL:<?=$CSC_PHONE__?>");
					location.reload();
				}
			},
			error:function(jqXHR,textStatus,errorThrown){		
				console.log("error");
				//pop_policy_new("3", "로딩에러", "", "" );
			},
			beforeSend: function () {
				show_loading();
			}
			,complete: function () {
				$("#div_ajax_load_image").hide();
			}
		});
		return false;
	});
	
//-->
</script>
<?
include ("../include/footer.php");
?>
<?
$PAGE_TITLE = "포인트";
include ("../include/header.php");

CHK_LOGIN("1");
//기본 검색 날짜 범위 1달

if ( isset( $_GET["sdate"] ) ) { $sdate = mysql_real_escape_string( $_GET['sdate'] );
}else { $sdate = date("Y-m-d" , strtotime("-12 week") ); }
if ( isset( $_GET["edate"] ) ) { $edate = mysql_real_escape_string( $_GET['edate'] );
}else { $edate = date("Y-m-d");}

//적립
$rst1 = mysql_query("select * from point_history where gubun='2' and date >='$sdate' and date <= '$edate' and id='" . $_SESSION["mstsp_idx"] . "' order by num desc ");
//사용
$rst2 = mysql_query("select * from point_history where gubun='1' and date >='$sdate' and date <= '$edate' and id='" . $_SESSION["mstsp_idx"] . "' order by num desc ");

$page_title = "마이페이지";

?>
<!-- BODY -->
<section class="sub-body">
	<div class="point-area">
		<div class="mypoint">
			<h2>포인트 사용/적립</h2>
			<h3>나의 포인트</h3>
			<ul>
				<li><div class="title">총 적립 포인트:</div><div class="point-txt"><?=number_format( $_MEM_INFO["point_total"] )?><span> point</span></div></li>
				<li><div class="title">사용한 포인트:</div><div class="point-txt"><?=number_format( $_MEM_INFO["point_use"] )?><span> point</span></div></li>
				<li><div class="title point bold">사용 가능 포인트:</div><div class="point-txt point bold"><?=number_format( $_MEM_INFO["point"] )?><span> point</span></div></li>
			</ul>
		</div>
		<div class="tab n2">
			<a href="#" id="ptabmenu1" class="point_tab current" data-tab="point-tab1" >적립내역</a>
			<a href="#" id="ptabmenu2" class="point_tab" data-tab="point-tab2">사용내역</a>
		</div>
		<div id="point-tab1" class="tabcontent current">
			<div class="group">
				<h4 class="hide">적립내역</h4>
				<h5>포인트 적립 상세내역입니다.</h5>
				<div class="day-date">
					<input type="text" name="sdate1" id="sdate1" placeholder="<?=( $sdate != "" ) ? $sdate : date("Y-m-d", strtotime("-3 month"))?>">~
					<input type="text" name="edate1" id="edate1" placeholder="<?=( $edate != "" ) ? $edate : date("Y-m-d")?>">
					<button type="button" class="btn btn_search" data-id="2">조회하기</button>
				</div>
			</div>
			
			<table class="mypoint-list">
				<thead>
					<tr>
						<th>내용</th>
						<th>적립금액</th>
						<th>상태</th>
					</tr>
				</thead>
				<tbody>
				<?
				$i = 0;
				while ( $row1 = mysql_fetch_array( $rst1 ) ) {
					if ($row1["view_chk"] == "0") {
						@mysql_query("update point_history set view_chk='1' where num='".$row1["num"]."'");
					}
				?>
				<tr>
					<td><span><?=substr( $row1["reg_date"] , 0 , 10 )?></span><br /><?=$row1["title"]?></td>
					<td>+<?=number_format( $row1["point"] )?> point</td>
					<td><?= ( $row["status"] == "0" ) ? "예정" : "확정" ?></td>
				</tr>
				<?$i++;}
				if ($i == 0 ) {
					echo "<tr><td colspan='3'>사용 내역이 없습니다.</td></tr>";
				}?>
				</tbody>
			</table>
		</div>
		<div id="point-tab2" class="tabcontent">
			<div class="group">
				<h4 class="hide">사용내역</h4>
				<h5>포인트 사용 상세내역입니다.</h5>
				<div class="day-date">
					<input type="text" name="sdate2" id="sdate2" placeholder="<?=( $sdate != "" ) ? $sdate : date("Y-m-d", strtotime("-3 month"))?>">~
					<input type="text" name="edate2" id="edate2" placeholder="<?=( $edate != "" ) ? $edate : date("Y-m-d")?>">
					<button type="button" class="btn btn_search" data-id="1">조회하기</button>
				</div>
			</div>
			<table class="mypoint-list">
				<thead>
					<tr>
						<th>내용</th>
						<th>사용금액</th>
					</tr>
				</thead>
				<tbody>
				<?
				$i = 0;
				while ( $row2 = mysql_fetch_array( $rst2 ) ) {
					if ($row2["view_chk"] == "0") {
						@mysql_query("update point_history set view_chk='1' where num='".$row2["num"]."'");
					}
				?>
				<tr>
					<td><span><?=substr( $row2["reg_date"] , 0 , 10 )?></span><br /><?=$row2["title"]?></td>
					<td>-<?=number_format( $row2["point"] )?> point</td>
				</tr>
				<?$i++;}
				if ($i == 0 ) {
					echo "<tr><td colspan='2'>사용 내역이 없습니다.</td></tr>";
				}?>
				</tbody>
			</table>
		</div>
	</div>
</section>

<script language="javascript">
$(".btn_search").click(function(){
	var sdate = $("#sdate"+$(this).data("id"));
	var edate = $("#edate"+$(this).data("id"));
	location.href="./point.php?sdate="+sdate+"&edate="+edate;
	return false;
});
$(".point_tab").click(function(){
	if ( $(this).data("tab") == "point-tab1" ) {
		$("#point-tab1").show();
		$("#point-tab2").hide();
		if ( !$("#ptabmenu1").hasClass("current") ) {
			$("#ptabmenu1").addClass("current");
		}
		if ( $("#ptabmenu2").hasClass("current") ) {
			$("#ptabmenu2").removeClass("current");
		}
	} else {
		$("#point-tab1").hide();
		$("#point-tab2").show();
		if ( !$("#ptabmenu2").hasClass("current") ) {
			$("#ptabmenu2").addClass("current");
		}
		if ( $("#ptabmenu1").hasClass("current") ) {
			$("#ptabmenu1").removeClass("current");
		}
	}
	return false;
});
$("#sdate1").datepicker().on('changeDate', function(e) {
	$("#sdate1").datepicker('hide');
});
$("#edate1").datepicker().on('changeDate', function(e) {
	$("#edate1").datepicker('hide');
});
$("#sdate2").datepicker().on('changeDate', function(e) {
	$("#sdate2").datepicker('hide');
});
$("#edate2").datepicker().on('changeDate', function(e) {
	$("#edate2").datepicker('hide');
});
</script>
<?
include ("../include/footer.php");
?>
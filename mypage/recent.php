<?
$PAGE_TITLE = "마이페이지";
include ("../include/header.php");
//$rst = mysql_query("select * from goods_top100 where sdate = '" . date("Ymd") . "' and cate_code='' and gubun = '' order by rank asc limit 0, 100 ");
$page_name = "recent";
//print_r( $_SERVER);
?>
<!-- BODY -->
<section class="sub-body">
	<?include ("../include/mypage_menu.php");?>

	<div class="order-area cart">
		<h2 class="hide">리스트</h2>
		<?
		if ( $_COOKIE["mstsp_recent"] != "" ) {
		?>
		<div class="order-title">
			<div class="checkbox">
				<div class="checks"><input type="checkbox" id="order-all" name="order-list" ><label for="order-all">전체선택/해제</label></div>
			</div>
			<div class="btn-area"><a href="#" class="btn" id="sel_delete">선택삭제</a></div>
		</div>		
		<ul class="order-list">
		<?
			$recent_info = explode( "," , $_COOKIE["mstsp_recent"] );
			$view_cnt = 0;
			for ( $i = 0 ; $i < count( $recent_info ); $i++ ) {
				$row = getdata("SELECT A.*, B.* FROM `goods` as A left join goods_detail as B ON A.no=B.no where A.no = '" . $recent_info[$i] . "' ");				
				$use_row = true;
				if ( $row["dateStart"] > date("Y-m-d H:i:s") || $row["dateEnd"] < date("Y-m-d H:i:s") || $row["status"] <> "판매중" ) {
					$use_row = false;
				}
				if ( $row["no"] == "" ) { //상품정보가 없으면
					continue;
				}elseif ( $use_row == false ) { //옵션 내부 확인하여 전부 판매중지이면 체크박스 막음
					continue;
				}else {
					$view_cnt++;
				}
				if ( $view_cnt > 10 ) {
					break;
				}
		?>
			<li id="list_id_<?=$recent_info[$i]?>">
				<div class="checks">
					<input type="checkbox" id="my-ck_<?=$recent_info[$i]?>" name="my_list" class="my_list" value="<?=$recent_info[$i]?>"><label for="my-ck_<?=$recent_info[$i]?>"></label>
				</div>
				<div class="product">
					<div class="img">
						<a href="../product/product_view.php?no=<?=$recent_info[$i]?>"><img src="<?=$row["thumb"]?>" style="border:1px solid #ededed;"></a>
					</div>
					<div class="text">
						<a href="../product/product_view.php?no=<?=$recent_info[$i]?>">
							<h3><?=$row["title"]?></h3>
							<p class="pay"><span class="bold size18 pt10"><?=number_format( $row["priceWe"] )?></span>원</p>
						</a>
					</div>
					<button class="btn-delete del_row" data-id="<?=$recent_info[$i]?>"><span class="hide">삭제</span></button>
				</div>
			</li>
		<?}?>
		</ul>
		<?}else {?>
		<div style="text-align:center;padding-top:10%;">최근 본 상품이 없습니다.</div>
		<?}?>
	</div>
</section>
<script type="text/javascript">
<!--
	$(document).on("click","#order-all", function(){
		if ( $(this).is(":checked") == true ) {
			$(".my_list").not(":disabled").prop("checked", true );
		}else {
			$(".my_list").prop("checked", false );
		}
	});

	$(document).ready(function(){
		$(".order_list").not(":disabled").prop("checked", true );
	});

	$(document).on("click","#sel_delete", function(){
		var nChk = document.getElementsByName("my_list");
		var gid = "";
		if(nChk){
			for(i=0;i<nChk.length;i++) { 
				if(nChk[i].checked){
					if(gid ==''){
						gid = nChk[i].value; 
					}else{
						gid =  gid+ ',' +nChk[i].value;
					}
				} 
			}
		}
		if ( gid != "" ) {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: './_proc_json.php',
				data: 'mode=del_row_recent&idx=' + gid,
				success:function (data) {
					console.log(data);
					if ( data.rst == "1" ) {
						if (gid.match(",")) {
							var arr_gid = gid.split(",");
							for ( i = 0 ; i<arr_gid.length ; i++ ) {
								$("#list_id_"+arr_gid[i]).remove();
							}
						} else {
							$("#list_id_"+gid).remove();
						}
					}else {
						alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
						location.reload();
					}
				}, error : function (data) {
					console.log(data);
				} 
			});
		}else {
			alert("선택하신 상품이 없습니다.");
		}
		return false;
	});

	$(document).on( "click" , ".del_row" , function(){		
		var gid = $(this).data("id");
		$.ajax({
			type: 'POST',
			cache: false,
			dataType: 'json',
			url: './_proc_json.php',
			data: 'mode=del_row_recent&idx=' + gid,
			success:function (data) {
				console.log(data);
				if ( data.rst == "1" ) {
					$("#list_id_"+gid).remove();
				}else {
					alert("삭제 도중 오류가 발생하였습니다. 다시시도해 주세요");
					location.reload();
				}
			}, error : function (data) {
				console.log(data);
			} 
		});
	});
//-->
</script>
<?
include ("../include/footer.php");
?>
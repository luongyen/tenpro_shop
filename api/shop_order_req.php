<?
	// 주문접수 페이지
	header('Content-Type: text/html; charset=utf-8');
	ini_set("display_errors", "Off");
	session_start();
	@extract($_SERVER);
	@extract($_GET);
	@extract($_POST);
	@extract($_FILES);
	$db_name = "10proshop";
	$db_user = "root";
	$db_pass1 = "!)proSHOP";
	$home_dir = "/";
	$MEDIA_CODE = "10proshop";
	$hash_master_key = "rlatlsdndroqkfwkWkd!!";
	
	include ("$DOCUMENT_ROOT/lib/fun.php");
	$db__ = DB__($db_user,$db_pass1,$db_name);
	mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $db__);

	//1. 필요입력값 확인
	$shopOrderNo = mysql_real_escape_string( $_POST["shopOrderNo"] );//업체주문번호
	$orderItemInfo = mysql_real_escape_string( $_POST["orderItemInfo"] );//상품코드
	$order_options = mysql_real_escape_string( $_POST["orderOptions"] );//주문옵션
	$deliPay = mysql_real_escape_string( $_POST["deliPay"] );//S : 무료배송(판매자부담) / B : 착불 / P : 선결제
	$shop_comment = mysql_real_escape_string( $_POST["shopComment"] );
	$deli_comment = mysql_real_escape_string( $_POST["deliComment"] );

	$rcv_name = mysql_real_escape_string( $_POST["rcvName"] );
	$order_email = mysql_real_escape_string( $_POST["orderEmail"] );
	$zip = mysql_real_escape_string( $_POST["zip"] );
	$addr1 = mysql_real_escape_string( $_POST["addr1"] );
	$addr2 = mysql_real_escape_string( $_POST["addr2"] );
	$rcv_phone1 = mysql_real_escape_string( $_POST["rcvPhone1"] );
	$rcv_phone2 = mysql_real_escape_string( $_POST["rcvPhone2"] );


	//2. 배송비, 업체 차감비 계산 
	$tot_price = 0;
	$deli_price = 0;
	$order_price = 0;
	$goods_info = getdata("select * from goods as A left join goods_detail as B ON A.no=B.no where no = '" . $orderItemInfo . "' and stats='판매중' and dateStart<='" . date("Y-m-d H:i:s") . "' and dateEnd>='" . date("Y-m-d H:i:s") . "' and descLicenseUsable = '1' and adminAgree = '1' ");
	if ( $goods_info["no"] == "" ) {
		$data["rst_code"] = "3000";
		$data["rst_msg"] = "정상 판매 상태가 아닌 상품입니다.";
	}else {
		//먼저 구매 총수량 및 상품가격 확인
		$arr_opt = explode( "@" , $order_options );
		$tot_qty = 0;
		for ( $i = 0 ; $i < count( $arr_opt ) ; $i++ ) {
			$arr_cnt = explode( "|" , $arr_opt );
			$tot_qty += $arr_cnt[1];
			$opt_goods = getdata("select * from goods_option_data where goods_no='" . $orderItemInfo . "' and okey='" . $arr_cnt[0] . "' ");
			if ( $goods_info["optType"] != "" ) {
				$optPrice = ( $goods_info["priceWe"] + $opt_goods["priceWe"] ) * $arr_data[1];
				$order_price += $optPrice;
			}else {
				$optPrice = $goods_info["priceWe"] * $arr_data[1];
				$order_price += $optPrice;
			}			
		}
		/* 배송비+도서산간or제주 계산*/
		if ( $deliPay == "P" ) {
			if ( $goods_info["fee"] == "고정배송비" ) {
				$deli_price = $goods_info["fee"];
			}elseif ( $goods_info["fee"] == "수량별차등" ) {			
				$arr_deli_tbl = explode( "|" , $goods_info["tbl"] );
				for ( $i = count( $arr_deli_tbl ) - 1 ; $i >= 0 ; $i-- ) {	
					$arr_a = explode( "+" , $arr_deli_tbl[$i] );
					if ( $arr_a[0] <= $tot_qty ) {
						$deli_price = $arr_a[1];
						break;
					}
				}
			}elseif ( $goods_info["fee"] == "수량별비례" ) {
				$arr_deli_tbl = explode( "|" , $goods_info["tbl"] );
				$arr_a = explode("+",$arr_deli_tbl[0]);
				$arr_b = explode("+",$arr_deli_tbl[1]);
				if ( $arr_a[0] >= $tot_qty ) {
					$deli_price = $arr_a[1];
				}else {
					$tmp_qty = $tot_qty - $arr_a[0]; // 두번째에 해당하는 물건 수
					$qty_set = ( $tmp_qty - ( $tmp_qty % $arr_b[0] ) ) / $arr_b[0]; //몫
					$qty_etc = $tmp_qty % $arr_b[0]; //나머지
					if ( $qty_set != 0 ) {
						$deli_price = $qty_set * $arr_b[1];
					}
					if ( $qty_etc != 0 ) {
						$deli_price = $delivery + $arr_b[1];
					}
					$deli_price = $delivery + $arr_a[1];
				}			
			}
		}
		$row_island = getdata("select * from island where zipcode='" . $zip . "' ");
		if ( $row_island["jeju"] == "1" ) {//제주
			$deli_price = $deli_price + $goods_info["jeju"];
		}elseif ( $row_island["etc"] == "1" ) {//도서산간
			$deli_price = $deli_price + $goods_info["islands"];		
		}

		$tot_price = $order_price + $deli_price;
		//3. 업체 포인트 확인
		if ( $order_price <= 0 ) {	
			$data["rst_code"] = "4000";
			$data["rst_msg"] = "주문내용 오류 또는 결제 금액이 0원입니다.";
		}else{
			$row_point = getdata("select  * from shop_point");
			if ( $tot_price > $row_point["point"] ) {
				$data["rst_code"] = "2000";
				$data["rst_msg"] = "예치금 포인트 부족";
			}else {
				//4. db 1차저장
				$sql = " INSERT INTO orders_shop SET shopOrderNo = '" . $shopOrderNo . "' , orderNo = '' , orderItemInfo = '" . $orderItemInfo . "' , orderStatus = '신규주문' , orderStatusMode = 'NEW' , orderAmount = '" . $order_price . "' , itemTitle = '" . $goods_info["title"] . "' , payAmount = '" . $tot_price . "' , supAmount = '" . $sup_price . "' , deliMethod = '' , deliWho = '' , deliFee = '" . $deli_price . "' , deliCode = '' , deliCompany = '' , deliCompanyName = '' , deliDate = '' , deliDateOver = '' , deliDateStart = '' , deliSellerChk = '' , deliMergeBasis = '' , deliMergeMethod = '' , deliMergeAmount = '' , deliMergeItems = '' , selectOpt = '" . $order_options . "' , isBack = '' , denyDate = '' , denyMemo = '' , denyId = '' , denyStat = '' , chgLog = '' , adminMemo = '' , orderContent = '" . json_encode( $_POST ) . "' , lastUpdate = '" . date("Y-m-d H:i:s") . "' , orderDate = '" . date("Y-m-d H:i:s") . "' ";

				$query = mysql_query( $sql );
				if ( $query ) {
					$data["rst_code"] = "0000";
					$data["rst_msg"] = "주문서 등록 성공";
				}else {
					$data["rst_code"] = "1000";
					$data["rst_msg"] = "주문서 등록 실패";
				}
			}
		}
	}
	//$idx = mysql_insert_id();

	//5. 전송여부 전달
	echo json_encode($data);
?>
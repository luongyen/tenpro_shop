<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title><?=$SITE_NAME__?></title>
	<link href="../assets/css/nanumsquareround.min.css" rel="stylesheet" />
	<link href="../assets/css/common.css?v=0.19" rel="stylesheet" />
	<link href="../assets/css/style.css?v=0.2" rel="stylesheet" />
	<link href="../assets/css/swiper.css?v=0.17" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/fontawesome/css/all.css" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-0EZBPDN0NT"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'G-0EZBPDN0NT');
	</script>
	<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
	<script src="../assets/js/swiper.min.js"></script>
	<script src="../assets/js/common.js?v=0.17" type="text/javascript"></script>
	<script src="../assets/js/main.js" type="text/javascript"></script>
	<script src="../assets/js/iScroll.js"></script>
	<script type="text/javascript" src="https://s3.ap-northeast-2.amazonaws.com/adpick.co.kr/apis/adpick_cps.js?d=100"></script>
	<style type="text/css" title="">	
		#slide_menu ul , #slide_menu li{ list-style:none; padding:0; margin:0; }
		#slide_menu li{ float:left; }
		.container{ width:100%; overflow:hidden; }
		#slide_menu{ width:1600px; }
		.wrapper-scroller {
			position: absolute;
			z-index: 1;
			top: 0;
			bottom: 0;
			left: 0;
			width: 100%;
			background-color: transparent;
			overflow: auto;
		}
		nav {border-bottom:1px solid #ddd;}
		.ul_box, .ul_box2, .ul_box3 {padding:0; margin:0;}
		.ul_box li, .ul_box2 li, .ul_box3 li {padding:12px 10px 11px; margin:0; list-style:none;display:inline-block;}
		.ul_box > li.active, .ul_box2 > li.active, .ul_box3 > li.active { border-bottom: 2px solid #574394; color: #000; }
		/*.ul_box > li > a.active, .ul_box2 > li > a.active { border-bottom: 2px solid #574394; color: #000; }*/
		.ul_box > li > a.active { border-bottom: 2px solid #574394; color: #000; }
		.ul_box a {font-size:15px;line-height:20px;color:#333;}

		.ul_box2 a {font-size:13px;line-height:20px;color:#333;padding:3px 0;}
		.ul_box2 > li > a.active {font-weight:bold; color: #574394;}
		.ul_box2 > li > a {width:max-content; color: #666; padding: 0; display: block; background: #fff; text-align: center; text-decoration: none; font-size: 14px; line-height: 20px; border-bottom: 2px solid transparent; -webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;}
		.ul_box2 > li > a > i {font-size:13px; margin-right:5px; color:#999;}
		.ul_box2 > li > a.active > i {font-weight:bold; color: #574394;}
		
		.ul_box3 a {font-size:13px;line-height:20px;color:#333;padding:3px 0;}
		.ul_box3 > li > a.active2 {font-weight:bold; color: #63bcc3;}
		.ul_box3 > li > a {width:max-content; color: #666; padding: 0; display: block; background: #fff; text-align: center; text-decoration: none; font-size: 14px; line-height: 20px; border-bottom: 2px solid transparent; -webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;}
		.ul_box3 > li > a > i {font-size:13px; margin-right:5px; color:#999;}
		.ul_box3 > li > a.active2 > i {font-weight:bold; color: #63bcc3;}
	</style>
</head>

<body>
<header class="menu">
	<div class="wrapper">
		<h1 class="hide"><?=$SITE_NAME__?></h1>
		<div class="logo"><a href="/main/"></a></div>
	</div>
	<div class="right-area">
		<a href="#" class="menu-search"><span class="hide">검색</span></a>
		<a href="../order/cart.php" class="btn-cart"><span class="hide">장바구니</span><?= ( $cart_c["cnt"] > 0 ) ? "<span class=\"badge\">" . $cart_c["cnt"] . "</span>" : "" ?></a>
	</div>

	<div class="search-box">
		<form name="search" method="get" action="../product/search_list.php">
			<input type="text" name="skey" value="" class="search-input" placeholder="검색어 입력">
			<button type="button" onclick = "document.search.submit();" class="search-button"><span class="hide">검색</span></button>
			<button type="button" class="search-close"><span class="hide">닫기</span></button>
		</form>
	</div>

	<nav class="menu-mobile"><span></span><span></span><span></span></nav>
	
	<nav class="menu">
		<ul>
			<?if ( $_SESSION["mstsp_id"] != "" ) {?>
			<!--login-->
			<li class="profile">
				<div class="img"><img src="<?=$_SESSION["mstsp_profile"]?>" onerror="this.src='/assets/images/icon_noimg_m.png'"></div>
				<div class="text"><strong><?=$_SESSION["mstsp_name"]?></strong>님 반갑습니다.<br>즐거운 쇼핑 되세요</div>
			</li>
			<?}else{?>
			<!--logout-->
			<li class="profile">
				<a href="login.php" class="btn">로그인</a>
			</li>
			<?}?>
			<!--li a 에 class menu-close 제거-->
			<li><a href="../product/search.php" class="menu-close icon01">검색<span class="gray">(국내/해외 직배송검색)</span></a></li>
			<li><a href="../product/category.php" class="menu-close icon04">카테고리<span class="gray">(국내/해외상품)</span></a></li>
			<li><a href="#" class=" icon02" id="btn_partner" target="_blank">10%샵 파트너스</a></li>
			<li class="line"></li>
			<? if ( $_SESSION["mstsp_id"] != "" ) { ?>
			<li><a href="../event/event_attend.php" class="menu-close icon05">출석체크</a></li>
			<?}?>
			<li><a href="../order/order_delivery.php" class="menu-close icon06">주문/배송조회</a></li>
			<!--li><a href="shopping_list.html" class="menu-close icon07">쇼핑내역</a></li-->
			<? if ( $_SESSION["mstsp_id"] != "" ) { ?>
			<li><a href="../mypage/" class="menu-close icon08">마이페이지<!--span class="badge">10</span--></a></li>
			<li><a href="../mypage/point.php" class="menu-close icon03">포인트</a></li>
			<?}?>
			<? if ( $_SESSION["mstsp_isapp"] == "1" ) {?>
			<li class="line"></li>
			<li><a href="../mypage/setting.php" class="icon09">설정</a></li>
			<?}?>
			<?if ( $_SESSION["mstsp_id"] != "" ) {?>
			<li><a href="#" class="icon11 btn-logout">로그아웃</a></li>
			<li><a href="../main/memberout.php" class="menu-close icon10">탈퇴하기</a></li>
			<?}?>
		</ul>
	</nav>

	<div class="layer-background"></div>

</header>

<!--NAV-->
<div class="" style="position:fixed;top:45px;z-index:9999;overflow:hidden;width:100%;max-width:900px;background-color:#fff;border-bottom:1px solid #ddd;">
	<nav  id="wrapper1" class=""style="width:100%;height:45px;overflow:hidden;position:relative;" >
		<ul class="ul_box " style="">
			<li class="<?= strpos( $_SERVER["PHP_SELF"] , "story_shop.php" ) ? "active" : "" ?>"><a href="./story_shop.php">딱!15일간만</a></li>
			<li class="<?= strpos( $_SERVER["PHP_SELF"] , "best.php" ) ? "active" : "" ?>"><a href="./best.php">BEST</a></li>
			<li class="<?= strpos( $_SERVER["PHP_SELF"] , "plus_delivery.php" ) ? "active" : "" ?>"><a href="./plus_delivery.php">묶음배송</a></li>
			<li class="<?= strpos( $_SERVER["PHP_SELF"] , "ttaeng_shop.php" ) ? "active" : "" ?>"><a href="./ttaeng_shop.php">알뜰쇼핑</a></li>
			<li class="<?= strpos( $_SERVER["PHP_SELF"] , "event.php" ) ? "active" : "" ?>"><a href="./event.php">이벤트</a></li>
			<!--li><a href="./new.php" class=" <?= strpos( $_SERVER["PHP_SELF"] , "new.php" ) ? "active" : "" ?> ">NEW</a></li-->
			<!--li><a href="./direct_shop.php" class="swiper-slide <?= strpos( $_SERVER["PHP_SELF"] , "direct_shop.php" ) ? "active" : "" ?> ">해외직배송</a></li>
			<li><a href="./happy_shop.php" class=" <?= strpos( $_SERVER["PHP_SELF"] , "happy_shop.php" ) ? "active" : "" ?> ">만원쇼핑</a></li-->
		</ul>
	</nav>
	<nav  id="wrapper3" class=""style="width:100%;height:45px;overflow:hidden;position:relative;" >
		<ul class="ul_box2">
			<li class="<?= ( $wcate == "" ) ? "active" : "" ?>"><a href="../main/cate_list.php" class="<?= ( $wcate == "" ) ? "active" : "" ?>"><i class="fas fa-shopping-cart"></i>전체</a></li>
			<li class="<?= ( $wcate == "1" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=01" class="<?= ( $wcate == "01" ) ? "active" : "" ?>"><i class="fas fa-tshirt"></i>패션</a></li>
			<li class="<?= ( $wcate == "2" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=02" class="<?= ( $wcate == "02" ) ? "active" : "" ?>"><i class="fas fa-pump-soap"></i>뷰티</a></li>
			<li class="<?= ( $wcate == "3" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=03" class="<?= ( $wcate == "03" ) ? "active" : "" ?>"><i class="fas fa-shopping-basket"></i>생활</a></li>
			<li class="<?= ( $wcate == "4" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=04" class="<?= ( $wcate == "04" ) ? "active" : "" ?>"><i class="fas fa-bread-slice"></i>식품</a></li>
			<li class="<?= ( $wcate == "5" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=05" class="<?= ( $wcate == "05" ) ? "active" : "" ?>"><i class="fas fa-socks"></i>잡화</a></li>
			<li class="<?= ( $wcate == "6" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=06" class="<?= ( $wcate == "06" ) ? "active" : "" ?>"><i class="fas fa-couch"></i>가전/가구</a></li>
			<li class="<?= ( $wcate == "7" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=07" class="<?= ( $wcate == "07" ) ? "active" : "" ?>"><i class="fas fa-baby-carriage"></i>유아동/문구</a></li>
			<li class="<?= ( $wcate == "8" ) ? "active" : "" ?>"><a href="../main/cate_list.php?wcate=08" class="<?= ( $wcate == "08" ) ? "active" : "" ?>"><i class="fas fa-car"></i>스포츠/자동차</a></li>
		</ul>
	</nav>
</div>
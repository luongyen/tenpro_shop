<?
include ("../config.php");
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>10%샵</title>
	<link href="../assets/css/nanumsquareround.min.css" rel="stylesheet" />
	<link rel='stylesheet' href='../assets/css/jquery-ui.css'>
	<link href="../assets/css/common.css?v=0.12" rel="stylesheet" />
	<link href="../assets/css/style.css?v=0.84" rel="stylesheet" />
	<link href="../assets/css/swiper.min.css" rel="stylesheet" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-0EZBPDN0NT"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'G-0EZBPDN0NT');
	</script>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/jquery-ui.js"></script>
	<script src="../assets/js/swiper.min.js"></script>
	<script src="../assets/js/common.js?v=0.16" type="text/javascript"></script>
	<script src="../assets/js/main.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://s3.ap-northeast-2.amazonaws.com/adpick.co.kr/apis/adpick_cps.js?d=100"></script>
</head>

<body>
<iframe name="ifr_proc" id="ifr_proc" src="" style="display:none;width:0;height:0;"></iframe>
<? include ("../include/head.php");?>
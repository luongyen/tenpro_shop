
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;"><img src='../assets/images/icon_top.png' border='0' alt=''></a>
<div class="lazy-group lazy-policy1" style="display: none;">
	<div class="lazy-header">
		이용약관
	</div>
	<div class="lazy-content alarm">
		<div class="inner-offset" style="">
			<div class="inner-text" style="max-height:300px;overflow-y:auto;"><?=$_POLICY["yak"]?></div>
			<div class="inner-fn"><a class="alarm" onclick="close_policy('1');return false;" href="#">닫기</a></div>
		</div>
	</div>
	<div class="lazy-close">
		<button><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
	</div>
</div>	

<div class="lazy-group lazy-policy2" style="display: none;">
	<div class="lazy-header">
		개인정보취급방침
	</div>
	<div class="lazy-content alarm">
		<div class="inner-offset">
			<div class="inner-text" style="max-height:300px;overflow-y:auto;"><?=$_POLICY["personal_data"]?></div>
			<div class="inner-fn">
				<a class="alarm" onclick="close_policy('2');return false;" href="#">닫기</a>
			</div>
		</div>
	</div>
	<div class="lazy-close">
		<button><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
	</div>
</div>

<section class="footer-area">
	<div class="cs">
		<div class="left">
			<h3>고객센터</h3>
			<p><?=$CSC_PHONE__?><br>평일 10:00 ~ 17:00<br>(점심시간 13:00 ~ 14:00)<br>주말.공휴일 휴무
			</p>
			<a href="tel:<?=$CSC_PHONE__?>">콜센터 연결</a>
		</div>
		<div class="right">
			<h3>무엇을 도와드릴까요?</h3>
			<p>상품/배송/기타 서비스와<br>관련한 빠른 상담을 <br>원하신다면 지금 바로<br> 카톡으로 문의하세요</p>
			<a href="#" class="kakaobtn">카톡상담하기</a>
		</div>
	</div>
	<div class="footer">
		<div class="footer-btn">
			<a href="#" onclick="javascript:pop_policy('1');return false;">이용약관</a>
			<span>|</span>
			<a href="#" onclick="javascript:pop_policy('2');return false;">개인정보취급방침</a>
		</div>
		<p><b>주식회사 셀럽브레이커스 대표 : 이지윤</b><br>서울시 구로구 디지털로 31길 41, 511호<br>사업자등록번호 :222-88-01529<br>통신판매업 신고번호 : 제2019-서울구로-2157호<br>제휴문의 : june_2019@naver.com</p>
	</div>
</section>

<?

if ( $_SESSION["mstsp_isapp"] != "1" && $_COOKIE["mstsp_useMobile"] != "1" && getMobile() ) {
$JEHUCODE = "_adpick";
//$APPLINK_AND?>
<div class="app_layout">
	<div class="logo"></div>
	<div class="button"><button type="button" class="btn app_btn">10proshop앱으로 보기 〉</button></div>
	<div class="text"><a href="#" id="itOkUseMobile" data-hide=".app_layout">괜찮습니다. 모바일웹으로 볼게요.</a></div>
</div>
<script type="text/javascript">
<!--
$(document).on( "click" , ".app_btn" , function(){
	location.href="<?=$APPLINK_AND . urlencode( $JEHUCODE )?>";
	return false;
});

$(document).on( "click" , "#itOkUseMobile" , function(){
	var params = {mode:'itOkUseMobile' }
	var referrer = "<?=$referrer?>";
	$.ajax({
		url:"../proc/_setting.php",
		data:params,
		type:"POST",
		dataType:"json",
		success:function(data){
			console.log(data);
			if ( data.content == "1" ) {	
				if ( referrer != "" ) {
					location.href = referrer;
				}else {
					location.reload();
					$(".app_layout").hide();
				}
			}
		},
		error:function(jqXHR,textStatus,errorThrown){		
			console.log("error");
		}
	});
});
//-->
</script>
<?}?>


<script type="text/javascript">
<!--
function pop_policy(val){
	$('.lazy-policy'+val).show();
	$('body').addClass('lazy-openner');

	$('.lazy-policy'+val).on('click',' .lazy-close>button',function(){
		$(this).parents('.lazy-policy'+val).fadeOut(function(){
			$('body').removeClass('lazy-openner');
		});
	});
}

function close_policy(val){
	var hide_yn = true;
	$('.lazy-policy'+val).fadeOut(function(){
		$(".lazy-group-popup").each(function(){
			if ($(this).css("display") == "block" ) {
				hide_yn = false;
			}
		});
		if ( hide_yn == true ) {
			$('body').removeClass('lazy-openner');
		}
	});
}

var loginSuccUrl = "http://<?=$SITE_DOMAIN__?>/main/logout.php";
var loginFailUrl = "http://<?=$SITE_DOMAIN__?>/main/logout.php";
$(document).on( "click" , ".btn-logout" , function(){
	var isApp = false;
	try{
		isApp = window.Android.isApp();
		if ( isApp ) {
			var isLogin = window.Android.isLogin();			
			$("#stat").append("<p>isLogin : "+isLogin+"</p>");
			if (isLogin == "naver" ) {
				isApp = window.Android.LogoutNaver( loginSuccUrl , loginFailUrl );
			}
			if (isLogin == "kakao" ) {
				isApp = window.Android.LogoutKakao( loginSuccUrl , loginFailUrl );
			}
			if (isLogin == "none" ) {
				location.href="/main/logout.php";
			}
		}
	}catch(e){
		location.href="/main/logout.php";
	}
});

	var boxW = 0 , liW = 0;
	$('#wrapper1 li').each(function(){
		liW = $(this).width();
		boxW += liW+25;
	});
	$(".ul_box").css('width', boxW);
	function loaded1(){
		var iscroll = new iScroll("wrapper1", {
			vScroll:false,
			hScroll:true,
			vScrollbar:false,
			hScrollbar:false,
			click:true,
			disablePointer: true

		});
	}
	boxW = 0;
	liW = 0;
	$('#wrapper3 li').each(function(){
		liW = $(this).width();
		boxW += liW+25;
	});
	$(".ul_box2").css('width', boxW);

	function loaded2(){
		var iscroll = new iScroll("wrapper3", {
			vScroll:false,
			hScroll:true,
			vScrollbar:false,
			hScrollbar:false,
			click:true,
			disablePointer: true
		});
	}

	document.addEventListener('DOMContentLoaded', loaded2, false);
	document.addEventListener('DOMContentLoaded', loaded1, false);
//-->
</script>
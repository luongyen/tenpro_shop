<header class="sub-header">
<?
if ( strpos( $_SERVER["PHP_SELF"] , "search.php" ) !== false || strpos( $_SERVER["PHP_SELF"] , "search_list.php" ) !== false || strpos( $_SERVER["PHP_SELF"] , "category_list.php" ) !== false ) {
?>
	<h1 class="hide">검색하기</h1>
	<div class="back-btn"><a href="../main/" onclick="javascript:location.href='/';return false;"><span class="hide">뒤로</span></a></div>
	<div class="sub-search">
		<form name="search_frm" id="search_frm" method="get" action="./<?=( strpos( $_SERVER["PHP_SELF"] , "search.php" ) !== false ||  strpos( $_SERVER["PHP_SELF"] , "search_list.php" )  !== false ) ? "search_list" : "category_list" ?>.php">
			<input type="hidden" name="sort" id="sort" value="" />
			<input type="hidden" name="cate1" id="cate1" value="<?=$_GET["cate1"]?>" />
			<input type="hidden" name="cate2" id="cate2" value="<?=$_GET["cate2"]?>" />
			<input type="hidden" name="cate3" id="cate3" value="<?=$_GET["cate3"]?>" />
			<input type="text" name="skey" id="skey" value="<?=$_GET["skey"]?>" class="search-input" placeholder="검색어를 입력해주세요">
			<button type="button" class="search-button"><span class="hide">검색</span></button>
		</form>
	</div>
<?}elseif ( strpos( $_SERVER["PHP_SELF"] , "story_view.php" ) !== false ) {?>

	<h1 class="sub-title ellipsis1 view-title"><?=$row["title"]?></h1>
	<div class="back-btn"><a href="../"><span class="hide">뒤로</span></a></div>
	<div class="right-area">
		<a href="#" class="btn-like"><span class="hide">좋아요</span></a>
		<a href="#" class="btn-share"><span class="hide">공유</span></a>
	</div>

<?}elseif ( strpos( $_SERVER["PHP_SELF"] , "event_view.php" ) !== false ) {?>
	<h1 class="sub-title">이벤트</h1>
	<div class="back-btn"><a href="../"><span class="hide">뒤로</span></a></div>
	<div class="right-area">
		<a href="#" class="btn-like"><span class="hide">좋아요</span></a>
		<a href="#" class="btn-share"><span class="hide">공유</span><!--<span class="badge">122</span>--></a>
	</div>
<?}elseif ( strpos( $_SERVER["PHP_SELF"] , "order_ok.php" ) !== false ) {?>
	<h1 class="sub-title"><?=$PAGE_TITLE?></h1>
	<div class="back-btn"><a href="../main/"><span class="hide">뒤로</span></a></div>
<?}else {?>
	<h1 class="sub-title"><?=$PAGE_TITLE?></h1>
	<div class="back-btn"><a href="../main/" class="_product_view"><span class="hide">뒤로</span></a></div>
	<?
	if ( strpos( $_SERVER["PHP_SELF"] , "cart.php" ) !== true  ) {
	?>
	<div class="right-area">
		<a href="../order/cart.php" class="btn-cart"><span class="hide">장바구니</span><?= ( $cart_c["cnt"] > 0 ) ? "<span class=\"badge\">" . $cart_c["cnt"] . "</span>" : "" ?></a>
	</div>
	<?}?>
<?}?>
</header>
<script type="text/javascript">
<!--
	$(document).on("click" , "._product_view" , function(){
		//alert(document.referrer);
		if ( document.referrer && document.referrer.indexOf("app_push") == -1  ) { 
			history.back();
		}else {
			location.href="../main/";
		}
		return false;
	});
//-->
</script>